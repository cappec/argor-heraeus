$(document).ready(() => {
  // First sticky title in page

  const stickyTitle = $("#sticky-title");
  const stickyContainer = $("#sticky-title-container");
  const width = $(window).width();
  if (width > 991) {
    $(window).scroll(function () {
      const containerLeft = stickyContainer.offset().left;
      const containerRight = stickyContainer.width() + containerLeft;
      if (containerLeft < 80 && containerRight - stickyTitle.width() > 0) {
        stickyTitle.css("margin-left", -(containerLeft - 100));
      }
    });
  }
});
