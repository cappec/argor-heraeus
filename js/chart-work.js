var chartOptions2 = {
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  tooltips: {
    enabled: false,
  },
  events: [],
  animation: {
    duration: 3000,
    ease: "easeOutCubic",
    onProgress: function (animation) {
      const value =
        animation.animationObject.currentStep /
        animation.animationObject.numSteps;
      const aniValue = 1 - Math.pow(1 - value, 4);

      var chartInstance = this.chart,
        ctx = chartInstance.ctx;
      ctx.font = Chart.helpers.fontString(
        14, // Chart.defaults.global.defaultFontSize,
        "bold", // Chart.defaults.global.defaultFontStyle,
        Chart.defaults.global.defaultFontFamily // "'Monumentextended', 'sans-serif'",
      );
      ctx.textAlign = "center";
      ctx.textBaseline = "bottom";
      this.data.datasets.forEach(function (dataset, i) {
        var meta = chartInstance.controller.getDatasetMeta(i);
        meta.data.forEach(function (bar, index) {
          var data = dataset.data[index];

          var isFinite = data % Math.ceil(data) > 0;

          var displayVal = isFinite
            ? parseFloat((data * aniValue).toFixed(2))
            : Math.round(data * aniValue);
          ctx.fillText(
            addMillis(displayVal),
            100 - 100 * aniValue + (bar._model.x + 25) * aniValue,
            bar._model.y
          );
        });
      });
    },
  },
  scales: {
    xAxes: [
      {
        ticks: {
          userCallback: function (label, index, labels) {
            return addMillis(label);
          },
          // max: item.max,
          fontSize: 14,
          beginAtZero: true,
          display: false,
        },
        gridLines: {
          display: false,
          drawBorder: false,
        },
      },
    ],
    yAxes: [
      {
        ticks: {
          fontFamily: "Lato, sans-serif",
          fontColor: "#000",
          fontSize: 14,
          min: 0,
        },
        gridLines: {
          display: false,
          drawBorder: false,
        },
        barPercentage: 0.8,
        categoryPercentage: 0.8,
      },
    ],
  },
  plugins: {
    datalabels: {
      color: "rgba(0, 0, 0, 0);",
      font: {
        weight: "bold",
        size: "15",
        family: "'Monumentextended', 'sans-serif'",
      },
      formatter: (value) => {
        return value;
      },
    },
  },
};

var selectedChart = "annual";

$(document).ready(function () {
  var chartElements = [
    {
      parentId: "home-block-10-graph-box-1",
      elements: [
        {
          id: "bar-chart-1-horizontal",
          data: {
            annual: {
              max: 67000,
              labels: ["2019", "2018", "2017"],
              values: [46950, 44027, 43843],
            },
            quarter: {
              max: 20000,
              labels: ["Q1 2020", "Q2 2020", "Q3 2020"],
              values: [13178, 13334],
            },
            colors: ["#5A8097", "#385465"],
          },
        },
        {
          id: "bar-chart-1-horizontal-6",
          data: {
            annual: {
              max: 400,
              labels: ["2019", "2018", "2017"],
              values: [294, 274, 197],
            },
            quarter: {
              max: 500,
              labels: ["Q1 2020", "Q2 2020", "Q3 2020"],
              values: [295, 293],
            },
            colors: ["#AD6C75", "#7E484F"],
          },
        },
      ],
    },
    {
      parentId: "home-block-10-graph-box-2",
      elements: [
        {
          id: "bar-chart-1-horizontal-2",
          data: {
            annual: {
              max: 67000,
              labels: ["2019", "2018", "2017"],
              values: [55814, 55441, 54207],
            },
            quarter: {
              max: 20000,
              labels: ["Q1 2020", "Q2 2020", "Q3 2020"],
              values: [16065, 14108],
            },
            colors: ["#5A8097", "#385465"],
          },
        },
        {
          id: "bar-chart-1-horizontal-7",
          data: {
            annual: {
              max: 300,
              labels: ["2019", "2018", "2017"],
              values: [39, 41, 53],
            },
            quarter: {
              max: 300,
              labels: ["Q1 2020", "Q2 2020", "Q3 2020"],
              values: [38, 37],
            },
            colors: ["#AD6C75", "#7E484F"],
          },
        },
      ],
    },
    {
      parentId: "home-block-10-graph-box-3",
      elements: [
        {
          id: "bar-chart-1-horizontal-3",
          data: {
            annual: {
              max: 1500,
              labels: ["2019", "2018", "2017"],
              values: [926, 885, 861],
            },
            quarter: {
              max: 500,
              labels: ["Q1 2020", "Q2 2020", "Q3 2020"],
              values: [308, 160],
            },
            colors: ["#5A8097", "#385465"],
          },
        },
        {
          id: "bar-chart-1-horizontal-8",
          duration: 2000,
          data: {
            annual: {
              max: 10,
              labels: ["2019", "2018", "2017"],
              values: [7.3, 4, 6.1],
            },
            quarter: {
              max: 5,
              labels: ["Q1 2020", "Q2 2020", "Q3 2020"],
              values: [0.02, 0.01],
            },
            colors: ["#6D6373", "#3A3743"],
          },
        },
      ],
    },
    {
      parentId: "home-block-10-graph-box-4",
      elements: [
        {
          id: "bar-chart-1-horizontal-4",
          duration: 2000,
          data: {
            annual: {
              max: 100,
              labels: ["2019", "2018", "2017"],
              values: [32.9, 16.8, 16.0],
            },
            quarter: {
              max: 25,
              labels: ["Q1 2020", "Q2 2020", "Q3 2020"],
              values: [14.8, 12.5],
            },
            colors: ["#496951", "#2C4533"],
          },
        },
        {
          id: "bar-chart-1-horizontal-9",
          max: 10,
          duration: 2000,
          data: {
            annual: {
              max: 100,
              labels: ["2019", "2018", "2017"],
              values: [0.34, 0.34, 0.13],
            },
            quarter: {
              max: 10,
              labels: ["Q1 2020", "Q2 2020", "Q3 2020"],
              values: [0.1, 0.07],
            },
            colors: ["#6D6373", "#3A3743"],
          },
        },
      ],
    },
    {
      parentId: "home-block-10-graph-box-5",
      elements: [
        {
          id: "bar-chart-1-horizontal-5",
          data: {
            annual: {
              max: 6000,
              labels: ["2019", "2018", "2017"],
              values: [4101, 3947, 3369],
            },
            quarter: {
              max: 2000,
              labels: ["Q1 2020", "Q2 2020", "Q3 2020"],
              values: [1055, 1131],
            },
            colors: ["#496951", "#2C4533"],
          },
        },
        {
          id: "bar-chart-1-horizontal-10",
          duration: 2000,
          data: {
            annual: {
              max: 3,
              labels: ["2019", "2018", "2017"],
              values: [0.82, 1.18, 0.78],
            },
            quarter: {
              max: 3,
              labels: ["Q1 2020", "Q2 2020", "Q3 2020"],
              values: [0.73, 0.76],
            },
            colors: ["#6D6373", "#3A3743"],
          },
        },
      ],
    },
  ];

  //chartElements.forEach((item) => {
  //var data = item.data;
  //var id  = item.id;
  //var barChartHorizontal = document.getElementById(id).getContext("2d");

  ////Gradient color
  //var gradientStroke = barChartHorizontal.createLinearGradient(500, 0, 100, 0);
  //gradientStroke.addColorStop(0, data.colors[0]);
  //gradientStroke.addColorStop(1, data.colors[1]);
  //// Define DATA
  //// Chart 1
  //var chartData1 = {
  //labels: data.labels,
  //datasets: [
  //{
  //label: "",
  //data: [43843, 44027, 46950],
  //fill: false,
  //backgroundColor: [gradientStroke, gradientStroke, gradientStroke],
  //borderColor: ['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0, 0)', 'rgba(0, 0, 0, 0)'],
  //borderWidth: 1,
  //scaleSteps:0.5,
  //scaleStartValue:0,
  //scaleStepWidth:1,
  //},
  //],
  //};
  //// Define OPTIONS

  //});
  // END OF LOOP
  var didFocus = false;
  function updateChart(force = false) {
    chartElements.forEach((parent) => {
      if (
        ($("#" + parent.parentId).css("opacity") > 0 && !parent.didFocus) ||
        force
      ) {
        //console.log("chart render done");
        parent.didFocus = true;
        parent.elements.forEach((item) => {
          var data = item.data;
          var id = item.id;
          if (!document.getElementById(id)) {
            return;
          }
          var barChartHorizontal = document.getElementById(id).getContext("2d");
          //barChartHorizontal.

          var gradientStroke = barChartHorizontal.createLinearGradient(
            500,
            0,
            100,
            0
          );
          gradientStroke.addColorStop(0, data.colors[0]);
          gradientStroke.addColorStop(1, data.colors[1]);
          chartOptions2.scales.xAxes[0].ticks.max = data[selectedChart].max;
          if (item.duration) {
            chartOptions2.animation.duration = item.duration;
          }
          // Define DATA
          // Chart 1
          var chartData1 = {
            labels: data[selectedChart].labels,
            datasets: [
              {
                label: data[selectedChart].labels,
                data: data[selectedChart].values,
                fill: false,
                backgroundColor: [
                  gradientStroke,
                  gradientStroke,
                  gradientStroke,
                ],
                borderColor: [
                  "rgba(0, 0, 0, 0)",
                  "rgba(0, 0, 0, 0)",
                  "rgba(0, 0, 0, 0)",
                ],
                borderWidth: 1,
                scaleSteps: 0.5,
                scaleStartValue: 0,
                scaleStepWidth: 1,
              },
            ],
          };

          item.chart && item.chart.destroy();

          item.chart = new Chart(barChartHorizontal, {
            type: "horizontalBar",
            data: chartData1,
            options: chartOptions2,
          });
        });
      } else if (!$("#" + parent.parentId).is(":visible") && parent.didFocus) {
        //console.log("notViible");
        parent.didFocus = false;
      }
    });
  }

  var timeout = setTimeout(function () {
    updateChart();
  }, 1000);

  $(window).scroll(function () {
    //clearTimeout(timeout);
    timeout = setTimeout(function () {
      updateChart();
    }, 400);
  });

  $("input[name='options']").change(function (d) {
    //console.log(d.target.value);
    selectedChart = d.target.value;
    updateChart(true);
    // Do something interesting here
  });
});
