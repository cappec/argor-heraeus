var gradientStroke1 = "#A6AFA9";
var gradientStroke2 = "#7E8D82";
var gradientStroke3 = "#55695A";
var gradientStroke4 = "#2B4533";

var chartOptions = {
  cutoutPercentage: 70,
  legend: {
    display: false,
  },
  tooltips: {
    mode: "index",
    intersect: false,
  },
  hover: {
    mode: "index",
    intersect: false,
  },

  //events: [],
  // circumference: Math.PI*2*5,
  animation: {
    duration: 1000,
    ease: "easeOutCubic",
    onProgress: function (animation) {
      const value =
        animation.animationObject.currentStep /
        animation.animationObject.numSteps;
      const aniValue = 1 - Math.pow(1 - value, 4);

      var chartInstance = this.chart,
        ctx = chartInstance.ctx;
      ctx.textAlign = "center";
      ctx.textBaseline = "bottom";

      this.data.datasets.forEach(function (dataset, i) {
        var meta = chartInstance.controller.getDatasetMeta(i);
        meta.data.forEach(function (bar, index) {
          //console.log(bar);
          var modal = bar._model;
          var data = dataset.data[index];
          var radius = (modal.innerRadius + modal.outerRadius) / 2;
          var x =
            modal.x +
            radius * Math.cos((modal.endAngle + modal.startAngle) / 2);
          var y =
            modal.y +
            radius * Math.sin((modal.endAngle + modal.startAngle) / 2);

          ctx.font = Chart.helpers.fontString(
            16, // Chart.defaults.global.defaultFontSize,
            "normal", // Chart.defaults.global.defaultFontStyle,
            "Monumentextended, sans-serif" //Chart.defaults.global.defaultFontFamily, // "'Monumentextended', 'sans-serif'",
          );
          ctx.fillStyle = "#fff";
          ctx.fillText(Math.round(data) + "", x, y);

          ctx.font = Chart.helpers.fontString(
            6, // Chart.defaults.global.defaultFontSize,
            "normal", // Chart.defaults.global.defaultFontStyle,
            "Monumentextended, sans-serif" //Chart.defaults.global.defaultFontFamily, // "'Monumentextended', 'sans-serif'",
          );
          ctx.fillText("%", x + 18, y);
        });
      });
    },
  },
  tooltips: {
    callbacks: {
      title: function (tooltipItem, data) {
        return data["labels"][tooltipItem[0]["index"]];
      },
      label: function (tooltipItem, data) {
        return null;
      },
      afterLabel: function (tooltipItem, data) {
        var dataset = data["datasets"][0];
        //var percent = Math.round((dataset['data'][tooltipItem['index']] / dataset["_meta"][0]['total']) * 100)
        //return '(' + percent + '%)';
      },
    },
    backgroundColor: "#FFF",
    titleFontSize: 16,
    titleFontColor: "#000",
    titleFontStyle: "normal",
    bodyFontColor: "#000",
    borderColor: "#496951",
    borderWidth: 1,
    xPadding: 20,
    titleFontFamily: "Lato, sans-serif",
    yPadding: 20,
    bodyFontSize: 14,
    displayColors: false,
  },
  plugins: {
    datalabels: {
      color: "#fff",
      font: {
        weight: "bold",
        size: "15",
        family: "'Monumentextended', 'sans-serif'",
      },
      formatter: (value) => {
        return value + " %";
      },
    },
  },
};

$(document).ready(function () {
  var chartElements = [
    {
      parentId: "pie-chart-box-1",
      data: {
        id: "pie-chart-1",
        labels: [
          "Trattamenti chimici ed elettrolitici",
          "Torri di raffreddamento acqua",
          "Acqua di raffreddamento",
          "Altro",
        ],
        backgroundColor: [
          gradientStroke1,
          gradientStroke2,
          gradientStroke3,
          gradientStroke4,
        ],
        borderColor: [
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
        ],
        borderWidth: 1,
        values: [35, 14, 39, 12],
      },
    },
    {
      parentId: "pie-chart-box-1",
      data: {
        id: "pie-chart-en-1",
        labels: [
          "Chemical and electrolytic treatments",
          "Cooling towers",
          "Cooling water",
          "Other",
        ],
        backgroundColor: [
          gradientStroke1,
          gradientStroke2,
          gradientStroke3,
          gradientStroke4,
        ],
        borderColor: [
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
        ],
        borderWidth: 1,
        values: [35, 14, 39, 12],
      },
    },
    {
      parentId: "pie-chart-box-2",
      data: {
        id: "pie-chart-2",
        labels: [
          "Acido cloridrico",
          "Acido nitrico",
          "Soda caustica",
          "Altre sostanze",
        ],
        backgroundColor: [
          gradientStroke1,
          gradientStroke2,
          gradientStroke3,
          gradientStroke4,
        ],
        borderColor: [
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
        ],
        borderWidth: 1,
        values: [24, 24, 35, 17],
      },
    },
    {
      parentId: "pie-chart-box-2",
      data: {
        id: "pie-chart-en-2",
        labels: [
          "Hydrochloric acid",
          "Nitric acid",
          "Caustic soda",
          "Other chemicals",
        ],
        backgroundColor: [
          gradientStroke1,
          gradientStroke2,
          gradientStroke3,
          gradientStroke4,
        ],
        borderColor: [
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
        ],
        borderWidth: 1,
        values: [24, 24, 35, 17],
      },
    },
    {
      parentId: "pie-chart-box-3",
      data: {
        id: "pie-chart-3",
        labels: ["Metano", "Corrente elettrica", "Fotovoltaico", "Propano"],
        backgroundColor: [
          gradientStroke1,
          gradientStroke2,
          gradientStroke3,
          gradientStroke4,
        ],
        borderColor: [
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
        ],
        borderWidth: 1,
        values: [25, 68, 2, 5],
      },
    },
    {
      parentId: "pie-chart-box-3",
      data: {
        id: "pie-chart-en-3",
        labels: ["Methane", "Electricity", "Photovoltaic", "Propane"],
        backgroundColor: [
          gradientStroke1,
          gradientStroke2,
          gradientStroke3,
          gradientStroke4,
        ],
        borderColor: [
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
        ],
        borderWidth: 1,
        values: [25, 68, 2, 5],
      },
    },
  ];

  // END OF LOOP
  var didFocus = false;
  function updateChart(force = false) {
    chartElements.forEach((item) => {
      if (
        ($("#" + item.parentId).css("opacity") > 0 && !item.didFocus) ||
        force
      ) {
        item.didFocus = true;
        var data = item.data;
        var id = data.id;
        console.log(id);
        if (!document.getElementById(id)) {
          return;
        }
        var donutsChart = document.getElementById(id).getContext("2d");

        var chartData = {
          labels: data.labels,
          datasets: [
            {
              labels: data.labels,
              data: data.values,
              fill: true,
              backgroundColor: data.backgroundColor,
              borderColor: [
                "rgba(0, 0, 0, 0)",
                "rgba(0, 0, 0, 0)",
                "rgba(0, 0, 0, 0)",
                "rgba(0, 0, 0, 0)",
              ],
              borderWidth: 1,
            },
          ],
        };

        item.chart && item.chart.destroy();

        item.chart = new Chart(donutsChart, {
          type: "doughnut",
          data: chartData,
          options: chartOptions,
        });
      } else if (!$("#" + item.parentId).is(":visible") && item.didFocus) {
        item.didFocus = false;
      }
    });
  }

  var timeout = setTimeout(function () {
    updateChart();
  }, 1500);

  $(window).scroll(function () {
    //clearTimeout(timeout);
    timeout = setTimeout(function () {
      updateChart();
    }, 400);
  });
  updateChart();
});

var barOptions = {
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  tooltips: {
    enabled: false,
  },
  events: [],
  animation: {
    duration: 3000,
    ease: "easeOutCubic",
    onProgress: function (animation) {
      const value =
        animation.animationObject.currentStep /
        animation.animationObject.numSteps;
      const aniValue = 1 - Math.pow(1 - value, 4);

      var chartInstance = this.chart,
        ctx = chartInstance.ctx;
      ctx.font = Chart.helpers.fontString(
        16, // Chart.defaults.global.defaultFontSize,
        "bold", // Chart.defaults.global.defaultFontStyle,
        "Lato, sans-serif"
      );
      ctx.textAlign = "center";
      ctx.textBaseline = "bottom";
      this.data.datasets.forEach(function (dataset, i) {
        var meta = chartInstance.controller.getDatasetMeta(i);
        meta.data.forEach(function (bar, index) {
          var data = dataset.data[index];
          ctx.fillText(
            addMillis(Math.round(data * aniValue)),
            100 - 100 * aniValue + (bar._model.x + 35) * aniValue,
            bar._model.y + 5
          );
        });
      });
    },
  },
  scales: {
    xAxes: [
      {
        ticks: {
          userCallback: function (label, index, labels) {
            return addMillis(label);
          },
          max: 67000,
          fontFamily: "Lato, sans-serif",
          fontSize: 16,
          beginAtZero: true,
          display: true,
          autoskip: true,
          autoSkipPadding: 50,
        },
        gridLines: {
          display: false,
          drawBorder: false,
        },
      },
    ],
    yAxes: [
      {
        ticks: {
          fontFamily: "Lato, sans-serif",
          fontColor: "#000",
          fontSize: 16,
          min: 0,
        },
        gridLines: {
          display: false,
          drawBorder: false,
        },
        barPercentage: 0.8,
        categoryPercentage: 0.8,
      },
    ],
  },
  plugins: {
    datalabels: {
      color: "rgba(0, 0, 0, 0);",
      font: {
        weight: "bold",
        size: "15",
        family: "'Monumentextended', 'sans-serif'",
      },
      formatter: (value) => {
        return value;
      },
    },
  },
};

var lineOptions = {
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  tooltips: {
    enabled: false,
  },
  hover: {
    animationDuration: 0,
  },
  scales: {
    xAxes: [
      {
        ticks: {
          max: 150,
          fontFamily: "Lato, sans-serif",
          fontColor: "rgba(0, 0, 0, 0)",
          fontSize: 16,
          beginAtZero: true,
          autoskip: true,
          autoSkipPadding: 50,
        },
        gridLines: {
          display: false,
          drawBorder: false,
        },
      },
    ],
    yAxes: [
      {
        ticks: {
          fontFamily: "Lato, sans-serif",
          fontColor: "rgba(0, 0, 0, 0)",
          fontSize: 16,
          min: 0,
        },
        gridLines: {
          display: false,
          drawBorder: false,
        },
        barPercentage: 0.8,
        categoryPercentage: 0.8,
      },
    ],
  },
};

var selectedChart = "switzerland";

$(document).ready(function () {
  var chartElements = [
    {
      parentId: "bar-chart-box",
      max: 72000,
      selectedChart: "switzerland",
      elements: [
        {
          id: "bar-chart-1",
          radioId: "bar-chart-box-radio",
          data: {
            switzerland: {
              labels: ["2019", "2018", "2017"],
              values: [46950, 44027, 43843],
            },
            deutschland: {
              labels: ["2019", "2018", "2017"],
              values: [1000, 1000, 1000],
            },
            italia: {
              labels: ["2019", "2018", "2017"],
              values: [548, 530, 498],
            },
            latina: {
              labels: ["2019", "2018", "2017"],
              values: [1095.7, 1095.7, 624],
            },
            aljiba: {
              labels: ["2019", "2018", "2017"],
              values: [1608, 879, 1025],
            },
            hongkong: {
              labels: ["2019", "2018"],
              values: [42289, 57430],
            },
            colors: ["#496951", "#2C4533"],
          },
        },
        {
          id: "bar-chart-en-1",
          radioId: "bar-chart-box-radio",
          data: {
            switzerland: {
              labels: ["2019", "2018", "2017"],
              values: [46950, 44027, 43843],
            },
            deutschland: {
              labels: ["2019", "2018", "2017"],
              values: [1000, 1000, 1000],
            },
            italia: {
              labels: ["2019", "2018", "2017"],
              values: [548, 530, 498],
            },
            latina: {
              labels: ["2019", "2018", "2017"],
              values: [1095.7, 1095.7, 624],
            },
            aljiba: {
              labels: ["2019", "2018", "2017"],
              values: [1608, 879, 1025],
            },
            hongkong: {
              labels: ["2019", "2018", "2017"],
              values: [42289, 57430],
            },
            colors: ["#496951", "#2C4533"],
          },
        },
      ],
    },
    {
      parentId: "bar-chart-box-2",
      max: 1200,
      selectedChart: "main",
      elements: [
        {
          id: "bar-chart-2",
          lineBar: "line-chart-3",
          data: {
            main: {
              lineMax: 1000,
              lineValues: [108, 112, 100],
              labels: ["2019", "2018", "2017"],
              values: [926, 885, 861],
            },
            colors: ["#496951", "#2C4533"],
          },
        },
      ],
    },
    {
      parentId: "bar-chart-box-3",
      max: 100,
      selectedChart: "main",
      elements: [
        {
          id: "bar-chart-3",
          lineBar: "line-chart-4",
          data: {
            main: {
              lineMax: 120,
              lineValues: [108, 112, 100],
              labels: ["2019", "2018", "2017"],
              values: [32.9, 16.8, 16],
            },
            colors: ["#496951", "#2C4533"],
          },
        },
      ],
    },
    {
      parentId: "bar-chart-box-4",
      max: 400,
      selectedChart: "id2017",
      elements: [
        {
          id: "bar-chart-4",
          radioId: "bar-chart-box-radio-4",
          data: {
            id2017: {
              labels: [
                ["Idrossidi metallici", "riciclabili (ton)"],
                ["Altri rifiuti", "riciclabili (ton)"],
                ["Rifiuti non", "riciclabili (ton)"],
              ],
              values: [197, 35, 29],
            },
            id2018: {
              labels: [
                ["Idrossidi metallici", "riciclabili (ton)"],
                ["Altri rifiuti", "riciclabili (ton)"],
                ["Rifiuti non", "riciclabili (ton)"],
              ],
              values: [274, 46, 14],
            },
            id2019: {
              labels: [
                ["Idrossidi metallici", "riciclabili (ton)"],
                ["Altri rifiuti", "riciclabili (ton)"],
                ["Rifiuti non", "riciclabili (ton)"],
              ],
              values: [294, 32, 22],
            },
            colors: ["#496951", "#2C4533"],
          },
        },
        {
          id: "bar-chart-en-4",
          radioId: "bar-chart-box-radio-4",
          data: {
            id2017: {
              labels: [
                ["Recyclable metal", "hydroxides (T)"],
                ["Other recyclable", "wastes (T)"],
                ["Non-recyclable", "wastes (T)"],
              ],
              values: [197, 35, 29],
            },
            id2018: {
              labels: [
                ["Recyclable metal", "hydroxides (T)"],
                ["Other recyclable", "wastes (T)"],
                ["Non-recyclable", "wastes (T)"],
              ],
              values: [274, 46, 14],
            },
            id2019: {
              labels: [
                ["Recyclable metal", "hydroxides (T)"],
                ["Other recyclable", "wastes (T)"],
                ["Non-recyclable", "wastes (T)"],
              ],
              values: [294, 32, 22],
            },
            colors: ["#496951", "#2C4533"],
          },
        },
      ],
    },
    {
      parentId: "bar-chart-box-5",
      max: 2500,
      selectedChart: "id2017",
      elements: [
        {
          id: "bar-chart-5",
          radioId: "bar-chart-box-radio-5",
          data: {
            id2017: {
              labels: [
                ["Rifiuti solidi", "urbani (m3)"],
                "Cartone (m3)",
                "PET (m3)",
                "Legname (ton)",
                ["Rifiuti plastici", "(ton)"],
                ["Rifiuti ferrosi", "(ton)"],
              ],
              values: [156, 95, 1983, 50, 14, 27],
            },
            id2018: {
              labels: [
                ["Rifiuti solidi", "urbani (m3)"],
                "Cartone (m3)",
                "PET (m3)",
                "Legname (ton)",
                ["Rifiuti plastici", "(ton)"],
                ["Rifiuti ferrosi", "(ton)"],
              ],
              values: [178, 96, 2039, 47, 13, 42],
            },
            id2019: {
              labels: [
                ["Rifiuti solidi", "urbani (m3)"],
                "Cartone (m3)",
                "PET (m3)",
                "Legname (ton)",
                ["Rifiuti plastici", "(ton)"],
                ["Rifiuti ferrosi", "(ton)"],
              ],
              values: [125, 152, 1977, 60, 16, 33],
            },
            colors: ["#496951", "#2C4533"],
          },
        },
        {
          id: "bar-chart-en-5",
          radioId: "bar-chart-box-radio-5",
          data: {
            id2017: {
              labels: [
                ["Solid urban", "wastes (m3)"],
                "Cardboard (m3)",
                "PET (m3)",
                "Wood (T)",
                ["Plastic wastes", "(T)"],
                ["Ferrous wastes", "(T)"],
              ],
              values: [156, 95, 1983, 50, 14, 27],
            },
            id2018: {
              labels: [
                ["Solid urban", "wastes (m3)"],
                "Cardboard (m3)",
                "PET (m3)",
                "Wood (T)",
                ["Plastic wastes", "(T)"],
                ["Ferrous wastes", "(T)"],
              ],
              values: [178, 96, 2039, 47, 13, 42],
            },
            id2019: {
              labels: [
                ["Solid urban", "wastes (m3)"],
                "Cardboard (m3)",
                "PET (m3)",
                "Wood (T)",
                ["Plastic wastes", "(T)"],
                ["Ferrous wastes", "(T)"],
              ],
              values: [125, 152, 1977, 60, 16, 33],
            },
            colors: ["#496951", "#2C4533"],
          },
        },
      ],
    },
  ];
  // END OF LOOP
  var didFocus = false;
  function updateChart(force = false, updateIndex) {
    chartElements.forEach((parent, childIndex) => {
      if (
        ($("#" + parent.parentId).css("opacity") > 0 && !parent.didFocus) ||
        force
      ) {
        //console.log("bar chart render done");
        parent.elements.forEach((item) => {
          if (force && updateIndex !== childIndex) {
            //console.log({ updateIndex, childIndex });
            return;
          }
          parent.didFocus = true;
          var data = item.data;
          var id = item.id;
          var lineId = item.lineBar;
          const selectedData = parent.selectedChart;
          if (!document.getElementById(id)) {
            return;
          }
          var barChartHorizontal = document.getElementById(id).getContext("2d");

          var gradientStroke = barChartHorizontal.createLinearGradient(
            500,
            0,
            100,
            0
          );
          gradientStroke.addColorStop(0, data.colors[0]);
          gradientStroke.addColorStop(1, data.colors[1]);
          barOptions.scales.xAxes[0].ticks.max = parent.max;
          // Define DATA
          // Chart 1
          var chartData1 = {
            labels: data[parent.selectedChart].labels,
            datasets: [
              {
                label: data[parent.selectedChart].labels,
                data: data[parent.selectedChart].values,
                fill: false,
                backgroundColor: [
                  gradientStroke,
                  gradientStroke,
                  gradientStroke,
                  gradientStroke,
                  gradientStroke,
                  gradientStroke,
                ],
                borderColor: [
                  "rgba(0, 0, 0, 0)",
                  "rgba(0, 0, 0, 0)",
                  "rgba(0, 0, 0, 0)",
                  "rgba(0, 0, 0, 0)",
                  "rgba(0, 0, 0, 0)",
                  "rgba(0, 0, 0, 0)",
                ],
                borderWidth: 1,
                scaleSteps: 0.5,
                scaleStartValue: 0,
                scaleStepWidth: 1,
              },
            ],
          };

          item.chart && item.chart.destroy();

          item.chart = new Chart(barChartHorizontal, {
            type: "horizontalBar",
            data: chartData1,
            options: barOptions,
          });

          if (document.getElementById(lineId)) {
            const lineValues = data[selectedData].lineValues;
            var lineChart = document.getElementById(lineId).getContext("2d");

            lineOptions.scales.xAxes[0].ticks.max = data[selectedData].lineMax;
            item.lineChart && item.lineChart.destroy();
            item.lineChart = new Chart(lineChart, {
              type: "horizontalBar",
              data: {
                datasets: [
                  {
                    label: "",
                    backgroundColor: "rgba(0,0,0,0)",
                    data: lineValues,
                  },
                  {
                    label: "",
                    borderWidth: 1,
                    lineTension: 0,
                    fill: false,
                    backgroundColor: "#000",
                    borderColor: "#000",
                    data: data[selectedData].labels.map((y, index) => ({
                      y,
                      x: lineValues[index],
                    })),
                    type: "line",
                  },
                ],
                labels: data[selectedData].labels,
              },
              options: lineOptions,
            });
          }

          if (item.radioId) {
            $("input[name='" + item.radioId + "']").unbind();
            $("input[name='" + item.radioId + "']").change(function (d) {
              parent.selectedChart = d.target.value;
              updateChart(true, childIndex);
            });
          }
        });
      } else if (!$("#" + parent.parentId).is(":visible") && parent.didFocus) {
        parent.didFocus = false;
      }
    });
  }

  var timeout = setTimeout(function () {
    updateChart();
  }, 1500);

  $(window).scroll(function () {
    //clearTimeout(timeout);
    timeout = setTimeout(function () {
      updateChart();
    }, 400);
  });
  updateChart();
});

var barMultiOptions = {
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  tooltips: {
    enabled: false,
  },
  hover: {
    animationDuration: 0,
  },
  animation: {
    duration: 3000,
    ease: "easeOutCubic",
    onProgress: function (animation) {
      const value =
        animation.animationObject.currentStep /
        animation.animationObject.numSteps;
      const aniValue = 1 - Math.pow(1 - value, 4);

      var chartInstance = this.chart,
        ctx = chartInstance.ctx;
      ctx.font = Chart.helpers.fontString(
        12, // Chart.defaults.global.defaultFontSize,
        "bold", // Chart.defaults.global.defaultFontStyle,
        Chart.defaults.global.defaultFontFamily // "'Monumentextended', 'sans-serif'",
      );
      ctx.textAlign = "center";
      ctx.textBaseline = "bottom";
      this.data.datasets.forEach(function (dataset, i) {
        var meta = chartInstance.controller.getDatasetMeta(i);
        var data = dataset.data.reduce((a, b) => a + b, 0);
        //var x = meta.data.reduce((a, b) => a + b._model.x, 0);
        //var y = meta.data.reduce((a, b) => a + b._model.y, 0);
        //console.log({x,y})
        //ctx.fillText(Math.round(data * aniValue), (100 - 100 * aniValue) + (x + 35) * aniValue, y + 5);
        //meta.data.forEach(function (bar, index) {
        //});
      });
    },
  },
  scales: {
    xAxes: [
      {
        stacked: true,
        ticks: {
          userCallback: function (label, index, labels) {
            return addMillis(label);
          },
          max: 67000,
          fontFamily: "Lato, sans-serif",
          fontColor: "rgba(0, 0, 0, 0);",
          fontSize: 16,
          beginAtZero: true,
          display: true,
          autoskip: true,
          autoSkipPadding: 50,
          maxRotation: 0,
          minRotation: 0,
        },
        gridLines: {
          display: false,
          drawBorder: false,
        },
      },
    ],
    yAxes: [
      {
        stacked: true,
        ticks: {
          fontFamily: "Lato, sans-serif",
          fontColor: "rgba(0, 0, 0, 0);",
          fontSize: 16,
          min: 0,
        },
        gridLines: {
          display: false,
          drawBorder: false,
        },
        barPercentage: 0.8,
        categoryPercentage: 0.8,
      },
    ],
  },
  plugins: {
    datalabels: {
      color: "rgba(0, 0, 0, 0);",
      font: {
        weight: "bold",
        size: "15",
        family: "'Lato', 'sans-serif'",
      },
      formatter: (value) => {
        return value;
      },
    },
  },
};

$(document).ready(function () {
  var chartElements = [
    {
      parentId: "bar-multi-chart-1-box",
      elements: [
        {
          id: "bar-multi-chart-1",
          selectedChart: "switzerland",
          radioId: "bar-multi-chart-1-options",
          lineImageContainer: "bar-multi-chart-1-image-cont",
          lineBar: "line-chart-1",
          //max: 4748944,
          data: {
            switzerland: {
              lineImage: "./images/svg/consumi-energia-SWITZERLAND.svg",
              labels: ["2019", "2018", "2017"],
              values: [
                [992080, 931224, 859729],
                [973542, 983957, 791790],
                [1448944, 1439894, 1197004],
                [686589, 591572, 520631],
                [108, 112, 100],
              ],
              lineMax: 150,
              lineValues: [108, 112, 100],
            },
            hongkong: {
              lineImage: "./images/svg/consumi-energia-HK.svg",
              labels: ["2019", "2018"],
              lineMax: 120,
              values: [
                [769750, 931224],
                [338030, 413385],
                [533475, 633200],
                [0, 0],
                [81, 100],
              ],
              lineValues: [81, 100],
            },
            colors: [
              ["#2C4533"],
              ["#4A6C54"],
              ["#6A9375"],
              ["#9CCDA9"],
              ["#C7F3D3"],
            ],
          },
        },
      ],
    },
    {
      parentId: "bar-multi-chart-2-box",
      elements: [
        {
          id: "bar-multi-chart-2",
          selectedChart: "switzerland",
          radioId: "bar-chart-box-radio-2",
          max: 60000,
          lineBar: "line-chart-2",
          data: {
            switzerland: {
              labels: ["2019", "2018", "2017"],
              values: [
                [0, 660, 328], // Gasolio
                [14092, 13287, 13262], // Metano
                [37796, 37796, 37796], // Corrente elettrica
                [1077, 1092, 1101], // Fotovoltaico
                [2849, 2606, 2650.53], // Propano
                [108, 112, 100], // Evoluzione della produzione
                // [0, 0, 0], // Energia
              ],
              lineMax: 130,
              lineValues: [108, 112, 100],
            },
            deutschland: {
              labels: ["2019", "2018", "2017"],
              values: [
                [0, 0, 0],
                [0, 0, 0],
                [226, 186, 160],
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0], // Energia
              ],
            },
            italia: {
              labels: ["2019", "2018", "2017"],
              values: [
                [0, 0, 0],
                [0, 0, 0],
                [269, 258, 267],
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0], // Energia
              ],
            },
            latina: {
              labels: ["2019", "2018", "2017"],
              values: [
                [0, 0, 0],
                [0, 0, 0],
                [1045, 1045, 1023],
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0], // Energia
              ],
            },
            aljiba: {
              labels: ["2019", "2018", "2017"],
              values: [
                [0, 0, 0],
                [0, 0, 0],
                [2494, 2128, 2183],
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0], // Energia
              ],
            },
            hongkong: {
              labels: ["2019", "2018"],
              values: [
                [0, 0],
                [0, 0],
                [0, 0],
                [0, 0],
                [0, 0],
                [81, 100],
                [21261, 21001], // Energia
              ],
              lineMax: 1000,
              lineValues: [81, 100],
            },
            colors: [
              ["#2C4533"], // Gasolio
              ["#4A6C54"], // Metano
              ["#6A9375"], // Corrente elettrica
              ["#9CCDA9"], // Fotovoltaico
              ["#C7F3D3"], // Propano
              ["#67ff95"], // Energia
              ["#87ff95"], // Energia
            ],
          },
        },
      ],
    },
  ];
  // END OF LOOP
  var didFocus = false;
  function updateChart(force = false, updateIndex) {
    chartElements.forEach((parent, childIndex) => {
      if (
        ($("#" + parent.parentId).css("opacity") > 0 && !parent.didFocus) ||
        force
      ) {
        parent.elements.forEach((item) => {
          if (force && updateIndex !== childIndex) {
            //console.log({ updateIndex, childIndex });
            return;
          }
          parent.didFocus = true;
          var data = item.data;
          var id = item.id;
          var lineId = item.lineBar;
          if (!document.getElementById(id)) {
            return;
          }
          var barChartHorizontal = document.getElementById(id).getContext("2d");
          var selectedData = item.selectedChart;
          $("#" + item.lineImageContainer).attr(
            "src",
            data[selectedData].lineImage
          ); // set line bar

          // Define DATA
          // Chart 1
          var chartData1 = {
            labels: data[selectedData].labels,
            datasets: data[selectedData].values.map(function (value, index) {
              const colors = data.colors[index];

              var gradientStroke = barChartHorizontal.createLinearGradient(
                500,
                0,
                0,
                0
              );
              colors.forEach(function (color, index) {
                gradientStroke.addColorStop(index, color);
              });
              barMultiOptions.scales.xAxes[0].ticks.max = item.max;
              return {
                label: data[selectedData].labels,
                data: value,
                fill: false,
                backgroundColor: [
                  gradientStroke,
                  gradientStroke,
                  gradientStroke,
                ],
                borderColor: [
                  "rgba(0, 0, 0, 0)",
                  "rgba(0, 0, 0, 0)",
                  "rgba(0, 0, 0, 0)",
                ],
                borderWidth: 1,
                scaleSteps: 0.5,
                scaleStartValue: 0,
                scaleStepWidth: 1,
              };
            }),
          };

          item.chart && item.chart.destroy();

          item.chart = new Chart(barChartHorizontal, {
            type: "horizontalBar",
            data: chartData1,
            options: barMultiOptions,
          });
          const lineValues = data[selectedData].lineValues;
          var lineChart = document.getElementById(lineId).getContext("2d");

          lineOptions.scales.xAxes[0].ticks.max = data[selectedData].lineMax;
          item.lineChart && item.lineChart.destroy();
          item.lineChart = new Chart(lineChart, {
            type: "horizontalBar",
            data: {
              datasets: [
                {
                  label: "",
                  backgroundColor: "rgba(0,0,0,0)",
                  data: lineValues,
                },
                {
                  label: "",
                  borderWidth: 1,
                  lineTension: 0,
                  fill: false,
                  backgroundColor: "#000",
                  borderColor: "#000",
                  data: data[selectedData].labels.map((y, index) => ({
                    y,
                    x: lineValues[index],
                  })),
                  type: "line",
                },
              ],
              labels: data[selectedData].labels,
            },
            options: lineOptions,
          });

          if (item.radioId) {
            $("input[name='" + item.radioId + "']").unbind();
            $("input[name='" + item.radioId + "']").change(function (d) {
              item.selectedChart = d.target.value;
              //console.log(d.target.value, item.selectedChart);
              updateChart(true, childIndex);
            });
          }
        });
      } else if (!$("#" + parent.parentId).is(":visible") && parent.didFocus) {
        parent.didFocus = false;
      }
    });
  }

  var timeout = setTimeout(function () {
    updateChart();
  }, 1500);

  $(window).scroll(function () {
    //clearTimeout(timeout);
    timeout = setTimeout(function () {
      updateChart();
    }, 400);
  });
  updateChart();
});
