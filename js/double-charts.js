/**
 * Bar Charts
 */
var barOptions = {
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  tooltips: {
    enabled: false,
  },
  events: [],
  animation: {
    duration: 3000,
    ease: "easeOutCubic",
    onComplete: function (animation) {
      console.log("complete");
      //const value = animation.animationObject.currentStep / animation.animationObject.numSteps;
      //const aniValue = 1 - Math.pow(1 - value, 4)

      //var chartInstance = this.chart,
      //ctx = chartInstance.ctx;
      //ctx.font = Chart.helpers.fontString(
      //14, // Chart.defaults.global.defaultFontSize,
      //'bold', // Chart.defaults.global.defaultFontStyle,
      //Chart.defaults.global.defaultFontFamily, // "'Monumentextended', 'sans-serif'",
      //);
      //ctx.textAlign = "center";
      //ctx.textBaseline = "bottom";
      //this.data.datasets.forEach(function (dataset, i) {
      //var meta = chartInstance.controller.getDatasetMeta(i);
      //meta.data.forEach(function (bar, index) {
      //var data = dataset.data[index];
      //ctx.fillText(Math.round(data * aniValue), (100 - 100 * aniValue) + (bar._model.x + 35) * aniValue, bar._model.y + 5);
      //});
      //});
    },
    onProgress: function (animation) {
      const value =
        animation.animationObject.currentStep /
        animation.animationObject.numSteps;
      const aniValue = 1 - Math.pow(1 - value, 4);

      var chartInstance = this.chart,
        ctx = chartInstance.ctx;
      ctx.font = Chart.helpers.fontString(
        14, // Chart.defaults.global.defaultFontSize,
        "bold", // Chart.defaults.global.defaultFontStyle,
        "Lato, sans-serif" // Chart.defaults.global.defaultFontFamily, // "'Monumentextended', 'sans-serif'",
      );
      ctx.textAlign = "center";
      ctx.textBaseline = "bottom";
      this.data.datasets.forEach(function (dataset, i) {
        var meta = chartInstance.controller.getDatasetMeta(i);
        meta.data.forEach(function (bar, index) {
          var data = dataset.data[index];
          if (data == 0) {
            return;
          }
          var isFinite = data % Math.ceil(data) > 0;

          var displayVal = isFinite
            ? parseFloat((data * aniValue).toFixed(2))
            : Math.round(data * aniValue);

          ctx.fillText(
            addMillis(displayVal),
            //parseFloat((data * aniValue).toFixed(2)).toString(),
            100 - 100 * aniValue + (bar._model.x + 35) * aniValue,
            bar._model.y + 8
          );
        });
      });
    },
  },
  scales: {
    xAxes: [
      {
        ticks: {
          userCallback: function (label, index, labels) {
            return addMillis(label);
          },
          max: 400,
          fontSize: 14,
          fontFamily: "Lato, sans-serif",
          beginAtZero: true,
          display: true,
        },
        gridLines: {
          display: false,
          drawBorder: false,
        },
      },
    ],
    yAxes: [
      {
        ticks: {
          fontFamily: "Lato, sans-serif",
          fontColor: "#000",
          fontSize: 16,
          min: 0,
        },
        gridLines: {
          display: false,
          drawBorder: false,
        },
        barPercentage: 1.0,
        categoryPercentage: 0.7,
      },
    ],
  },
  plugins: {
    datalabels: {
      color: "rgba(0, 0, 0, 0);",
      font: {
        weight: "bold",
        size: "15",
        family: "'Monumentextended', 'sans-serif'",
      },
      formatter: (value) => {
        return value;
      },
    },
  },
};

var selectedChart = "switzerland";

$(document).ready(function () {
  var chartElements = [
    {
      parentId: "bar-chart-box",
      elements: [
        {
          selectedChart: "switzerland",
          id: "bar-chart-1",
          radioName: "bar-chart-1-options",
          max: 400,
          data: {
            switzerland: {
              labels: ["2019", "2018", "2017"],
              values: [
                [294, 247, 225],
                [39, 41, 53],
              ],
            },
            deutschland: {
              labels: ["2019", "2018", "2017"],
              values: [[12, 10, 9]],
            },
            italia: {
              labels: ["2019", "2018", "2017"],
              values: [[9.3, 8.5, 11]],
            },
            latina: {
              labels: ["2019", "2018", "2017"],
              values: [[16, 19, 14]],
            },
            aljiba: {
              labels: ["2019", "2018", "2017"],
              values: [[32, 28, 30]],
            },
            hongkong: {
              labels: ["2019", "2018"],
              values: [[132, 131]],
            },
            colors: [["#6D6373", "#3A3743"], ["#80779A"]],
          },
        },
      ],
    },
    {
      parentId: "bar-double-chart-box-2",
      elements: [
        {
          id: "bar-double-chart-2",
          selectedChart: "switzerland",
          radioName: "bar-chart-2-options",
          max: 14,
          data: {
            switzerland: {
              labels: ["2019", "2018", "2017"],
              values: [
                [0.34, 0.34, 0.13],
                [0.81, 1.18, 0.78],
              ],
            },
            hongkong: {
              labels: ["2019", "2018"],
              values: [
                [0, 0, 0],
                [11.9, 7.48],
              ],
            },
            colors: [["#6D6373", "#3A3743"], ["#80779A"]],
          },
        },
      ],
    },
    {
      parentId: "bar-double-chart-box-3",
      elements: [
        {
          id: "bar-double-chart-3",
          selectedChart: "main",
          max: 1100,
          data: {
            main: {
              labels: [
                ["Formazione su", "qualità e processi"],
                ["Formazione su", "crescita personale", "e professionale"],
                ["Formazione", "sicurezza"],
              ],
              values: [
                [279, 0, 20],
                [0, 900, 209],
              ],
            },
            colors: [["#6D6373", "#3A3743"], ["#80779A"]],
          },
        },
        {
          id: "bar-double-chart-en-3",
          selectedChart: "main",
          max: 1100,
          data: {
            main: {
              labels: [
                ["Quality and processes", ""],
                ["Personal and", "professional growth", ""],
                ["Safety", ""],
              ],
              values: [
                [279, 0, 20],
                [0, 900, 209],
              ],
            },
            colors: [["#6D6373", "#3A3743"], ["#80779A"]],
          },
        },
      ],
    },
    {
      parentId: "bar-double-chart-box-4",
      elements: [
        {
          id: "bar-double-chart-4",
          selectedChart: "main",
          max: 400,
          data: {
            main: {
              labels: ["2019", "2018", "2017"],
              values: [
                [20, 122, 22],
                [209, 151, 329],
              ],
            },
            colors: [["#6D6373", "#3A3743"], ["#80779A"]],
          },
        },
      ],
    },
  ];
  // END OF LOOP
  var didFocus = false;
  function updateChart(force = false, updateIndex) {
    chartElements.forEach((parent, childIndex) => {
      if (
        ($("#" + parent.parentId).css("opacity") > 0 && !parent.didFocus) ||
        force
      ) {
        parent.elements.forEach((item) => {
          if (force && updateIndex !== childIndex) {
            console.log({ updateIndex, childIndex });
            return;
          }
          parent.didFocus = true;
          var data = item.data;
          var id = item.id;
          if (!document.getElementById(id)) {
            return;
          }
          var barChartHorizontal = document.getElementById(id).getContext("2d");
          var selectedData = item.selectedChart;

          // Define DATA
          // Chart 1
          var chartData1 = {
            labels: data[selectedData].labels,
            datasets: data[selectedData].values.map(function (value, index) {
              const colors = data.colors[index];

              var gradientStroke = barChartHorizontal.createLinearGradient(
                500,
                0,
                0,
                0
              );
              colors.forEach(function (color, index) {
                gradientStroke.addColorStop(index, color);
              });
              barOptions.scales.xAxes[0].ticks.max = item.max;
              return {
                label: data[selectedData].labels,
                data: value,
                fill: false,
                backgroundColor: [
                  gradientStroke,
                  gradientStroke,
                  gradientStroke,
                ],
                borderColor: [
                  "rgba(0, 0, 0, 0)",
                  "rgba(0, 0, 0, 0)",
                  "rgba(0, 0, 0, 0)",
                ],
                borderWidth: 1,
                scaleSteps: 0.5,
                scaleStartValue: 0,
                scaleStepWidth: 1,
              };
            }),
          };

          item.chart && item.chart.destroy();

          item.chart = new Chart(barChartHorizontal, {
            type: "horizontalBar",
            data: chartData1,
            options: barOptions,
          });

          if (item.radioName) {
            console.log("radio::: ", childIndex);
            $("input[name='" + item.radioName + "']").unbind();
            $("input[name='" + item.radioName + "']").change(function (d) {
              //$("input[name='"+item.radioName+"']").prop("checked", false);
              //$(this).prop("checked", true);
              item.selectedChart = d.target.value;
              console.log(d.target.value, item.selectedChart);
              updateChart(true, childIndex);
              // Do something interesting here
            });
          }
        });
      } else if (!$("#" + parent.parentId).is(":visible") && parent.didFocus) {
        parent.didFocus = false;
      }
    });
  }

  var timeout = setTimeout(function () {
    updateChart();
  }, 1500);

  $(window).scroll(function () {
    //clearTimeout(timeout);
    timeout = setTimeout(function () {
      updateChart();
    }, 300);
  });
});

// Purple color
var gradientStroke1 = "#3A3743";
var gradientStroke2 = "#5a5469";
var gradientStroke5 = "#514967";
var gradientStroke6 = "#80779A";
var gradientStroke7 = "#a49db8";
var gradientStroke8 = "#d2cce3";

var gradientStroke3 = "#55695A";
var gradientStroke4 = "#2B4533";

var chartOptions = {
  maintainAspectRatio: false, // This logic doesn't work
  cutoutPercentage: 70,
  legend: {
    display: false,
  },
  tooltips: {
    mode: "index",
    intersect: false,
  },
  hover: {
    //animationDuration: 0 // duration of animations when hovering an item
  },
  //events: [],
  // circumference: Math.PI*2*5,
  animation: {
    duration: 1000,
    ease: "easeOutCubic",
    onProgress: function (animation) {
      const value =
        animation.animationObject.currentStep /
        animation.animationObject.numSteps;
      const aniValue = 1 - Math.pow(1 - value, 4);

      var chartInstance = this.chart,
        ctx = chartInstance.ctx;
      ctx.textAlign = "center";
      ctx.textBaseline = "bottom";

      this.data.datasets.forEach(function (dataset, i) {
        var meta = chartInstance.controller.getDatasetMeta(i);
        meta.data.forEach(function (bar, index) {
          //console.log(bar);
          var modal = bar._model;
          var data = dataset.data[index];
          var radius = (modal.innerRadius + modal.outerRadius) / 2;
          var x =
            modal.x +
            radius * Math.cos((modal.endAngle + modal.startAngle) / 2);
          var y =
            modal.y +
            radius * Math.sin((modal.endAngle + modal.startAngle) / 2);

          ctx.font = Chart.helpers.fontString(
            10, // Chart.defaults.global.defaultFontSize,
            "normal", // Chart.defaults.global.defaultFontStyle,
            "Monumentextended, sans-serif" //Chart.defaults.global.defaultFontFamily, // "'Monumentextended', 'sans-serif'",
          );
          ctx.fillStyle = "#fff";
          ctx.fillText(Math.round(data) + "", x - 2, y);

          ctx.font = Chart.helpers.fontString(
            6, // Chart.defaults.global.defaultFontSize,
            "normal", // Chart.defaults.global.defaultFontStyle,
            "Monumentextended, sans-serif" //Chart.defaults.global.defaultFontFamily, // "'Monumentextended', 'sans-serif'",
          );
          ctx.fillText("%", x + 12, y);
        });
      });
    },
  },
  tooltips: {
    callbacks: {
      title: function (tooltipItem, data) {
        return data["labels"][tooltipItem[0]["index"]];
      },
      label: function (tooltipItem, data) {
        return null;
      },
      afterLabel: function (tooltipItem, data) {
        var dataset = data["datasets"][0];
        //var percent = Math.round((dataset['data'][tooltipItem['index']] / dataset["_meta"][0]['total']) * 100)
        //return '(' + percent + '%)';
      },
    },
    backgroundColor: "#FFF",
    titleFontSize: 16,
    titleFontColor: "#000",
    titleFontStyle: "normal",
    bodyFontColor: "#000",
    borderColor: "#496951",
    borderWidth: 1,
    xPadding: 20,
    titleFontFamily: "Lato, sans-serif",
    yPadding: 20,
    bodyFontSize: 14,
    displayColors: false,
  },
  plugins: {
    datalabels: {
      color: "#fff",
      font: {
        weight: "bold",
        size: "14",
        family: "'Monumentextended', 'sans-serif'",
      },
      formatter: (value) => {
        return value + " %";
      },
    },
  },
};

$(document).ready(function () {
  var chartElements = [
    //{
    //parentId: "pie-chart-box-1",
    //data: {
    //id: "pie-chart-1",
    //labels: ["Meno di 5 anni", "Da 5 a 9 anni", "10 + anni"],
    //backgroundColor: [gradientStroke1, gradientStroke2, gradientStroke5],
    //borderColor: ["rgba(0, 0, 0, 0)"],
    //borderWidth: 1,
    //values: [39, 19, 42],
    //},
    //},

    // TODO: Implement this logic inside this donut chart

    {
      parentId: "pie-chart-box-1",
      elements: [
        {
          id: "pie-chart-1",
          radioId: "bar-chart-box-radio-1",
          selectedChart: "switzerland",
          data: {
            switzerland: {
              labels: ["Meno di 5 anni", "Da 5 a 9 anni", "10 + anni"],
              backgroundColor: [
                gradientStroke1,
                gradientStroke2,
                gradientStroke5,
              ],
              borderColor: ["rgba(0, 0, 0, 0)"],
              values: [39, 19, 42],
              borderWidth: 1,
            },
            honkong: {
              labels: ["Meno di 5 anni", "Da 5 a 9 anni", "10 + anni"],
              backgroundColor: [
                gradientStroke1,
                gradientStroke2,
                gradientStroke5,
              ],
              borderColor: ["rgba(0, 0, 0, 0)"],
              values: [42, 12, 46],
              borderWidth: 1,
            },
          },
        },
      ],
    },

    {
      parentId: "pie-chart-box-1",
      elements: [
        {
          id: "pie-chart-en-1",
          radioId: "bar-chart-box-radio-1",
          selectedChart: "switzerland",
          data: {
            switzerland: {
              labels: ["Less than 5 years", "From 5 to 9 years", "+10 years"],
              backgroundColor: [
                gradientStroke1,
                gradientStroke2,
                gradientStroke5,
              ],
              borderColor: ["rgba(0, 0, 0, 0)"],
              values: [39, 19, 42],
              borderWidth: 1,
            },
            honkong: {
              labels: ["Less than 5 years", "From 5 to 9 years", "+10 years"],
              backgroundColor: [
                gradientStroke1,
                gradientStroke2,
                gradientStroke5,
              ],
              borderColor: ["rgba(0, 0, 0, 0)"],
              values: [42, 12, 46],
              borderWidth: 1,
            },
          },
        },
      ],
    },

    {
      parentId: "pie-chart-box-2",
      elements: [
        {
          id: "pie-chart-2",
          selectedChart: "switzerland",
          radioId: "bar-chart-box-radio-2",
          data: {
            switzerland: {
              labels: [
                "Scuola dell’obbligo",
                "Formazione professionale superiore",
                "SUP e università",
                "Maturità",
                "Formazione all’interno dell’impresa",
                "Formazione professionale completa (tirocinio)",
              ],
              backgroundColor: [
                gradientStroke1,
                gradientStroke2,
                gradientStroke5,
                gradientStroke6,
                gradientStroke7,
                gradientStroke8,
              ],
              borderColor: ["rgba(0, 0, 0, 0)"],
              borderWidth: 1,
              values: [14.63, 6.8, 51.02, 22, 4.76, 1.36],
            },
            honkong: {
              labels: [
                "Scuola dell’obbligo",
                "Formazione professionale superiore",
                "SUP e università",
                "Maturità",
                "Formazione all’interno dell’impresa",
                "Formazione professionale completa (tirocinio)",
              ],
              backgroundColor: [
                gradientStroke1,
                gradientStroke2,
                gradientStroke5,
                gradientStroke6,
                gradientStroke7,
                gradientStroke8,
              ],
              borderColor: ["rgba(0, 0, 0, 0)"],
              borderWidth: 1,
              values: [14.63, 6.8, 51.02, 22, 4.76, 1.36],
            },
          },
        },
      ],
    },

    {
      parentId: "pie-chart-box-2",
      elements: [
        {
          id: "pie-chart-en-2",
          selectedChart: "switzerland",
          radioId: "bar-chart-box-radio-2",
          data: {
            switzerland: {
              labels: [
                "Compulsory education",
                "Professional trade school",
                "Professional university trade school and university",
                "High school",
                "In-house training",
                "Complete professional training (with apprenticeship)",
              ],
              backgroundColor: [
                gradientStroke1,
                gradientStroke2,
                gradientStroke5,
                gradientStroke6,
                gradientStroke7,
                gradientStroke8,
              ],
              borderColor: ["rgba(0, 0, 0, 0)"],
              borderWidth: 1,
              values: [14.63, 6.8, 51.02, 22, 4.76, 1.36],
            },
            honkong: {
              labels: [
                "Compulsory education",
                "Professional trade school",
                "Professional university trade school and university",
                "High school",
                "In-house training",
                "Complete professional training (with apprenticeship)",
              ],
              backgroundColor: [
                gradientStroke1,
                gradientStroke2,
                gradientStroke5,
                gradientStroke6,
                gradientStroke7,
                gradientStroke8,
              ],
              borderColor: ["rgba(0, 0, 0, 0)"],
              borderWidth: 1,
              values: [14.63, 6.8, 51.02, 22, 4.76, 1.36],
            },
          },
        },
      ],
    },
  ];

  // END OF LOOP
  var didFocus = false;
  function updateChart(force = false, updateIndex) {
    chartElements.forEach((parent, childIndex) => {
      parent.elements.forEach(function (item) {
        if (
          ($("#" + parent.parentId).css("opacity") > 0 && !item.didFocus) ||
          force
        ) {
          if (force && updateIndex !== childIndex) {
            //console.log({ updateIndex, childIndex });
            return;
          }
          item.didFocus = true;
          var data = item.data;
          var id = item.id;
          if (!document.getElementById(id)) {
            return;
          }
          var donutsChart = document.getElementById(id).getContext("2d");
          const selectedChart = item.selectedChart;

          var chartData = {
            labels: data[selectedChart].labels,
            datasets: [
              {
                labels: data[selectedChart].labels,
                data: data[selectedChart].values,
                fill: true,
                backgroundColor: data[selectedChart].backgroundColor,
                borderColor: ["rgba(0, 0, 0, 0)"],
                borderWidth: 1,
              },
            ],
          };

          item.chart && item.chart.destroy();

          item.chart = new Chart(donutsChart, {
            type: "doughnut",
            data: chartData,
            options: chartOptions,
          });

          if (item.radioId) {
            $("input[name='" + item.radioId + "']").unbind();
            $("input[name='" + item.radioId + "']").change(function (d) {
              //console.log(parent.selectedChart);
              item.selectedChart = d.target.value;
              updateChart(true, childIndex);
              // Do something interesting here
            });
          }
        } else if (!$("#" + parent.parentId).is(":visible") && item.didFocus) {
          item.didFocus = false;
        }
      });
    });
  }

  var timeout = setTimeout(function () {
    updateChart();
  }, 1500);

  $(window).scroll(function () {
    //clearTimeout(timeout);
    timeout = setTimeout(function () {
      updateChart();
    }, 300);
  });
});

var singleChartOption = {
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  tooltips: {
    enabled: false,
  },
  hover: {
    animationDuration: 0,
  },
  events: [],
  animation: {
    duration: 3000,
    ease: "easeOutCubic",
    onProgress: function (animation) {
      const value =
        animation.animationObject.currentStep /
        animation.animationObject.numSteps;
      const aniValue = 1 - Math.pow(1 - value, 4);

      var chartInstance = this.chart,
        ctx = chartInstance.ctx;
      ctx.font = Chart.helpers.fontString(
        13, // Chart.defaults.global.defaultFontSize,
        "bold", // Chart.defaults.global.defaultFontStyle,
        "Lato, sans-serif" // "'Monumentextended', 'sans-serif'",
      );
      ctx.textAlign = "center";
      ctx.textBaseline = "bottom";
      this.data.datasets.forEach(function (dataset, i) {
        var meta = chartInstance.controller.getDatasetMeta(i);
        meta.data.forEach(function (bar, index) {
          var data = dataset.data[index];
          ctx.fillText(
            (data * aniValue).toFixed(1) + "%",
            100 - 100 * aniValue + (bar._model.x + 35) * aniValue,
            bar._model.y
          );
        });
      });
    },
  },
  scales: {
    xAxes: [
      {
        ticks: {
          userCallback: function (label, index, labels) {
            return addMillis(label);
          },
          max: 10,
          fontSize: 14,
          beginAtZero: true,
          display: true,
        },
        gridLines: {
          display: false,
          drawBorder: false,
        },
      },
    ],
    yAxes: [
      {
        ticks: {
          fontFamily: "Lato, sans-serif",
          fontColor: "#000",
          fontSize: 15,
          min: 0,
        },
        gridLines: {
          display: false,
          drawBorder: false,
        },
        barPercentage: 0.8,
        categoryPercentage: 0.8,
      },
    ],
  },
  plugins: {
    datalabels: {
      color: "rgba(0, 0, 0, 0);",
      font: {
        weight: "bold",
        size: "13",
        family: "'Monumentextended', 'sans-serif'",
      },
      formatter: (value) => {
        return value;
      },
    },
  },
};

$(document).ready(function () {
  var chartElements = [
    {
      parentId: "bar-chart-box-2",
      elements: [
        {
          id: "bar-chart-2",
          colors: ["#6D6373", "#3A3743"],
          max: 10,
          data: {
            labels: ["2019", "2018", "2017"],
            values: [7.3, 4, 6.1],
          },
        },
      ],
    },
  ];

  var didFocus = false;
  function updateChart(force = false) {
    chartElements.forEach((parent) => {
      if (
        ($("#" + parent.parentId).css("opacity") > 0 && !parent.didFocus) ||
        force
      ) {
        console.log("chart render done");

        parent.didFocus = true;
        parent.elements.forEach((item) => {
          var data = item.data;
          var id = item.id;
          if (!document.getElementById(id)) {
            return;
          }
          var barChartHorizontal = document.getElementById(id).getContext("2d");
          //barChartHorizontal.

          var gradientStroke = barChartHorizontal.createLinearGradient(
            500,
            0,
            100,
            0
          );
          gradientStroke.addColorStop(0, item.colors[0]);
          gradientStroke.addColorStop(1, item.colors[1]);
          // Define DATA
          // Chart 1
          var chartData1 = {
            labels: data.labels,
            datasets: [
              {
                label: data.labels,
                data: data.values,
                fill: false,
                backgroundColor: [
                  gradientStroke,
                  gradientStroke,
                  gradientStroke,
                ],
                borderColor: [
                  "rgba(0, 0, 0, 0)",
                  "rgba(0, 0, 0, 0)",
                  "rgba(0, 0, 0, 0)",
                ],
                borderWidth: 1,
                scaleSteps: 0.5,
                scaleStartValue: 0,
                scaleStepWidth: 1,
              },
            ],
          };

          item.chart && item.chart.destroy();

          item.chart = new Chart(barChartHorizontal, {
            type: "horizontalBar",
            data: chartData1,
            options: singleChartOption,
          });
        });
      } else if (!$("#" + parent.parentId).is(":visible") && parent.didFocus) {
        console.log("notViible");
        parent.didFocus = false;
      }
    });
  }

  var timeout = setTimeout(function () {
    updateChart();
  }, 1500);

  $(window).scroll(function () {
    //clearTimeout(timeout);
    timeout = setTimeout(function () {
      updateChart();
    }, 400);
  });

  $("input[name='options']").change(function (d) {
    console.log(d.target.value);
    selectedChart = d.target.value;
    updateChart(true);
    // Do something interesting here
  });
});
