var gradientStroke1 = "#AFAEAA";
var gradientStroke2 = "#605F57";
var gradientStroke3 = "#878780";
var gradientStroke4 = "#3B3A2F";

var chartOptions = {
  cutoutPercentage: 70,
  legend: {
    display: false,
  },
  tooltips: {
    mode: "index",
    intersect: false,
  },
  hover: {
    mode: "index",
    intersect: false,
  },
  tooltips: {
    callbacks: {
      title: function (tooltipItem, data) {
        return data["labels"][tooltipItem[0]["index"]];
      },
      label: function (tooltipItem, data) {
        return null;
      },
      afterLabel: function (tooltipItem, data) {
        var dataset = data["datasets"][0];
        //var percent = Math.round((dataset['data'][tooltipItem['index']] / dataset["_meta"][0]['total']) * 100)
        //return '(' + percent + '%)';
      },
    },
    backgroundColor: "#FFF",
    titleFontSize: 16,
    titleFontColor: "#000",
    titleFontStyle: "normal",
    bodyFontColor: "#000",
    borderColor: "#496951",
    borderWidth: 1,
    xPadding: 20,
    titleFontFamily: "Lato, sans-serif",
    yPadding: 20,
    bodyFontSize: 14,
    displayColors: false,
  },
  // circumference: Math.PI*2*5,
  animation: {
    duration: 1000,
    ease: "easeOutCubic",
    onProgress: function (animation) {
      const value =
        animation.animationObject.currentStep /
        animation.animationObject.numSteps;
      const aniValue = 1 - Math.pow(1 - value, 4);

      var chartInstance = this.chart,
        ctx = chartInstance.ctx;
      ctx.textAlign = "center";
      ctx.textBaseline = "bottom";

      this.data.datasets.forEach(function (dataset, i) {
        var meta = chartInstance.controller.getDatasetMeta(i);
        meta.data.forEach(function (bar, index) {
          //console.log(bar);
          var modal = bar._model;
          var data = dataset.data[index];
          var radius = (modal.innerRadius + modal.outerRadius) / 2;
          var x =
            modal.x +
            radius * Math.cos((modal.endAngle + modal.startAngle) / 2);
          var y =
            modal.y +
            radius * Math.sin((modal.endAngle + modal.startAngle) / 2);

          ctx.font = Chart.helpers.fontString(
            14, // Chart.defaults.global.defaultFontSize,
            "normal", // Chart.defaults.global.defaultFontStyle,
            "Monumentextended, sans-serif" //Chart.defaults.global.defaultFontFamily, // "'Monumentextended', 'sans-serif'",
          );
          ctx.fillStyle = "#fff";
          ctx.fillText(Math.round(data) + "", x, y);

          ctx.font = Chart.helpers.fontString(
            8, // Chart.defaults.global.defaultFontSize,
            "normal", // Chart.defaults.global.defaultFontStyle,
            "Monumentextended, sans-serif" //Chart.defaults.global.defaultFontFamily, // "'Monumentextended', 'sans-serif'",
          );
          ctx.fillText("%", x + 20, y);
        });
      });
    },
  },
  plugins: {
    datalabels: {
      color: "#fff",
      font: {
        weight: "bold",
        size: "15",
        family: "'Monumentextended', 'sans-serif'",
      },
      formatter: (value) => {
        return value + " %";
      },
    },
  },
};

$(document).ready(function () {
  var chartElements = [
    {
      parentId: "pie-chart-box-1",
      data: {
        id: "pie-chart-1",
        labels: ["Azienda", "Shareholder", "Collaboratori", "Istituzioni"],
        backgroundColor: [
          gradientStroke1,
          gradientStroke2,
          gradientStroke3,
          gradientStroke4,
        ],
        borderColor: [
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
        ],
        borderWidth: 1,
        values: [34, 56, 9],
      },
    },
    {
      parentId: "pie-chart-box-1",
      data: {
        id: "pie-chart-en-1",
        labels: ["Company", "Shareholder", "Employees", "Institutions"],
        backgroundColor: [
          gradientStroke1,
          gradientStroke2,
          gradientStroke3,
          gradientStroke4,
        ],
        borderColor: [
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
        ],
        borderWidth: 1,
        values: [34, 56, 9],
      },
    },
    {
      parentId: "pie-chart-box-2",
      data: {
        id: "pie-chart-2",
        labels: [
          "Canton Ticino",
          "Resto della Svizzera",
          "Unione Europea",
          "Extra UE",
        ],
        backgroundColor: [
          gradientStroke1,
          gradientStroke2,
          gradientStroke3,
          gradientStroke4,
        ],
        borderColor: [
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
        ],
        borderWidth: 1,
        values: [26, 45, 21, 9],
      },
    },
    {
      parentId: "pie-chart-box-2",
      data: {
        id: "pie-chart-en-2",
        labels: [
          "Ticino Canton",
          "Rest of Switzerland",
          "European Union",
          "Extra UE",
        ],
        backgroundColor: [
          gradientStroke1,
          gradientStroke2,
          gradientStroke3,
          gradientStroke4,
        ],
        borderColor: [
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
          "rgba(0, 0, 0, 0)",
        ],
        borderWidth: 1,
        values: [26, 45, 21, 9],
      },
    },
  ];

  // END OF LOOP
  var didFocus = false;
  function updateChart(force = false) {
    chartElements.forEach((item) => {
      if (
        ($("#" + item.parentId).css("opacity") > 0 && !item.didFocus) ||
        force
      ) {
        item.didFocus = true;
        var data = item.data;
        var id = data.id;
        if (!document.getElementById(id)) {
          return;
        }
        var donutsChart = document.getElementById(id).getContext("2d");
        //console.log(data.values);

        var chartData = {
          labels: data.labels,
          datasets: [
            {
              labels: data.labels,
              data: data.values,
              fill: true,
              backgroundColor: data.backgroundColor,
              borderColor: [
                "rgba(0, 0, 0, 0)",
                "rgba(0, 0, 0, 0)",
                "rgba(0, 0, 0, 0)",
                "rgba(0, 0, 0, 0)",
              ],
              borderWidth: 1,
            },
          ],
        };

        item.chart && item.chart.destroy();

        item.chart = new Chart(donutsChart, {
          type: "doughnut",
          data: chartData,
          options: chartOptions,
        });
      } else if (!$("#" + item.parentId).is(":visible") && item.didFocus) {
        item.didFocus = false;
      }
    });
  }

  var timeout = setTimeout(function () {
    updateChart();
  }, 1000);

  $(window).scroll(function () {
    //clearTimeout(timeout);
    timeout = setTimeout(function () {
      updateChart();
    }, 400);
  });

  $("input[name='options']").change(function (d) {
    //console.log(d.target.value);
    selectedChart = d.target.value;
    updateChart(true);
    // Do something interesting here
  });
  $(function () {
    // Button 1
    //$("input#option1[type='radio']").click(function (event) {
    //if ($(this).is(":checked")) {
    //var barChartHorizontal = document.getElementById("bar-chart-1-horizontal").getContext("2d");
    //myChart.destroy();
    //myChart = new Chart(barChartHorizontal, {
    //type: "horizontalBar",
    //data: chartData1,
    //options: chartOptions
    //});
    //}
    //});
  });
  //myChart = new Chart(barChartHorizontal, {
  //   type: "horizontalBar",
  //  data: chartData1,
  // options: chartOptions2
  //});
  // Radio button interaction
});
