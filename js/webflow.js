/*!
 * Webflow: Front-end site library
 * @license MIT
 * Inline scripts may access the api using an async handler:
 *   var Webflow = Webflow || [];
 *   Webflow.push(readyFunction);
 */ !(function (t) {
  var e = {};
  function n(r) {
    if (e[r]) return e[r].exports;
    var i = (e[r] = { i: r, l: !1, exports: {} });
    return t[r].call(i.exports, i, i.exports, n), (i.l = !0), i.exports;
  }
  (n.m = t),
    (n.c = e),
    (n.d = function (t, e, r) {
      n.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: r });
    }),
    (n.r = function (t) {
      "undefined" != typeof Symbol &&
        Symbol.toStringTag &&
        Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }),
        Object.defineProperty(t, "__esModule", { value: !0 });
    }),
    (n.t = function (t, e) {
      if ((1 & e && (t = n(t)), 8 & e)) return t;
      if (4 & e && "object" == typeof t && t && t.__esModule) return t;
      var r = Object.create(null);
      if (
        (n.r(r),
        Object.defineProperty(r, "default", { enumerable: !0, value: t }),
        2 & e && "string" != typeof t)
      )
        for (var i in t)
          n.d(
            r,
            i,
            function (e) {
              return t[e];
            }.bind(null, i)
          );
      return r;
    }),
    (n.n = function (t) {
      var e =
        t && t.__esModule
          ? function () {
              return t.default;
            }
          : function () {
              return t;
            };
      return n.d(e, "a", e), e;
    }),
    (n.o = function (t, e) {
      return Object.prototype.hasOwnProperty.call(t, e);
    }),
    (n.p = ""),
    n((n.s = 120));
})([
  function (t, e) {
    t.exports = function (t) {
      return t && t.__esModule ? t : { default: t };
    };
  },
  function (t, e) {
    var n = Array.isArray;
    t.exports = n;
  },
  function (t, e, n) {
    "use strict";
    var r = n(13);
    Object.defineProperty(e, "__esModule", { value: !0 });
    var i = { IX2EngineActionTypes: !0, IX2EngineConstants: !0 };
    e.IX2EngineConstants = e.IX2EngineActionTypes = void 0;
    var o = n(169);
    Object.keys(o).forEach(function (t) {
      "default" !== t &&
        "__esModule" !== t &&
        (Object.prototype.hasOwnProperty.call(i, t) ||
          Object.defineProperty(e, t, {
            enumerable: !0,
            get: function () {
              return o[t];
            },
          }));
    });
    var a = n(170);
    Object.keys(a).forEach(function (t) {
      "default" !== t &&
        "__esModule" !== t &&
        (Object.prototype.hasOwnProperty.call(i, t) ||
          Object.defineProperty(e, t, {
            enumerable: !0,
            get: function () {
              return a[t];
            },
          }));
    });
    var u = n(171);
    Object.keys(u).forEach(function (t) {
      "default" !== t &&
        "__esModule" !== t &&
        (Object.prototype.hasOwnProperty.call(i, t) ||
          Object.defineProperty(e, t, {
            enumerable: !0,
            get: function () {
              return u[t];
            },
          }));
    });
    var c = r(n(172));
    e.IX2EngineActionTypes = c;
    var s = r(n(173));
    e.IX2EngineConstants = s;
  },
  function (t, e, n) {
    (function (e) {
      var n = "object",
        r = function (t) {
          return t && t.Math == Math && t;
        };
      t.exports =
        r(typeof globalThis == n && globalThis) ||
        r(typeof window == n && window) ||
        r(typeof self == n && self) ||
        r(typeof e == n && e) ||
        Function("return this")();
    }.call(this, n(22)));
  },
  function (t, e, n) {
    var r = n(89),
      i = "object" == typeof self && self && self.Object === Object && self,
      o = r || i || Function("return this")();
    t.exports = o;
  },
  function (t, e) {
    t.exports = function (t) {
      var e = typeof t;
      return null != t && ("object" == e || "function" == e);
    };
  },
  function (t, e, n) {
    "use strict";
    var r = {},
      i = {},
      o = [],
      a = window.Webflow || [],
      u = window.jQuery,
      c = u(window),
      s = u(document),
      f = u.isFunction,
      l = (r._ = n(122)),
      d = (r.tram = n(65) && u.tram),
      p = !1,
      v = !1;
    function h(t) {
      r.env() &&
        (f(t.design) && c.on("__wf_design", t.design),
        f(t.preview) && c.on("__wf_preview", t.preview)),
        f(t.destroy) && c.on("__wf_destroy", t.destroy),
        t.ready &&
          f(t.ready) &&
          (function (t) {
            if (p) return void t.ready();
            if (l.contains(o, t.ready)) return;
            o.push(t.ready);
          })(t);
    }
    function E(t) {
      f(t.design) && c.off("__wf_design", t.design),
        f(t.preview) && c.off("__wf_preview", t.preview),
        f(t.destroy) && c.off("__wf_destroy", t.destroy),
        t.ready &&
          f(t.ready) &&
          (function (t) {
            o = l.filter(o, function (e) {
              return e !== t.ready;
            });
          })(t);
    }
    (d.config.hideBackface = !1),
      (d.config.keepInherited = !0),
      (r.define = function (t, e, n) {
        i[t] && E(i[t]);
        var r = (i[t] = e(u, l, n) || {});
        return h(r), r;
      }),
      (r.require = function (t) {
        return i[t];
      }),
      (r.push = function (t) {
        p ? f(t) && t() : a.push(t);
      }),
      (r.env = function (t) {
        var e = window.__wf_design,
          n = void 0 !== e;
        return t
          ? "design" === t
            ? n && e
            : "preview" === t
            ? n && !e
            : "slug" === t
            ? n && window.__wf_slug
            : "editor" === t
            ? window.WebflowEditor
            : "test" === t
            ? window.__wf_test
            : "frame" === t
            ? window !== window.top
            : void 0
          : n;
      });
    var _,
      g = navigator.userAgent.toLowerCase(),
      y = (r.env.touch =
        "ontouchstart" in window ||
        (window.DocumentTouch && document instanceof window.DocumentTouch)),
      I = (r.env.chrome =
        /chrome/.test(g) &&
        /Google/.test(navigator.vendor) &&
        parseInt(g.match(/chrome\/(\d+)\./)[1], 10)),
      m = (r.env.ios = /(ipod|iphone|ipad)/.test(g));
    (r.env.safari = /safari/.test(g) && !I && !m),
      y &&
        s.on("touchstart mousedown", function (t) {
          _ = t.target;
        }),
      (r.validClick = y
        ? function (t) {
            return t === _ || u.contains(t, _);
          }
        : function () {
            return !0;
          });
    var T,
      O = "resize.webflow orientationchange.webflow load.webflow";
    function b(t, e) {
      var n = [],
        r = {};
      return (
        (r.up = l.throttle(function (t) {
          l.each(n, function (e) {
            e(t);
          });
        })),
        t && e && t.on(e, r.up),
        (r.on = function (t) {
          "function" == typeof t && (l.contains(n, t) || n.push(t));
        }),
        (r.off = function (t) {
          n = arguments.length
            ? l.filter(n, function (e) {
                return e !== t;
              })
            : [];
        }),
        r
      );
    }
    function A(t) {
      f(t) && t();
    }
    function S() {
      T && (T.reject(), c.off("load", T.resolve)),
        (T = new u.Deferred()),
        c.on("load", T.resolve);
    }
    (r.resize = b(c, O)),
      (r.scroll = b(
        c,
        "scroll.webflow resize.webflow orientationchange.webflow load.webflow"
      )),
      (r.redraw = b()),
      (r.location = function (t) {
        window.location = t;
      }),
      r.env() && (r.location = function () {}),
      (r.ready = function () {
        (p = !0),
          v ? ((v = !1), l.each(i, h)) : l.each(o, A),
          l.each(a, A),
          r.resize.up();
      }),
      (r.load = function (t) {
        T.then(t);
      }),
      (r.destroy = function (t) {
        (t = t || {}),
          (v = !0),
          c.triggerHandler("__wf_destroy"),
          null != t.domready && (p = t.domready),
          l.each(i, E),
          r.resize.off(),
          r.scroll.off(),
          r.redraw.off(),
          (o = []),
          (a = []),
          "pending" === T.state() && S();
      }),
      u(r.ready),
      S(),
      (t.exports = window.Webflow = r);
  },
  function (t, e, n) {
    var r = n(176),
      i = n(230),
      o = n(59),
      a = n(1),
      u = n(239);
    t.exports = function (t) {
      return "function" == typeof t
        ? t
        : null == t
        ? o
        : "object" == typeof t
        ? a(t)
          ? i(t[0], t[1])
          : r(t)
        : u(t);
    };
  },
  function (t, e, n) {
    var r = n(188),
      i = n(193);
    t.exports = function (t, e) {
      var n = i(t, e);
      return r(n) ? n : void 0;
    };
  },
  function (t, e) {
    t.exports = function (t) {
      return null != t && "object" == typeof t;
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(13);
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.IX2VanillaUtils = e.IX2VanillaPlugins = e.IX2ElementsReducer = e.IX2EasingUtils = e.IX2Easings = e.IX2BrowserSupport = void 0);
    var i = r(n(45));
    e.IX2BrowserSupport = i;
    var o = r(n(106));
    e.IX2Easings = o;
    var a = r(n(108));
    e.IX2EasingUtils = a;
    var u = r(n(246));
    e.IX2ElementsReducer = u;
    var c = r(n(110));
    e.IX2VanillaPlugins = c;
    var s = r(n(248));
    e.IX2VanillaUtils = s;
  },
  function (t, e, n) {
    var r = n(19),
      i = n(189),
      o = n(190),
      a = "[object Null]",
      u = "[object Undefined]",
      c = r ? r.toStringTag : void 0;
    t.exports = function (t) {
      return null == t
        ? void 0 === t
          ? u
          : a
        : c && c in Object(t)
        ? i(t)
        : o(t);
    };
  },
  function (t, e, n) {
    var r = n(88),
      i = n(53);
    t.exports = function (t) {
      return null != t && i(t.length) && !r(t);
    };
  },
  function (t, e) {
    t.exports = function (t) {
      if (t && t.__esModule) return t;
      var e = {};
      if (null != t)
        for (var n in t)
          if (Object.prototype.hasOwnProperty.call(t, n)) {
            var r =
              Object.defineProperty && Object.getOwnPropertyDescriptor
                ? Object.getOwnPropertyDescriptor(t, n)
                : {};
            r.get || r.set ? Object.defineProperty(e, n, r) : (e[n] = t[n]);
          }
      return (e.default = t), e;
    };
  },
  function (t, e, n) {
    var r = n(15);
    t.exports = !r(function () {
      return (
        7 !=
        Object.defineProperty({}, "a", {
          get: function () {
            return 7;
          },
        }).a
      );
    });
  },
  function (t, e) {
    t.exports = function (t) {
      try {
        return !!t();
      } catch (t) {
        return !0;
      }
    };
  },
  function (t, e) {
    var n = {}.hasOwnProperty;
    t.exports = function (t, e) {
      return n.call(t, e);
    };
  },
  function (t, e, n) {
    var r = n(14),
      i = n(39),
      o = n(67);
    t.exports = r
      ? function (t, e, n) {
          return i.f(t, e, o(1, n));
        }
      : function (t, e, n) {
          return (t[e] = n), t;
        };
  },
  function (t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", { value: !0 });
    var r =
      "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
        ? function (t) {
            return typeof t;
          }
        : function (t) {
            return t &&
              "function" == typeof Symbol &&
              t.constructor === Symbol &&
              t !== Symbol.prototype
              ? "symbol"
              : typeof t;
          };
    (e.clone = c),
      (e.addLast = l),
      (e.addFirst = d),
      (e.removeLast = p),
      (e.removeFirst = v),
      (e.insert = h),
      (e.removeAt = E),
      (e.replaceAt = _),
      (e.getIn = g),
      (e.set = y),
      (e.setIn = I),
      (e.update = m),
      (e.updateIn = T),
      (e.merge = O),
      (e.mergeDeep = b),
      (e.mergeIn = A),
      (e.omit = S),
      (e.addDefaults = w);
    /*!
     * Timm
     *
     * Immutability helpers with fast reads and acceptable writes.
     *
     * @copyright Guillermo Grau Panea 2016
     * @license MIT
     */
    var i = "INVALID_ARGS";
    function o(t) {
      throw new Error(t);
    }
    function a(t) {
      var e = Object.keys(t);
      return Object.getOwnPropertySymbols
        ? e.concat(Object.getOwnPropertySymbols(t))
        : e;
    }
    var u = {}.hasOwnProperty;
    function c(t) {
      if (Array.isArray(t)) return t.slice();
      for (var e = a(t), n = {}, r = 0; r < e.length; r++) {
        var i = e[r];
        n[i] = t[i];
      }
      return n;
    }
    function s(t, e, n) {
      var r = n;
      null == r && o(i);
      for (
        var u = !1, l = arguments.length, d = Array(l > 3 ? l - 3 : 0), p = 3;
        p < l;
        p++
      )
        d[p - 3] = arguments[p];
      for (var v = 0; v < d.length; v++) {
        var h = d[v];
        if (null != h) {
          var E = a(h);
          if (E.length)
            for (var _ = 0; _ <= E.length; _++) {
              var g = E[_];
              if (!t || void 0 === r[g]) {
                var y = h[g];
                e && f(r[g]) && f(y) && (y = s(t, e, r[g], y)),
                  void 0 !== y &&
                    y !== r[g] &&
                    (u || ((u = !0), (r = c(r))), (r[g] = y));
              }
            }
        }
      }
      return r;
    }
    function f(t) {
      var e = void 0 === t ? "undefined" : r(t);
      return null != t && ("object" === e || "function" === e);
    }
    function l(t, e) {
      return Array.isArray(e) ? t.concat(e) : t.concat([e]);
    }
    function d(t, e) {
      return Array.isArray(e) ? e.concat(t) : [e].concat(t);
    }
    function p(t) {
      return t.length ? t.slice(0, t.length - 1) : t;
    }
    function v(t) {
      return t.length ? t.slice(1) : t;
    }
    function h(t, e, n) {
      return t
        .slice(0, e)
        .concat(Array.isArray(n) ? n : [n])
        .concat(t.slice(e));
    }
    function E(t, e) {
      return e >= t.length || e < 0 ? t : t.slice(0, e).concat(t.slice(e + 1));
    }
    function _(t, e, n) {
      if (t[e] === n) return t;
      for (var r = t.length, i = Array(r), o = 0; o < r; o++) i[o] = t[o];
      return (i[e] = n), i;
    }
    function g(t, e) {
      if ((!Array.isArray(e) && o(i), null != t)) {
        for (var n = t, r = 0; r < e.length; r++) {
          var a = e[r];
          if (void 0 === (n = null != n ? n[a] : void 0)) return n;
        }
        return n;
      }
    }
    function y(t, e, n) {
      var r = null == t ? ("number" == typeof e ? [] : {}) : t;
      if (r[e] === n) return r;
      var i = c(r);
      return (i[e] = n), i;
    }
    function I(t, e, n) {
      return e.length
        ? (function t(e, n, r, i) {
            var o = void 0,
              a = n[i];
            o =
              i === n.length - 1
                ? r
                : t(
                    f(e) && f(e[a])
                      ? e[a]
                      : "number" == typeof n[i + 1]
                      ? []
                      : {},
                    n,
                    r,
                    i + 1
                  );
            return y(e, a, o);
          })(t, e, n, 0)
        : n;
    }
    function m(t, e, n) {
      return y(t, e, n(null == t ? void 0 : t[e]));
    }
    function T(t, e, n) {
      return I(t, e, n(g(t, e)));
    }
    function O(t, e, n, r, i, o) {
      for (
        var a = arguments.length, u = Array(a > 6 ? a - 6 : 0), c = 6;
        c < a;
        c++
      )
        u[c - 6] = arguments[c];
      return u.length
        ? s.call.apply(s, [null, !1, !1, t, e, n, r, i, o].concat(u))
        : s(!1, !1, t, e, n, r, i, o);
    }
    function b(t, e, n, r, i, o) {
      for (
        var a = arguments.length, u = Array(a > 6 ? a - 6 : 0), c = 6;
        c < a;
        c++
      )
        u[c - 6] = arguments[c];
      return u.length
        ? s.call.apply(s, [null, !1, !0, t, e, n, r, i, o].concat(u))
        : s(!1, !0, t, e, n, r, i, o);
    }
    function A(t, e, n, r, i, o, a) {
      var u = g(t, e);
      null == u && (u = {});
      for (
        var c = arguments.length, f = Array(c > 7 ? c - 7 : 0), l = 7;
        l < c;
        l++
      )
        f[l - 7] = arguments[l];
      return I(
        t,
        e,
        f.length
          ? s.call.apply(s, [null, !1, !1, u, n, r, i, o, a].concat(f))
          : s(!1, !1, u, n, r, i, o, a)
      );
    }
    function S(t, e) {
      for (var n = Array.isArray(e) ? e : [e], r = !1, i = 0; i < n.length; i++)
        if (u.call(t, n[i])) {
          r = !0;
          break;
        }
      if (!r) return t;
      for (var o = {}, c = a(t), s = 0; s < c.length; s++) {
        var f = c[s];
        n.indexOf(f) >= 0 || (o[f] = t[f]);
      }
      return o;
    }
    function w(t, e, n, r, i, o) {
      for (
        var a = arguments.length, u = Array(a > 6 ? a - 6 : 0), c = 6;
        c < a;
        c++
      )
        u[c - 6] = arguments[c];
      return u.length
        ? s.call.apply(s, [null, !0, !1, t, e, n, r, i, o].concat(u))
        : s(!0, !1, t, e, n, r, i, o);
    }
    var R = {
      clone: c,
      addLast: l,
      addFirst: d,
      removeLast: p,
      removeFirst: v,
      insert: h,
      removeAt: E,
      replaceAt: _,
      getIn: g,
      set: y,
      setIn: I,
      update: m,
      updateIn: T,
      merge: O,
      mergeDeep: b,
      mergeIn: A,
      omit: S,
      addDefaults: w,
    };
    e.default = R;
  },
  function (t, e, n) {
    var r = n(4).Symbol;
    t.exports = r;
  },
  function (t, e, n) {
    var r = n(36),
      i = 1 / 0;
    t.exports = function (t) {
      if ("string" == typeof t || r(t)) return t;
      var e = t + "";
      return "0" == e && 1 / t == -i ? "-0" : e;
    };
  },
  function (t, e) {
    function n(t) {
      return (n =
        "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
          ? function (t) {
              return typeof t;
            }
          : function (t) {
              return t &&
                "function" == typeof Symbol &&
                t.constructor === Symbol &&
                t !== Symbol.prototype
                ? "symbol"
                : typeof t;
            })(t);
    }
    function r(e) {
      return (
        "function" == typeof Symbol && "symbol" === n(Symbol.iterator)
          ? (t.exports = r = function (t) {
              return n(t);
            })
          : (t.exports = r = function (t) {
              return t &&
                "function" == typeof Symbol &&
                t.constructor === Symbol &&
                t !== Symbol.prototype
                ? "symbol"
                : n(t);
            }),
        r(e)
      );
    }
    t.exports = r;
  },
  function (t, e) {
    var n;
    n = (function () {
      return this;
    })();
    try {
      n = n || new Function("return this")();
    } catch (t) {
      "object" == typeof window && (n = window);
    }
    t.exports = n;
  },
  function (t, e) {
    t.exports = function (t) {
      return "object" == typeof t ? null !== t : "function" == typeof t;
    };
  },
  function (t, e, n) {
    var r = n(23);
    t.exports = function (t) {
      if (!r(t)) throw TypeError(String(t) + " is not an object");
      return t;
    };
  },
  function (t, e, n) {
    var r = n(3),
      i = n(40),
      o = n(135),
      a = r["__core-js_shared__"] || i("__core-js_shared__", {});
    (t.exports = function (t, e) {
      return a[t] || (a[t] = void 0 !== e ? e : {});
    })("versions", []).push({
      version: "3.1.3",
      mode: o ? "pure" : "global",
      copyright: "© 2019 Denis Pushkarev (zloirock.ru)",
    });
  },
  function (t, e) {
    t.exports = function (t, e, n) {
      return (
        e in t
          ? Object.defineProperty(t, e, {
              value: n,
              enumerable: !0,
              configurable: !0,
              writable: !0,
            })
          : (t[e] = n),
        t
      );
    };
  },
  function (t, e) {
    function n() {
      return (
        (t.exports = n =
          Object.assign ||
          function (t) {
            for (var e = 1; e < arguments.length; e++) {
              var n = arguments[e];
              for (var r in n)
                Object.prototype.hasOwnProperty.call(n, r) && (t[r] = n[r]);
            }
            return t;
          }),
        n.apply(this, arguments)
      );
    }
    t.exports = n;
  },
  function (t, e, n) {
    var r = n(178),
      i = n(179),
      o = n(180),
      a = n(181),
      u = n(182);
    function c(t) {
      var e = -1,
        n = null == t ? 0 : t.length;
      for (this.clear(); ++e < n; ) {
        var r = t[e];
        this.set(r[0], r[1]);
      }
    }
    (c.prototype.clear = r),
      (c.prototype.delete = i),
      (c.prototype.get = o),
      (c.prototype.has = a),
      (c.prototype.set = u),
      (t.exports = c);
  },
  function (t, e, n) {
    var r = n(46);
    t.exports = function (t, e) {
      for (var n = t.length; n--; ) if (r(t[n][0], e)) return n;
      return -1;
    };
  },
  function (t, e, n) {
    var r = n(8)(Object, "create");
    t.exports = r;
  },
  function (t, e, n) {
    var r = n(202);
    t.exports = function (t, e) {
      var n = t.__data__;
      return r(e) ? n["string" == typeof e ? "string" : "hash"] : n.map;
    };
  },
  function (t, e, n) {
    var r = n(96),
      i = n(54),
      o = n(12);
    t.exports = function (t) {
      return o(t) ? r(t) : i(t);
    };
  },
  function (t, e, n) {
    var r = n(220),
      i = n(9),
      o = Object.prototype,
      a = o.hasOwnProperty,
      u = o.propertyIsEnumerable,
      c = r(
        (function () {
          return arguments;
        })()
      )
        ? r
        : function (t) {
            return i(t) && a.call(t, "callee") && !u.call(t, "callee");
          };
    t.exports = c;
  },
  function (t, e, n) {
    var r = n(57);
    t.exports = function (t, e, n) {
      var i = null == t ? void 0 : r(t, e);
      return void 0 === i ? n : i;
    };
  },
  function (t, e, n) {
    var r = n(1),
      i = n(58),
      o = n(231),
      a = n(234);
    t.exports = function (t, e) {
      return r(t) ? t : i(t, e) ? [t] : o(a(t));
    };
  },
  function (t, e, n) {
    var r = n(11),
      i = n(9),
      o = "[object Symbol]";
    t.exports = function (t) {
      return "symbol" == typeof t || (i(t) && r(t) == o);
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(123);
    function i(t, e) {
      var n = document.createEvent("CustomEvent");
      n.initCustomEvent(e, !0, !0, null), t.dispatchEvent(n);
    }
    var o = window.jQuery,
      a = {},
      u = {
        reset: function (t, e) {
          r.triggers.reset(t, e);
        },
        intro: function (t, e) {
          r.triggers.intro(t, e), i(e, "COMPONENT_ACTIVE");
        },
        outro: function (t, e) {
          r.triggers.outro(t, e), i(e, "COMPONENT_INACTIVE");
        },
      };
    (a.triggers = {}),
      (a.types = { INTRO: "w-ix-intro.w-ix", OUTRO: "w-ix-outro.w-ix" }),
      o.extend(a.triggers, u),
      (t.exports = a);
  },
  function (t, e, n) {
    var r = n(131),
      i = n(133);
    t.exports = function (t) {
      return r(i(t));
    };
  },
  function (t, e, n) {
    var r = n(14),
      i = n(69),
      o = n(24),
      a = n(68),
      u = Object.defineProperty;
    e.f = r
      ? u
      : function (t, e, n) {
          if ((o(t), (e = a(e, !0)), o(n), i))
            try {
              return u(t, e, n);
            } catch (t) {}
          if ("get" in n || "set" in n)
            throw TypeError("Accessors not supported");
          return "value" in n && (t[e] = n.value), t;
        };
  },
  function (t, e, n) {
    var r = n(3),
      i = n(17);
    t.exports = function (t, e) {
      try {
        i(r, t, e);
      } catch (n) {
        r[t] = e;
      }
      return e;
    };
  },
  function (t, e) {
    t.exports = {};
  },
  function (t, e) {
    t.exports = [
      "constructor",
      "hasOwnProperty",
      "isPrototypeOf",
      "propertyIsEnumerable",
      "toLocaleString",
      "toString",
      "valueOf",
    ];
  },
  function (t, e, n) {
    "use strict";
    n.r(e),
      n.d(e, "ActionTypes", function () {
        return o;
      }),
      n.d(e, "default", function () {
        return a;
      });
    var r = n(79),
      i = n(164),
      o = { INIT: "@@redux/INIT" };
    function a(t, e, n) {
      var u;
      if (
        ("function" == typeof e && void 0 === n && ((n = e), (e = void 0)),
        void 0 !== n)
      ) {
        if ("function" != typeof n)
          throw new Error("Expected the enhancer to be a function.");
        return n(a)(t, e);
      }
      if ("function" != typeof t)
        throw new Error("Expected the reducer to be a function.");
      var c = t,
        s = e,
        f = [],
        l = f,
        d = !1;
      function p() {
        l === f && (l = f.slice());
      }
      function v() {
        return s;
      }
      function h(t) {
        if ("function" != typeof t)
          throw new Error("Expected listener to be a function.");
        var e = !0;
        return (
          p(),
          l.push(t),
          function () {
            if (e) {
              (e = !1), p();
              var n = l.indexOf(t);
              l.splice(n, 1);
            }
          }
        );
      }
      function E(t) {
        if (!Object(r.default)(t))
          throw new Error(
            "Actions must be plain objects. Use custom middleware for async actions."
          );
        if (void 0 === t.type)
          throw new Error(
            'Actions may not have an undefined "type" property. Have you misspelled a constant?'
          );
        if (d) throw new Error("Reducers may not dispatch actions.");
        try {
          (d = !0), (s = c(s, t));
        } finally {
          d = !1;
        }
        for (var e = (f = l), n = 0; n < e.length; n++) e[n]();
        return t;
      }
      return (
        E({ type: o.INIT }),
        ((u = {
          dispatch: E,
          subscribe: h,
          getState: v,
          replaceReducer: function (t) {
            if ("function" != typeof t)
              throw new Error("Expected the nextReducer to be a function.");
            (c = t), E({ type: o.INIT });
          },
        })[i.default] = function () {
          var t,
            e = h;
          return (
            ((t = {
              subscribe: function (t) {
                if ("object" != typeof t)
                  throw new TypeError("Expected the observer to be an object.");
                function n() {
                  t.next && t.next(v());
                }
                return n(), { unsubscribe: e(n) };
              },
            })[i.default] = function () {
              return this;
            }),
            t
          );
        }),
        u
      );
    }
  },
  function (t, e, n) {
    "use strict";
    function r() {
      for (var t = arguments.length, e = Array(t), n = 0; n < t; n++)
        e[n] = arguments[n];
      if (0 === e.length)
        return function (t) {
          return t;
        };
      if (1 === e.length) return e[0];
      var r = e[e.length - 1],
        i = e.slice(0, -1);
      return function () {
        return i.reduceRight(function (t, e) {
          return e(t);
        }, r.apply(void 0, arguments));
      };
    }
    n.r(e),
      n.d(e, "default", function () {
        return r;
      });
  },
  function (t, e, n) {
    "use strict";
    var r = n(0);
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.TRANSFORM_STYLE_PREFIXED = e.TRANSFORM_PREFIXED = e.FLEX_PREFIXED = e.ELEMENT_MATCHES = e.withBrowser = e.IS_BROWSER_ENV = void 0);
    var i = r(n(85)),
      o = "undefined" != typeof window;
    e.IS_BROWSER_ENV = o;
    var a = function (t, e) {
      return o ? t() : e;
    };
    e.withBrowser = a;
    var u = a(function () {
      return (0,
      i.default)(["matches", "matchesSelector", "mozMatchesSelector", "msMatchesSelector", "oMatchesSelector", "webkitMatchesSelector"], function (t) {
        return t in Element.prototype;
      });
    });
    e.ELEMENT_MATCHES = u;
    var c = a(function () {
      var t = document.createElement("i"),
        e = ["flex", "-webkit-flex", "-ms-flexbox", "-moz-box", "-webkit-box"];
      try {
        for (var n = e.length, r = 0; r < n; r++) {
          var i = e[r];
          if (((t.style.display = i), t.style.display === i)) return i;
        }
        return "";
      } catch (t) {
        return "";
      }
    }, "flex");
    e.FLEX_PREFIXED = c;
    var s = a(function () {
      var t = document.createElement("i");
      if (null == t.style.transform)
        for (var e = ["Webkit", "Moz", "ms"], n = e.length, r = 0; r < n; r++) {
          var i = e[r] + "Transform";
          if (void 0 !== t.style[i]) return i;
        }
      return "transform";
    }, "transform");
    e.TRANSFORM_PREFIXED = s;
    var f = s.split("transform")[0],
      l = f ? f + "TransformStyle" : "transformStyle";
    e.TRANSFORM_STYLE_PREFIXED = l;
  },
  function (t, e) {
    t.exports = function (t, e) {
      return t === e || (t != t && e != e);
    };
  },
  function (t, e, n) {
    var r = n(8)(n(4), "Map");
    t.exports = r;
  },
  function (t, e, n) {
    var r = n(194),
      i = n(201),
      o = n(203),
      a = n(204),
      u = n(205);
    function c(t) {
      var e = -1,
        n = null == t ? 0 : t.length;
      for (this.clear(); ++e < n; ) {
        var r = t[e];
        this.set(r[0], r[1]);
      }
    }
    (c.prototype.clear = r),
      (c.prototype.delete = i),
      (c.prototype.get = o),
      (c.prototype.has = a),
      (c.prototype.set = u),
      (t.exports = c);
  },
  function (t, e) {
    t.exports = function (t, e) {
      for (var n = -1, r = e.length, i = t.length; ++n < r; ) t[i + n] = e[n];
      return t;
    };
  },
  function (t, e, n) {
    (function (t) {
      var r = n(4),
        i = n(221),
        o = e && !e.nodeType && e,
        a = o && "object" == typeof t && t && !t.nodeType && t,
        u = a && a.exports === o ? r.Buffer : void 0,
        c = (u ? u.isBuffer : void 0) || i;
      t.exports = c;
    }.call(this, n(97)(t)));
  },
  function (t, e) {
    var n = 9007199254740991,
      r = /^(?:0|[1-9]\d*)$/;
    t.exports = function (t, e) {
      var i = typeof t;
      return (
        !!(e = null == e ? n : e) &&
        ("number" == i || ("symbol" != i && r.test(t))) &&
        t > -1 &&
        t % 1 == 0 &&
        t < e
      );
    };
  },
  function (t, e, n) {
    var r = n(222),
      i = n(223),
      o = n(224),
      a = o && o.isTypedArray,
      u = a ? i(a) : r;
    t.exports = u;
  },
  function (t, e) {
    var n = 9007199254740991;
    t.exports = function (t) {
      return "number" == typeof t && t > -1 && t % 1 == 0 && t <= n;
    };
  },
  function (t, e, n) {
    var r = n(55),
      i = n(225),
      o = Object.prototype.hasOwnProperty;
    t.exports = function (t) {
      if (!r(t)) return i(t);
      var e = [];
      for (var n in Object(t)) o.call(t, n) && "constructor" != n && e.push(n);
      return e;
    };
  },
  function (t, e) {
    var n = Object.prototype;
    t.exports = function (t) {
      var e = t && t.constructor;
      return t === (("function" == typeof e && e.prototype) || n);
    };
  },
  function (t, e, n) {
    var r = n(226),
      i = n(47),
      o = n(227),
      a = n(228),
      u = n(99),
      c = n(11),
      s = n(90),
      f = s(r),
      l = s(i),
      d = s(o),
      p = s(a),
      v = s(u),
      h = c;
    ((r && "[object DataView]" != h(new r(new ArrayBuffer(1)))) ||
      (i && "[object Map]" != h(new i())) ||
      (o && "[object Promise]" != h(o.resolve())) ||
      (a && "[object Set]" != h(new a())) ||
      (u && "[object WeakMap]" != h(new u()))) &&
      (h = function (t) {
        var e = c(t),
          n = "[object Object]" == e ? t.constructor : void 0,
          r = n ? s(n) : "";
        if (r)
          switch (r) {
            case f:
              return "[object DataView]";
            case l:
              return "[object Map]";
            case d:
              return "[object Promise]";
            case p:
              return "[object Set]";
            case v:
              return "[object WeakMap]";
          }
        return e;
      }),
      (t.exports = h);
  },
  function (t, e, n) {
    var r = n(35),
      i = n(20);
    t.exports = function (t, e) {
      for (var n = 0, o = (e = r(e, t)).length; null != t && n < o; )
        t = t[i(e[n++])];
      return n && n == o ? t : void 0;
    };
  },
  function (t, e, n) {
    var r = n(1),
      i = n(36),
      o = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
      a = /^\w*$/;
    t.exports = function (t, e) {
      if (r(t)) return !1;
      var n = typeof t;
      return (
        !(
          "number" != n &&
          "symbol" != n &&
          "boolean" != n &&
          null != t &&
          !i(t)
        ) ||
        a.test(t) ||
        !o.test(t) ||
        (null != e && t in Object(e))
      );
    };
  },
  function (t, e) {
    t.exports = function (t) {
      return t;
    };
  },
  function (t, e, n) {
    var r = n(5),
      i = n(36),
      o = NaN,
      a = /^\s+|\s+$/g,
      u = /^[-+]0x[0-9a-f]+$/i,
      c = /^0b[01]+$/i,
      s = /^0o[0-7]+$/i,
      f = parseInt;
    t.exports = function (t) {
      if ("number" == typeof t) return t;
      if (i(t)) return o;
      if (r(t)) {
        var e = "function" == typeof t.valueOf ? t.valueOf() : t;
        t = r(e) ? e + "" : e;
      }
      if ("string" != typeof t) return 0 === t ? t : +t;
      t = t.replace(a, "");
      var n = c.test(t);
      return n || s.test(t) ? f(t.slice(2), n ? 2 : 8) : u.test(t) ? o : +t;
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(0);
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.mediaQueriesDefined = e.viewportWidthChanged = e.actionListPlaybackChanged = e.elementStateChanged = e.instanceRemoved = e.instanceStarted = e.instanceAdded = e.parameterChanged = e.animationFrameChanged = e.eventStateChanged = e.testFrameRendered = e.eventListenerAdded = e.clearRequested = e.stopRequested = e.playbackRequested = e.previewRequested = e.sessionStopped = e.sessionStarted = e.sessionInitialized = e.rawDataImported = void 0);
    var i = r(n(27)),
      o = n(2),
      a = n(10),
      u = o.IX2EngineActionTypes,
      c = u.IX2_RAW_DATA_IMPORTED,
      s = u.IX2_SESSION_INITIALIZED,
      f = u.IX2_SESSION_STARTED,
      l = u.IX2_SESSION_STOPPED,
      d = u.IX2_PREVIEW_REQUESTED,
      p = u.IX2_PLAYBACK_REQUESTED,
      v = u.IX2_STOP_REQUESTED,
      h = u.IX2_CLEAR_REQUESTED,
      E = u.IX2_EVENT_LISTENER_ADDED,
      _ = u.IX2_TEST_FRAME_RENDERED,
      g = u.IX2_EVENT_STATE_CHANGED,
      y = u.IX2_ANIMATION_FRAME_CHANGED,
      I = u.IX2_PARAMETER_CHANGED,
      m = u.IX2_INSTANCE_ADDED,
      T = u.IX2_INSTANCE_STARTED,
      O = u.IX2_INSTANCE_REMOVED,
      b = u.IX2_ELEMENT_STATE_CHANGED,
      A = u.IX2_ACTION_LIST_PLAYBACK_CHANGED,
      S = u.IX2_VIEWPORT_WIDTH_CHANGED,
      w = u.IX2_MEDIA_QUERIES_DEFINED,
      R = a.IX2VanillaUtils.reifyState;
    e.rawDataImported = function (t) {
      return { type: c, payload: (0, i.default)({}, R(t)) };
    };
    e.sessionInitialized = function (t) {
      var e = t.hasBoundaryNodes;
      return { type: s, payload: { hasBoundaryNodes: e } };
    };
    e.sessionStarted = function () {
      return { type: f };
    };
    e.sessionStopped = function () {
      return { type: l };
    };
    e.previewRequested = function (t) {
      var e = t.rawData,
        n = t.defer;
      return { type: d, payload: { defer: n, rawData: e } };
    };
    e.playbackRequested = function (t) {
      var e = t.actionTypeId,
        n = void 0 === e ? o.ActionTypeConsts.GENERAL_START_ACTION : e,
        r = t.actionListId,
        i = t.actionItemId,
        a = t.eventId,
        u = t.allowEvents,
        c = t.immediate,
        s = t.testManual,
        f = t.verbose,
        l = t.rawData;
      return {
        type: p,
        payload: {
          actionTypeId: n,
          actionListId: r,
          actionItemId: i,
          testManual: s,
          eventId: a,
          allowEvents: u,
          immediate: c,
          verbose: f,
          rawData: l,
        },
      };
    };
    e.stopRequested = function (t) {
      return { type: v, payload: { actionListId: t } };
    };
    e.clearRequested = function () {
      return { type: h };
    };
    e.eventListenerAdded = function (t, e) {
      return { type: E, payload: { target: t, listenerParams: e } };
    };
    e.testFrameRendered = function () {
      var t =
        arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 1;
      return { type: _, payload: { step: t } };
    };
    e.eventStateChanged = function (t, e) {
      return { type: g, payload: { stateKey: t, newState: e } };
    };
    e.animationFrameChanged = function (t, e) {
      return { type: y, payload: { now: t, parameters: e } };
    };
    e.parameterChanged = function (t, e) {
      return { type: I, payload: { key: t, value: e } };
    };
    e.instanceAdded = function (t) {
      return { type: m, payload: (0, i.default)({}, t) };
    };
    e.instanceStarted = function (t, e) {
      return { type: T, payload: { instanceId: t, time: e } };
    };
    e.instanceRemoved = function (t) {
      return { type: O, payload: { instanceId: t } };
    };
    e.elementStateChanged = function (t, e, n, r) {
      return {
        type: b,
        payload: { elementId: t, actionTypeId: e, current: n, actionItem: r },
      };
    };
    e.actionListPlaybackChanged = function (t) {
      var e = t.actionListId,
        n = t.isPlaying;
      return { type: A, payload: { actionListId: e, isPlaying: n } };
    };
    e.viewportWidthChanged = function (t) {
      var e = t.width,
        n = t.mediaQueries;
      return { type: S, payload: { width: e, mediaQueries: n } };
    };
    e.mediaQueriesDefined = function () {
      return { type: w };
    };
  },
  function (t, e, n) {
    var r = n(117),
      i = n(63);
    function o(t, e) {
      (this.__wrapped__ = t),
        (this.__actions__ = []),
        (this.__chain__ = !!e),
        (this.__index__ = 0),
        (this.__values__ = void 0);
    }
    (o.prototype = r(i.prototype)),
      (o.prototype.constructor = o),
      (t.exports = o);
  },
  function (t, e) {
    t.exports = function () {};
  },
  function (t, e, n) {
    var r = n(117),
      i = n(63),
      o = 4294967295;
    function a(t) {
      (this.__wrapped__ = t),
        (this.__actions__ = []),
        (this.__dir__ = 1),
        (this.__filtered__ = !1),
        (this.__iteratees__ = []),
        (this.__takeCount__ = o),
        (this.__views__ = []);
    }
    (a.prototype = r(i.prototype)),
      (a.prototype.constructor = a),
      (t.exports = a);
  },
  function (t, e, n) {
    "use strict";
    var r = n(0)(n(21));
    window.tram = (function (t) {
      function e(t, e) {
        return new F.Bare().init(t, e);
      }
      function n(t) {
        return t.replace(/[A-Z]/g, function (t) {
          return "-" + t.toLowerCase();
        });
      }
      function i(t) {
        var e = parseInt(t.slice(1), 16);
        return [(e >> 16) & 255, (e >> 8) & 255, 255 & e];
      }
      function o(t, e, n) {
        return (
          "#" + ((1 << 24) | (t << 16) | (e << 8) | n).toString(16).slice(1)
        );
      }
      function a() {}
      function u(t, e, n) {
        s("Units do not match [" + t + "]: " + e + ", " + n);
      }
      function c(t, e, n) {
        if ((void 0 !== e && (n = e), void 0 === t)) return n;
        var r = n;
        return (
          $.test(t) || !Z.test(t)
            ? (r = parseInt(t, 10))
            : Z.test(t) && (r = 1e3 * parseFloat(t)),
          0 > r && (r = 0),
          r == r ? r : n
        );
      }
      function s(t) {
        H.debug && window && window.console.warn(t);
      }
      var f = (function (t, e, n) {
          function i(t) {
            return "object" == (0, r.default)(t);
          }
          function o(t) {
            return "function" == typeof t;
          }
          function a() {}
          return function r(u, c) {
            function s() {
              var t = new f();
              return o(t.init) && t.init.apply(t, arguments), t;
            }
            function f() {}
            c === n && ((c = u), (u = Object)), (s.Bare = f);
            var l,
              d = (a[t] = u[t]),
              p = (f[t] = s[t] = new a());
            return (
              (p.constructor = s),
              (s.mixin = function (e) {
                return (f[t] = s[t] = r(s, e)[t]), s;
              }),
              (s.open = function (t) {
                if (
                  ((l = {}),
                  o(t) ? (l = t.call(s, p, d, s, u)) : i(t) && (l = t),
                  i(l))
                )
                  for (var n in l) e.call(l, n) && (p[n] = l[n]);
                return o(p.init) || (p.init = u), s;
              }),
              s.open(c)
            );
          };
        })("prototype", {}.hasOwnProperty),
        l = {
          ease: [
            "ease",
            function (t, e, n, r) {
              var i = (t /= r) * t,
                o = i * t;
              return (
                e +
                n * (-2.75 * o * i + 11 * i * i + -15.5 * o + 8 * i + 0.25 * t)
              );
            },
          ],
          "ease-in": [
            "ease-in",
            function (t, e, n, r) {
              var i = (t /= r) * t,
                o = i * t;
              return e + n * (-1 * o * i + 3 * i * i + -3 * o + 2 * i);
            },
          ],
          "ease-out": [
            "ease-out",
            function (t, e, n, r) {
              var i = (t /= r) * t,
                o = i * t;
              return (
                e +
                n * (0.3 * o * i + -1.6 * i * i + 2.2 * o + -1.8 * i + 1.9 * t)
              );
            },
          ],
          "ease-in-out": [
            "ease-in-out",
            function (t, e, n, r) {
              var i = (t /= r) * t,
                o = i * t;
              return e + n * (2 * o * i + -5 * i * i + 2 * o + 2 * i);
            },
          ],
          linear: [
            "linear",
            function (t, e, n, r) {
              return (n * t) / r + e;
            },
          ],
          "ease-in-quad": [
            "cubic-bezier(0.550, 0.085, 0.680, 0.530)",
            function (t, e, n, r) {
              return n * (t /= r) * t + e;
            },
          ],
          "ease-out-quad": [
            "cubic-bezier(0.250, 0.460, 0.450, 0.940)",
            function (t, e, n, r) {
              return -n * (t /= r) * (t - 2) + e;
            },
          ],
          "ease-in-out-quad": [
            "cubic-bezier(0.455, 0.030, 0.515, 0.955)",
            function (t, e, n, r) {
              return (t /= r / 2) < 1
                ? (n / 2) * t * t + e
                : (-n / 2) * (--t * (t - 2) - 1) + e;
            },
          ],
          "ease-in-cubic": [
            "cubic-bezier(0.550, 0.055, 0.675, 0.190)",
            function (t, e, n, r) {
              return n * (t /= r) * t * t + e;
            },
          ],
          "ease-out-cubic": [
            "cubic-bezier(0.215, 0.610, 0.355, 1)",
            function (t, e, n, r) {
              return n * ((t = t / r - 1) * t * t + 1) + e;
            },
          ],
          "ease-in-out-cubic": [
            "cubic-bezier(0.645, 0.045, 0.355, 1)",
            function (t, e, n, r) {
              return (t /= r / 2) < 1
                ? (n / 2) * t * t * t + e
                : (n / 2) * ((t -= 2) * t * t + 2) + e;
            },
          ],
          "ease-in-quart": [
            "cubic-bezier(0.895, 0.030, 0.685, 0.220)",
            function (t, e, n, r) {
              return n * (t /= r) * t * t * t + e;
            },
          ],
          "ease-out-quart": [
            "cubic-bezier(0.165, 0.840, 0.440, 1)",
            function (t, e, n, r) {
              return -n * ((t = t / r - 1) * t * t * t - 1) + e;
            },
          ],
          "ease-in-out-quart": [
            "cubic-bezier(0.770, 0, 0.175, 1)",
            function (t, e, n, r) {
              return (t /= r / 2) < 1
                ? (n / 2) * t * t * t * t + e
                : (-n / 2) * ((t -= 2) * t * t * t - 2) + e;
            },
          ],
          "ease-in-quint": [
            "cubic-bezier(0.755, 0.050, 0.855, 0.060)",
            function (t, e, n, r) {
              return n * (t /= r) * t * t * t * t + e;
            },
          ],
          "ease-out-quint": [
            "cubic-bezier(0.230, 1, 0.320, 1)",
            function (t, e, n, r) {
              return n * ((t = t / r - 1) * t * t * t * t + 1) + e;
            },
          ],
          "ease-in-out-quint": [
            "cubic-bezier(0.860, 0, 0.070, 1)",
            function (t, e, n, r) {
              return (t /= r / 2) < 1
                ? (n / 2) * t * t * t * t * t + e
                : (n / 2) * ((t -= 2) * t * t * t * t + 2) + e;
            },
          ],
          "ease-in-sine": [
            "cubic-bezier(0.470, 0, 0.745, 0.715)",
            function (t, e, n, r) {
              return -n * Math.cos((t / r) * (Math.PI / 2)) + n + e;
            },
          ],
          "ease-out-sine": [
            "cubic-bezier(0.390, 0.575, 0.565, 1)",
            function (t, e, n, r) {
              return n * Math.sin((t / r) * (Math.PI / 2)) + e;
            },
          ],
          "ease-in-out-sine": [
            "cubic-bezier(0.445, 0.050, 0.550, 0.950)",
            function (t, e, n, r) {
              return (-n / 2) * (Math.cos((Math.PI * t) / r) - 1) + e;
            },
          ],
          "ease-in-expo": [
            "cubic-bezier(0.950, 0.050, 0.795, 0.035)",
            function (t, e, n, r) {
              return 0 === t ? e : n * Math.pow(2, 10 * (t / r - 1)) + e;
            },
          ],
          "ease-out-expo": [
            "cubic-bezier(0.190, 1, 0.220, 1)",
            function (t, e, n, r) {
              return t === r ? e + n : n * (1 - Math.pow(2, (-10 * t) / r)) + e;
            },
          ],
          "ease-in-out-expo": [
            "cubic-bezier(1, 0, 0, 1)",
            function (t, e, n, r) {
              return 0 === t
                ? e
                : t === r
                ? e + n
                : (t /= r / 2) < 1
                ? (n / 2) * Math.pow(2, 10 * (t - 1)) + e
                : (n / 2) * (2 - Math.pow(2, -10 * --t)) + e;
            },
          ],
          "ease-in-circ": [
            "cubic-bezier(0.600, 0.040, 0.980, 0.335)",
            function (t, e, n, r) {
              return -n * (Math.sqrt(1 - (t /= r) * t) - 1) + e;
            },
          ],
          "ease-out-circ": [
            "cubic-bezier(0.075, 0.820, 0.165, 1)",
            function (t, e, n, r) {
              return n * Math.sqrt(1 - (t = t / r - 1) * t) + e;
            },
          ],
          "ease-in-out-circ": [
            "cubic-bezier(0.785, 0.135, 0.150, 0.860)",
            function (t, e, n, r) {
              return (t /= r / 2) < 1
                ? (-n / 2) * (Math.sqrt(1 - t * t) - 1) + e
                : (n / 2) * (Math.sqrt(1 - (t -= 2) * t) + 1) + e;
            },
          ],
          "ease-in-back": [
            "cubic-bezier(0.600, -0.280, 0.735, 0.045)",
            function (t, e, n, r, i) {
              return (
                void 0 === i && (i = 1.70158),
                n * (t /= r) * t * ((i + 1) * t - i) + e
              );
            },
          ],
          "ease-out-back": [
            "cubic-bezier(0.175, 0.885, 0.320, 1.275)",
            function (t, e, n, r, i) {
              return (
                void 0 === i && (i = 1.70158),
                n * ((t = t / r - 1) * t * ((i + 1) * t + i) + 1) + e
              );
            },
          ],
          "ease-in-out-back": [
            "cubic-bezier(0.680, -0.550, 0.265, 1.550)",
            function (t, e, n, r, i) {
              return (
                void 0 === i && (i = 1.70158),
                (t /= r / 2) < 1
                  ? (n / 2) * t * t * ((1 + (i *= 1.525)) * t - i) + e
                  : (n / 2) *
                      ((t -= 2) * t * ((1 + (i *= 1.525)) * t + i) + 2) +
                    e
              );
            },
          ],
        },
        d = {
          "ease-in-back": "cubic-bezier(0.600, 0, 0.735, 0.045)",
          "ease-out-back": "cubic-bezier(0.175, 0.885, 0.320, 1)",
          "ease-in-out-back": "cubic-bezier(0.680, 0, 0.265, 1)",
        },
        p = document,
        v = window,
        h = "bkwld-tram",
        E = /[\-\.0-9]/g,
        _ = /[A-Z]/,
        g = "number",
        y = /^(rgb|#)/,
        I = /(em|cm|mm|in|pt|pc|px)$/,
        m = /(em|cm|mm|in|pt|pc|px|%)$/,
        T = /(deg|rad|turn)$/,
        O = "unitless",
        b = /(all|none) 0s ease 0s/,
        A = /^(width|height)$/,
        S = " ",
        w = p.createElement("a"),
        R = ["Webkit", "Moz", "O", "ms"],
        x = ["-webkit-", "-moz-", "-o-", "-ms-"],
        N = function (t) {
          if (t in w.style) return { dom: t, css: t };
          var e,
            n,
            r = "",
            i = t.split("-");
          for (e = 0; e < i.length; e++)
            r += i[e].charAt(0).toUpperCase() + i[e].slice(1);
          for (e = 0; e < R.length; e++)
            if ((n = R[e] + r) in w.style) return { dom: n, css: x[e] + t };
        },
        C = (e.support = {
          bind: Function.prototype.bind,
          transform: N("transform"),
          transition: N("transition"),
          backface: N("backface-visibility"),
          timing: N("transition-timing-function"),
        });
      if (C.transition) {
        var L = C.timing.dom;
        if (((w.style[L] = l["ease-in-back"][0]), !w.style[L]))
          for (var D in d) l[D][0] = d[D];
      }
      var P = (e.frame = (function () {
          var t =
            v.requestAnimationFrame ||
            v.webkitRequestAnimationFrame ||
            v.mozRequestAnimationFrame ||
            v.oRequestAnimationFrame ||
            v.msRequestAnimationFrame;
          return t && C.bind
            ? t.bind(v)
            : function (t) {
                v.setTimeout(t, 16);
              };
        })()),
        M = (e.now = (function () {
          var t = v.performance,
            e = t && (t.now || t.webkitNow || t.msNow || t.mozNow);
          return e && C.bind
            ? e.bind(t)
            : Date.now ||
                function () {
                  return +new Date();
                };
        })()),
        j = f(function (e) {
          function i(t, e) {
            var n = (function (t) {
                for (var e = -1, n = t ? t.length : 0, r = []; ++e < n; ) {
                  var i = t[e];
                  i && r.push(i);
                }
                return r;
              })(("" + t).split(S)),
              r = n[0];
            e = e || {};
            var i = Q[r];
            if (!i) return s("Unsupported property: " + r);
            if (!e.weak || !this.props[r]) {
              var o = i[0],
                a = this.props[r];
              return (
                a || (a = this.props[r] = new o.Bare()),
                a.init(this.$el, n, i, e),
                a
              );
            }
          }
          function o(t, e, n) {
            if (t) {
              var o = (0, r.default)(t);
              if (
                (e ||
                  (this.timer && this.timer.destroy(),
                  (this.queue = []),
                  (this.active = !1)),
                "number" == o && e)
              )
                return (
                  (this.timer = new W({
                    duration: t,
                    context: this,
                    complete: a,
                  })),
                  void (this.active = !0)
                );
              if ("string" == o && e) {
                switch (t) {
                  case "hide":
                    f.call(this);
                    break;
                  case "stop":
                    u.call(this);
                    break;
                  case "redraw":
                    l.call(this);
                    break;
                  default:
                    i.call(this, t, n && n[1]);
                }
                return a.call(this);
              }
              if ("function" == o) return void t.call(this, this);
              if ("object" == o) {
                var s = 0;
                p.call(
                  this,
                  t,
                  function (t, e) {
                    t.span > s && (s = t.span), t.stop(), t.animate(e);
                  },
                  function (t) {
                    "wait" in t && (s = c(t.wait, 0));
                  }
                ),
                  d.call(this),
                  s > 0 &&
                    ((this.timer = new W({ duration: s, context: this })),
                    (this.active = !0),
                    e && (this.timer.complete = a));
                var v = this,
                  h = !1,
                  E = {};
                P(function () {
                  p.call(v, t, function (t) {
                    t.active && ((h = !0), (E[t.name] = t.nextStyle));
                  }),
                    h && v.$el.css(E);
                });
              }
            }
          }
          function a() {
            if (
              (this.timer && this.timer.destroy(),
              (this.active = !1),
              this.queue.length)
            ) {
              var t = this.queue.shift();
              o.call(this, t.options, !0, t.args);
            }
          }
          function u(t) {
            var e;
            this.timer && this.timer.destroy(),
              (this.queue = []),
              (this.active = !1),
              "string" == typeof t
                ? ((e = {})[t] = 1)
                : (e =
                    "object" == (0, r.default)(t) && null != t
                      ? t
                      : this.props),
              p.call(this, e, v),
              d.call(this);
          }
          function f() {
            u.call(this), (this.el.style.display = "none");
          }
          function l() {
            this.el.offsetHeight;
          }
          function d() {
            var t,
              e,
              n = [];
            for (t in (this.upstream && n.push(this.upstream), this.props))
              (e = this.props[t]).active && n.push(e.string);
            (n = n.join(",")),
              this.style !== n &&
                ((this.style = n), (this.el.style[C.transition.dom] = n));
          }
          function p(t, e, r) {
            var o,
              a,
              u,
              c,
              s = e !== v,
              f = {};
            for (o in t)
              (u = t[o]),
                o in q
                  ? (f.transform || (f.transform = {}), (f.transform[o] = u))
                  : (_.test(o) && (o = n(o)),
                    o in Q ? (f[o] = u) : (c || (c = {}), (c[o] = u)));
            for (o in f) {
              if (((u = f[o]), !(a = this.props[o]))) {
                if (!s) continue;
                a = i.call(this, o);
              }
              e.call(this, a, u);
            }
            r && c && r.call(this, c);
          }
          function v(t) {
            t.stop();
          }
          function E(t, e) {
            t.set(e);
          }
          function g(t) {
            this.$el.css(t);
          }
          function y(t, n) {
            e[t] = function () {
              return this.children
                ? function (t, e) {
                    var n,
                      r = this.children.length;
                    for (n = 0; r > n; n++) t.apply(this.children[n], e);
                    return this;
                  }.call(this, n, arguments)
                : (this.el && n.apply(this, arguments), this);
            };
          }
          (e.init = function (e) {
            if (
              ((this.$el = t(e)),
              (this.el = this.$el[0]),
              (this.props = {}),
              (this.queue = []),
              (this.style = ""),
              (this.active = !1),
              H.keepInherited && !H.fallback)
            ) {
              var n = Y(this.el, "transition");
              n && !b.test(n) && (this.upstream = n);
            }
            C.backface &&
              H.hideBackface &&
              z(this.el, C.backface.css, "hidden");
          }),
            y("add", i),
            y("start", o),
            y("wait", function (t) {
              (t = c(t, 0)),
                this.active
                  ? this.queue.push({ options: t })
                  : ((this.timer = new W({
                      duration: t,
                      context: this,
                      complete: a,
                    })),
                    (this.active = !0));
            }),
            y("then", function (t) {
              return this.active
                ? (this.queue.push({ options: t, args: arguments }),
                  void (this.timer.complete = a))
                : s(
                    "No active transition timer. Use start() or wait() before then()."
                  );
            }),
            y("next", a),
            y("stop", u),
            y("set", function (t) {
              u.call(this, t), p.call(this, t, E, g);
            }),
            y("show", function (t) {
              "string" != typeof t && (t = "block"),
                (this.el.style.display = t);
            }),
            y("hide", f),
            y("redraw", l),
            y("destroy", function () {
              u.call(this),
                t.removeData(this.el, h),
                (this.$el = this.el = null);
            });
        }),
        F = f(j, function (e) {
          function n(e, n) {
            var r = t.data(e, h) || t.data(e, h, new j.Bare());
            return r.el || r.init(e), n ? r.start(n) : r;
          }
          e.init = function (e, r) {
            var i = t(e);
            if (!i.length) return this;
            if (1 === i.length) return n(i[0], r);
            var o = [];
            return (
              i.each(function (t, e) {
                o.push(n(e, r));
              }),
              (this.children = o),
              this
            );
          };
        }),
        k = f(function (t) {
          function e() {
            var t = this.get();
            this.update("auto");
            var e = this.get();
            return this.update(t), e;
          }
          function n(t) {
            var e = /rgba?\((\d+),\s*(\d+),\s*(\d+)/.exec(t);
            return (e ? o(e[1], e[2], e[3]) : t).replace(
              /#(\w)(\w)(\w)$/,
              "#$1$1$2$2$3$3"
            );
          }
          var i = 500,
            a = "ease",
            u = 0;
          (t.init = function (t, e, n, r) {
            (this.$el = t), (this.el = t[0]);
            var o = e[0];
            n[2] && (o = n[2]),
              K[o] && (o = K[o]),
              (this.name = o),
              (this.type = n[1]),
              (this.duration = c(e[1], this.duration, i)),
              (this.ease = (function (t, e, n) {
                return void 0 !== e && (n = e), t in l ? t : n;
              })(e[2], this.ease, a)),
              (this.delay = c(e[3], this.delay, u)),
              (this.span = this.duration + this.delay),
              (this.active = !1),
              (this.nextStyle = null),
              (this.auto = A.test(this.name)),
              (this.unit = r.unit || this.unit || H.defaultUnit),
              (this.angle = r.angle || this.angle || H.defaultAngle),
              H.fallback || r.fallback
                ? (this.animate = this.fallback)
                : ((this.animate = this.transition),
                  (this.string =
                    this.name +
                    S +
                    this.duration +
                    "ms" +
                    ("ease" != this.ease ? S + l[this.ease][0] : "") +
                    (this.delay ? S + this.delay + "ms" : "")));
          }),
            (t.set = function (t) {
              (t = this.convert(t, this.type)), this.update(t), this.redraw();
            }),
            (t.transition = function (t) {
              (this.active = !0),
                (t = this.convert(t, this.type)),
                this.auto &&
                  ("auto" == this.el.style[this.name] &&
                    (this.update(this.get()), this.redraw()),
                  "auto" == t && (t = e.call(this))),
                (this.nextStyle = t);
            }),
            (t.fallback = function (t) {
              var n =
                this.el.style[this.name] || this.convert(this.get(), this.type);
              (t = this.convert(t, this.type)),
                this.auto &&
                  ("auto" == n && (n = this.convert(this.get(), this.type)),
                  "auto" == t && (t = e.call(this))),
                (this.tween = new U({
                  from: n,
                  to: t,
                  duration: this.duration,
                  delay: this.delay,
                  ease: this.ease,
                  update: this.update,
                  context: this,
                }));
            }),
            (t.get = function () {
              return Y(this.el, this.name);
            }),
            (t.update = function (t) {
              z(this.el, this.name, t);
            }),
            (t.stop = function () {
              (this.active || this.nextStyle) &&
                ((this.active = !1),
                (this.nextStyle = null),
                z(this.el, this.name, this.get()));
              var t = this.tween;
              t && t.context && t.destroy();
            }),
            (t.convert = function (t, e) {
              if ("auto" == t && this.auto) return t;
              var i,
                o = "number" == typeof t,
                a = "string" == typeof t;
              switch (e) {
                case g:
                  if (o) return t;
                  if (a && "" === t.replace(E, "")) return +t;
                  i = "number(unitless)";
                  break;
                case y:
                  if (a) {
                    if ("" === t && this.original) return this.original;
                    if (e.test(t))
                      return "#" == t.charAt(0) && 7 == t.length ? t : n(t);
                  }
                  i = "hex or rgb string";
                  break;
                case I:
                  if (o) return t + this.unit;
                  if (a && e.test(t)) return t;
                  i = "number(px) or string(unit)";
                  break;
                case m:
                  if (o) return t + this.unit;
                  if (a && e.test(t)) return t;
                  i = "number(px) or string(unit or %)";
                  break;
                case T:
                  if (o) return t + this.angle;
                  if (a && e.test(t)) return t;
                  i = "number(deg) or string(angle)";
                  break;
                case O:
                  if (o) return t;
                  if (a && m.test(t)) return t;
                  i = "number(unitless) or string(unit or %)";
              }
              return (
                (function (t, e) {
                  s(
                    "Type warning: Expected: [" +
                      t +
                      "] Got: [" +
                      (0, r.default)(e) +
                      "] " +
                      e
                  );
                })(i, t),
                t
              );
            }),
            (t.redraw = function () {
              this.el.offsetHeight;
            });
        }),
        X = f(k, function (t, e) {
          t.init = function () {
            e.init.apply(this, arguments),
              this.original || (this.original = this.convert(this.get(), y));
          };
        }),
        G = f(k, function (t, e) {
          (t.init = function () {
            e.init.apply(this, arguments), (this.animate = this.fallback);
          }),
            (t.get = function () {
              return this.$el[this.name]();
            }),
            (t.update = function (t) {
              this.$el[this.name](t);
            });
        }),
        V = f(k, function (t, e) {
          function n(t, e) {
            var n, r, i, o, a;
            for (n in t)
              (i = (o = q[n])[0]),
                (r = o[1] || n),
                (a = this.convert(t[n], i)),
                e.call(this, r, a, i);
          }
          (t.init = function () {
            e.init.apply(this, arguments),
              this.current ||
                ((this.current = {}),
                q.perspective &&
                  H.perspective &&
                  ((this.current.perspective = H.perspective),
                  z(this.el, this.name, this.style(this.current)),
                  this.redraw()));
          }),
            (t.set = function (t) {
              n.call(this, t, function (t, e) {
                this.current[t] = e;
              }),
                z(this.el, this.name, this.style(this.current)),
                this.redraw();
            }),
            (t.transition = function (t) {
              var e = this.values(t);
              this.tween = new B({
                current: this.current,
                values: e,
                duration: this.duration,
                delay: this.delay,
                ease: this.ease,
              });
              var n,
                r = {};
              for (n in this.current) r[n] = n in e ? e[n] : this.current[n];
              (this.active = !0), (this.nextStyle = this.style(r));
            }),
            (t.fallback = function (t) {
              var e = this.values(t);
              this.tween = new B({
                current: this.current,
                values: e,
                duration: this.duration,
                delay: this.delay,
                ease: this.ease,
                update: this.update,
                context: this,
              });
            }),
            (t.update = function () {
              z(this.el, this.name, this.style(this.current));
            }),
            (t.style = function (t) {
              var e,
                n = "";
              for (e in t) n += e + "(" + t[e] + ") ";
              return n;
            }),
            (t.values = function (t) {
              var e,
                r = {};
              return (
                n.call(this, t, function (t, n, i) {
                  (r[t] = n),
                    void 0 === this.current[t] &&
                      ((e = 0),
                      ~t.indexOf("scale") && (e = 1),
                      (this.current[t] = this.convert(e, i)));
                }),
                r
              );
            });
        }),
        U = f(function (e) {
          function n() {
            var t,
              e,
              r,
              i = c.length;
            if (i) for (P(n), e = M(), t = i; t--; ) (r = c[t]) && r.render(e);
          }
          var r = { ease: l.ease[1], from: 0, to: 1 };
          (e.init = function (t) {
            (this.duration = t.duration || 0), (this.delay = t.delay || 0);
            var e = t.ease || r.ease;
            l[e] && (e = l[e][1]),
              "function" != typeof e && (e = r.ease),
              (this.ease = e),
              (this.update = t.update || a),
              (this.complete = t.complete || a),
              (this.context = t.context || this),
              (this.name = t.name);
            var n = t.from,
              i = t.to;
            void 0 === n && (n = r.from),
              void 0 === i && (i = r.to),
              (this.unit = t.unit || ""),
              "number" == typeof n && "number" == typeof i
                ? ((this.begin = n), (this.change = i - n))
                : this.format(i, n),
              (this.value = this.begin + this.unit),
              (this.start = M()),
              !1 !== t.autoplay && this.play();
          }),
            (e.play = function () {
              var t;
              this.active ||
                (this.start || (this.start = M()),
                (this.active = !0),
                (t = this),
                1 === c.push(t) && P(n));
            }),
            (e.stop = function () {
              var e, n, r;
              this.active &&
                ((this.active = !1),
                (e = this),
                (r = t.inArray(e, c)) >= 0 &&
                  ((n = c.slice(r + 1)),
                  (c.length = r),
                  n.length && (c = c.concat(n))));
            }),
            (e.render = function (t) {
              var e,
                n = t - this.start;
              if (this.delay) {
                if (n <= this.delay) return;
                n -= this.delay;
              }
              if (n < this.duration) {
                var r = this.ease(n, 0, 1, this.duration);
                return (
                  (e = this.startRGB
                    ? (function (t, e, n) {
                        return o(
                          t[0] + n * (e[0] - t[0]),
                          t[1] + n * (e[1] - t[1]),
                          t[2] + n * (e[2] - t[2])
                        );
                      })(this.startRGB, this.endRGB, r)
                    : (function (t) {
                        return Math.round(t * s) / s;
                      })(this.begin + r * this.change)),
                  (this.value = e + this.unit),
                  void this.update.call(this.context, this.value)
                );
              }
              (e = this.endHex || this.begin + this.change),
                (this.value = e + this.unit),
                this.update.call(this.context, this.value),
                this.complete.call(this.context),
                this.destroy();
            }),
            (e.format = function (t, e) {
              if (((e += ""), "#" == (t += "").charAt(0)))
                return (
                  (this.startRGB = i(e)),
                  (this.endRGB = i(t)),
                  (this.endHex = t),
                  (this.begin = 0),
                  void (this.change = 1)
                );
              if (!this.unit) {
                var n = e.replace(E, "");
                n !== t.replace(E, "") && u("tween", e, t), (this.unit = n);
              }
              (e = parseFloat(e)),
                (t = parseFloat(t)),
                (this.begin = this.value = e),
                (this.change = t - e);
            }),
            (e.destroy = function () {
              this.stop(),
                (this.context = null),
                (this.ease = this.update = this.complete = a);
            });
          var c = [],
            s = 1e3;
        }),
        W = f(U, function (t) {
          (t.init = function (t) {
            (this.duration = t.duration || 0),
              (this.complete = t.complete || a),
              (this.context = t.context),
              this.play();
          }),
            (t.render = function (t) {
              t - this.start < this.duration ||
                (this.complete.call(this.context), this.destroy());
            });
        }),
        B = f(U, function (t, e) {
          (t.init = function (t) {
            var e, n;
            for (e in ((this.context = t.context),
            (this.update = t.update),
            (this.tweens = []),
            (this.current = t.current),
            t.values))
              (n = t.values[e]),
                this.current[e] !== n &&
                  this.tweens.push(
                    new U({
                      name: e,
                      from: this.current[e],
                      to: n,
                      duration: t.duration,
                      delay: t.delay,
                      ease: t.ease,
                      autoplay: !1,
                    })
                  );
            this.play();
          }),
            (t.render = function (t) {
              var e,
                n,
                r = !1;
              for (e = this.tweens.length; e--; )
                (n = this.tweens[e]).context &&
                  (n.render(t), (this.current[n.name] = n.value), (r = !0));
              return r
                ? void (this.update && this.update.call(this.context))
                : this.destroy();
            }),
            (t.destroy = function () {
              if ((e.destroy.call(this), this.tweens)) {
                var t;
                for (t = this.tweens.length; t--; ) this.tweens[t].destroy();
                (this.tweens = null), (this.current = null);
              }
            });
        }),
        H = (e.config = {
          debug: !1,
          defaultUnit: "px",
          defaultAngle: "deg",
          keepInherited: !1,
          hideBackface: !1,
          perspective: "",
          fallback: !C.transition,
          agentTests: [],
        });
      (e.fallback = function (t) {
        if (!C.transition) return (H.fallback = !0);
        H.agentTests.push("(" + t + ")");
        var e = new RegExp(H.agentTests.join("|"), "i");
        H.fallback = e.test(navigator.userAgent);
      }),
        e.fallback("6.0.[2-5] Safari"),
        (e.tween = function (t) {
          return new U(t);
        }),
        (e.delay = function (t, e, n) {
          return new W({ complete: e, duration: t, context: n });
        }),
        (t.fn.tram = function (t) {
          return e.call(null, this, t);
        });
      var z = t.style,
        Y = t.css,
        K = { transform: C.transform && C.transform.css },
        Q = {
          color: [X, y],
          background: [X, y, "background-color"],
          "outline-color": [X, y],
          "border-color": [X, y],
          "border-top-color": [X, y],
          "border-right-color": [X, y],
          "border-bottom-color": [X, y],
          "border-left-color": [X, y],
          "border-width": [k, I],
          "border-top-width": [k, I],
          "border-right-width": [k, I],
          "border-bottom-width": [k, I],
          "border-left-width": [k, I],
          "border-spacing": [k, I],
          "letter-spacing": [k, I],
          margin: [k, I],
          "margin-top": [k, I],
          "margin-right": [k, I],
          "margin-bottom": [k, I],
          "margin-left": [k, I],
          padding: [k, I],
          "padding-top": [k, I],
          "padding-right": [k, I],
          "padding-bottom": [k, I],
          "padding-left": [k, I],
          "outline-width": [k, I],
          opacity: [k, g],
          top: [k, m],
          right: [k, m],
          bottom: [k, m],
          left: [k, m],
          "font-size": [k, m],
          "text-indent": [k, m],
          "word-spacing": [k, m],
          width: [k, m],
          "min-width": [k, m],
          "max-width": [k, m],
          height: [k, m],
          "min-height": [k, m],
          "max-height": [k, m],
          "line-height": [k, O],
          "scroll-top": [G, g, "scrollTop"],
          "scroll-left": [G, g, "scrollLeft"],
        },
        q = {};
      C.transform &&
        ((Q.transform = [V]),
        (q = {
          x: [m, "translateX"],
          y: [m, "translateY"],
          rotate: [T],
          rotateX: [T],
          rotateY: [T],
          scale: [g],
          scaleX: [g],
          scaleY: [g],
          skew: [T],
          skewX: [T],
          skewY: [T],
        })),
        C.transform &&
          C.backface &&
          ((q.z = [m, "translateZ"]),
          (q.rotateZ = [T]),
          (q.scaleZ = [g]),
          (q.perspective = [I]));
      var $ = /ms/,
        Z = /s|\./;
      return (t.tram = e);
    })(window.jQuery);
  },
  function (t, e, n) {
    var r = n(14),
      i = n(130),
      o = n(67),
      a = n(38),
      u = n(68),
      c = n(16),
      s = n(69),
      f = Object.getOwnPropertyDescriptor;
    e.f = r
      ? f
      : function (t, e) {
          if (((t = a(t)), (e = u(e, !0)), s))
            try {
              return f(t, e);
            } catch (t) {}
          if (c(t, e)) return o(!i.f.call(t, e), t[e]);
        };
  },
  function (t, e) {
    t.exports = function (t, e) {
      return {
        enumerable: !(1 & t),
        configurable: !(2 & t),
        writable: !(4 & t),
        value: e,
      };
    };
  },
  function (t, e, n) {
    var r = n(23);
    t.exports = function (t, e) {
      if (!r(t)) return t;
      var n, i;
      if (e && "function" == typeof (n = t.toString) && !r((i = n.call(t))))
        return i;
      if ("function" == typeof (n = t.valueOf) && !r((i = n.call(t)))) return i;
      if (!e && "function" == typeof (n = t.toString) && !r((i = n.call(t))))
        return i;
      throw TypeError("Can't convert object to primitive value");
    };
  },
  function (t, e, n) {
    var r = n(14),
      i = n(15),
      o = n(70);
    t.exports =
      !r &&
      !i(function () {
        return (
          7 !=
          Object.defineProperty(o("div"), "a", {
            get: function () {
              return 7;
            },
          }).a
        );
      });
  },
  function (t, e, n) {
    var r = n(3),
      i = n(23),
      o = r.document,
      a = i(o) && i(o.createElement);
    t.exports = function (t) {
      return a ? o.createElement(t) : {};
    };
  },
  function (t, e, n) {
    var r = n(25);
    t.exports = r("native-function-to-string", Function.toString);
  },
  function (t, e, n) {
    var r = n(25),
      i = n(73),
      o = r("keys");
    t.exports = function (t) {
      return o[t] || (o[t] = i(t));
    };
  },
  function (t, e) {
    var n = 0,
      r = Math.random();
    t.exports = function (t) {
      return (
        "Symbol(" +
        String(void 0 === t ? "" : t) +
        ")_" +
        (++n + r).toString(36)
      );
    };
  },
  function (t, e, n) {
    var r = n(140),
      i = n(3),
      o = function (t) {
        return "function" == typeof t ? t : void 0;
      };
    t.exports = function (t, e) {
      return arguments.length < 2
        ? o(r[t]) || o(i[t])
        : (r[t] && r[t][e]) || (i[t] && i[t][e]);
    };
  },
  function (t, e, n) {
    var r = n(16),
      i = n(38),
      o = n(76).indexOf,
      a = n(41);
    t.exports = function (t, e) {
      var n,
        u = i(t),
        c = 0,
        s = [];
      for (n in u) !r(a, n) && r(u, n) && s.push(n);
      for (; e.length > c; ) r(u, (n = e[c++])) && (~o(s, n) || s.push(n));
      return s;
    };
  },
  function (t, e, n) {
    var r = n(38),
      i = n(142),
      o = n(143),
      a = function (t) {
        return function (e, n, a) {
          var u,
            c = r(e),
            s = i(c.length),
            f = o(a, s);
          if (t && n != n) {
            for (; s > f; ) if ((u = c[f++]) != u) return !0;
          } else
            for (; s > f; f++)
              if ((t || f in c) && c[f] === n) return t || f || 0;
          return !t && -1;
        };
      };
    t.exports = { includes: a(!0), indexOf: a(!1) };
  },
  function (t, e) {
    var n = Math.ceil,
      r = Math.floor;
    t.exports = function (t) {
      return isNaN((t = +t)) ? 0 : (t > 0 ? r : n)(t);
    };
  },
  function (t, e, n) {
    "use strict";
    n.r(e);
    var r = n(43);
    n.d(e, "createStore", function () {
      return r.default;
    });
    var i = n(81);
    n.d(e, "combineReducers", function () {
      return i.default;
    });
    var o = n(83);
    n.d(e, "bindActionCreators", function () {
      return o.default;
    });
    var a = n(84);
    n.d(e, "applyMiddleware", function () {
      return a.default;
    });
    var u = n(44);
    n.d(e, "compose", function () {
      return u.default;
    });
    n(82);
  },
  function (t, e, n) {
    "use strict";
    n.r(e);
    var r = n(156),
      i = n(161),
      o = n(163),
      a = "[object Object]",
      u = Function.prototype,
      c = Object.prototype,
      s = u.toString,
      f = c.hasOwnProperty,
      l = s.call(Object);
    e.default = function (t) {
      if (!Object(o.default)(t) || Object(r.default)(t) != a) return !1;
      var e = Object(i.default)(t);
      if (null === e) return !0;
      var n = f.call(e, "constructor") && e.constructor;
      return "function" == typeof n && n instanceof n && s.call(n) == l;
    };
  },
  function (t, e, n) {
    "use strict";
    n.r(e);
    var r = n(157).default.Symbol;
    e.default = r;
  },
  function (t, e, n) {
    "use strict";
    n.r(e),
      n.d(e, "default", function () {
        return o;
      });
    var r = n(43);
    n(79), n(82);
    function i(t, e) {
      var n = e && e.type;
      return (
        "Given action " +
        ((n && '"' + n.toString() + '"') || "an action") +
        ', reducer "' +
        t +
        '" returned undefined. To ignore an action, you must explicitly return the previous state.'
      );
    }
    function o(t) {
      for (var e = Object.keys(t), n = {}, o = 0; o < e.length; o++) {
        var a = e[o];
        0, "function" == typeof t[a] && (n[a] = t[a]);
      }
      var u,
        c = Object.keys(n);
      try {
        !(function (t) {
          Object.keys(t).forEach(function (e) {
            var n = t[e];
            if (void 0 === n(void 0, { type: r.ActionTypes.INIT }))
              throw new Error(
                'Reducer "' +
                  e +
                  '" returned undefined during initialization. If the state passed to the reducer is undefined, you must explicitly return the initial state. The initial state may not be undefined.'
              );
            if (
              void 0 ===
              n(void 0, {
                type:
                  "@@redux/PROBE_UNKNOWN_ACTION_" +
                  Math.random().toString(36).substring(7).split("").join("."),
              })
            )
              throw new Error(
                'Reducer "' +
                  e +
                  "\" returned undefined when probed with a random type. Don't try to handle " +
                  r.ActionTypes.INIT +
                  ' or other actions in "redux/*" namespace. They are considered private. Instead, you must return the current state for any unknown actions, unless it is undefined, in which case you must return the initial state, regardless of the action type. The initial state may not be undefined.'
              );
          });
        })(n);
      } catch (t) {
        u = t;
      }
      return function () {
        var t =
            arguments.length <= 0 || void 0 === arguments[0]
              ? {}
              : arguments[0],
          e = arguments[1];
        if (u) throw u;
        for (var r = !1, o = {}, a = 0; a < c.length; a++) {
          var s = c[a],
            f = n[s],
            l = t[s],
            d = f(l, e);
          if (void 0 === d) {
            var p = i(s, e);
            throw new Error(p);
          }
          (o[s] = d), (r = r || d !== l);
        }
        return r ? o : t;
      };
    }
  },
  function (t, e, n) {
    "use strict";
    function r(t) {
      "undefined" != typeof console &&
        "function" == typeof console.error &&
        console.error(t);
      try {
        throw new Error(t);
      } catch (t) {}
    }
    n.r(e),
      n.d(e, "default", function () {
        return r;
      });
  },
  function (t, e, n) {
    "use strict";
    function r(t, e) {
      return function () {
        return e(t.apply(void 0, arguments));
      };
    }
    function i(t, e) {
      if ("function" == typeof t) return r(t, e);
      if ("object" != typeof t || null === t)
        throw new Error(
          "bindActionCreators expected an object or a function, instead received " +
            (null === t ? "null" : typeof t) +
            '. Did you write "import ActionCreators from" instead of "import * as ActionCreators from"?'
        );
      for (var n = Object.keys(t), i = {}, o = 0; o < n.length; o++) {
        var a = n[o],
          u = t[a];
        "function" == typeof u && (i[a] = r(u, e));
      }
      return i;
    }
    n.r(e),
      n.d(e, "default", function () {
        return i;
      });
  },
  function (t, e, n) {
    "use strict";
    n.r(e),
      n.d(e, "default", function () {
        return o;
      });
    var r = n(44),
      i =
        Object.assign ||
        function (t) {
          for (var e = 1; e < arguments.length; e++) {
            var n = arguments[e];
            for (var r in n)
              Object.prototype.hasOwnProperty.call(n, r) && (t[r] = n[r]);
          }
          return t;
        };
    function o() {
      for (var t = arguments.length, e = Array(t), n = 0; n < t; n++)
        e[n] = arguments[n];
      return function (t) {
        return function (n, o, a) {
          var u,
            c = t(n, o, a),
            s = c.dispatch,
            f = {
              getState: c.getState,
              dispatch: function (t) {
                return s(t);
              },
            };
          return (
            (u = e.map(function (t) {
              return t(f);
            })),
            (s = r.default.apply(void 0, u)(c.dispatch)),
            i({}, c, { dispatch: s })
          );
        };
      };
    }
  },
  function (t, e, n) {
    var r = n(86)(n(241));
    t.exports = r;
  },
  function (t, e, n) {
    var r = n(7),
      i = n(12),
      o = n(32);
    t.exports = function (t) {
      return function (e, n, a) {
        var u = Object(e);
        if (!i(e)) {
          var c = r(n, 3);
          (e = o(e)),
            (n = function (t) {
              return c(u[t], t, u);
            });
        }
        var s = t(e, n, a);
        return s > -1 ? u[c ? e[s] : s] : void 0;
      };
    };
  },
  function (t, e, n) {
    var r = n(28),
      i = n(183),
      o = n(184),
      a = n(185),
      u = n(186),
      c = n(187);
    function s(t) {
      var e = (this.__data__ = new r(t));
      this.size = e.size;
    }
    (s.prototype.clear = i),
      (s.prototype.delete = o),
      (s.prototype.get = a),
      (s.prototype.has = u),
      (s.prototype.set = c),
      (t.exports = s);
  },
  function (t, e, n) {
    var r = n(11),
      i = n(5),
      o = "[object AsyncFunction]",
      a = "[object Function]",
      u = "[object GeneratorFunction]",
      c = "[object Proxy]";
    t.exports = function (t) {
      if (!i(t)) return !1;
      var e = r(t);
      return e == a || e == u || e == o || e == c;
    };
  },
  function (t, e, n) {
    (function (e) {
      var n = "object" == typeof e && e && e.Object === Object && e;
      t.exports = n;
    }.call(this, n(22)));
  },
  function (t, e) {
    var n = Function.prototype.toString;
    t.exports = function (t) {
      if (null != t) {
        try {
          return n.call(t);
        } catch (t) {}
        try {
          return t + "";
        } catch (t) {}
      }
      return "";
    };
  },
  function (t, e, n) {
    var r = n(206),
      i = n(9);
    t.exports = function t(e, n, o, a, u) {
      return (
        e === n ||
        (null == e || null == n || (!i(e) && !i(n))
          ? e != e && n != n
          : r(e, n, o, a, t, u))
      );
    };
  },
  function (t, e, n) {
    var r = n(207),
      i = n(210),
      o = n(211),
      a = 1,
      u = 2;
    t.exports = function (t, e, n, c, s, f) {
      var l = n & a,
        d = t.length,
        p = e.length;
      if (d != p && !(l && p > d)) return !1;
      var v = f.get(t);
      if (v && f.get(e)) return v == e;
      var h = -1,
        E = !0,
        _ = n & u ? new r() : void 0;
      for (f.set(t, e), f.set(e, t); ++h < d; ) {
        var g = t[h],
          y = e[h];
        if (c) var I = l ? c(y, g, h, e, t, f) : c(g, y, h, t, e, f);
        if (void 0 !== I) {
          if (I) continue;
          E = !1;
          break;
        }
        if (_) {
          if (
            !i(e, function (t, e) {
              if (!o(_, e) && (g === t || s(g, t, n, c, f))) return _.push(e);
            })
          ) {
            E = !1;
            break;
          }
        } else if (g !== y && !s(g, y, n, c, f)) {
          E = !1;
          break;
        }
      }
      return f.delete(t), f.delete(e), E;
    };
  },
  function (t, e, n) {
    var r = n(49),
      i = n(1);
    t.exports = function (t, e, n) {
      var o = e(t);
      return i(t) ? o : r(o, n(t));
    };
  },
  function (t, e, n) {
    var r = n(218),
      i = n(95),
      o = Object.prototype.propertyIsEnumerable,
      a = Object.getOwnPropertySymbols,
      u = a
        ? function (t) {
            return null == t
              ? []
              : ((t = Object(t)),
                r(a(t), function (e) {
                  return o.call(t, e);
                }));
          }
        : i;
    t.exports = u;
  },
  function (t, e) {
    t.exports = function () {
      return [];
    };
  },
  function (t, e, n) {
    var r = n(219),
      i = n(33),
      o = n(1),
      a = n(50),
      u = n(51),
      c = n(52),
      s = Object.prototype.hasOwnProperty;
    t.exports = function (t, e) {
      var n = o(t),
        f = !n && i(t),
        l = !n && !f && a(t),
        d = !n && !f && !l && c(t),
        p = n || f || l || d,
        v = p ? r(t.length, String) : [],
        h = v.length;
      for (var E in t)
        (!e && !s.call(t, E)) ||
          (p &&
            ("length" == E ||
              (l && ("offset" == E || "parent" == E)) ||
              (d &&
                ("buffer" == E || "byteLength" == E || "byteOffset" == E)) ||
              u(E, h))) ||
          v.push(E);
      return v;
    };
  },
  function (t, e) {
    t.exports = function (t) {
      return (
        t.webpackPolyfill ||
          ((t.deprecate = function () {}),
          (t.paths = []),
          t.children || (t.children = []),
          Object.defineProperty(t, "loaded", {
            enumerable: !0,
            get: function () {
              return t.l;
            },
          }),
          Object.defineProperty(t, "id", {
            enumerable: !0,
            get: function () {
              return t.i;
            },
          }),
          (t.webpackPolyfill = 1)),
        t
      );
    };
  },
  function (t, e) {
    t.exports = function (t, e) {
      return function (n) {
        return t(e(n));
      };
    };
  },
  function (t, e, n) {
    var r = n(8)(n(4), "WeakMap");
    t.exports = r;
  },
  function (t, e, n) {
    var r = n(5);
    t.exports = function (t) {
      return t == t && !r(t);
    };
  },
  function (t, e) {
    t.exports = function (t, e) {
      return function (n) {
        return null != n && n[t] === e && (void 0 !== e || t in Object(n));
      };
    };
  },
  function (t, e) {
    t.exports = function (t, e) {
      for (var n = -1, r = null == t ? 0 : t.length, i = Array(r); ++n < r; )
        i[n] = e(t[n], n, t);
      return i;
    };
  },
  function (t, e) {
    t.exports = function (t) {
      return function (e) {
        return null == e ? void 0 : e[t];
      };
    };
  },
  function (t, e) {
    t.exports = function (t, e, n, r) {
      for (var i = t.length, o = n + (r ? 1 : -1); r ? o-- : ++o < i; )
        if (e(t[o], o, t)) return o;
      return -1;
    };
  },
  function (t, e, n) {
    var r = n(242);
    t.exports = function (t) {
      var e = r(t),
        n = e % 1;
      return e == e ? (n ? e - n : e) : 0;
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(0);
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.inQuad = function (t) {
        return Math.pow(t, 2);
      }),
      (e.outQuad = function (t) {
        return -(Math.pow(t - 1, 2) - 1);
      }),
      (e.inOutQuad = function (t) {
        if ((t /= 0.5) < 1) return 0.5 * Math.pow(t, 2);
        return -0.5 * ((t -= 2) * t - 2);
      }),
      (e.inCubic = function (t) {
        return Math.pow(t, 3);
      }),
      (e.outCubic = function (t) {
        return Math.pow(t - 1, 3) + 1;
      }),
      (e.inOutCubic = function (t) {
        if ((t /= 0.5) < 1) return 0.5 * Math.pow(t, 3);
        return 0.5 * (Math.pow(t - 2, 3) + 2);
      }),
      (e.inQuart = function (t) {
        return Math.pow(t, 4);
      }),
      (e.outQuart = function (t) {
        return -(Math.pow(t - 1, 4) - 1);
      }),
      (e.inOutQuart = function (t) {
        if ((t /= 0.5) < 1) return 0.5 * Math.pow(t, 4);
        return -0.5 * ((t -= 2) * Math.pow(t, 3) - 2);
      }),
      (e.inQuint = function (t) {
        return Math.pow(t, 5);
      }),
      (e.outQuint = function (t) {
        return Math.pow(t - 1, 5) + 1;
      }),
      (e.inOutQuint = function (t) {
        if ((t /= 0.5) < 1) return 0.5 * Math.pow(t, 5);
        return 0.5 * (Math.pow(t - 2, 5) + 2);
      }),
      (e.inSine = function (t) {
        return 1 - Math.cos(t * (Math.PI / 2));
      }),
      (e.outSine = function (t) {
        return Math.sin(t * (Math.PI / 2));
      }),
      (e.inOutSine = function (t) {
        return -0.5 * (Math.cos(Math.PI * t) - 1);
      }),
      (e.inExpo = function (t) {
        return 0 === t ? 0 : Math.pow(2, 10 * (t - 1));
      }),
      (e.outExpo = function (t) {
        return 1 === t ? 1 : 1 - Math.pow(2, -10 * t);
      }),
      (e.inOutExpo = function (t) {
        if (0 === t) return 0;
        if (1 === t) return 1;
        if ((t /= 0.5) < 1) return 0.5 * Math.pow(2, 10 * (t - 1));
        return 0.5 * (2 - Math.pow(2, -10 * --t));
      }),
      (e.inCirc = function (t) {
        return -(Math.sqrt(1 - t * t) - 1);
      }),
      (e.outCirc = function (t) {
        return Math.sqrt(1 - Math.pow(t - 1, 2));
      }),
      (e.inOutCirc = function (t) {
        if ((t /= 0.5) < 1) return -0.5 * (Math.sqrt(1 - t * t) - 1);
        return 0.5 * (Math.sqrt(1 - (t -= 2) * t) + 1);
      }),
      (e.outBounce = function (t) {
        return t < 1 / 2.75
          ? 7.5625 * t * t
          : t < 2 / 2.75
          ? 7.5625 * (t -= 1.5 / 2.75) * t + 0.75
          : t < 2.5 / 2.75
          ? 7.5625 * (t -= 2.25 / 2.75) * t + 0.9375
          : 7.5625 * (t -= 2.625 / 2.75) * t + 0.984375;
      }),
      (e.inBack = function (t) {
        return t * t * ((o + 1) * t - o);
      }),
      (e.outBack = function (t) {
        return (t -= 1) * t * ((o + 1) * t + o) + 1;
      }),
      (e.inOutBack = function (t) {
        var e = o;
        if ((t /= 0.5) < 1) return t * t * ((1 + (e *= 1.525)) * t - e) * 0.5;
        return 0.5 * ((t -= 2) * t * ((1 + (e *= 1.525)) * t + e) + 2);
      }),
      (e.inElastic = function (t) {
        var e = o,
          n = 0,
          r = 1;
        if (0 === t) return 0;
        if (1 === t) return 1;
        n || (n = 0.3);
        r < 1
          ? ((r = 1), (e = n / 4))
          : (e = (n / (2 * Math.PI)) * Math.asin(1 / r));
        return (
          -r *
          Math.pow(2, 10 * (t -= 1)) *
          Math.sin(((t - e) * (2 * Math.PI)) / n)
        );
      }),
      (e.outElastic = function (t) {
        var e = o,
          n = 0,
          r = 1;
        if (0 === t) return 0;
        if (1 === t) return 1;
        n || (n = 0.3);
        r < 1
          ? ((r = 1), (e = n / 4))
          : (e = (n / (2 * Math.PI)) * Math.asin(1 / r));
        return (
          r * Math.pow(2, -10 * t) * Math.sin(((t - e) * (2 * Math.PI)) / n) + 1
        );
      }),
      (e.inOutElastic = function (t) {
        var e = o,
          n = 0,
          r = 1;
        if (0 === t) return 0;
        if (2 == (t /= 0.5)) return 1;
        n || (n = 0.3 * 1.5);
        r < 1
          ? ((r = 1), (e = n / 4))
          : (e = (n / (2 * Math.PI)) * Math.asin(1 / r));
        if (t < 1)
          return (
            r *
            Math.pow(2, 10 * (t -= 1)) *
            Math.sin(((t - e) * (2 * Math.PI)) / n) *
            -0.5
          );
        return (
          r *
            Math.pow(2, -10 * (t -= 1)) *
            Math.sin(((t - e) * (2 * Math.PI)) / n) *
            0.5 +
          1
        );
      }),
      (e.swingFromTo = function (t) {
        var e = o;
        return (t /= 0.5) < 1
          ? t * t * ((1 + (e *= 1.525)) * t - e) * 0.5
          : 0.5 * ((t -= 2) * t * ((1 + (e *= 1.525)) * t + e) + 2);
      }),
      (e.swingFrom = function (t) {
        return t * t * ((o + 1) * t - o);
      }),
      (e.swingTo = function (t) {
        return (t -= 1) * t * ((o + 1) * t + o) + 1;
      }),
      (e.bounce = function (t) {
        return t < 1 / 2.75
          ? 7.5625 * t * t
          : t < 2 / 2.75
          ? 7.5625 * (t -= 1.5 / 2.75) * t + 0.75
          : t < 2.5 / 2.75
          ? 7.5625 * (t -= 2.25 / 2.75) * t + 0.9375
          : 7.5625 * (t -= 2.625 / 2.75) * t + 0.984375;
      }),
      (e.bouncePast = function (t) {
        return t < 1 / 2.75
          ? 7.5625 * t * t
          : t < 2 / 2.75
          ? 2 - (7.5625 * (t -= 1.5 / 2.75) * t + 0.75)
          : t < 2.5 / 2.75
          ? 2 - (7.5625 * (t -= 2.25 / 2.75) * t + 0.9375)
          : 2 - (7.5625 * (t -= 2.625 / 2.75) * t + 0.984375);
      }),
      (e.easeInOut = e.easeOut = e.easeIn = e.ease = void 0);
    var i = r(n(107)),
      o = 1.70158,
      a = (0, i.default)(0.25, 0.1, 0.25, 1);
    e.ease = a;
    var u = (0, i.default)(0.42, 0, 1, 1);
    e.easeIn = u;
    var c = (0, i.default)(0, 0, 0.58, 1);
    e.easeOut = c;
    var s = (0, i.default)(0.42, 0, 0.58, 1);
    e.easeInOut = s;
  },
  function (t, e) {
    var n = 4,
      r = 0.001,
      i = 1e-7,
      o = 10,
      a = 11,
      u = 1 / (a - 1),
      c = "function" == typeof Float32Array;
    function s(t, e) {
      return 1 - 3 * e + 3 * t;
    }
    function f(t, e) {
      return 3 * e - 6 * t;
    }
    function l(t) {
      return 3 * t;
    }
    function d(t, e, n) {
      return ((s(e, n) * t + f(e, n)) * t + l(e)) * t;
    }
    function p(t, e, n) {
      return 3 * s(e, n) * t * t + 2 * f(e, n) * t + l(e);
    }
    t.exports = function (t, e, s, f) {
      if (!(0 <= t && t <= 1 && 0 <= s && s <= 1))
        throw new Error("bezier x values must be in [0, 1] range");
      var l = c ? new Float32Array(a) : new Array(a);
      if (t !== e || s !== f) for (var v = 0; v < a; ++v) l[v] = d(v * u, t, s);
      function h(e) {
        for (var c = 0, f = 1, v = a - 1; f !== v && l[f] <= e; ++f) c += u;
        var h = c + ((e - l[--f]) / (l[f + 1] - l[f])) * u,
          E = p(h, t, s);
        return E >= r
          ? (function (t, e, r, i) {
              for (var o = 0; o < n; ++o) {
                var a = p(e, r, i);
                if (0 === a) return e;
                e -= (d(e, r, i) - t) / a;
              }
              return e;
            })(e, h, t, s)
          : 0 === E
          ? h
          : (function (t, e, n, r, a) {
              var u,
                c,
                s = 0;
              do {
                (u = d((c = e + (n - e) / 2), r, a) - t) > 0
                  ? (n = c)
                  : (e = c);
              } while (Math.abs(u) > i && ++s < o);
              return c;
            })(e, c, c + u, t, s);
      }
      return function (n) {
        return t === e && s === f
          ? n
          : 0 === n
          ? 0
          : 1 === n
          ? 1
          : d(h(n), e, f);
      };
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(0)(n(109)),
      i = n(0),
      o = n(13);
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.optimizeFloat = c),
      (e.createBezierEasing = function (t) {
        return u.default.apply(void 0, (0, r.default)(t));
      }),
      (e.applyEasing = function (t, e, n) {
        if (0 === e) return 0;
        if (1 === e) return 1;
        if (n) return c(e > 0 ? n(e) : e);
        return c(e > 0 && t && a[t] ? a[t](e) : e);
      });
    var a = o(n(106)),
      u = i(n(107));
    function c(t) {
      var e =
          arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 5,
        n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 10,
        r = Math.pow(n, e),
        i = Number(Math.round(t * r) / r);
      return Math.abs(i) > 1e-4 ? i : 0;
    }
  },
  function (t, e, n) {
    var r = n(243),
      i = n(244),
      o = n(245);
    t.exports = function (t) {
      return r(t) || i(t) || o();
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(0)(n(26));
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.isPluginType = function (t) {
        return t === o.ActionTypeConsts.PLUGIN_LOTTIE;
      }),
      (e.clearPlugin = e.renderPlugin = e.createPluginInstance = e.getPluginDestination = e.getPluginDuration = e.getPluginOrigin = e.getPluginConfig = void 0);
    var i = n(247),
      o = n(2),
      a = n(45),
      u = (0, r.default)({}, o.ActionTypeConsts.PLUGIN_LOTTIE, {
        getConfig: i.getPluginConfig,
        getOrigin: i.getPluginOrigin,
        getDuration: i.getPluginDuration,
        getDestination: i.getPluginDestination,
        createInstance: i.createPluginInstance,
        render: i.renderPlugin,
        clear: i.clearPlugin,
      });
    var c = function (t) {
        return function (e) {
          if (!a.IS_BROWSER_ENV)
            return function () {
              return null;
            };
          var n = u[e];
          if (!n) throw new Error("IX2 no plugin configured for: ".concat(e));
          var r = n[t];
          if (!r) throw new Error("IX2 invalid plugin method: ".concat(t));
          return r;
        };
      },
      s = c("getConfig");
    e.getPluginConfig = s;
    var f = c("getOrigin");
    e.getPluginOrigin = f;
    var l = c("getDuration");
    e.getPluginDuration = l;
    var d = c("getDestination");
    e.getPluginDestination = d;
    var p = c("createInstance");
    e.createPluginInstance = p;
    var v = c("render");
    e.renderPlugin = v;
    var h = c("clear");
    e.clearPlugin = h;
  },
  function (t, e, n) {
    var r = n(112),
      i = n(254)(r);
    t.exports = i;
  },
  function (t, e, n) {
    var r = n(252),
      i = n(32);
    t.exports = function (t, e) {
      return t && r(t, e, i);
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(258);
    (e.__esModule = !0), (e.default = void 0);
    var i = r(n(259)).default;
    e.default = i;
  },
  function (t, e, n) {
    "use strict";
    var r = n(0)(n(109)),
      i = n(13),
      o = n(0);
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.observeRequests = function (t) {
        D({
          store: t,
          select: function (t) {
            var e = t.ixRequest;
            return e.preview;
          },
          onChange: tt,
        }),
          D({
            store: t,
            select: function (t) {
              var e = t.ixRequest;
              return e.playback;
            },
            onChange: nt,
          }),
          D({
            store: t,
            select: function (t) {
              var e = t.ixRequest;
              return e.stop;
            },
            onChange: rt,
          }),
          D({
            store: t,
            select: function (t) {
              var e = t.ixRequest;
              return e.clear;
            },
            onChange: it,
          });
      }),
      (e.startEngine = ot),
      (e.stopEngine = at),
      (e.stopAllActionGroups = vt),
      (e.stopActionGroup = ht),
      (e.startActionGroup = Et);
    var a = o(n(27)),
      u = o(n(262)),
      c = o(n(85)),
      s = o(n(34)),
      f = o(n(263)),
      l = o(n(269)),
      d = o(n(281)),
      p = o(n(282)),
      v = o(n(283)),
      h = o(n(286)),
      E = o(n(113)),
      _ = n(2),
      g = n(289),
      y = n(10),
      I = n(61),
      m = i(n(291)),
      T = o(n(292)),
      O = _.IX2EngineConstants,
      b = O.COLON_DELIMITER,
      A = O.BOUNDARY_SELECTOR,
      S = O.HTML_ELEMENT,
      w = O.RENDER_GENERAL,
      R = O.W_MOD_IX,
      x = y.IX2VanillaUtils,
      N = x.getAffectedElements,
      C = x.getElementId,
      L = x.getDestinationValues,
      D = x.observeStore,
      P = x.getInstanceId,
      M = x.renderHTMLElement,
      j = x.clearAllStyles,
      F = x.getMaxDurationItemIndex,
      k = x.getComputedStyle,
      X = x.getInstanceOrigin,
      G = x.reduceListToGroup,
      V = x.shouldNamespaceEventParameter,
      U = x.getNamespacedParameterId,
      W = x.shouldAllowMediaQuery,
      B = x.cleanupHTMLElement,
      H = x.stringifyTarget,
      z = x.mediaQueriesEqual,
      Y = y.IX2VanillaPlugins,
      K = Y.isPluginType,
      Q = Y.createPluginInstance,
      q = Y.getPluginDuration,
      $ = navigator.userAgent,
      Z = $.match(/iPad/i) || $.match(/iPhone/),
      J = 12;
    function tt(t, e) {
      var n = t.rawData,
        r = function () {
          ot({ store: e, rawData: n, allowEvents: !0 }), et();
        };
      t.defer ? setTimeout(r, 0) : r();
    }
    function et() {
      document.dispatchEvent(new CustomEvent("IX2_PAGE_UPDATE"));
    }
    function nt(t, e) {
      var n = t.actionTypeId,
        r = t.actionListId,
        i = t.actionItemId,
        o = t.eventId,
        a = t.allowEvents,
        u = t.immediate,
        c = t.testManual,
        s = t.verbose,
        f = void 0 === s || s,
        l = t.rawData;
      if (r && i && l && u) {
        var d = l.actionLists[r];
        d && (l = G({ actionList: d, actionItemId: i, rawData: l }));
      }
      if (
        (ot({ store: e, rawData: l, allowEvents: a, testManual: c }),
        (r && n === _.ActionTypeConsts.GENERAL_START_ACTION) ||
          (0, g.isQuickEffect)(n))
      ) {
        ht({ store: e, actionListId: r }),
          pt({ store: e, actionListId: r, eventId: o });
        var p = Et({
          store: e,
          eventId: o,
          actionListId: r,
          immediate: u,
          verbose: f,
        });
        f &&
          p &&
          e.dispatch(
            (0, I.actionListPlaybackChanged)({ actionListId: r, isPlaying: !u })
          );
      }
    }
    function rt(t, e) {
      var n = t.actionListId;
      n ? ht({ store: e, actionListId: n }) : vt({ store: e }), at(e);
    }
    function it(t, e) {
      at(e), j({ store: e, elementApi: m });
    }
    function ot(t) {
      var e,
        n = t.store,
        i = t.rawData,
        o = t.allowEvents,
        a = t.testManual,
        u = n.getState().ixSession;
      i && n.dispatch((0, I.rawDataImported)(i)),
        u.active ||
          (n.dispatch(
            (0, I.sessionInitialized)({
              hasBoundaryNodes: Boolean(document.querySelector(A)),
            })
          ),
          o &&
            ((function (t) {
              var e = t.getState().ixData.eventTypeMap;
              st(t),
                (0, v.default)(e, function (e, n) {
                  var i = T.default[n];
                  i
                    ? (function (t) {
                        var e = t.logic,
                          n = t.store,
                          i = t.events;
                        !(function (t) {
                          if (Z) {
                            var e = {},
                              n = "";
                            for (var r in t) {
                              var i = t[r],
                                o = i.eventTypeId,
                                a = i.target,
                                u = m.getQuerySelector(a);
                              e[u] ||
                                (o !== _.EventTypeConsts.MOUSE_CLICK &&
                                  o !== _.EventTypeConsts.MOUSE_SECOND_CLICK) ||
                                ((e[u] = !0),
                                (n +=
                                  u +
                                  "{cursor: pointer;touch-action: manipulation;}"));
                            }
                            if (n) {
                              var c = document.createElement("style");
                              (c.textContent = n), document.body.appendChild(c);
                            }
                          }
                        })(i);
                        var o = e.types,
                          a = e.handler,
                          u = n.getState().ixData,
                          l = u.actionLists,
                          d = ft(i, dt);
                        if ((0, f.default)(d)) {
                          (0, v.default)(d, function (t, e) {
                            var o = i[e],
                              a = o.action,
                              f = o.id,
                              d = o.mediaQueries,
                              p = void 0 === d ? u.mediaQueryKeys : d,
                              v = a.config.actionListId;
                            if (
                              (z(p, u.mediaQueryKeys) ||
                                n.dispatch((0, I.mediaQueriesDefined)()),
                              a.actionTypeId ===
                                _.ActionTypeConsts.GENERAL_CONTINUOUS_ACTION)
                            ) {
                              var h = Array.isArray(o.config)
                                ? o.config
                                : [o.config];
                              h.forEach(function (e) {
                                var i = e.continuousParameterGroupId,
                                  o = (0, s.default)(
                                    l,
                                    "".concat(v, ".continuousParameterGroups"),
                                    []
                                  ),
                                  a = (0, c.default)(o, function (t) {
                                    var e = t.id;
                                    return e === i;
                                  }),
                                  u = (e.smoothing || 0) / 100,
                                  d = (e.restingState || 0) / 100;
                                a &&
                                  t.forEach(function (t, i) {
                                    var o = f + b + i;
                                    !(function (t) {
                                      var e = t.store,
                                        n = t.eventStateKey,
                                        i = t.eventTarget,
                                        o = t.eventId,
                                        a = t.eventConfig,
                                        u = t.actionListId,
                                        c = t.parameterGroup,
                                        f = t.smoothing,
                                        l = t.restingValue,
                                        d = e.getState(),
                                        p = d.ixData,
                                        v = d.ixSession,
                                        h = p.events[o],
                                        E = h.eventTypeId,
                                        _ = {},
                                        g = {},
                                        y = [],
                                        I = c.continuousActionGroups,
                                        T = c.id;
                                      V(E, a) && (T = U(n, T));
                                      var O =
                                        v.hasBoundaryNodes && i
                                          ? m.getClosestElement(i, A)
                                          : null;
                                      I.forEach(function (t) {
                                        var e = t.keyframe,
                                          n = t.actionItems;
                                        n.forEach(function (t) {
                                          var n = t.actionTypeId,
                                            o = t.config.target;
                                          if (o) {
                                            var a = o.boundaryMode ? O : null,
                                              u = H(o) + b + n;
                                            if (
                                              ((g[u] = (function () {
                                                var t,
                                                  e =
                                                    arguments.length > 0 &&
                                                    void 0 !== arguments[0]
                                                      ? arguments[0]
                                                      : [],
                                                  n =
                                                    arguments.length > 1
                                                      ? arguments[1]
                                                      : void 0,
                                                  i =
                                                    arguments.length > 2
                                                      ? arguments[2]
                                                      : void 0,
                                                  o = (0, r.default)(e);
                                                return (
                                                  o.some(function (e, r) {
                                                    return (
                                                      e.keyframe === n &&
                                                      ((t = r), !0)
                                                    );
                                                  }),
                                                  null == t &&
                                                    ((t = o.length),
                                                    o.push({
                                                      keyframe: n,
                                                      actionItems: [],
                                                    })),
                                                  o[t].actionItems.push(i),
                                                  o
                                                );
                                              })(g[u], e, t)),
                                              !_[u])
                                            ) {
                                              _[u] = !0;
                                              var c = t.config;
                                              N({
                                                config: c,
                                                event: h,
                                                eventTarget: i,
                                                elementRoot: a,
                                                elementApi: m,
                                              }).forEach(function (t) {
                                                y.push({ element: t, key: u });
                                              });
                                            }
                                          }
                                        });
                                      }),
                                        y.forEach(function (t) {
                                          var n = t.element,
                                            r = t.key,
                                            i = g[r],
                                            a = (0, s.default)(
                                              i,
                                              "[0].actionItems[0]",
                                              {}
                                            ),
                                            c = a.actionTypeId,
                                            d = K(c) ? Q(c)(n, a) : null,
                                            p = L(
                                              {
                                                element: n,
                                                actionItem: a,
                                                elementApi: m,
                                              },
                                              d
                                            );
                                          _t({
                                            store: e,
                                            element: n,
                                            eventId: o,
                                            actionListId: u,
                                            actionItem: a,
                                            destination: p,
                                            continuous: !0,
                                            parameterId: T,
                                            actionGroups: i,
                                            smoothing: f,
                                            restingValue: l,
                                            pluginInstance: d,
                                          });
                                        });
                                    })({
                                      store: n,
                                      eventStateKey: o,
                                      eventTarget: t,
                                      eventId: f,
                                      eventConfig: e,
                                      actionListId: v,
                                      parameterGroup: a,
                                      smoothing: u,
                                      restingValue: d,
                                    });
                                  });
                              });
                            }
                            (a.actionTypeId ===
                              _.ActionTypeConsts.GENERAL_START_ACTION ||
                              (0, g.isQuickEffect)(a.actionTypeId)) &&
                              pt({ store: n, actionListId: v, eventId: f });
                          });
                          var p = function (t) {
                              var e = n.getState(),
                                r = e.ixSession;
                              lt(d, function (e, o, c) {
                                var s = i[o],
                                  f = r.eventState[c],
                                  l = s.action,
                                  d = s.mediaQueries,
                                  p = void 0 === d ? u.mediaQueryKeys : d;
                                if (W(p, r.mediaQueryKey)) {
                                  var v = function () {
                                    var r =
                                        arguments.length > 0 &&
                                        void 0 !== arguments[0]
                                          ? arguments[0]
                                          : {},
                                      i = a(
                                        {
                                          store: n,
                                          element: e,
                                          event: s,
                                          eventConfig: r,
                                          nativeEvent: t,
                                          eventStateKey: c,
                                        },
                                        f
                                      );
                                    (0, E.default)(i, f) ||
                                      n.dispatch(
                                        (0, I.eventStateChanged)(c, i)
                                      );
                                  };
                                  if (
                                    l.actionTypeId ===
                                    _.ActionTypeConsts.GENERAL_CONTINUOUS_ACTION
                                  ) {
                                    var h = Array.isArray(s.config)
                                      ? s.config
                                      : [s.config];
                                    h.forEach(v);
                                  } else v();
                                }
                              });
                            },
                            y = (0, h.default)(p, J),
                            T = function (t) {
                              var e = t.target,
                                r = void 0 === e ? document : e,
                                i = t.types,
                                o = t.throttle;
                              i.split(" ")
                                .filter(Boolean)
                                .forEach(function (t) {
                                  var e = o ? y : p;
                                  r.addEventListener(t, e),
                                    n.dispatch(
                                      (0, I.eventListenerAdded)(r, [t, e])
                                    );
                                });
                            };
                          Array.isArray(o)
                            ? o.forEach(T)
                            : "string" == typeof o && T(e);
                        }
                      })({ logic: i, store: t, events: e })
                    : console.warn("IX2 event type not configured: ".concat(n));
                }),
                t.getState().ixSession.eventListeners.length &&
                  (function (t) {
                    var e = function () {
                      st(t);
                    };
                    ct.forEach(function (n) {
                      window.addEventListener(n, e),
                        t.dispatch((0, I.eventListenerAdded)(window, [n, e]));
                    }),
                      e();
                  })(t);
            })(n),
            -1 === (e = document.documentElement).className.indexOf(R) &&
              (e.className += " ".concat(R)),
            n.getState().ixSession.hasDefinedMediaQueries &&
              (function (t) {
                D({
                  store: t,
                  select: function (t) {
                    return t.ixSession.mediaQueryKey;
                  },
                  onChange: function () {
                    at(t),
                      j({ store: t, elementApi: m }),
                      ot({ store: t, allowEvents: !0 }),
                      et();
                  },
                });
              })(n)),
          n.dispatch((0, I.sessionStarted)()),
          (function (t, e) {
            !(function n(r) {
              var i = t.getState(),
                o = i.ixSession,
                a = i.ixParameters;
              o.active &&
                (t.dispatch((0, I.animationFrameChanged)(r, a)),
                e
                  ? (function (t, e) {
                      var n = D({
                        store: t,
                        select: function (t) {
                          return t.ixSession.tick;
                        },
                        onChange: function (t) {
                          e(t), n();
                        },
                      });
                    })(t, n)
                  : requestAnimationFrame(n));
            })(window.performance.now());
          })(n, a));
    }
    function at(t) {
      var e = t.getState().ixSession;
      e.active &&
        (e.eventListeners.forEach(ut), t.dispatch((0, I.sessionStopped)()));
    }
    function ut(t) {
      var e = t.target,
        n = t.listenerParams;
      e.removeEventListener.apply(e, n);
    }
    var ct = ["resize", "orientationchange"];
    function st(t) {
      var e = t.getState(),
        n = e.ixSession,
        r = e.ixData,
        i = window.innerWidth;
      if (i !== n.viewportWidth) {
        var o = r.mediaQueries;
        t.dispatch((0, I.viewportWidthChanged)({ width: i, mediaQueries: o }));
      }
    }
    var ft = function (t, e) {
        return (0, l.default)((0, p.default)(t, e), d.default);
      },
      lt = function (t, e) {
        (0, v.default)(t, function (t, n) {
          t.forEach(function (t, r) {
            e(t, n, n + b + r);
          });
        });
      },
      dt = function (t) {
        var e = { target: t.target };
        return N({ config: e, elementApi: m });
      };
    function pt(t) {
      var e = t.store,
        n = t.actionListId,
        r = t.eventId,
        i = e.getState(),
        o = i.ixData,
        a = i.ixSession,
        u = o.actionLists,
        c = o.events[r],
        f = u[n];
      if (f && f.useFirstGroupAsInitialState) {
        var l = (0, s.default)(f, "actionItemGroups[0].actionItems", []),
          d = (0, s.default)(c, "mediaQueries", o.mediaQueryKeys);
        if (!W(d, a.mediaQueryKey)) return;
        l.forEach(function (t) {
          var i = t.config,
            o = t.actionTypeId,
            a = N({ config: i, event: c, elementApi: m }),
            u = K(o);
          a.forEach(function (i) {
            var a = u ? Q(o)(i, t) : null;
            _t({
              destination: L({ element: i, actionItem: t, elementApi: m }, a),
              immediate: !0,
              store: e,
              element: i,
              eventId: r,
              actionItem: t,
              actionListId: n,
              pluginInstance: a,
            });
          });
        });
      }
    }
    function vt(t) {
      var e = t.store,
        n = e.getState().ixInstances;
      (0, v.default)(n, function (t) {
        if (!t.continuous) {
          var n = t.actionListId,
            r = t.verbose;
          gt(t, e),
            r &&
              e.dispatch(
                (0, I.actionListPlaybackChanged)({
                  actionListId: n,
                  isPlaying: !1,
                })
              );
        }
      });
    }
    function ht(t) {
      var e = t.store,
        n = t.eventId,
        r = t.eventTarget,
        i = t.eventStateKey,
        o = t.actionListId,
        a = e.getState(),
        u = a.ixInstances,
        c =
          a.ixSession.hasBoundaryNodes && r ? m.getClosestElement(r, A) : null;
      (0, v.default)(u, function (t) {
        var r = (0, s.default)(t, "actionItem.config.target.boundaryMode"),
          a = !i || t.eventStateKey === i;
        if (t.actionListId === o && t.eventId === n && a) {
          if (c && r && !m.elementContains(c, t.element)) return;
          gt(t, e),
            t.verbose &&
              e.dispatch(
                (0, I.actionListPlaybackChanged)({
                  actionListId: o,
                  isPlaying: !1,
                })
              );
        }
      });
    }
    function Et(t) {
      var e,
        n = t.store,
        r = t.eventId,
        i = t.eventTarget,
        o = t.eventStateKey,
        a = t.actionListId,
        u = t.groupIndex,
        c = void 0 === u ? 0 : u,
        f = t.immediate,
        l = t.verbose,
        d = n.getState(),
        p = d.ixData,
        v = d.ixSession,
        h = p.events[r] || {},
        E = h.mediaQueries,
        _ = void 0 === E ? p.mediaQueryKeys : E,
        y = (0, s.default)(p, "actionLists.".concat(a), {}),
        I = y.actionItemGroups,
        T = y.useFirstGroupAsInitialState;
      if (!I || !I.length) return !1;
      c >= I.length && (0, s.default)(h, "config.loop") && (c = 0),
        0 === c && T && c++;
      var O =
          (0 === c || (1 === c && T)) &&
          (0, g.isQuickEffect)(
            null === (e = h.action) || void 0 === e ? void 0 : e.actionTypeId
          )
            ? h.config.delay
            : void 0,
        b = (0, s.default)(I, [c, "actionItems"], []);
      if (!b.length) return !1;
      if (!W(_, v.mediaQueryKey)) return !1;
      var S = v.hasBoundaryNodes && i ? m.getClosestElement(i, A) : null,
        w = F(b),
        R = !1;
      return (
        b.forEach(function (t, e) {
          var u = t.config,
            s = t.actionTypeId,
            d = K(s),
            p = u.target;
          if (p) {
            var v = p.boundaryMode ? S : null;
            N({
              config: u,
              event: h,
              eventTarget: i,
              elementRoot: v,
              elementApi: m,
            }).forEach(function (u, p) {
              var v = d ? Q(s)(u, t) : null,
                h = d ? q(s)(u, t) : null;
              R = !0;
              var E = w === e && 0 === p,
                _ = k({ element: u, actionItem: t }),
                g = L({ element: u, actionItem: t, elementApi: m }, v);
              _t({
                store: n,
                element: u,
                actionItem: t,
                eventId: r,
                eventTarget: i,
                eventStateKey: o,
                actionListId: a,
                groupIndex: c,
                isCarrier: E,
                computedStyle: _,
                destination: g,
                immediate: f,
                verbose: l,
                pluginInstance: v,
                pluginDuration: h,
                instanceDelay: O,
              });
            });
          }
        }),
        R
      );
    }
    function _t(t) {
      var e = t.store,
        n = t.computedStyle,
        r = (0, u.default)(t, ["store", "computedStyle"]),
        i = !r.continuous,
        o = r.element,
        c = r.actionItem,
        s = r.immediate,
        f = r.pluginInstance,
        l = P(),
        d = e.getState(),
        p = d.ixElements,
        v = d.ixSession,
        h = C(p, o),
        E = (p[h] || {}).refState,
        _ = m.getRefType(o),
        g = X(o, E, n, c, m, f);
      e.dispatch(
        (0, I.instanceAdded)(
          (0, a.default)(
            { instanceId: l, elementId: h, origin: g, refType: _ },
            r
          )
        )
      ),
        yt(document.body, "ix2-animation-started", l),
        s
          ? (function (t, e) {
              var n = t.getState().ixParameters;
              t.dispatch((0, I.instanceStarted)(e, 0)),
                t.dispatch((0, I.animationFrameChanged)(performance.now(), n)),
                It(t.getState().ixInstances[e], t);
            })(e, l)
          : (D({
              store: e,
              select: function (t) {
                return t.ixInstances[l];
              },
              onChange: It,
            }),
            i && e.dispatch((0, I.instanceStarted)(l, v.tick)));
    }
    function gt(t, e) {
      yt(document.body, "ix2-animation-stopping", {
        instanceId: t.id,
        state: e.getState(),
      });
      var n = t.elementId,
        r = t.actionItem,
        i = e.getState().ixElements[n] || {},
        o = i.ref;
      i.refType === S && B(o, r, m), e.dispatch((0, I.instanceRemoved)(t.id));
    }
    function yt(t, e, n) {
      var r = document.createEvent("CustomEvent");
      r.initCustomEvent(e, !0, !0, n), t.dispatchEvent(r);
    }
    function It(t, e) {
      var n = t.active,
        r = t.continuous,
        i = t.complete,
        o = t.elementId,
        a = t.actionItem,
        u = t.actionTypeId,
        c = t.renderType,
        s = t.current,
        f = t.groupIndex,
        l = t.eventId,
        d = t.eventTarget,
        p = t.eventStateKey,
        v = t.actionListId,
        h = t.isCarrier,
        E = t.styleProp,
        _ = t.verbose,
        g = t.pluginInstance,
        y = e.getState(),
        T = y.ixData,
        O = y.ixSession,
        b = (T.events[l] || {}).mediaQueries,
        A = void 0 === b ? T.mediaQueryKeys : b;
      if (W(A, O.mediaQueryKey) && (r || n || i)) {
        if (s || (c === w && i)) {
          e.dispatch((0, I.elementStateChanged)(o, u, s, a));
          var R = e.getState().ixElements[o] || {},
            x = R.ref,
            N = R.refType,
            C = R.refState,
            L = C && C[u];
          switch (N) {
            case S:
              M(x, C, L, l, a, E, m, c, g);
          }
        }
        if (i) {
          if (h) {
            var D = Et({
              store: e,
              eventId: l,
              eventTarget: d,
              eventStateKey: p,
              actionListId: v,
              groupIndex: f + 1,
              verbose: _,
            });
            _ &&
              !D &&
              e.dispatch(
                (0, I.actionListPlaybackChanged)({
                  actionListId: v,
                  isPlaying: !1,
                })
              );
          }
          gt(t, e);
        }
      }
    }
  },
  function (t, e, n) {
    var r = n(116);
    t.exports = function (t, e, n) {
      "__proto__" == e && r
        ? r(t, e, { configurable: !0, enumerable: !0, value: n, writable: !0 })
        : (t[e] = n);
    };
  },
  function (t, e, n) {
    var r = n(8),
      i = (function () {
        try {
          var t = r(Object, "defineProperty");
          return t({}, "", {}), t;
        } catch (t) {}
      })();
    t.exports = i;
  },
  function (t, e, n) {
    var r = n(5),
      i = Object.create,
      o = (function () {
        function t() {}
        return function (e) {
          if (!r(e)) return {};
          if (i) return i(e);
          t.prototype = e;
          var n = new t();
          return (t.prototype = void 0), n;
        };
      })();
    t.exports = o;
  },
  function (t, e, n) {
    var r = n(305),
      i = n(306),
      o = r
        ? function (t) {
            return r.get(t);
          }
        : i;
    t.exports = o;
  },
  function (t, e, n) {
    var r = n(307),
      i = Object.prototype.hasOwnProperty;
    t.exports = function (t) {
      for (
        var e = t.name + "", n = r[e], o = i.call(r, e) ? n.length : 0;
        o--;

      ) {
        var a = n[o],
          u = a.func;
        if (null == u || u == t) return a.name;
      }
      return e;
    };
  },
  function (t, e, n) {
    n(121), n(37), n(124), n(314), n(315), n(316), n(317), (t.exports = n(318));
  },
  function (t, e, n) {
    "use strict";
    var r = n(6);
    r.define(
      "brand",
      (t.exports = function (t) {
        var e,
          n = {},
          i = document,
          o = t("html"),
          a = t("body"),
          u = ".w-webflow-badge",
          c = window.location,
          s = /PhantomJS/i.test(navigator.userAgent),
          f =
            "fullscreenchange webkitfullscreenchange mozfullscreenchange msfullscreenchange";
        function l() {
          var n =
            i.fullScreen ||
            i.mozFullScreen ||
            i.webkitIsFullScreen ||
            i.msFullscreenElement ||
            Boolean(i.webkitFullscreenElement);
          t(e).attr("style", n ? "display: none !important;" : "");
        }
        function d() {
          var t = a.children(u),
            n = t.length && t.get(0) === e,
            i = r.env("editor");
          n ? i && t.remove() : (t.length && t.remove(), i || a.append(e));
        }
        return (
          (n.ready = function () {
            var n,
              r,
              a,
              u = o.attr("data-wf-status"),
              p = o.attr("data-wf-domain") || "";
            /\.webflow\.io$/i.test(p) && c.hostname !== p && (u = !0),
              u &&
                !s &&
                ((e =
                  e ||
                  ((n = t('<a class="w-webflow-badge"></a>').attr(
                    "href",
                    "https://webflow.com?utm_campaign=brandjs"
                  )),
                  (r = t("<img>")
                    .attr(
                      "src",
                      "https://d3e54v103j8qbb.cloudfront.net/img/webflow-badge-icon.f67cd735e3.svg"
                    )
                    .attr("alt", "")
                    .css({ marginRight: "8px", width: "16px" })),
                  (a = t("<img>")
                    .attr(
                      "src",
                      "https://d1otoma47x30pg.cloudfront.net/img/webflow-badge-text.6faa6a38cd.svg"
                    )
                    .attr("alt", "Made in Webflow")),
                  n.append(r, a),
                  n[0])),
                d(),
                setTimeout(d, 500),
                t(i).off(f, l).on(f, l));
          }),
          n
        );
      })
    );
  },
  function (t, e, n) {
    "use strict";
    var r = window.$,
      i = n(65) && r.tram;
    /*!
     * Webflow._ (aka) Underscore.js 1.6.0 (custom build)
     * _.each
     * _.map
     * _.find
     * _.filter
     * _.any
     * _.contains
     * _.delay
     * _.defer
     * _.throttle (webflow)
     * _.debounce
     * _.keys
     * _.has
     * _.now
     *
     * http://underscorejs.org
     * (c) 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
     * Underscore may be freely distributed under the MIT license.
     * @license MIT
     */
    t.exports = (function () {
      var t = { VERSION: "1.6.0-Webflow" },
        e = {},
        n = Array.prototype,
        r = Object.prototype,
        o = Function.prototype,
        a = (n.push, n.slice),
        u = (n.concat, r.toString, r.hasOwnProperty),
        c = n.forEach,
        s = n.map,
        f = (n.reduce, n.reduceRight, n.filter),
        l = (n.every, n.some),
        d = n.indexOf,
        p = (n.lastIndexOf, Array.isArray, Object.keys),
        v =
          (o.bind,
          (t.each = t.forEach = function (n, r, i) {
            if (null == n) return n;
            if (c && n.forEach === c) n.forEach(r, i);
            else if (n.length === +n.length) {
              for (var o = 0, a = n.length; o < a; o++)
                if (r.call(i, n[o], o, n) === e) return;
            } else {
              var u = t.keys(n);
              for (o = 0, a = u.length; o < a; o++)
                if (r.call(i, n[u[o]], u[o], n) === e) return;
            }
            return n;
          }));
      (t.map = t.collect = function (t, e, n) {
        var r = [];
        return null == t
          ? r
          : s && t.map === s
          ? t.map(e, n)
          : (v(t, function (t, i, o) {
              r.push(e.call(n, t, i, o));
            }),
            r);
      }),
        (t.find = t.detect = function (t, e, n) {
          var r;
          return (
            h(t, function (t, i, o) {
              if (e.call(n, t, i, o)) return (r = t), !0;
            }),
            r
          );
        }),
        (t.filter = t.select = function (t, e, n) {
          var r = [];
          return null == t
            ? r
            : f && t.filter === f
            ? t.filter(e, n)
            : (v(t, function (t, i, o) {
                e.call(n, t, i, o) && r.push(t);
              }),
              r);
        });
      var h = (t.some = t.any = function (n, r, i) {
        r || (r = t.identity);
        var o = !1;
        return null == n
          ? o
          : l && n.some === l
          ? n.some(r, i)
          : (v(n, function (t, n, a) {
              if (o || (o = r.call(i, t, n, a))) return e;
            }),
            !!o);
      });
      (t.contains = t.include = function (t, e) {
        return (
          null != t &&
          (d && t.indexOf === d
            ? -1 != t.indexOf(e)
            : h(t, function (t) {
                return t === e;
              }))
        );
      }),
        (t.delay = function (t, e) {
          var n = a.call(arguments, 2);
          return setTimeout(function () {
            return t.apply(null, n);
          }, e);
        }),
        (t.defer = function (e) {
          return t.delay.apply(t, [e, 1].concat(a.call(arguments, 1)));
        }),
        (t.throttle = function (t) {
          var e, n, r;
          return function () {
            e ||
              ((e = !0),
              (n = arguments),
              (r = this),
              i.frame(function () {
                (e = !1), t.apply(r, n);
              }));
          };
        }),
        (t.debounce = function (e, n, r) {
          var i,
            o,
            a,
            u,
            c,
            s = function s() {
              var f = t.now() - u;
              f < n
                ? (i = setTimeout(s, n - f))
                : ((i = null), r || ((c = e.apply(a, o)), (a = o = null)));
            };
          return function () {
            (a = this), (o = arguments), (u = t.now());
            var f = r && !i;
            return (
              i || (i = setTimeout(s, n)),
              f && ((c = e.apply(a, o)), (a = o = null)),
              c
            );
          };
        }),
        (t.defaults = function (e) {
          if (!t.isObject(e)) return e;
          for (var n = 1, r = arguments.length; n < r; n++) {
            var i = arguments[n];
            for (var o in i) void 0 === e[o] && (e[o] = i[o]);
          }
          return e;
        }),
        (t.keys = function (e) {
          if (!t.isObject(e)) return [];
          if (p) return p(e);
          var n = [];
          for (var r in e) t.has(e, r) && n.push(r);
          return n;
        }),
        (t.has = function (t, e) {
          return u.call(t, e);
        }),
        (t.isObject = function (t) {
          return t === Object(t);
        }),
        (t.now =
          Date.now ||
          function () {
            return new Date().getTime();
          }),
        (t.templateSettings = {
          evaluate: /<%([\s\S]+?)%>/g,
          interpolate: /<%=([\s\S]+?)%>/g,
          escape: /<%-([\s\S]+?)%>/g,
        });
      var E = /(.)^/,
        _ = {
          "'": "'",
          "\\": "\\",
          "\r": "r",
          "\n": "n",
          "\u2028": "u2028",
          "\u2029": "u2029",
        },
        g = /\\|'|\r|\n|\u2028|\u2029/g,
        y = function (t) {
          return "\\" + _[t];
        };
      return (
        (t.template = function (e, n, r) {
          !n && r && (n = r), (n = t.defaults({}, n, t.templateSettings));
          var i = RegExp(
              [
                (n.escape || E).source,
                (n.interpolate || E).source,
                (n.evaluate || E).source,
              ].join("|") + "|$",
              "g"
            ),
            o = 0,
            a = "__p+='";
          e.replace(i, function (t, n, r, i, u) {
            return (
              (a += e.slice(o, u).replace(g, y)),
              (o = u + t.length),
              n
                ? (a += "'+\n((__t=(" + n + "))==null?'':_.escape(__t))+\n'")
                : r
                ? (a += "'+\n((__t=(" + r + "))==null?'':__t)+\n'")
                : i && (a += "';\n" + i + "\n__p+='"),
              t
            );
          }),
            (a += "';\n"),
            n.variable || (a = "with(obj||{}){\n" + a + "}\n"),
            (a =
              "var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};\n" +
              a +
              "return __p;\n");
          try {
            var u = new Function(n.variable || "obj", "_", a);
          } catch (t) {
            throw ((t.source = a), t);
          }
          var c = function (e) {
              return u.call(this, e, t);
            },
            s = n.variable || "obj";
          return (c.source = "function(" + s + "){\n" + a + "}"), c;
        }),
        t
      );
    })();
  },
  function (t, e, n) {
    "use strict";
    var r = window.jQuery,
      i = {},
      o = [],
      a = {
        reset: function (t, e) {
          e.__wf_intro = null;
        },
        intro: function (t, e) {
          e.__wf_intro ||
            ((e.__wf_intro = !0), r(e).triggerHandler(i.types.INTRO));
        },
        outro: function (t, e) {
          e.__wf_intro &&
            ((e.__wf_intro = null), r(e).triggerHandler(i.types.OUTRO));
        },
      };
    (i.triggers = {}),
      (i.types = { INTRO: "w-ix-intro.w-ix", OUTRO: "w-ix-outro.w-ix" }),
      (i.init = function () {
        for (var t = o.length, e = 0; e < t; e++) {
          var n = o[e];
          n[0](0, n[1]);
        }
        (o = []), r.extend(i.triggers, a);
      }),
      (i.async = function () {
        for (var t in a) {
          var e = a[t];
          a.hasOwnProperty(t) &&
            (i.triggers[t] = function (t, n) {
              o.push([e, n]);
            });
        }
      }),
      i.async(),
      (t.exports = i);
  },
  function (t, e, n) {
    "use strict";
    var r = n(6),
      i = n(125);
    i.setEnv(r.env),
      r.define(
        "ix2",
        (t.exports = function () {
          return i;
        })
      );
  },
  function (t, e, n) {
    "use strict";
    var r = n(13),
      i = n(0);
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.setEnv = function (t) {
        t() && (0, u.observeRequests)(s);
      }),
      (e.init = function (t) {
        f(), (0, u.startEngine)({ store: s, rawData: t, allowEvents: !0 });
      }),
      (e.destroy = f),
      (e.actions = e.store = void 0),
      n(126);
    var o = n(78),
      a = i(n(167)),
      u = n(114),
      c = r(n(61));
    e.actions = c;
    var s = (0, o.createStore)(a.default);
    function f() {
      (0, u.stopEngine)(s);
    }
    e.store = s;
  },
  function (t, e, n) {
    t.exports = n(127);
  },
  function (t, e, n) {
    n(128);
    var r = n(153);
    t.exports = r("Array", "includes");
  },
  function (t, e, n) {
    "use strict";
    var r = n(129),
      i = n(76).includes,
      o = n(146);
    r(
      { target: "Array", proto: !0 },
      {
        includes: function (t) {
          return i(this, t, arguments.length > 1 ? arguments[1] : void 0);
        },
      }
    ),
      o("includes");
  },
  function (t, e, n) {
    var r = n(3),
      i = n(66).f,
      o = n(17),
      a = n(134),
      u = n(40),
      c = n(138),
      s = n(145);
    t.exports = function (t, e) {
      var n,
        f,
        l,
        d,
        p,
        v = t.target,
        h = t.global,
        E = t.stat;
      if ((n = h ? r : E ? r[v] || u(v, {}) : (r[v] || {}).prototype))
        for (f in e) {
          if (
            ((d = e[f]),
            (l = t.noTargetGet ? (p = i(n, f)) && p.value : n[f]),
            !s(h ? f : v + (E ? "." : "#") + f, t.forced) && void 0 !== l)
          ) {
            if (typeof d == typeof l) continue;
            c(d, l);
          }
          (t.sham || (l && l.sham)) && o(d, "sham", !0), a(n, f, d, t);
        }
    };
  },
  function (t, e, n) {
    "use strict";
    var r = {}.propertyIsEnumerable,
      i = Object.getOwnPropertyDescriptor,
      o = i && !r.call({ 1: 2 }, 1);
    e.f = o
      ? function (t) {
          var e = i(this, t);
          return !!e && e.enumerable;
        }
      : r;
  },
  function (t, e, n) {
    var r = n(15),
      i = n(132),
      o = "".split;
    t.exports = r(function () {
      return !Object("z").propertyIsEnumerable(0);
    })
      ? function (t) {
          return "String" == i(t) ? o.call(t, "") : Object(t);
        }
      : Object;
  },
  function (t, e) {
    var n = {}.toString;
    t.exports = function (t) {
      return n.call(t).slice(8, -1);
    };
  },
  function (t, e) {
    t.exports = function (t) {
      if (null == t) throw TypeError("Can't call method on " + t);
      return t;
    };
  },
  function (t, e, n) {
    var r = n(3),
      i = n(25),
      o = n(17),
      a = n(16),
      u = n(40),
      c = n(71),
      s = n(136),
      f = s.get,
      l = s.enforce,
      d = String(c).split("toString");
    i("inspectSource", function (t) {
      return c.call(t);
    }),
      (t.exports = function (t, e, n, i) {
        var c = !!i && !!i.unsafe,
          s = !!i && !!i.enumerable,
          f = !!i && !!i.noTargetGet;
        "function" == typeof n &&
          ("string" != typeof e || a(n, "name") || o(n, "name", e),
          (l(n).source = d.join("string" == typeof e ? e : ""))),
          t !== r
            ? (c ? !f && t[e] && (s = !0) : delete t[e],
              s ? (t[e] = n) : o(t, e, n))
            : s
            ? (t[e] = n)
            : u(e, n);
      })(Function.prototype, "toString", function () {
        return ("function" == typeof this && f(this).source) || c.call(this);
      });
  },
  function (t, e) {
    t.exports = !1;
  },
  function (t, e, n) {
    var r,
      i,
      o,
      a = n(137),
      u = n(3),
      c = n(23),
      s = n(17),
      f = n(16),
      l = n(72),
      d = n(41),
      p = u.WeakMap;
    if (a) {
      var v = new p(),
        h = v.get,
        E = v.has,
        _ = v.set;
      (r = function (t, e) {
        return _.call(v, t, e), e;
      }),
        (i = function (t) {
          return h.call(v, t) || {};
        }),
        (o = function (t) {
          return E.call(v, t);
        });
    } else {
      var g = l("state");
      (d[g] = !0),
        (r = function (t, e) {
          return s(t, g, e), e;
        }),
        (i = function (t) {
          return f(t, g) ? t[g] : {};
        }),
        (o = function (t) {
          return f(t, g);
        });
    }
    t.exports = {
      set: r,
      get: i,
      has: o,
      enforce: function (t) {
        return o(t) ? i(t) : r(t, {});
      },
      getterFor: function (t) {
        return function (e) {
          var n;
          if (!c(e) || (n = i(e)).type !== t)
            throw TypeError("Incompatible receiver, " + t + " required");
          return n;
        };
      },
    };
  },
  function (t, e, n) {
    var r = n(3),
      i = n(71),
      o = r.WeakMap;
    t.exports = "function" == typeof o && /native code/.test(i.call(o));
  },
  function (t, e, n) {
    var r = n(16),
      i = n(139),
      o = n(66),
      a = n(39);
    t.exports = function (t, e) {
      for (var n = i(e), u = a.f, c = o.f, s = 0; s < n.length; s++) {
        var f = n[s];
        r(t, f) || u(t, f, c(e, f));
      }
    };
  },
  function (t, e, n) {
    var r = n(74),
      i = n(141),
      o = n(144),
      a = n(24);
    t.exports =
      r("Reflect", "ownKeys") ||
      function (t) {
        var e = i.f(a(t)),
          n = o.f;
        return n ? e.concat(n(t)) : e;
      };
  },
  function (t, e, n) {
    t.exports = n(3);
  },
  function (t, e, n) {
    var r = n(75),
      i = n(42).concat("length", "prototype");
    e.f =
      Object.getOwnPropertyNames ||
      function (t) {
        return r(t, i);
      };
  },
  function (t, e, n) {
    var r = n(77),
      i = Math.min;
    t.exports = function (t) {
      return t > 0 ? i(r(t), 9007199254740991) : 0;
    };
  },
  function (t, e, n) {
    var r = n(77),
      i = Math.max,
      o = Math.min;
    t.exports = function (t, e) {
      var n = r(t);
      return n < 0 ? i(n + e, 0) : o(n, e);
    };
  },
  function (t, e) {
    e.f = Object.getOwnPropertySymbols;
  },
  function (t, e, n) {
    var r = n(15),
      i = /#|\.prototype\./,
      o = function (t, e) {
        var n = u[a(t)];
        return n == s || (n != c && ("function" == typeof e ? r(e) : !!e));
      },
      a = (o.normalize = function (t) {
        return String(t).replace(i, ".").toLowerCase();
      }),
      u = (o.data = {}),
      c = (o.NATIVE = "N"),
      s = (o.POLYFILL = "P");
    t.exports = o;
  },
  function (t, e, n) {
    var r = n(147),
      i = n(149),
      o = n(17),
      a = r("unscopables"),
      u = Array.prototype;
    null == u[a] && o(u, a, i(null)),
      (t.exports = function (t) {
        u[a][t] = !0;
      });
  },
  function (t, e, n) {
    var r = n(3),
      i = n(25),
      o = n(73),
      a = n(148),
      u = r.Symbol,
      c = i("wks");
    t.exports = function (t) {
      return c[t] || (c[t] = (a && u[t]) || (a ? u : o)("Symbol." + t));
    };
  },
  function (t, e, n) {
    var r = n(15);
    t.exports =
      !!Object.getOwnPropertySymbols &&
      !r(function () {
        return !String(Symbol());
      });
  },
  function (t, e, n) {
    var r = n(24),
      i = n(150),
      o = n(42),
      a = n(41),
      u = n(152),
      c = n(70),
      s = n(72)("IE_PROTO"),
      f = function () {},
      l = function () {
        var t,
          e = c("iframe"),
          n = o.length;
        for (
          e.style.display = "none",
            u.appendChild(e),
            e.src = String("javascript:"),
            (t = e.contentWindow.document).open(),
            t.write("<script>document.F=Object</script>"),
            t.close(),
            l = t.F;
          n--;

        )
          delete l.prototype[o[n]];
        return l();
      };
    (t.exports =
      Object.create ||
      function (t, e) {
        var n;
        return (
          null !== t
            ? ((f.prototype = r(t)),
              (n = new f()),
              (f.prototype = null),
              (n[s] = t))
            : (n = l()),
          void 0 === e ? n : i(n, e)
        );
      }),
      (a[s] = !0);
  },
  function (t, e, n) {
    var r = n(14),
      i = n(39),
      o = n(24),
      a = n(151);
    t.exports = r
      ? Object.defineProperties
      : function (t, e) {
          o(t);
          for (var n, r = a(e), u = r.length, c = 0; u > c; )
            i.f(t, (n = r[c++]), e[n]);
          return t;
        };
  },
  function (t, e, n) {
    var r = n(75),
      i = n(42);
    t.exports =
      Object.keys ||
      function (t) {
        return r(t, i);
      };
  },
  function (t, e, n) {
    var r = n(74);
    t.exports = r("document", "documentElement");
  },
  function (t, e, n) {
    var r = n(3),
      i = n(154),
      o = Function.call;
    t.exports = function (t, e, n) {
      return i(o, r[t].prototype[e], n);
    };
  },
  function (t, e, n) {
    var r = n(155);
    t.exports = function (t, e, n) {
      if ((r(t), void 0 === e)) return t;
      switch (n) {
        case 0:
          return function () {
            return t.call(e);
          };
        case 1:
          return function (n) {
            return t.call(e, n);
          };
        case 2:
          return function (n, r) {
            return t.call(e, n, r);
          };
        case 3:
          return function (n, r, i) {
            return t.call(e, n, r, i);
          };
      }
      return function () {
        return t.apply(e, arguments);
      };
    };
  },
  function (t, e) {
    t.exports = function (t) {
      if ("function" != typeof t)
        throw TypeError(String(t) + " is not a function");
      return t;
    };
  },
  function (t, e, n) {
    "use strict";
    n.r(e);
    var r = n(80),
      i = n(159),
      o = n(160),
      a = "[object Null]",
      u = "[object Undefined]",
      c = r.default ? r.default.toStringTag : void 0;
    e.default = function (t) {
      return null == t
        ? void 0 === t
          ? u
          : a
        : c && c in Object(t)
        ? Object(i.default)(t)
        : Object(o.default)(t);
    };
  },
  function (t, e, n) {
    "use strict";
    n.r(e);
    var r = n(158),
      i = "object" == typeof self && self && self.Object === Object && self,
      o = r.default || i || Function("return this")();
    e.default = o;
  },
  function (t, e, n) {
    "use strict";
    n.r(e),
      function (t) {
        var n = "object" == typeof t && t && t.Object === Object && t;
        e.default = n;
      }.call(this, n(22));
  },
  function (t, e, n) {
    "use strict";
    n.r(e);
    var r = n(80),
      i = Object.prototype,
      o = i.hasOwnProperty,
      a = i.toString,
      u = r.default ? r.default.toStringTag : void 0;
    e.default = function (t) {
      var e = o.call(t, u),
        n = t[u];
      try {
        t[u] = void 0;
        var r = !0;
      } catch (t) {}
      var i = a.call(t);
      return r && (e ? (t[u] = n) : delete t[u]), i;
    };
  },
  function (t, e, n) {
    "use strict";
    n.r(e);
    var r = Object.prototype.toString;
    e.default = function (t) {
      return r.call(t);
    };
  },
  function (t, e, n) {
    "use strict";
    n.r(e);
    var r = n(162),
      i = Object(r.default)(Object.getPrototypeOf, Object);
    e.default = i;
  },
  function (t, e, n) {
    "use strict";
    n.r(e),
      (e.default = function (t, e) {
        return function (n) {
          return t(e(n));
        };
      });
  },
  function (t, e, n) {
    "use strict";
    n.r(e),
      (e.default = function (t) {
        return null != t && "object" == typeof t;
      });
  },
  function (t, e, n) {
    "use strict";
    n.r(e),
      function (t, r) {
        var i,
          o = n(166);
        i =
          "undefined" != typeof self
            ? self
            : "undefined" != typeof window
            ? window
            : void 0 !== t
            ? t
            : r;
        var a = Object(o.default)(i);
        e.default = a;
      }.call(this, n(22), n(165)(t));
  },
  function (t, e) {
    t.exports = function (t) {
      if (!t.webpackPolyfill) {
        var e = Object.create(t);
        e.children || (e.children = []),
          Object.defineProperty(e, "loaded", {
            enumerable: !0,
            get: function () {
              return e.l;
            },
          }),
          Object.defineProperty(e, "id", {
            enumerable: !0,
            get: function () {
              return e.i;
            },
          }),
          Object.defineProperty(e, "exports", { enumerable: !0 }),
          (e.webpackPolyfill = 1);
      }
      return e;
    };
  },
  function (t, e, n) {
    "use strict";
    function r(t) {
      var e,
        n = t.Symbol;
      return (
        "function" == typeof n
          ? n.observable
            ? (e = n.observable)
            : ((e = n("observable")), (n.observable = e))
          : (e = "@@observable"),
        e
      );
    }
    n.r(e),
      n.d(e, "default", function () {
        return r;
      });
  },
  function (t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", { value: !0 }), (e.default = void 0);
    var r = n(78),
      i = n(168),
      o = n(174),
      a = n(175),
      u = n(10),
      c = n(260),
      s = n(261),
      f = u.IX2ElementsReducer.ixElements,
      l = (0, r.combineReducers)({
        ixData: i.ixData,
        ixRequest: o.ixRequest,
        ixSession: a.ixSession,
        ixElements: f,
        ixInstances: c.ixInstances,
        ixParameters: s.ixParameters,
      });
    e.default = l;
  },
  function (t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", { value: !0 }), (e.ixData = void 0);
    var r = n(2).IX2EngineActionTypes.IX2_RAW_DATA_IMPORTED;
    e.ixData = function () {
      var t =
          arguments.length > 0 && void 0 !== arguments[0]
            ? arguments[0]
            : Object.freeze({}),
        e = arguments.length > 1 ? arguments[1] : void 0;
      switch (e.type) {
        case r:
          return e.payload.ixData || Object.freeze({});
        default:
          return t;
      }
    };
  },
  function (t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.QuickEffectDirectionConsts = e.QuickEffectIds = e.EventLimitAffectedElements = e.EventContinuousMouseAxes = e.EventBasedOn = e.EventAppliesTo = e.EventTypeConsts = void 0);
    e.EventTypeConsts = {
      NAVBAR_OPEN: "NAVBAR_OPEN",
      NAVBAR_CLOSE: "NAVBAR_CLOSE",
      TAB_ACTIVE: "TAB_ACTIVE",
      TAB_INACTIVE: "TAB_INACTIVE",
      SLIDER_ACTIVE: "SLIDER_ACTIVE",
      SLIDER_INACTIVE: "SLIDER_INACTIVE",
      DROPDOWN_OPEN: "DROPDOWN_OPEN",
      DROPDOWN_CLOSE: "DROPDOWN_CLOSE",
      MOUSE_CLICK: "MOUSE_CLICK",
      MOUSE_SECOND_CLICK: "MOUSE_SECOND_CLICK",
      MOUSE_DOWN: "MOUSE_DOWN",
      MOUSE_UP: "MOUSE_UP",
      MOUSE_OVER: "MOUSE_OVER",
      MOUSE_OUT: "MOUSE_OUT",
      MOUSE_MOVE: "MOUSE_MOVE",
      MOUSE_MOVE_IN_VIEWPORT: "MOUSE_MOVE_IN_VIEWPORT",
      SCROLL_INTO_VIEW: "SCROLL_INTO_VIEW",
      SCROLL_OUT_OF_VIEW: "SCROLL_OUT_OF_VIEW",
      SCROLLING_IN_VIEW: "SCROLLING_IN_VIEW",
      ECOMMERCE_CART_OPEN: "ECOMMERCE_CART_OPEN",
      ECOMMERCE_CART_CLOSE: "ECOMMERCE_CART_CLOSE",
      PAGE_START: "PAGE_START",
      PAGE_FINISH: "PAGE_FINISH",
      PAGE_SCROLL_UP: "PAGE_SCROLL_UP",
      PAGE_SCROLL_DOWN: "PAGE_SCROLL_DOWN",
      PAGE_SCROLL: "PAGE_SCROLL",
    };
    e.EventAppliesTo = { ELEMENT: "ELEMENT", CLASS: "CLASS", PAGE: "PAGE" };
    e.EventBasedOn = { ELEMENT: "ELEMENT", VIEWPORT: "VIEWPORT" };
    e.EventContinuousMouseAxes = { X_AXIS: "X_AXIS", Y_AXIS: "Y_AXIS" };
    e.EventLimitAffectedElements = {
      CHILDREN: "CHILDREN",
      SIBLINGS: "SIBLINGS",
      IMMEDIATE_CHILDREN: "IMMEDIATE_CHILDREN",
    };
    e.QuickEffectIds = {
      FADE_EFFECT: "FADE_EFFECT",
      SLIDE_EFFECT: "SLIDE_EFFECT",
      GROW_EFFECT: "GROW_EFFECT",
      SHRINK_EFFECT: "SHRINK_EFFECT",
      SPIN_EFFECT: "SPIN_EFFECT",
      FLY_EFFECT: "FLY_EFFECT",
      POP_EFFECT: "POP_EFFECT",
      FLIP_EFFECT: "FLIP_EFFECT",
      JIGGLE_EFFECT: "JIGGLE_EFFECT",
      PULSE_EFFECT: "PULSE_EFFECT",
      DROP_EFFECT: "DROP_EFFECT",
      BLINK_EFFECT: "BLINK_EFFECT",
      BOUNCE_EFFECT: "BOUNCE_EFFECT",
      FLIP_LEFT_TO_RIGHT_EFFECT: "FLIP_LEFT_TO_RIGHT_EFFECT",
      FLIP_RIGHT_TO_LEFT_EFFECT: "FLIP_RIGHT_TO_LEFT_EFFECT",
      RUBBER_BAND_EFFECT: "RUBBER_BAND_EFFECT",
      JELLO_EFFECT: "JELLO_EFFECT",
      GROW_BIG_EFFECT: "GROW_BIG_EFFECT",
      SHRINK_BIG_EFFECT: "SHRINK_BIG_EFFECT",
      PLUGIN_LOTTIE_EFFECT: "PLUGIN_LOTTIE_EFFECT",
    };
    e.QuickEffectDirectionConsts = {
      LEFT: "LEFT",
      RIGHT: "RIGHT",
      BOTTOM: "BOTTOM",
      TOP: "TOP",
      BOTTOM_LEFT: "BOTTOM_LEFT",
      BOTTOM_RIGHT: "BOTTOM_RIGHT",
      TOP_RIGHT: "TOP_RIGHT",
      TOP_LEFT: "TOP_LEFT",
      CLOCKWISE: "CLOCKWISE",
      COUNTER_CLOCKWISE: "COUNTER_CLOCKWISE",
    };
  },
  function (t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.ActionAppliesTo = e.ActionTypeConsts = void 0);
    e.ActionTypeConsts = {
      TRANSFORM_MOVE: "TRANSFORM_MOVE",
      TRANSFORM_SCALE: "TRANSFORM_SCALE",
      TRANSFORM_ROTATE: "TRANSFORM_ROTATE",
      TRANSFORM_SKEW: "TRANSFORM_SKEW",
      STYLE_OPACITY: "STYLE_OPACITY",
      STYLE_SIZE: "STYLE_SIZE",
      STYLE_FILTER: "STYLE_FILTER",
      STYLE_BACKGROUND_COLOR: "STYLE_BACKGROUND_COLOR",
      STYLE_BORDER: "STYLE_BORDER",
      STYLE_TEXT_COLOR: "STYLE_TEXT_COLOR",
      PLUGIN_LOTTIE: "PLUGIN_LOTTIE",
      GENERAL_DISPLAY: "GENERAL_DISPLAY",
      GENERAL_START_ACTION: "GENERAL_START_ACTION",
      GENERAL_CONTINUOUS_ACTION: "GENERAL_CONTINUOUS_ACTION",
      GENERAL_COMBO_CLASS: "GENERAL_COMBO_CLASS",
      GENERAL_STOP_ACTION: "GENERAL_STOP_ACTION",
      GENERAL_LOOP: "GENERAL_LOOP",
      STYLE_BOX_SHADOW: "STYLE_BOX_SHADOW",
    };
    e.ActionAppliesTo = {
      ELEMENT: "ELEMENT",
      ELEMENT_CLASS: "ELEMENT_CLASS",
      TRIGGER_ELEMENT: "TRIGGER_ELEMENT",
    };
  },
  function (t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.InteractionTypeConsts = void 0);
    e.InteractionTypeConsts = {
      MOUSE_CLICK_INTERACTION: "MOUSE_CLICK_INTERACTION",
      MOUSE_HOVER_INTERACTION: "MOUSE_HOVER_INTERACTION",
      MOUSE_MOVE_INTERACTION: "MOUSE_MOVE_INTERACTION",
      SCROLL_INTO_VIEW_INTERACTION: "SCROLL_INTO_VIEW_INTERACTION",
      SCROLLING_IN_VIEW_INTERACTION: "SCROLLING_IN_VIEW_INTERACTION",
      MOUSE_MOVE_IN_VIEWPORT_INTERACTION: "MOUSE_MOVE_IN_VIEWPORT_INTERACTION",
      PAGE_IS_SCROLLING_INTERACTION: "PAGE_IS_SCROLLING_INTERACTION",
      PAGE_LOAD_INTERACTION: "PAGE_LOAD_INTERACTION",
      PAGE_SCROLLED_INTERACTION: "PAGE_SCROLLED_INTERACTION",
      NAVBAR_INTERACTION: "NAVBAR_INTERACTION",
      DROPDOWN_INTERACTION: "DROPDOWN_INTERACTION",
      ECOMMERCE_CART_INTERACTION: "ECOMMERCE_CART_INTERACTION",
      TAB_INTERACTION: "TAB_INTERACTION",
      SLIDER_INTERACTION: "SLIDER_INTERACTION",
    };
  },
  function (t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.IX2_TEST_FRAME_RENDERED = e.IX2_MEDIA_QUERIES_DEFINED = e.IX2_VIEWPORT_WIDTH_CHANGED = e.IX2_ACTION_LIST_PLAYBACK_CHANGED = e.IX2_ELEMENT_STATE_CHANGED = e.IX2_INSTANCE_REMOVED = e.IX2_INSTANCE_STARTED = e.IX2_INSTANCE_ADDED = e.IX2_PARAMETER_CHANGED = e.IX2_ANIMATION_FRAME_CHANGED = e.IX2_EVENT_STATE_CHANGED = e.IX2_EVENT_LISTENER_ADDED = e.IX2_CLEAR_REQUESTED = e.IX2_STOP_REQUESTED = e.IX2_PLAYBACK_REQUESTED = e.IX2_PREVIEW_REQUESTED = e.IX2_SESSION_STOPPED = e.IX2_SESSION_STARTED = e.IX2_SESSION_INITIALIZED = e.IX2_RAW_DATA_IMPORTED = void 0);
    e.IX2_RAW_DATA_IMPORTED = "IX2_RAW_DATA_IMPORTED";
    e.IX2_SESSION_INITIALIZED = "IX2_SESSION_INITIALIZED";
    e.IX2_SESSION_STARTED = "IX2_SESSION_STARTED";
    e.IX2_SESSION_STOPPED = "IX2_SESSION_STOPPED";
    e.IX2_PREVIEW_REQUESTED = "IX2_PREVIEW_REQUESTED";
    e.IX2_PLAYBACK_REQUESTED = "IX2_PLAYBACK_REQUESTED";
    e.IX2_STOP_REQUESTED = "IX2_STOP_REQUESTED";
    e.IX2_CLEAR_REQUESTED = "IX2_CLEAR_REQUESTED";
    e.IX2_EVENT_LISTENER_ADDED = "IX2_EVENT_LISTENER_ADDED";
    e.IX2_EVENT_STATE_CHANGED = "IX2_EVENT_STATE_CHANGED";
    e.IX2_ANIMATION_FRAME_CHANGED = "IX2_ANIMATION_FRAME_CHANGED";
    e.IX2_PARAMETER_CHANGED = "IX2_PARAMETER_CHANGED";
    e.IX2_INSTANCE_ADDED = "IX2_INSTANCE_ADDED";
    e.IX2_INSTANCE_STARTED = "IX2_INSTANCE_STARTED";
    e.IX2_INSTANCE_REMOVED = "IX2_INSTANCE_REMOVED";
    e.IX2_ELEMENT_STATE_CHANGED = "IX2_ELEMENT_STATE_CHANGED";
    e.IX2_ACTION_LIST_PLAYBACK_CHANGED = "IX2_ACTION_LIST_PLAYBACK_CHANGED";
    e.IX2_VIEWPORT_WIDTH_CHANGED = "IX2_VIEWPORT_WIDTH_CHANGED";
    e.IX2_MEDIA_QUERIES_DEFINED = "IX2_MEDIA_QUERIES_DEFINED";
    e.IX2_TEST_FRAME_RENDERED = "IX2_TEST_FRAME_RENDERED";
  },
  function (t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.RENDER_PLUGIN = e.RENDER_STYLE = e.RENDER_GENERAL = e.RENDER_TRANSFORM = e.ABSTRACT_NODE = e.PLAIN_OBJECT = e.HTML_ELEMENT = e.PRESERVE_3D = e.PARENT = e.SIBLINGS = e.IMMEDIATE_CHILDREN = e.CHILDREN = e.BAR_DELIMITER = e.COLON_DELIMITER = e.COMMA_DELIMITER = e.AUTO = e.WILL_CHANGE = e.FLEX = e.DISPLAY = e.COLOR = e.BORDER_COLOR = e.BACKGROUND = e.BACKGROUND_COLOR = e.HEIGHT = e.WIDTH = e.FILTER = e.OPACITY = e.SKEW_Y = e.SKEW_X = e.SKEW = e.ROTATE_Z = e.ROTATE_Y = e.ROTATE_X = e.SCALE_3D = e.SCALE_Z = e.SCALE_Y = e.SCALE_X = e.TRANSLATE_3D = e.TRANSLATE_Z = e.TRANSLATE_Y = e.TRANSLATE_X = e.TRANSFORM = e.CONFIG_UNIT = e.CONFIG_Z_UNIT = e.CONFIG_Y_UNIT = e.CONFIG_X_UNIT = e.CONFIG_VALUE = e.CONFIG_Z_VALUE = e.CONFIG_Y_VALUE = e.CONFIG_X_VALUE = e.BOUNDARY_SELECTOR = e.W_MOD_IX = e.W_MOD_JS = e.WF_PAGE = e.IX2_ID_DELIMITER = void 0);
    e.IX2_ID_DELIMITER = "|";
    e.WF_PAGE = "data-wf-page";
    e.W_MOD_JS = "w-mod-js";
    e.W_MOD_IX = "w-mod-ix";
    e.BOUNDARY_SELECTOR = ".w-dyn-item";
    e.CONFIG_X_VALUE = "xValue";
    e.CONFIG_Y_VALUE = "yValue";
    e.CONFIG_Z_VALUE = "zValue";
    e.CONFIG_VALUE = "value";
    e.CONFIG_X_UNIT = "xUnit";
    e.CONFIG_Y_UNIT = "yUnit";
    e.CONFIG_Z_UNIT = "zUnit";
    e.CONFIG_UNIT = "unit";
    e.TRANSFORM = "transform";
    e.TRANSLATE_X = "translateX";
    e.TRANSLATE_Y = "translateY";
    e.TRANSLATE_Z = "translateZ";
    e.TRANSLATE_3D = "translate3d";
    e.SCALE_X = "scaleX";
    e.SCALE_Y = "scaleY";
    e.SCALE_Z = "scaleZ";
    e.SCALE_3D = "scale3d";
    e.ROTATE_X = "rotateX";
    e.ROTATE_Y = "rotateY";
    e.ROTATE_Z = "rotateZ";
    e.SKEW = "skew";
    e.SKEW_X = "skewX";
    e.SKEW_Y = "skewY";
    e.OPACITY = "opacity";
    e.FILTER = "filter";
    e.WIDTH = "width";
    e.HEIGHT = "height";
    e.BACKGROUND_COLOR = "backgroundColor";
    e.BACKGROUND = "background";
    e.BORDER_COLOR = "borderColor";
    e.COLOR = "color";
    e.DISPLAY = "display";
    e.FLEX = "flex";
    e.WILL_CHANGE = "willChange";
    e.AUTO = "AUTO";
    e.COMMA_DELIMITER = ",";
    e.COLON_DELIMITER = ":";
    e.BAR_DELIMITER = "|";
    e.CHILDREN = "CHILDREN";
    e.IMMEDIATE_CHILDREN = "IMMEDIATE_CHILDREN";
    e.SIBLINGS = "SIBLINGS";
    e.PARENT = "PARENT";
    e.PRESERVE_3D = "preserve-3d";
    e.HTML_ELEMENT = "HTML_ELEMENT";
    e.PLAIN_OBJECT = "PLAIN_OBJECT";
    e.ABSTRACT_NODE = "ABSTRACT_NODE";
    e.RENDER_TRANSFORM = "RENDER_TRANSFORM";
    e.RENDER_GENERAL = "RENDER_GENERAL";
    e.RENDER_STYLE = "RENDER_STYLE";
    e.RENDER_PLUGIN = "RENDER_PLUGIN";
  },
  function (t, e, n) {
    "use strict";
    var r,
      i = n(0)(n(26)),
      o = n(0);
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.ixRequest = void 0);
    var a = o(n(27)),
      u = n(2),
      c = n(18),
      s = u.IX2EngineActionTypes,
      f = s.IX2_PREVIEW_REQUESTED,
      l = s.IX2_PLAYBACK_REQUESTED,
      d = s.IX2_STOP_REQUESTED,
      p = s.IX2_CLEAR_REQUESTED,
      v = { preview: {}, playback: {}, stop: {}, clear: {} },
      h = Object.create(
        null,
        ((r = {}),
        (0, i.default)(r, f, { value: "preview" }),
        (0, i.default)(r, l, { value: "playback" }),
        (0, i.default)(r, d, { value: "stop" }),
        (0, i.default)(r, p, { value: "clear" }),
        r)
      );
    e.ixRequest = function () {
      var t =
          arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : v,
        e = arguments.length > 1 ? arguments[1] : void 0;
      if (e.type in h) {
        var n = [h[e.type]];
        return (0, c.setIn)(t, [n], (0, a.default)({}, e.payload));
      }
      return t;
    };
  },
  function (t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.ixSession = void 0);
    var r = n(2),
      i = n(18),
      o = r.IX2EngineActionTypes,
      a = o.IX2_SESSION_INITIALIZED,
      u = o.IX2_SESSION_STARTED,
      c = o.IX2_TEST_FRAME_RENDERED,
      s = o.IX2_SESSION_STOPPED,
      f = o.IX2_EVENT_LISTENER_ADDED,
      l = o.IX2_EVENT_STATE_CHANGED,
      d = o.IX2_ANIMATION_FRAME_CHANGED,
      p = o.IX2_ACTION_LIST_PLAYBACK_CHANGED,
      v = o.IX2_VIEWPORT_WIDTH_CHANGED,
      h = o.IX2_MEDIA_QUERIES_DEFINED,
      E = {
        active: !1,
        tick: 0,
        eventListeners: [],
        eventState: {},
        playbackState: {},
        viewportWidth: 0,
        mediaQueryKey: null,
        hasBoundaryNodes: !1,
        hasDefinedMediaQueries: !1,
      };
    e.ixSession = function () {
      var t =
          arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : E,
        e = arguments.length > 1 ? arguments[1] : void 0;
      switch (e.type) {
        case a:
          var n = e.payload.hasBoundaryNodes;
          return (0, i.set)(t, "hasBoundaryNodes", n);
        case u:
          return (0, i.set)(t, "active", !0);
        case c:
          var r = e.payload.step,
            o = void 0 === r ? 20 : r;
          return (0, i.set)(t, "tick", t.tick + o);
        case s:
          return E;
        case d:
          var _ = e.payload.now;
          return (0, i.set)(t, "tick", _);
        case f:
          var g = (0, i.addLast)(t.eventListeners, e.payload);
          return (0, i.set)(t, "eventListeners", g);
        case l:
          var y = e.payload,
            I = y.stateKey,
            m = y.newState;
          return (0, i.setIn)(t, ["eventState", I], m);
        case p:
          var T = e.payload,
            O = T.actionListId,
            b = T.isPlaying;
          return (0, i.setIn)(t, ["playbackState", O], b);
        case v:
          for (
            var A = e.payload,
              S = A.width,
              w = A.mediaQueries,
              R = w.length,
              x = null,
              N = 0;
            N < R;
            N++
          ) {
            var C = w[N],
              L = C.key,
              D = C.min,
              P = C.max;
            if (S >= D && S <= P) {
              x = L;
              break;
            }
          }
          return (0, i.merge)(t, { viewportWidth: S, mediaQueryKey: x });
        case h:
          return (0, i.set)(t, "hasDefinedMediaQueries", !0);
        default:
          return t;
      }
    };
  },
  function (t, e, n) {
    var r = n(177),
      i = n(229),
      o = n(101);
    t.exports = function (t) {
      var e = i(t);
      return 1 == e.length && e[0][2]
        ? o(e[0][0], e[0][1])
        : function (n) {
            return n === t || r(n, t, e);
          };
    };
  },
  function (t, e, n) {
    var r = n(87),
      i = n(91),
      o = 1,
      a = 2;
    t.exports = function (t, e, n, u) {
      var c = n.length,
        s = c,
        f = !u;
      if (null == t) return !s;
      for (t = Object(t); c--; ) {
        var l = n[c];
        if (f && l[2] ? l[1] !== t[l[0]] : !(l[0] in t)) return !1;
      }
      for (; ++c < s; ) {
        var d = (l = n[c])[0],
          p = t[d],
          v = l[1];
        if (f && l[2]) {
          if (void 0 === p && !(d in t)) return !1;
        } else {
          var h = new r();
          if (u) var E = u(p, v, d, t, e, h);
          if (!(void 0 === E ? i(v, p, o | a, u, h) : E)) return !1;
        }
      }
      return !0;
    };
  },
  function (t, e) {
    t.exports = function () {
      (this.__data__ = []), (this.size = 0);
    };
  },
  function (t, e, n) {
    var r = n(29),
      i = Array.prototype.splice;
    t.exports = function (t) {
      var e = this.__data__,
        n = r(e, t);
      return !(
        n < 0 || (n == e.length - 1 ? e.pop() : i.call(e, n, 1), --this.size, 0)
      );
    };
  },
  function (t, e, n) {
    var r = n(29);
    t.exports = function (t) {
      var e = this.__data__,
        n = r(e, t);
      return n < 0 ? void 0 : e[n][1];
    };
  },
  function (t, e, n) {
    var r = n(29);
    t.exports = function (t) {
      return r(this.__data__, t) > -1;
    };
  },
  function (t, e, n) {
    var r = n(29);
    t.exports = function (t, e) {
      var n = this.__data__,
        i = r(n, t);
      return i < 0 ? (++this.size, n.push([t, e])) : (n[i][1] = e), this;
    };
  },
  function (t, e, n) {
    var r = n(28);
    t.exports = function () {
      (this.__data__ = new r()), (this.size = 0);
    };
  },
  function (t, e) {
    t.exports = function (t) {
      var e = this.__data__,
        n = e.delete(t);
      return (this.size = e.size), n;
    };
  },
  function (t, e) {
    t.exports = function (t) {
      return this.__data__.get(t);
    };
  },
  function (t, e) {
    t.exports = function (t) {
      return this.__data__.has(t);
    };
  },
  function (t, e, n) {
    var r = n(28),
      i = n(47),
      o = n(48),
      a = 200;
    t.exports = function (t, e) {
      var n = this.__data__;
      if (n instanceof r) {
        var u = n.__data__;
        if (!i || u.length < a - 1)
          return u.push([t, e]), (this.size = ++n.size), this;
        n = this.__data__ = new o(u);
      }
      return n.set(t, e), (this.size = n.size), this;
    };
  },
  function (t, e, n) {
    var r = n(88),
      i = n(191),
      o = n(5),
      a = n(90),
      u = /^\[object .+?Constructor\]$/,
      c = Function.prototype,
      s = Object.prototype,
      f = c.toString,
      l = s.hasOwnProperty,
      d = RegExp(
        "^" +
          f
            .call(l)
            .replace(/[\\^$.*+?()[\]{}|]/g, "\\$&")
            .replace(
              /hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g,
              "$1.*?"
            ) +
          "$"
      );
    t.exports = function (t) {
      return !(!o(t) || i(t)) && (r(t) ? d : u).test(a(t));
    };
  },
  function (t, e, n) {
    var r = n(19),
      i = Object.prototype,
      o = i.hasOwnProperty,
      a = i.toString,
      u = r ? r.toStringTag : void 0;
    t.exports = function (t) {
      var e = o.call(t, u),
        n = t[u];
      try {
        t[u] = void 0;
        var r = !0;
      } catch (t) {}
      var i = a.call(t);
      return r && (e ? (t[u] = n) : delete t[u]), i;
    };
  },
  function (t, e) {
    var n = Object.prototype.toString;
    t.exports = function (t) {
      return n.call(t);
    };
  },
  function (t, e, n) {
    var r,
      i = n(192),
      o = (r = /[^.]+$/.exec((i && i.keys && i.keys.IE_PROTO) || ""))
        ? "Symbol(src)_1." + r
        : "";
    t.exports = function (t) {
      return !!o && o in t;
    };
  },
  function (t, e, n) {
    var r = n(4)["__core-js_shared__"];
    t.exports = r;
  },
  function (t, e) {
    t.exports = function (t, e) {
      return null == t ? void 0 : t[e];
    };
  },
  function (t, e, n) {
    var r = n(195),
      i = n(28),
      o = n(47);
    t.exports = function () {
      (this.size = 0),
        (this.__data__ = {
          hash: new r(),
          map: new (o || i)(),
          string: new r(),
        });
    };
  },
  function (t, e, n) {
    var r = n(196),
      i = n(197),
      o = n(198),
      a = n(199),
      u = n(200);
    function c(t) {
      var e = -1,
        n = null == t ? 0 : t.length;
      for (this.clear(); ++e < n; ) {
        var r = t[e];
        this.set(r[0], r[1]);
      }
    }
    (c.prototype.clear = r),
      (c.prototype.delete = i),
      (c.prototype.get = o),
      (c.prototype.has = a),
      (c.prototype.set = u),
      (t.exports = c);
  },
  function (t, e, n) {
    var r = n(30);
    t.exports = function () {
      (this.__data__ = r ? r(null) : {}), (this.size = 0);
    };
  },
  function (t, e) {
    t.exports = function (t) {
      var e = this.has(t) && delete this.__data__[t];
      return (this.size -= e ? 1 : 0), e;
    };
  },
  function (t, e, n) {
    var r = n(30),
      i = "__lodash_hash_undefined__",
      o = Object.prototype.hasOwnProperty;
    t.exports = function (t) {
      var e = this.__data__;
      if (r) {
        var n = e[t];
        return n === i ? void 0 : n;
      }
      return o.call(e, t) ? e[t] : void 0;
    };
  },
  function (t, e, n) {
    var r = n(30),
      i = Object.prototype.hasOwnProperty;
    t.exports = function (t) {
      var e = this.__data__;
      return r ? void 0 !== e[t] : i.call(e, t);
    };
  },
  function (t, e, n) {
    var r = n(30),
      i = "__lodash_hash_undefined__";
    t.exports = function (t, e) {
      var n = this.__data__;
      return (
        (this.size += this.has(t) ? 0 : 1),
        (n[t] = r && void 0 === e ? i : e),
        this
      );
    };
  },
  function (t, e, n) {
    var r = n(31);
    t.exports = function (t) {
      var e = r(this, t).delete(t);
      return (this.size -= e ? 1 : 0), e;
    };
  },
  function (t, e) {
    t.exports = function (t) {
      var e = typeof t;
      return "string" == e || "number" == e || "symbol" == e || "boolean" == e
        ? "__proto__" !== t
        : null === t;
    };
  },
  function (t, e, n) {
    var r = n(31);
    t.exports = function (t) {
      return r(this, t).get(t);
    };
  },
  function (t, e, n) {
    var r = n(31);
    t.exports = function (t) {
      return r(this, t).has(t);
    };
  },
  function (t, e, n) {
    var r = n(31);
    t.exports = function (t, e) {
      var n = r(this, t),
        i = n.size;
      return n.set(t, e), (this.size += n.size == i ? 0 : 1), this;
    };
  },
  function (t, e, n) {
    var r = n(87),
      i = n(92),
      o = n(212),
      a = n(216),
      u = n(56),
      c = n(1),
      s = n(50),
      f = n(52),
      l = 1,
      d = "[object Arguments]",
      p = "[object Array]",
      v = "[object Object]",
      h = Object.prototype.hasOwnProperty;
    t.exports = function (t, e, n, E, _, g) {
      var y = c(t),
        I = c(e),
        m = y ? p : u(t),
        T = I ? p : u(e),
        O = (m = m == d ? v : m) == v,
        b = (T = T == d ? v : T) == v,
        A = m == T;
      if (A && s(t)) {
        if (!s(e)) return !1;
        (y = !0), (O = !1);
      }
      if (A && !O)
        return (
          g || (g = new r()),
          y || f(t) ? i(t, e, n, E, _, g) : o(t, e, m, n, E, _, g)
        );
      if (!(n & l)) {
        var S = O && h.call(t, "__wrapped__"),
          w = b && h.call(e, "__wrapped__");
        if (S || w) {
          var R = S ? t.value() : t,
            x = w ? e.value() : e;
          return g || (g = new r()), _(R, x, n, E, g);
        }
      }
      return !!A && (g || (g = new r()), a(t, e, n, E, _, g));
    };
  },
  function (t, e, n) {
    var r = n(48),
      i = n(208),
      o = n(209);
    function a(t) {
      var e = -1,
        n = null == t ? 0 : t.length;
      for (this.__data__ = new r(); ++e < n; ) this.add(t[e]);
    }
    (a.prototype.add = a.prototype.push = i),
      (a.prototype.has = o),
      (t.exports = a);
  },
  function (t, e) {
    var n = "__lodash_hash_undefined__";
    t.exports = function (t) {
      return this.__data__.set(t, n), this;
    };
  },
  function (t, e) {
    t.exports = function (t) {
      return this.__data__.has(t);
    };
  },
  function (t, e) {
    t.exports = function (t, e) {
      for (var n = -1, r = null == t ? 0 : t.length; ++n < r; )
        if (e(t[n], n, t)) return !0;
      return !1;
    };
  },
  function (t, e) {
    t.exports = function (t, e) {
      return t.has(e);
    };
  },
  function (t, e, n) {
    var r = n(19),
      i = n(213),
      o = n(46),
      a = n(92),
      u = n(214),
      c = n(215),
      s = 1,
      f = 2,
      l = "[object Boolean]",
      d = "[object Date]",
      p = "[object Error]",
      v = "[object Map]",
      h = "[object Number]",
      E = "[object RegExp]",
      _ = "[object Set]",
      g = "[object String]",
      y = "[object Symbol]",
      I = "[object ArrayBuffer]",
      m = "[object DataView]",
      T = r ? r.prototype : void 0,
      O = T ? T.valueOf : void 0;
    t.exports = function (t, e, n, r, T, b, A) {
      switch (n) {
        case m:
          if (t.byteLength != e.byteLength || t.byteOffset != e.byteOffset)
            return !1;
          (t = t.buffer), (e = e.buffer);
        case I:
          return !(t.byteLength != e.byteLength || !b(new i(t), new i(e)));
        case l:
        case d:
        case h:
          return o(+t, +e);
        case p:
          return t.name == e.name && t.message == e.message;
        case E:
        case g:
          return t == e + "";
        case v:
          var S = u;
        case _:
          var w = r & s;
          if ((S || (S = c), t.size != e.size && !w)) return !1;
          var R = A.get(t);
          if (R) return R == e;
          (r |= f), A.set(t, e);
          var x = a(S(t), S(e), r, T, b, A);
          return A.delete(t), x;
        case y:
          if (O) return O.call(t) == O.call(e);
      }
      return !1;
    };
  },
  function (t, e, n) {
    var r = n(4).Uint8Array;
    t.exports = r;
  },
  function (t, e) {
    t.exports = function (t) {
      var e = -1,
        n = Array(t.size);
      return (
        t.forEach(function (t, r) {
          n[++e] = [r, t];
        }),
        n
      );
    };
  },
  function (t, e) {
    t.exports = function (t) {
      var e = -1,
        n = Array(t.size);
      return (
        t.forEach(function (t) {
          n[++e] = t;
        }),
        n
      );
    };
  },
  function (t, e, n) {
    var r = n(217),
      i = 1,
      o = Object.prototype.hasOwnProperty;
    t.exports = function (t, e, n, a, u, c) {
      var s = n & i,
        f = r(t),
        l = f.length;
      if (l != r(e).length && !s) return !1;
      for (var d = l; d--; ) {
        var p = f[d];
        if (!(s ? p in e : o.call(e, p))) return !1;
      }
      var v = c.get(t);
      if (v && c.get(e)) return v == e;
      var h = !0;
      c.set(t, e), c.set(e, t);
      for (var E = s; ++d < l; ) {
        var _ = t[(p = f[d])],
          g = e[p];
        if (a) var y = s ? a(g, _, p, e, t, c) : a(_, g, p, t, e, c);
        if (!(void 0 === y ? _ === g || u(_, g, n, a, c) : y)) {
          h = !1;
          break;
        }
        E || (E = "constructor" == p);
      }
      if (h && !E) {
        var I = t.constructor,
          m = e.constructor;
        I != m &&
          "constructor" in t &&
          "constructor" in e &&
          !(
            "function" == typeof I &&
            I instanceof I &&
            "function" == typeof m &&
            m instanceof m
          ) &&
          (h = !1);
      }
      return c.delete(t), c.delete(e), h;
    };
  },
  function (t, e, n) {
    var r = n(93),
      i = n(94),
      o = n(32);
    t.exports = function (t) {
      return r(t, o, i);
    };
  },
  function (t, e) {
    t.exports = function (t, e) {
      for (var n = -1, r = null == t ? 0 : t.length, i = 0, o = []; ++n < r; ) {
        var a = t[n];
        e(a, n, t) && (o[i++] = a);
      }
      return o;
    };
  },
  function (t, e) {
    t.exports = function (t, e) {
      for (var n = -1, r = Array(t); ++n < t; ) r[n] = e(n);
      return r;
    };
  },
  function (t, e, n) {
    var r = n(11),
      i = n(9),
      o = "[object Arguments]";
    t.exports = function (t) {
      return i(t) && r(t) == o;
    };
  },
  function (t, e) {
    t.exports = function () {
      return !1;
    };
  },
  function (t, e, n) {
    var r = n(11),
      i = n(53),
      o = n(9),
      a = {};
    (a["[object Float32Array]"] = a["[object Float64Array]"] = a[
      "[object Int8Array]"
    ] = a["[object Int16Array]"] = a["[object Int32Array]"] = a[
      "[object Uint8Array]"
    ] = a["[object Uint8ClampedArray]"] = a["[object Uint16Array]"] = a[
      "[object Uint32Array]"
    ] = !0),
      (a["[object Arguments]"] = a["[object Array]"] = a[
        "[object ArrayBuffer]"
      ] = a["[object Boolean]"] = a["[object DataView]"] = a[
        "[object Date]"
      ] = a["[object Error]"] = a["[object Function]"] = a["[object Map]"] = a[
        "[object Number]"
      ] = a["[object Object]"] = a["[object RegExp]"] = a["[object Set]"] = a[
        "[object String]"
      ] = a["[object WeakMap]"] = !1),
      (t.exports = function (t) {
        return o(t) && i(t.length) && !!a[r(t)];
      });
  },
  function (t, e) {
    t.exports = function (t) {
      return function (e) {
        return t(e);
      };
    };
  },
  function (t, e, n) {
    (function (t) {
      var r = n(89),
        i = e && !e.nodeType && e,
        o = i && "object" == typeof t && t && !t.nodeType && t,
        a = o && o.exports === i && r.process,
        u = (function () {
          try {
            var t = o && o.require && o.require("util").types;
            return t || (a && a.binding && a.binding("util"));
          } catch (t) {}
        })();
      t.exports = u;
    }.call(this, n(97)(t)));
  },
  function (t, e, n) {
    var r = n(98)(Object.keys, Object);
    t.exports = r;
  },
  function (t, e, n) {
    var r = n(8)(n(4), "DataView");
    t.exports = r;
  },
  function (t, e, n) {
    var r = n(8)(n(4), "Promise");
    t.exports = r;
  },
  function (t, e, n) {
    var r = n(8)(n(4), "Set");
    t.exports = r;
  },
  function (t, e, n) {
    var r = n(100),
      i = n(32);
    t.exports = function (t) {
      for (var e = i(t), n = e.length; n--; ) {
        var o = e[n],
          a = t[o];
        e[n] = [o, a, r(a)];
      }
      return e;
    };
  },
  function (t, e, n) {
    var r = n(91),
      i = n(34),
      o = n(236),
      a = n(58),
      u = n(100),
      c = n(101),
      s = n(20),
      f = 1,
      l = 2;
    t.exports = function (t, e) {
      return a(t) && u(e)
        ? c(s(t), e)
        : function (n) {
            var a = i(n, t);
            return void 0 === a && a === e ? o(n, t) : r(e, a, f | l);
          };
    };
  },
  function (t, e, n) {
    var r = n(232),
      i = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g,
      o = /\\(\\)?/g,
      a = r(function (t) {
        var e = [];
        return (
          46 === t.charCodeAt(0) && e.push(""),
          t.replace(i, function (t, n, r, i) {
            e.push(r ? i.replace(o, "$1") : n || t);
          }),
          e
        );
      });
    t.exports = a;
  },
  function (t, e, n) {
    var r = n(233),
      i = 500;
    t.exports = function (t) {
      var e = r(t, function (t) {
          return n.size === i && n.clear(), t;
        }),
        n = e.cache;
      return e;
    };
  },
  function (t, e, n) {
    var r = n(48),
      i = "Expected a function";
    function o(t, e) {
      if ("function" != typeof t || (null != e && "function" != typeof e))
        throw new TypeError(i);
      var n = function () {
        var r = arguments,
          i = e ? e.apply(this, r) : r[0],
          o = n.cache;
        if (o.has(i)) return o.get(i);
        var a = t.apply(this, r);
        return (n.cache = o.set(i, a) || o), a;
      };
      return (n.cache = new (o.Cache || r)()), n;
    }
    (o.Cache = r), (t.exports = o);
  },
  function (t, e, n) {
    var r = n(235);
    t.exports = function (t) {
      return null == t ? "" : r(t);
    };
  },
  function (t, e, n) {
    var r = n(19),
      i = n(102),
      o = n(1),
      a = n(36),
      u = 1 / 0,
      c = r ? r.prototype : void 0,
      s = c ? c.toString : void 0;
    t.exports = function t(e) {
      if ("string" == typeof e) return e;
      if (o(e)) return i(e, t) + "";
      if (a(e)) return s ? s.call(e) : "";
      var n = e + "";
      return "0" == n && 1 / e == -u ? "-0" : n;
    };
  },
  function (t, e, n) {
    var r = n(237),
      i = n(238);
    t.exports = function (t, e) {
      return null != t && i(t, e, r);
    };
  },
  function (t, e) {
    t.exports = function (t, e) {
      return null != t && e in Object(t);
    };
  },
  function (t, e, n) {
    var r = n(35),
      i = n(33),
      o = n(1),
      a = n(51),
      u = n(53),
      c = n(20);
    t.exports = function (t, e, n) {
      for (var s = -1, f = (e = r(e, t)).length, l = !1; ++s < f; ) {
        var d = c(e[s]);
        if (!(l = null != t && n(t, d))) break;
        t = t[d];
      }
      return l || ++s != f
        ? l
        : !!(f = null == t ? 0 : t.length) && u(f) && a(d, f) && (o(t) || i(t));
    };
  },
  function (t, e, n) {
    var r = n(103),
      i = n(240),
      o = n(58),
      a = n(20);
    t.exports = function (t) {
      return o(t) ? r(a(t)) : i(t);
    };
  },
  function (t, e, n) {
    var r = n(57);
    t.exports = function (t) {
      return function (e) {
        return r(e, t);
      };
    };
  },
  function (t, e, n) {
    var r = n(104),
      i = n(7),
      o = n(105),
      a = Math.max;
    t.exports = function (t, e, n) {
      var u = null == t ? 0 : t.length;
      if (!u) return -1;
      var c = null == n ? 0 : o(n);
      return c < 0 && (c = a(u + c, 0)), r(t, i(e, 3), c);
    };
  },
  function (t, e, n) {
    var r = n(60),
      i = 1 / 0,
      o = 1.7976931348623157e308;
    t.exports = function (t) {
      return t
        ? (t = r(t)) === i || t === -i
          ? (t < 0 ? -1 : 1) * o
          : t == t
          ? t
          : 0
        : 0 === t
        ? t
        : 0;
    };
  },
  function (t, e) {
    t.exports = function (t) {
      if (Array.isArray(t)) {
        for (var e = 0, n = new Array(t.length); e < t.length; e++) n[e] = t[e];
        return n;
      }
    };
  },
  function (t, e) {
    t.exports = function (t) {
      if (
        Symbol.iterator in Object(t) ||
        "[object Arguments]" === Object.prototype.toString.call(t)
      )
        return Array.from(t);
    };
  },
  function (t, e) {
    t.exports = function () {
      throw new TypeError("Invalid attempt to spread non-iterable instance");
    };
  },
  function (t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.createElementState = m),
      (e.mergeActionState = T),
      (e.ixElements = void 0);
    var r = n(18),
      i = n(2),
      o = i.IX2EngineConstants,
      a = (o.HTML_ELEMENT, o.PLAIN_OBJECT),
      u = (o.ABSTRACT_NODE, o.CONFIG_X_VALUE),
      c = o.CONFIG_Y_VALUE,
      s = o.CONFIG_Z_VALUE,
      f = o.CONFIG_VALUE,
      l = o.CONFIG_X_UNIT,
      d = o.CONFIG_Y_UNIT,
      p = o.CONFIG_Z_UNIT,
      v = o.CONFIG_UNIT,
      h = i.IX2EngineActionTypes,
      E = h.IX2_SESSION_STOPPED,
      _ = h.IX2_INSTANCE_ADDED,
      g = h.IX2_ELEMENT_STATE_CHANGED,
      y = {},
      I = "refState";
    function m(t, e, n, i, o) {
      var u =
        n === a ? (0, r.getIn)(o, ["config", "target", "objectId"]) : null;
      return (0, r.mergeIn)(t, [i], { id: i, ref: e, refId: u, refType: n });
    }
    function T(t, e, n, i, o) {
      var a = (function (t) {
          var e = t.config;
          return O.reduce(function (t, n) {
            var r = n[0],
              i = n[1],
              o = e[r],
              a = e[i];
            return null != o && null != a && (t[i] = a), t;
          }, {});
        })(o),
        u = [e, I, n];
      return (0, r.mergeIn)(t, u, i, a);
    }
    e.ixElements = function () {
      var t =
          arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : y,
        e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
      switch (e.type) {
        case E:
          return y;
        case _:
          var n = e.payload,
            i = n.elementId,
            o = n.element,
            a = n.origin,
            u = n.actionItem,
            c = n.refType,
            s = u.actionTypeId,
            f = t;
          return (
            (0, r.getIn)(f, [i, o]) !== o && (f = m(f, o, c, i, u)),
            T(f, i, s, a, u)
          );
        case g:
          var l = e.payload;
          return T(t, l.elementId, l.actionTypeId, l.current, l.actionItem);
        default:
          return t;
      }
    };
    var O = [
      [u, l],
      [c, d],
      [s, p],
      [f, v],
    ];
  },
  function (t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.clearPlugin = e.renderPlugin = e.createPluginInstance = e.getPluginDestination = e.getPluginOrigin = e.getPluginDuration = e.getPluginConfig = void 0);
    e.getPluginConfig = function (t) {
      return t.value;
    };
    e.getPluginDuration = function (t, e) {
      if ("auto" !== e.config.duration) return null;
      var n = parseFloat(t.getAttribute("data-duration"));
      return n > 0
        ? 1e3 * n
        : 1e3 * parseFloat(t.getAttribute("data-default-duration"));
    };
    e.getPluginOrigin = function (t) {
      return t || { value: 0 };
    };
    e.getPluginDestination = function (t) {
      return { value: t.value };
    };
    e.createPluginInstance = function (t) {
      var e = window.Webflow.require("lottie").createInstance(t);
      return e.stop(), e.setSubframe(!0), e;
    };
    e.renderPlugin = function (t, e, n) {
      if (t) {
        var r = e[n.actionTypeId].value / 100;
        t.goToFrame(t.frames * r);
      }
    };
    e.clearPlugin = function (t) {
      window.Webflow.require("lottie").createInstance(t).stop();
    };
  },
  function (t, e, n) {
    "use strict";
    var r,
      i,
      o,
      a = n(0),
      u = a(n(21)),
      c = a(n(26)),
      s = n(0);
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.getInstanceId = function () {
        return "i" + ht++;
      }),
      (e.getElementId = function (t, e) {
        for (var n in t) {
          var r = t[n];
          if (r && r.ref === e) return r.id;
        }
        return "e" + Et++;
      }),
      (e.reifyState = function () {
        var t =
            arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
          e = t.events,
          n = t.actionLists,
          r = t.site,
          i = (0, l.default)(
            e,
            function (t, e) {
              var n = e.eventTypeId;
              return t[n] || (t[n] = {}), (t[n][e.id] = e), t;
            },
            {}
          ),
          o = r && r.mediaQueries,
          a = [];
        o
          ? (a = o.map(function (t) {
              return t.key;
            }))
          : ((o = []), console.warn("IX2 missing mediaQueries in site data"));
        return {
          ixData: {
            events: e,
            actionLists: n,
            eventTypeMap: i,
            mediaQueries: o,
            mediaQueryKeys: a,
          },
        };
      }),
      (e.observeStore = function (t) {
        var e = t.store,
          n = t.select,
          r = t.onChange,
          i = t.comparator,
          o = void 0 === i ? _t : i,
          a = e.getState,
          u = (0, e.subscribe)(function () {
            var t = n(a());
            if (null == t) return void u();
            o(t, c) || r((c = t), e);
          }),
          c = n(a());
        return u;
      }),
      (e.getAffectedElements = yt),
      (e.getComputedStyle = function (t) {
        var e = t.element,
          n = t.actionItem;
        if (!y.IS_BROWSER_ENV) return {};
        switch (n.actionTypeId) {
          case ot:
          case at:
          case ut:
          case ct:
          case st:
            return window.getComputedStyle(e);
          default:
            return {};
        }
      }),
      (e.getInstanceOrigin = function (t) {
        var e =
            arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
          n =
            arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {},
          r = arguments.length > 3 ? arguments[3] : void 0,
          i = (arguments.length > 4 ? arguments[4] : void 0).getStyle,
          o = r.actionTypeId,
          a = r.config;
        if ((0, g.isPluginType)(o)) return (0, g.getPluginOrigin)(o)(e[o]);
        switch (o) {
          case J:
          case tt:
          case et:
          case nt:
            return e[o] || bt[o];
          case it:
            return mt(e[o], r.config.filters);
          case rt:
            return { value: (0, f.default)(parseFloat(i(t, C)), 1) };
          case ot:
            var u,
              c,
              s = i(t, D),
              l = i(t, P);
            return (
              (u =
                a.widthUnit === B
                  ? It.test(s)
                    ? parseFloat(s)
                    : parseFloat(n.width)
                  : (0, f.default)(parseFloat(s), parseFloat(n.width))),
              (c =
                a.heightUnit === B
                  ? It.test(l)
                    ? parseFloat(l)
                    : parseFloat(n.height)
                  : (0, f.default)(parseFloat(l), parseFloat(n.height))),
              { widthValue: u, heightValue: c }
            );
          case at:
          case ut:
          case ct:
            return (function (t) {
              var e = t.element,
                n = t.actionTypeId,
                r = t.computedStyle,
                i = t.getStyle,
                o = dt[n],
                a = i(e, o),
                u = Rt.test(a) ? a : r[o],
                c = (function (t, e) {
                  var n = t.exec(e);
                  return n ? n[1] : "";
                })(xt, u).split(H);
              return {
                rValue: (0, f.default)(parseInt(c[0], 10), 255),
                gValue: (0, f.default)(parseInt(c[1], 10), 255),
                bValue: (0, f.default)(parseInt(c[2], 10), 255),
                aValue: (0, f.default)(parseFloat(c[3]), 1),
              };
            })({ element: t, actionTypeId: o, computedStyle: n, getStyle: i });
          case st:
            return { value: (0, f.default)(i(t, U), n.display) };
          case ft:
            return e[o] || { value: 0 };
          default:
            return;
        }
      }),
      (e.getDestinationValues = function (t) {
        var e = t.element,
          n = t.actionItem,
          r = t.elementApi,
          i = n.actionTypeId;
        if ((0, g.isPluginType)(i))
          return (0, g.getPluginDestination)(i)(n.config);
        switch (i) {
          case J:
          case tt:
          case et:
          case nt:
            var o = n.config,
              a = o.xValue,
              u = o.yValue,
              c = o.zValue;
            return { xValue: a, yValue: u, zValue: c };
          case ot:
            var s = r.getStyle,
              f = r.setStyle,
              l = r.getProperty,
              d = n.config,
              p = d.widthUnit,
              v = d.heightUnit,
              h = n.config,
              E = h.widthValue,
              _ = h.heightValue;
            if (!y.IS_BROWSER_ENV) return { widthValue: E, heightValue: _ };
            if (p === B) {
              var I = s(e, D);
              f(e, D, ""), (E = l(e, "offsetWidth")), f(e, D, I);
            }
            if (v === B) {
              var m = s(e, P);
              f(e, P, ""), (_ = l(e, "offsetHeight")), f(e, P, m);
            }
            return { widthValue: E, heightValue: _ };
          case at:
          case ut:
          case ct:
            var T = n.config,
              O = T.rValue,
              b = T.gValue,
              A = T.bValue,
              S = T.aValue;
            return { rValue: O, gValue: b, bValue: A, aValue: S };
          case it:
            return n.config.filters.reduce(Tt, {});
          default:
            var w = n.config.value;
            return { value: w };
        }
      }),
      (e.getRenderType = Ot),
      (e.getStyleProp = function (t, e) {
        return t === q ? e.replace("STYLE_", "").toLowerCase() : null;
      }),
      (e.renderHTMLElement = function (t, e, n, r, i, o, a, u, c) {
        switch (u) {
          case K:
            return (function (t, e, n, r, i) {
              var o = wt
                  .map(function (t) {
                    var n = bt[t],
                      r = e[t] || {},
                      i = r.xValue,
                      o = void 0 === i ? n.xValue : i,
                      a = r.yValue,
                      u = void 0 === a ? n.yValue : a,
                      c = r.zValue,
                      s = void 0 === c ? n.zValue : c,
                      f = r.xUnit,
                      l = void 0 === f ? "" : f,
                      d = r.yUnit,
                      p = void 0 === d ? "" : d,
                      v = r.zUnit,
                      h = void 0 === v ? "" : v;
                    switch (t) {
                      case J:
                        return ""
                          .concat(O, "(")
                          .concat(o)
                          .concat(l, ", ")
                          .concat(u)
                          .concat(p, ", ")
                          .concat(s)
                          .concat(h, ")");
                      case tt:
                        return ""
                          .concat(b, "(")
                          .concat(o)
                          .concat(l, ", ")
                          .concat(u)
                          .concat(p, ", ")
                          .concat(s)
                          .concat(h, ")");
                      case et:
                        return ""
                          .concat(A, "(")
                          .concat(o)
                          .concat(l, ") ")
                          .concat(S, "(")
                          .concat(u)
                          .concat(p, ") ")
                          .concat(w, "(")
                          .concat(s)
                          .concat(h, ")");
                      case nt:
                        return ""
                          .concat(R, "(")
                          .concat(o)
                          .concat(l, ", ")
                          .concat(u)
                          .concat(p, ")");
                      default:
                        return "";
                    }
                  })
                  .join(" "),
                a = i.setStyle;
              Nt(t, y.TRANSFORM_PREFIXED, i),
                a(t, y.TRANSFORM_PREFIXED, o),
                (u = r),
                (c = n),
                (s = u.actionTypeId),
                (f = c.xValue),
                (l = c.yValue),
                (d = c.zValue),
                ((s === J && void 0 !== d) ||
                  (s === tt && void 0 !== d) ||
                  (s === et && (void 0 !== f || void 0 !== l))) &&
                  a(t, y.TRANSFORM_STYLE_PREFIXED, x);
              var u, c, s, f, l, d;
            })(t, e, n, i, a);
          case q:
            return (function (t, e, n, r, i, o) {
              var a = o.setStyle,
                u = r.actionTypeId,
                c = r.config;
              switch (u) {
                case ot:
                  var s = r.config,
                    f = s.widthUnit,
                    d = void 0 === f ? "" : f,
                    p = s.heightUnit,
                    v = void 0 === p ? "" : p,
                    h = n.widthValue,
                    E = n.heightValue;
                  void 0 !== h &&
                    (d === B && (d = "px"), Nt(t, D, o), a(t, D, h + d)),
                    void 0 !== E &&
                      (v === B && (v = "px"), Nt(t, P, o), a(t, P, E + v));
                  break;
                case it:
                  !(function (t, e, n, r) {
                    var i = (0, l.default)(
                        e,
                        function (t, e, r) {
                          return ""
                            .concat(t, " ")
                            .concat(r, "(")
                            .concat(e)
                            .concat(St(r, n), ")");
                        },
                        ""
                      ),
                      o = r.setStyle;
                    Nt(t, L, r), o(t, L, i);
                  })(t, n, c, o);
                  break;
                case at:
                case ut:
                case ct:
                  var _ = dt[u],
                    g = Math.round(n.rValue),
                    y = Math.round(n.gValue),
                    I = Math.round(n.bValue),
                    m = n.aValue;
                  Nt(t, _, o),
                    a(
                      t,
                      _,
                      m >= 1
                        ? "rgb(".concat(g, ",").concat(y, ",").concat(I, ")")
                        : "rgba("
                            .concat(g, ",")
                            .concat(y, ",")
                            .concat(I, ",")
                            .concat(m, ")")
                    );
                  break;
                default:
                  var T = c.unit,
                    O = void 0 === T ? "" : T;
                  Nt(t, i, o), a(t, i, n.value + O);
              }
            })(t, 0, n, i, o, a);
          case Q:
            return (function (t, e, n) {
              var r = n.setStyle;
              switch (e.actionTypeId) {
                case st:
                  var i = e.config.value;
                  return void (i === N && y.IS_BROWSER_ENV
                    ? r(t, U, y.FLEX_PREFIXED)
                    : r(t, U, i));
              }
            })(t, i, a);
          case $:
            var s = i.actionTypeId;
            if ((0, g.isPluginType)(s)) return (0, g.renderPlugin)(s)(c, e, i);
        }
      }),
      (e.clearAllStyles = function (t) {
        var e = t.store,
          n = t.elementApi,
          r = e.getState().ixData,
          i = r.events,
          o = void 0 === i ? {} : i,
          a = r.actionLists,
          u = void 0 === a ? {} : a;
        Object.keys(o).forEach(function (t) {
          var e = o[t],
            r = e.action.config,
            i = r.actionListId,
            a = u[i];
          a && Lt({ actionList: a, event: e, elementApi: n });
        }),
          Object.keys(u).forEach(function (t) {
            Lt({ actionList: u[t], elementApi: n });
          });
      }),
      (e.cleanupHTMLElement = function (t, e, n) {
        var r = n.setStyle,
          i = n.getStyle,
          o = e.actionTypeId;
        if (o === ot) {
          var a = e.config;
          a.widthUnit === B && r(t, D, ""), a.heightUnit === B && r(t, P, "");
        }
        i(t, W) && Pt({ effect: Ct, actionTypeId: o, elementApi: n })(t);
      }),
      (e.getMaxDurationItemIndex = jt),
      (e.getActionListProgress = function (t, e) {
        var n = t.actionItemGroups,
          r = t.useFirstGroupAsInitialState,
          i = e.actionItem,
          o = e.verboseTimeElapsed,
          a = void 0 === o ? 0 : o,
          u = 0,
          c = 0;
        return (
          n.forEach(function (t, e) {
            if (!r || 0 !== e) {
              var n = t.actionItems,
                o = n[jt(n)],
                s = o.config,
                f = o.actionTypeId;
              i.id === o.id && (c = u + a);
              var l = Ot(f) === Q ? 0 : s.duration;
              u += s.delay + l;
            }
          }),
          u > 0 ? (0, _.optimizeFloat)(c / u) : 0
        );
      }),
      (e.reduceListToGroup = function (t) {
        var e = t.actionList,
          n = t.actionItemId,
          r = t.rawData,
          i = e.actionItemGroups,
          o = e.continuousParameterGroups,
          a = [],
          u = function (t) {
            return (
              a.push((0, v.mergeIn)(t, ["config"], { delay: 0, duration: 0 })),
              t.id === n
            );
          };
        return (
          i &&
            i.some(function (t) {
              return t.actionItems.some(u);
            }),
          o &&
            o.some(function (t) {
              return t.continuousActionGroups.some(function (t) {
                return t.actionItems.some(u);
              });
            }),
          (0, v.setIn)(
            r,
            ["actionLists"],
            (0, c.default)({}, e.id, {
              id: e.id,
              actionItemGroups: [{ actionItems: a }],
            })
          )
        );
      }),
      (e.shouldNamespaceEventParameter = function (t, e) {
        var n = e.basedOn;
        return (
          (t === E.EventTypeConsts.SCROLLING_IN_VIEW &&
            (n === E.EventBasedOn.ELEMENT || null == n)) ||
          (t === E.EventTypeConsts.MOUSE_MOVE && n === E.EventBasedOn.ELEMENT)
        );
      }),
      (e.getNamespacedParameterId = function (t, e) {
        return t + z + e;
      }),
      (e.shouldAllowMediaQuery = function (t, e) {
        if (null == e) return !0;
        return -1 !== t.indexOf(e);
      }),
      (e.mediaQueriesEqual = function (t, e) {
        return (0, h.default)(t && t.sort(), e && e.sort());
      }),
      (e.stringifyTarget = function (t) {
        if ("string" == typeof t) return t;
        var e = t.id,
          n = void 0 === e ? "" : e,
          r = t.selector,
          i = void 0 === r ? "" : r,
          o = t.useEventTarget;
        return n + Y + i + Y + (void 0 === o ? "" : o);
      }),
      (e.getItemConfigByKey = void 0);
    var f = s(n(249)),
      l = s(n(250)),
      d = s(n(256)),
      p = s(n(34)),
      v = n(18),
      h = s(n(113)),
      E = n(2),
      _ = n(108),
      g = n(110),
      y = n(45),
      I = E.IX2EngineConstants,
      m = I.BACKGROUND,
      T = I.TRANSFORM,
      O = I.TRANSLATE_3D,
      b = I.SCALE_3D,
      A = I.ROTATE_X,
      S = I.ROTATE_Y,
      w = I.ROTATE_Z,
      R = I.SKEW,
      x = I.PRESERVE_3D,
      N = I.FLEX,
      C = I.OPACITY,
      L = I.FILTER,
      D = I.WIDTH,
      P = I.HEIGHT,
      M = I.BACKGROUND_COLOR,
      j = I.BORDER_COLOR,
      F = I.COLOR,
      k = I.CHILDREN,
      X = I.IMMEDIATE_CHILDREN,
      G = I.SIBLINGS,
      V = I.PARENT,
      U = I.DISPLAY,
      W = I.WILL_CHANGE,
      B = I.AUTO,
      H = I.COMMA_DELIMITER,
      z = I.COLON_DELIMITER,
      Y = I.BAR_DELIMITER,
      K = I.RENDER_TRANSFORM,
      Q = I.RENDER_GENERAL,
      q = I.RENDER_STYLE,
      $ = I.RENDER_PLUGIN,
      Z = E.ActionTypeConsts,
      J = Z.TRANSFORM_MOVE,
      tt = Z.TRANSFORM_SCALE,
      et = Z.TRANSFORM_ROTATE,
      nt = Z.TRANSFORM_SKEW,
      rt = Z.STYLE_OPACITY,
      it = Z.STYLE_FILTER,
      ot = Z.STYLE_SIZE,
      at = Z.STYLE_BACKGROUND_COLOR,
      ut = Z.STYLE_BORDER,
      ct = Z.STYLE_TEXT_COLOR,
      st = Z.GENERAL_DISPLAY,
      ft = "OBJECT_VALUE",
      lt = function (t) {
        return t.trim();
      },
      dt = Object.freeze(
        ((r = {}),
        (0, c.default)(r, at, M),
        (0, c.default)(r, ut, j),
        (0, c.default)(r, ct, F),
        r)
      ),
      pt = Object.freeze(
        ((i = {}),
        (0, c.default)(i, y.TRANSFORM_PREFIXED, T),
        (0, c.default)(i, M, m),
        (0, c.default)(i, C, C),
        (0, c.default)(i, L, L),
        (0, c.default)(i, D, D),
        (0, c.default)(i, P, P),
        i)
      ),
      vt = {},
      ht = 1;
    var Et = 1;
    var _t = function (t, e) {
      return t === e;
    };
    function gt(t) {
      var e = (0, u.default)(t);
      return "string" === e
        ? { id: t }
        : null != t && "object" === e
        ? {
            id: t.id,
            objectId: t.objectId,
            selector: t.selector,
            selectorGuids: t.selectorGuids,
            appliesTo: t.appliesTo,
            useEventTarget: t.useEventTarget,
          }
        : {};
    }
    function yt(t) {
      var e = t.config,
        n = t.event,
        r = t.eventTarget,
        i = t.elementRoot,
        o = t.elementApi;
      if (!o) throw new Error("IX2 missing elementApi");
      var a = o.getValidDocument,
        u = o.getQuerySelector,
        c = o.queryDocument,
        s = o.getChildElements,
        f = o.getSiblingElements,
        l = o.matchSelector,
        d = o.elementContains,
        v = o.isSiblingNode,
        h = e.target;
      if (!h) return [];
      var _ = gt(h),
        g = _.id,
        I = _.objectId,
        m = _.selector,
        T = _.selectorGuids,
        O = _.appliesTo,
        b = _.useEventTarget;
      if (I) return [vt[I] || (vt[I] = {})];
      if (O === E.EventAppliesTo.PAGE) {
        var A = a(g);
        return A ? [A] : [];
      }
      var S,
        w,
        R,
        x =
          (0, p.default)(n, "action.config.affectedElements", {})[g || m] || {},
        N = Boolean(x.id || x.selector),
        C = n && u(gt(n.target));
      if (
        (N
          ? ((S = x.limitAffectedElements), (w = C), (R = u(x)))
          : (w = R = u({ id: g, selector: m, selectorGuids: T })),
        n && b)
      ) {
        var L = r && (R || !0 === b) ? [r] : c(C);
        if (R) {
          if (b === V)
            return c(R).filter(function (t) {
              return L.some(function (e) {
                return d(t, e);
              });
            });
          if (b === k)
            return c(R).filter(function (t) {
              return L.some(function (e) {
                return d(e, t);
              });
            });
          if (b === G)
            return c(R).filter(function (t) {
              return L.some(function (e) {
                return v(e, t);
              });
            });
        }
        return L;
      }
      return null == w || null == R
        ? []
        : y.IS_BROWSER_ENV && i
        ? c(R).filter(function (t) {
            return i.contains(t);
          })
        : S === k
        ? c(w, R)
        : S === X
        ? s(c(w)).filter(l(R))
        : S === G
        ? f(c(w)).filter(l(R))
        : c(R);
    }
    var It = /px/,
      mt = function (t, e) {
        return e.reduce(function (t, e) {
          return null == t[e.type] && (t[e.type] = At[e.type]), t;
        }, t || {});
      };
    var Tt = function (t, e) {
      return e && (t[e.type] = e.value || 0), t;
    };
    function Ot(t) {
      return /^TRANSFORM_/.test(t)
        ? K
        : /^STYLE_/.test(t)
        ? q
        : /^GENERAL_/.test(t)
        ? Q
        : /^PLUGIN_/.test(t)
        ? $
        : void 0;
    }
    e.getItemConfigByKey = function (t, e, n) {
      if ((0, g.isPluginType)(t)) return (0, g.getPluginConfig)(t)(n, e);
      switch (t) {
        case it:
          var r = (0, d.default)(n.filters, function (t) {
            return t.type === e;
          });
          return r ? r.value : 0;
        default:
          return n[e];
      }
    };
    var bt =
        ((o = {}),
        (0, c.default)(
          o,
          J,
          Object.freeze({ xValue: 0, yValue: 0, zValue: 0 })
        ),
        (0, c.default)(
          o,
          tt,
          Object.freeze({ xValue: 1, yValue: 1, zValue: 1 })
        ),
        (0, c.default)(
          o,
          et,
          Object.freeze({ xValue: 0, yValue: 0, zValue: 0 })
        ),
        (0, c.default)(o, nt, Object.freeze({ xValue: 0, yValue: 0 })),
        o),
      At = Object.freeze({
        blur: 0,
        "hue-rotate": 0,
        invert: 0,
        grayscale: 0,
        saturate: 100,
        sepia: 0,
        contrast: 100,
        brightness: 100,
      }),
      St = function (t, e) {
        var n = (0, d.default)(e.filters, function (e) {
          return e.type === t;
        });
        if (n && n.unit) return n.unit;
        switch (t) {
          case "blur":
            return "px";
          case "hue-rotate":
            return "deg";
          default:
            return "%";
        }
      },
      wt = Object.keys(bt);
    var Rt = /^rgb/,
      xt = RegExp("rgba?".concat("\\(([^)]+)\\)"));
    function Nt(t, e, n) {
      if (y.IS_BROWSER_ENV) {
        var r = pt[e];
        if (r) {
          var i = n.getStyle,
            o = n.setStyle,
            a = i(t, W);
          if (a) {
            var u = a.split(H).map(lt);
            -1 === u.indexOf(r) && o(t, W, u.concat(r).join(H));
          } else o(t, W, r);
        }
      }
    }
    function Ct(t, e, n) {
      if (y.IS_BROWSER_ENV) {
        var r = pt[e];
        if (r) {
          var i = n.getStyle,
            o = n.setStyle,
            a = i(t, W);
          a &&
            -1 !== a.indexOf(r) &&
            o(
              t,
              W,
              a
                .split(H)
                .map(lt)
                .filter(function (t) {
                  return t !== r;
                })
                .join(H)
            );
        }
      }
    }
    function Lt(t) {
      var e = t.actionList,
        n = void 0 === e ? {} : e,
        r = t.event,
        i = t.elementApi,
        o = n.actionItemGroups,
        a = n.continuousParameterGroups;
      o &&
        o.forEach(function (t) {
          Dt({ actionGroup: t, event: r, elementApi: i });
        }),
        a &&
          a.forEach(function (t) {
            t.continuousActionGroups.forEach(function (t) {
              Dt({ actionGroup: t, event: r, elementApi: i });
            });
          });
    }
    function Dt(t) {
      var e = t.actionGroup,
        n = t.event,
        r = t.elementApi;
      e.actionItems.forEach(function (t) {
        var e,
          i = t.actionTypeId,
          o = t.config;
        (e = (0, g.isPluginType)(i)
          ? (0, g.clearPlugin)(i)
          : Pt({ effect: Mt, actionTypeId: i, elementApi: r })),
          yt({ config: o, event: n, elementApi: r }).forEach(e);
      });
    }
    var Pt = function (t) {
      var e = t.effect,
        n = t.actionTypeId,
        r = t.elementApi;
      return function (t) {
        switch (n) {
          case J:
          case tt:
          case et:
          case nt:
            e(t, y.TRANSFORM_PREFIXED, r);
            break;
          case it:
            e(t, L, r);
            break;
          case rt:
            e(t, C, r);
            break;
          case ot:
            e(t, D, r), e(t, P, r);
            break;
          case at:
          case ut:
          case ct:
            e(t, dt[n], r);
            break;
          case st:
            e(t, U, r);
        }
      };
    };
    function Mt(t, e, n) {
      var r = n.setStyle;
      Ct(t, e, n),
        r(t, e, ""),
        e === y.TRANSFORM_PREFIXED && r(t, y.TRANSFORM_STYLE_PREFIXED, "");
    }
    function jt(t) {
      var e = 0,
        n = 0;
      return (
        t.forEach(function (t, r) {
          var i = t.config,
            o = i.delay + i.duration;
          o >= e && ((e = o), (n = r));
        }),
        n
      );
    }
  },
  function (t, e) {
    t.exports = function (t, e) {
      return null == t || t != t ? e : t;
    };
  },
  function (t, e, n) {
    var r = n(251),
      i = n(111),
      o = n(7),
      a = n(255),
      u = n(1);
    t.exports = function (t, e, n) {
      var c = u(t) ? r : a,
        s = arguments.length < 3;
      return c(t, o(e, 4), n, s, i);
    };
  },
  function (t, e) {
    t.exports = function (t, e, n, r) {
      var i = -1,
        o = null == t ? 0 : t.length;
      for (r && o && (n = t[++i]); ++i < o; ) n = e(n, t[i], i, t);
      return n;
    };
  },
  function (t, e, n) {
    var r = n(253)();
    t.exports = r;
  },
  function (t, e) {
    t.exports = function (t) {
      return function (e, n, r) {
        for (var i = -1, o = Object(e), a = r(e), u = a.length; u--; ) {
          var c = a[t ? u : ++i];
          if (!1 === n(o[c], c, o)) break;
        }
        return e;
      };
    };
  },
  function (t, e, n) {
    var r = n(12);
    t.exports = function (t, e) {
      return function (n, i) {
        if (null == n) return n;
        if (!r(n)) return t(n, i);
        for (
          var o = n.length, a = e ? o : -1, u = Object(n);
          (e ? a-- : ++a < o) && !1 !== i(u[a], a, u);

        );
        return n;
      };
    };
  },
  function (t, e) {
    t.exports = function (t, e, n, r, i) {
      return (
        i(t, function (t, i, o) {
          n = r ? ((r = !1), t) : e(n, t, i, o);
        }),
        n
      );
    };
  },
  function (t, e, n) {
    var r = n(86)(n(257));
    t.exports = r;
  },
  function (t, e, n) {
    var r = n(104),
      i = n(7),
      o = n(105),
      a = Math.max,
      u = Math.min;
    t.exports = function (t, e, n) {
      var c = null == t ? 0 : t.length;
      if (!c) return -1;
      var s = c - 1;
      return (
        void 0 !== n && ((s = o(n)), (s = n < 0 ? a(c + s, 0) : u(s, c - 1))),
        r(t, i(e, 3), s, !0)
      );
    };
  },
  function (t, e) {
    t.exports = function (t) {
      return t && t.__esModule ? t : { default: t };
    };
  },
  function (t, e, n) {
    "use strict";
    var r = Object.prototype.hasOwnProperty;
    function i(t, e) {
      return t === e ? 0 !== t || 0 !== e || 1 / t == 1 / e : t != t && e != e;
    }
    t.exports = function (t, e) {
      if (i(t, e)) return !0;
      if (
        "object" != typeof t ||
        null === t ||
        "object" != typeof e ||
        null === e
      )
        return !1;
      var n = Object.keys(t),
        o = Object.keys(e);
      if (n.length !== o.length) return !1;
      for (var a = 0; a < n.length; a++)
        if (!r.call(e, n[a]) || !i(t[n[a]], e[n[a]])) return !1;
      return !0;
    };
  },
  function (t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.ixInstances = void 0);
    var r = n(2),
      i = n(10),
      o = n(18),
      a = r.IX2EngineActionTypes,
      u = a.IX2_RAW_DATA_IMPORTED,
      c = a.IX2_SESSION_STOPPED,
      s = a.IX2_INSTANCE_ADDED,
      f = a.IX2_INSTANCE_STARTED,
      l = a.IX2_INSTANCE_REMOVED,
      d = a.IX2_ANIMATION_FRAME_CHANGED,
      p = i.IX2EasingUtils,
      v = p.optimizeFloat,
      h = p.applyEasing,
      E = p.createBezierEasing,
      _ = r.IX2EngineConstants.RENDER_GENERAL,
      g = i.IX2VanillaUtils,
      y = g.getItemConfigByKey,
      I = g.getRenderType,
      m = g.getStyleProp,
      T = function (t, e) {
        var n = t.position,
          r = t.parameterId,
          i = t.actionGroups,
          a = t.destinationKeys,
          u = t.smoothing,
          c = t.restingValue,
          s = t.actionTypeId,
          f = t.customEasingFn,
          l = e.payload.parameters,
          d = Math.max(1 - u, 0.01),
          p = l[r];
        null == p && ((d = 1), (p = c));
        var E,
          _,
          g,
          I,
          m = Math.max(p, 0) || 0,
          T = v(m - n),
          O = v(n + T * d),
          b = 100 * O;
        if (O === n && t.current) return t;
        for (var A = 0, S = i.length; A < S; A++) {
          var w = i[A],
            R = w.keyframe,
            x = w.actionItems;
          if ((0 === A && (E = x[0]), b >= R)) {
            E = x[0];
            var N = i[A + 1],
              C = N && b !== R;
            (_ = C ? N.actionItems[0] : null),
              C && ((g = R / 100), (I = (N.keyframe - R) / 100));
          }
        }
        var L = {};
        if (E && !_)
          for (var D = 0, P = a.length; D < P; D++) {
            var M = a[D];
            L[M] = y(s, M, E.config);
          }
        else if (E && _ && void 0 !== g && void 0 !== I)
          for (
            var j = (O - g) / I,
              F = E.config.easing,
              k = h(F, j, f),
              X = 0,
              G = a.length;
            X < G;
            X++
          ) {
            var V = a[X],
              U = y(s, V, E.config),
              W = (y(s, V, _.config) - U) * k + U;
            L[V] = W;
          }
        return (0, o.merge)(t, { position: O, current: L });
      },
      O = function (t, e) {
        var n = t,
          r = n.active,
          i = n.origin,
          a = n.start,
          u = n.immediate,
          c = n.renderType,
          s = n.verbose,
          f = n.actionItem,
          l = n.destination,
          d = n.destinationKeys,
          p = n.pluginDuration,
          E = n.instanceDelay,
          g = n.customEasingFn,
          y = f.config.easing,
          I = f.config,
          m = I.duration,
          T = I.delay;
        null != p && (m = p),
          (T = null != E ? E : T),
          c === _ ? (m = 0) : u && (m = T = 0);
        var O = e.payload.now;
        if (r && i) {
          var b = O - (a + T);
          if (s) {
            var A = O - a,
              S = m + T,
              w = v(Math.min(Math.max(0, A / S), 1));
            t = (0, o.set)(t, "verboseTimeElapsed", S * w);
          }
          if (b < 0) return t;
          var R = v(Math.min(Math.max(0, b / m), 1)),
            x = h(y, R, g),
            N = {},
            C = null;
          return (
            d.length &&
              (C = d.reduce(function (t, e) {
                var n = l[e],
                  r = parseFloat(i[e]) || 0,
                  o = (parseFloat(n) - r) * x + r;
                return (t[e] = o), t;
              }, {})),
            (N.current = C),
            (N.position = R),
            1 === R && ((N.active = !1), (N.complete = !0)),
            (0, o.merge)(t, N)
          );
        }
        return t;
      };
    e.ixInstances = function () {
      var t =
          arguments.length > 0 && void 0 !== arguments[0]
            ? arguments[0]
            : Object.freeze({}),
        e = arguments.length > 1 ? arguments[1] : void 0;
      switch (e.type) {
        case u:
          return e.payload.ixInstances || Object.freeze({});
        case c:
          return Object.freeze({});
        case s:
          var n = e.payload,
            r = n.instanceId,
            i = n.elementId,
            a = n.actionItem,
            p = n.eventId,
            v = n.eventTarget,
            h = n.eventStateKey,
            _ = n.actionListId,
            g = n.groupIndex,
            y = n.isCarrier,
            b = n.origin,
            A = n.destination,
            S = n.immediate,
            w = n.verbose,
            R = n.continuous,
            x = n.parameterId,
            N = n.actionGroups,
            C = n.smoothing,
            L = n.restingValue,
            D = n.pluginInstance,
            P = n.pluginDuration,
            M = n.instanceDelay,
            j = a.actionTypeId,
            F = I(j),
            k = m(F, j),
            X = Object.keys(A).filter(function (t) {
              return null != A[t];
            }),
            G = a.config.easing;
          return (0, o.set)(t, r, {
            id: r,
            elementId: i,
            active: !1,
            position: 0,
            start: 0,
            origin: b,
            destination: A,
            destinationKeys: X,
            immediate: S,
            verbose: w,
            current: null,
            actionItem: a,
            actionTypeId: j,
            eventId: p,
            eventTarget: v,
            eventStateKey: h,
            actionListId: _,
            groupIndex: g,
            renderType: F,
            isCarrier: y,
            styleProp: k,
            continuous: R,
            parameterId: x,
            actionGroups: N,
            smoothing: C,
            restingValue: L,
            pluginInstance: D,
            pluginDuration: P,
            instanceDelay: M,
            customEasingFn: Array.isArray(G) && 4 === G.length ? E(G) : void 0,
          });
        case f:
          var V = e.payload,
            U = V.instanceId,
            W = V.time;
          return (0, o.mergeIn)(t, [U], { active: !0, complete: !1, start: W });
        case l:
          var B = e.payload.instanceId;
          if (!t[B]) return t;
          for (
            var H = {}, z = Object.keys(t), Y = z.length, K = 0;
            K < Y;
            K++
          ) {
            var Q = z[K];
            Q !== B && (H[Q] = t[Q]);
          }
          return H;
        case d:
          for (var q = t, $ = Object.keys(t), Z = $.length, J = 0; J < Z; J++) {
            var tt = $[J],
              et = t[tt],
              nt = et.continuous ? T : O;
            q = (0, o.set)(q, tt, nt(et, e));
          }
          return q;
        default:
          return t;
      }
    };
  },
  function (t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.ixParameters = void 0);
    var r = n(2).IX2EngineActionTypes,
      i = r.IX2_RAW_DATA_IMPORTED,
      o = r.IX2_SESSION_STOPPED,
      a = r.IX2_PARAMETER_CHANGED;
    e.ixParameters = function () {
      var t =
          arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
        e = arguments.length > 1 ? arguments[1] : void 0;
      switch (e.type) {
        case i:
          return e.payload.ixParameters || {};
        case o:
          return {};
        case a:
          var n = e.payload,
            r = n.key,
            u = n.value;
          return (t[r] = u), t;
        default:
          return t;
      }
    };
  },
  function (t, e) {
    t.exports = function (t, e) {
      if (null == t) return {};
      var n,
        r,
        i = {},
        o = Object.keys(t);
      for (r = 0; r < o.length; r++)
        (n = o[r]), e.indexOf(n) >= 0 || (i[n] = t[n]);
      return i;
    };
  },
  function (t, e, n) {
    var r = n(54),
      i = n(56),
      o = n(12),
      a = n(264),
      u = n(265),
      c = "[object Map]",
      s = "[object Set]";
    t.exports = function (t) {
      if (null == t) return 0;
      if (o(t)) return a(t) ? u(t) : t.length;
      var e = i(t);
      return e == c || e == s ? t.size : r(t).length;
    };
  },
  function (t, e, n) {
    var r = n(11),
      i = n(1),
      o = n(9),
      a = "[object String]";
    t.exports = function (t) {
      return "string" == typeof t || (!i(t) && o(t) && r(t) == a);
    };
  },
  function (t, e, n) {
    var r = n(266),
      i = n(267),
      o = n(268);
    t.exports = function (t) {
      return i(t) ? o(t) : r(t);
    };
  },
  function (t, e, n) {
    var r = n(103)("length");
    t.exports = r;
  },
  function (t, e) {
    var n = RegExp(
      "[\\u200d\\ud800-\\udfff\\u0300-\\u036f\\ufe20-\\ufe2f\\u20d0-\\u20ff\\ufe0e\\ufe0f]"
    );
    t.exports = function (t) {
      return n.test(t);
    };
  },
  function (t, e) {
    var n = "[\\ud800-\\udfff]",
      r = "[\\u0300-\\u036f\\ufe20-\\ufe2f\\u20d0-\\u20ff]",
      i = "\\ud83c[\\udffb-\\udfff]",
      o = "[^\\ud800-\\udfff]",
      a = "(?:\\ud83c[\\udde6-\\uddff]){2}",
      u = "[\\ud800-\\udbff][\\udc00-\\udfff]",
      c = "(?:" + r + "|" + i + ")" + "?",
      s =
        "[\\ufe0e\\ufe0f]?" +
        c +
        ("(?:\\u200d(?:" +
          [o, a, u].join("|") +
          ")[\\ufe0e\\ufe0f]?" +
          c +
          ")*"),
      f = "(?:" + [o + r + "?", r, a, u, n].join("|") + ")",
      l = RegExp(i + "(?=" + i + ")|" + f + s, "g");
    t.exports = function (t) {
      for (var e = (l.lastIndex = 0); l.test(t); ) ++e;
      return e;
    };
  },
  function (t, e, n) {
    var r = n(7),
      i = n(270),
      o = n(271);
    t.exports = function (t, e) {
      return o(t, i(r(e)));
    };
  },
  function (t, e) {
    var n = "Expected a function";
    t.exports = function (t) {
      if ("function" != typeof t) throw new TypeError(n);
      return function () {
        var e = arguments;
        switch (e.length) {
          case 0:
            return !t.call(this);
          case 1:
            return !t.call(this, e[0]);
          case 2:
            return !t.call(this, e[0], e[1]);
          case 3:
            return !t.call(this, e[0], e[1], e[2]);
        }
        return !t.apply(this, e);
      };
    };
  },
  function (t, e, n) {
    var r = n(102),
      i = n(7),
      o = n(272),
      a = n(275);
    t.exports = function (t, e) {
      if (null == t) return {};
      var n = r(a(t), function (t) {
        return [t];
      });
      return (
        (e = i(e)),
        o(t, n, function (t, n) {
          return e(t, n[0]);
        })
      );
    };
  },
  function (t, e, n) {
    var r = n(57),
      i = n(273),
      o = n(35);
    t.exports = function (t, e, n) {
      for (var a = -1, u = e.length, c = {}; ++a < u; ) {
        var s = e[a],
          f = r(t, s);
        n(f, s) && i(c, o(s, t), f);
      }
      return c;
    };
  },
  function (t, e, n) {
    var r = n(274),
      i = n(35),
      o = n(51),
      a = n(5),
      u = n(20);
    t.exports = function (t, e, n, c) {
      if (!a(t)) return t;
      for (
        var s = -1, f = (e = i(e, t)).length, l = f - 1, d = t;
        null != d && ++s < f;

      ) {
        var p = u(e[s]),
          v = n;
        if (s != l) {
          var h = d[p];
          void 0 === (v = c ? c(h, p, d) : void 0) &&
            (v = a(h) ? h : o(e[s + 1]) ? [] : {});
        }
        r(d, p, v), (d = d[p]);
      }
      return t;
    };
  },
  function (t, e, n) {
    var r = n(115),
      i = n(46),
      o = Object.prototype.hasOwnProperty;
    t.exports = function (t, e, n) {
      var a = t[e];
      (o.call(t, e) && i(a, n) && (void 0 !== n || e in t)) || r(t, e, n);
    };
  },
  function (t, e, n) {
    var r = n(93),
      i = n(276),
      o = n(278);
    t.exports = function (t) {
      return r(t, o, i);
    };
  },
  function (t, e, n) {
    var r = n(49),
      i = n(277),
      o = n(94),
      a = n(95),
      u = Object.getOwnPropertySymbols
        ? function (t) {
            for (var e = []; t; ) r(e, o(t)), (t = i(t));
            return e;
          }
        : a;
    t.exports = u;
  },
  function (t, e, n) {
    var r = n(98)(Object.getPrototypeOf, Object);
    t.exports = r;
  },
  function (t, e, n) {
    var r = n(96),
      i = n(279),
      o = n(12);
    t.exports = function (t) {
      return o(t) ? r(t, !0) : i(t);
    };
  },
  function (t, e, n) {
    var r = n(5),
      i = n(55),
      o = n(280),
      a = Object.prototype.hasOwnProperty;
    t.exports = function (t) {
      if (!r(t)) return o(t);
      var e = i(t),
        n = [];
      for (var u in t)
        ("constructor" != u || (!e && a.call(t, u))) && n.push(u);
      return n;
    };
  },
  function (t, e) {
    t.exports = function (t) {
      var e = [];
      if (null != t) for (var n in Object(t)) e.push(n);
      return e;
    };
  },
  function (t, e, n) {
    var r = n(54),
      i = n(56),
      o = n(33),
      a = n(1),
      u = n(12),
      c = n(50),
      s = n(55),
      f = n(52),
      l = "[object Map]",
      d = "[object Set]",
      p = Object.prototype.hasOwnProperty;
    t.exports = function (t) {
      if (null == t) return !0;
      if (
        u(t) &&
        (a(t) ||
          "string" == typeof t ||
          "function" == typeof t.splice ||
          c(t) ||
          f(t) ||
          o(t))
      )
        return !t.length;
      var e = i(t);
      if (e == l || e == d) return !t.size;
      if (s(t)) return !r(t).length;
      for (var n in t) if (p.call(t, n)) return !1;
      return !0;
    };
  },
  function (t, e, n) {
    var r = n(115),
      i = n(112),
      o = n(7);
    t.exports = function (t, e) {
      var n = {};
      return (
        (e = o(e, 3)),
        i(t, function (t, i, o) {
          r(n, i, e(t, i, o));
        }),
        n
      );
    };
  },
  function (t, e, n) {
    var r = n(284),
      i = n(111),
      o = n(285),
      a = n(1);
    t.exports = function (t, e) {
      return (a(t) ? r : i)(t, o(e));
    };
  },
  function (t, e) {
    t.exports = function (t, e) {
      for (
        var n = -1, r = null == t ? 0 : t.length;
        ++n < r && !1 !== e(t[n], n, t);

      );
      return t;
    };
  },
  function (t, e, n) {
    var r = n(59);
    t.exports = function (t) {
      return "function" == typeof t ? t : r;
    };
  },
  function (t, e, n) {
    var r = n(287),
      i = n(5),
      o = "Expected a function";
    t.exports = function (t, e, n) {
      var a = !0,
        u = !0;
      if ("function" != typeof t) throw new TypeError(o);
      return (
        i(n) &&
          ((a = "leading" in n ? !!n.leading : a),
          (u = "trailing" in n ? !!n.trailing : u)),
        r(t, e, { leading: a, maxWait: e, trailing: u })
      );
    };
  },
  function (t, e, n) {
    var r = n(5),
      i = n(288),
      o = n(60),
      a = "Expected a function",
      u = Math.max,
      c = Math.min;
    t.exports = function (t, e, n) {
      var s,
        f,
        l,
        d,
        p,
        v,
        h = 0,
        E = !1,
        _ = !1,
        g = !0;
      if ("function" != typeof t) throw new TypeError(a);
      function y(e) {
        var n = s,
          r = f;
        return (s = f = void 0), (h = e), (d = t.apply(r, n));
      }
      function I(t) {
        var n = t - v;
        return void 0 === v || n >= e || n < 0 || (_ && t - h >= l);
      }
      function m() {
        var t = i();
        if (I(t)) return T(t);
        p = setTimeout(
          m,
          (function (t) {
            var n = e - (t - v);
            return _ ? c(n, l - (t - h)) : n;
          })(t)
        );
      }
      function T(t) {
        return (p = void 0), g && s ? y(t) : ((s = f = void 0), d);
      }
      function O() {
        var t = i(),
          n = I(t);
        if (((s = arguments), (f = this), (v = t), n)) {
          if (void 0 === p)
            return (function (t) {
              return (h = t), (p = setTimeout(m, e)), E ? y(t) : d;
            })(v);
          if (_) return clearTimeout(p), (p = setTimeout(m, e)), y(v);
        }
        return void 0 === p && (p = setTimeout(m, e)), d;
      }
      return (
        (e = o(e) || 0),
        r(n) &&
          ((E = !!n.leading),
          (l = (_ = "maxWait" in n) ? u(o(n.maxWait) || 0, e) : l),
          (g = "trailing" in n ? !!n.trailing : g)),
        (O.cancel = function () {
          void 0 !== p && clearTimeout(p), (h = 0), (s = v = f = p = void 0);
        }),
        (O.flush = function () {
          return void 0 === p ? d : T(i());
        }),
        O
      );
    };
  },
  function (t, e, n) {
    var r = n(4);
    t.exports = function () {
      return r.Date.now();
    };
  },
  function (t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", { value: !0 });
    var r = n(290);
    Object.keys(r).forEach(function (t) {
      "default" !== t &&
        "__esModule" !== t &&
        Object.defineProperty(e, t, {
          enumerable: !0,
          get: function () {
            return r[t];
          },
        });
    });
  },
  function (t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.isQuickEffect = void 0);
    var r = n(2),
      i = Object.keys(r.QuickEffectIds);
    e.isQuickEffect = function (t) {
      return i.includes(t);
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(0)(n(21));
    Object.defineProperty(e, "__esModule", { value: !0 }),
      (e.setStyle = function (t, e, n) {
        t.style[e] = n;
      }),
      (e.getStyle = function (t, e) {
        return t.style[e];
      }),
      (e.getProperty = function (t, e) {
        return t[e];
      }),
      (e.matchSelector = function (t) {
        return function (e) {
          return e[a](t);
        };
      }),
      (e.getQuerySelector = function (t) {
        var e = t.id,
          n = t.selector;
        if (e) {
          var r = e;
          if (-1 !== e.indexOf(c)) {
            var i = e.split(c),
              o = i[0];
            if (((r = i[1]), o !== document.documentElement.getAttribute(l)))
              return null;
          }
          return '[data-w-id^="'.concat(r, '"]');
        }
        return n;
      }),
      (e.getValidDocument = function (t) {
        if (null == t || t === document.documentElement.getAttribute(l))
          return document;
        return null;
      }),
      (e.queryDocument = function (t, e) {
        return Array.prototype.slice.call(
          document.querySelectorAll(e ? t + " " + e : t)
        );
      }),
      (e.elementContains = function (t, e) {
        return t.contains(e);
      }),
      (e.isSiblingNode = function (t, e) {
        return t !== e && t.parentNode === e.parentNode;
      }),
      (e.getChildElements = function (t) {
        for (var e = [], n = 0, r = (t || []).length; n < r; n++) {
          var i = t[n].children,
            o = i.length;
          if (o) for (var a = 0; a < o; a++) e.push(i[a]);
        }
        return e;
      }),
      (e.getSiblingElements = function () {
        for (
          var t =
              arguments.length > 0 && void 0 !== arguments[0]
                ? arguments[0]
                : [],
            e = [],
            n = [],
            r = 0,
            i = t.length;
          r < i;
          r++
        ) {
          var o = t[r].parentNode;
          if (o && o.children && o.children.length && -1 === n.indexOf(o)) {
            n.push(o);
            for (var a = o.firstElementChild; null != a; )
              -1 === t.indexOf(a) && e.push(a), (a = a.nextElementSibling);
          }
        }
        return e;
      }),
      (e.getRefType = function (t) {
        if (null != t && "object" == (0, r.default)(t))
          return t instanceof Element ? s : f;
        return null;
      }),
      (e.getClosestElement = void 0);
    var i = n(10),
      o = n(2),
      a = i.IX2BrowserSupport.ELEMENT_MATCHES,
      u = o.IX2EngineConstants,
      c = u.IX2_ID_DELIMITER,
      s = u.HTML_ELEMENT,
      f = u.PLAIN_OBJECT,
      l = u.WF_PAGE;
    var d = Element.prototype.closest
      ? function (t, e) {
          return document.documentElement.contains(t) ? t.closest(e) : null;
        }
      : function (t, e) {
          if (!document.documentElement.contains(t)) return null;
          var n = t;
          do {
            if (n[a] && n[a](e)) return n;
            n = n.parentNode;
          } while (null != n);
          return null;
        };
    e.getClosestElement = d;
  },
  function (t, e, n) {
    "use strict";
    var r,
      i = n(0),
      o = i(n(26)),
      a = i(n(21)),
      u = n(0);
    Object.defineProperty(e, "__esModule", { value: !0 }), (e.default = void 0);
    var c,
      s,
      f,
      l = u(n(27)),
      d = u(n(293)),
      p = u(n(34)),
      v = u(n(312)),
      h = n(2),
      E = n(114),
      _ = n(61),
      g = n(10),
      y = h.EventTypeConsts,
      I = y.MOUSE_CLICK,
      m = y.MOUSE_SECOND_CLICK,
      T = y.MOUSE_DOWN,
      O = y.MOUSE_UP,
      b = y.MOUSE_OVER,
      A = y.MOUSE_OUT,
      S = y.DROPDOWN_CLOSE,
      w = y.DROPDOWN_OPEN,
      R = y.SLIDER_ACTIVE,
      x = y.SLIDER_INACTIVE,
      N = y.TAB_ACTIVE,
      C = y.TAB_INACTIVE,
      L = y.NAVBAR_CLOSE,
      D = y.NAVBAR_OPEN,
      P = y.MOUSE_MOVE,
      M = y.PAGE_SCROLL_DOWN,
      j = y.SCROLL_INTO_VIEW,
      F = y.SCROLL_OUT_OF_VIEW,
      k = y.PAGE_SCROLL_UP,
      X = y.SCROLLING_IN_VIEW,
      G = y.PAGE_FINISH,
      V = y.ECOMMERCE_CART_CLOSE,
      U = y.ECOMMERCE_CART_OPEN,
      W = y.PAGE_START,
      B = y.PAGE_SCROLL,
      H = "COMPONENT_ACTIVE",
      z = "COMPONENT_INACTIVE",
      Y = h.IX2EngineConstants.COLON_DELIMITER,
      K = g.IX2VanillaUtils.getNamespacedParameterId,
      Q = function (t) {
        return function (e) {
          return !("object" !== (0, a.default)(e) || !t(e)) || e;
        };
      },
      q = Q(function (t) {
        return t.element === t.nativeEvent.target;
      }),
      $ = Q(function (t) {
        var e = t.element,
          n = t.nativeEvent;
        return e.contains(n.target);
      }),
      Z = (0, d.default)([q, $]),
      J = function (t, e) {
        if (e) {
          var n = t.getState().ixData.events[e];
          if (n && !at[n.eventTypeId]) return n;
        }
        return null;
      },
      tt = function (t, e) {
        var n = t.store,
          r = t.event,
          i = t.element,
          o = t.eventStateKey,
          a = r.action,
          u = r.id,
          c = a.config,
          s = c.actionListId,
          f = c.autoStopEventId,
          l = J(n, f);
        return (
          l &&
            (0, E.stopActionGroup)({
              store: n,
              eventId: f,
              eventTarget: i,
              eventStateKey: f + Y + o.split(Y)[1],
              actionListId: (0, p.default)(l, "action.config.actionListId"),
            }),
          (0, E.stopActionGroup)({
            store: n,
            eventId: u,
            eventTarget: i,
            eventStateKey: o,
            actionListId: s,
          }),
          (0, E.startActionGroup)({
            store: n,
            eventId: u,
            eventTarget: i,
            eventStateKey: o,
            actionListId: s,
          }),
          e
        );
      },
      et = function (t, e) {
        return function (n, r) {
          return !0 === t(n, r) ? e(n, r) : r;
        };
      },
      nt = { handler: et(Z, tt) },
      rt = (0, l.default)({}, nt, { types: [H, z].join(" ") }),
      it = [
        { target: window, types: "resize orientationchange", throttle: !0 },
        {
          target: document,
          types: "scroll wheel readystatechange IX2_PAGE_UPDATE",
          throttle: !0,
        },
      ],
      ot = { types: it },
      at = { PAGE_START: W, PAGE_FINISH: G },
      ut =
        ((c = void 0 !== window.pageXOffset),
        (s =
          "CSS1Compat" === document.compatMode
            ? document.documentElement
            : document.body),
        function () {
          return {
            scrollLeft: c ? window.pageXOffset : s.scrollLeft,
            scrollTop: c ? window.pageYOffset : s.scrollTop,
            stiffScrollTop: (0, v.default)(
              c ? window.pageYOffset : s.scrollTop,
              0,
              s.scrollHeight - window.innerHeight
            ),
            scrollWidth: s.scrollWidth,
            scrollHeight: s.scrollHeight,
            clientWidth: s.clientWidth,
            clientHeight: s.clientHeight,
            innerWidth: window.innerWidth,
            innerHeight: window.innerHeight,
          };
        }),
      ct = function (t) {
        var e = t.element,
          n = t.nativeEvent,
          r = n.type,
          i = n.target,
          o = n.relatedTarget,
          a = e.contains(i);
        if ("mouseover" === r && a) return !0;
        var u = e.contains(o);
        return !("mouseout" !== r || !a || !u);
      },
      st = function (t) {
        var e,
          n,
          r = t.element,
          i = t.event.config,
          o = ut(),
          a = o.clientWidth,
          u = o.clientHeight,
          c = i.scrollOffsetValue,
          s = "PX" === i.scrollOffsetUnit ? c : (u * (c || 0)) / 100;
        return (
          (e = r.getBoundingClientRect()),
          (n = { left: 0, top: s, right: a, bottom: u - s }),
          !(
            e.left > n.right ||
            e.right < n.left ||
            e.top > n.bottom ||
            e.bottom < n.top
          )
        );
      },
      ft = function (t) {
        return function (e, n) {
          var r = e.nativeEvent.type,
            i = -1 !== [H, z].indexOf(r) ? r === H : n.isActive,
            o = (0, l.default)({}, n, { isActive: i });
          return n && o.isActive === n.isActive ? o : t(e, o) || o;
        };
      },
      lt = function (t) {
        return function (e, n) {
          var r = { elementHovered: ct(e) };
          return (
            ((n ? r.elementHovered !== n.elementHovered : r.elementHovered) &&
              t(e, r)) ||
            r
          );
        };
      },
      dt = function (t) {
        return function (e) {
          var n =
              arguments.length > 1 && void 0 !== arguments[1]
                ? arguments[1]
                : {},
            r = ut(),
            i = r.stiffScrollTop,
            o = r.scrollHeight,
            a = r.innerHeight,
            u = e.event,
            c = u.config,
            s = u.eventTypeId,
            f = c.scrollOffsetValue,
            d = "PX" === c.scrollOffsetUnit,
            p = o - a,
            v = Number((i / p).toFixed(2));
          if (n && n.percentTop === v) return n;
          var h,
            E,
            _ = (d ? f : (a * (f || 0)) / 100) / p,
            g = 0;
          n &&
            ((h = v > n.percentTop),
            (g = (E = n.scrollingDown !== h) ? v : n.anchorTop));
          var y = s === M ? v >= g + _ : v <= g - _,
            I = (0, l.default)({}, n, {
              percentTop: v,
              inBounds: y,
              anchorTop: g,
              scrollingDown: h,
            });
          return (n && y && (E || I.inBounds !== n.inBounds) && t(e, I)) || I;
        };
      },
      pt = function (t) {
        return function (e) {
          var n =
              arguments.length > 1 && void 0 !== arguments[1]
                ? arguments[1]
                : { clickCount: 0 },
            r = { clickCount: (n.clickCount % 2) + 1 };
          return (r.clickCount !== n.clickCount && t(e, r)) || r;
        };
      },
      vt = function () {
        var t =
          !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0];
        return (0, l.default)({}, rt, {
          handler: et(
            t ? Z : q,
            ft(function (t, e) {
              return e.isActive ? nt.handler(t, e) : e;
            })
          ),
        });
      },
      ht = function () {
        var t =
          !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0];
        return (0, l.default)({}, rt, {
          handler: et(
            t ? Z : q,
            ft(function (t, e) {
              return e.isActive ? e : nt.handler(t, e);
            })
          ),
        });
      },
      Et = (0, l.default)({}, ot, {
        handler:
          ((f = function (t, e) {
            var n = e.elementVisible,
              r = t.event;
            return !t.store.getState().ixData.events[
              r.action.config.autoStopEventId
            ] && e.triggered
              ? e
              : (r.eventTypeId === j) === n
              ? (tt(t), (0, l.default)({}, e, { triggered: !0 }))
              : e;
          }),
          function (t, e) {
            var n = (0, l.default)({}, e, { elementVisible: st(t) });
            return (
              ((e ? n.elementVisible !== e.elementVisible : n.elementVisible) &&
                f(t, n)) ||
              n
            );
          }),
      }),
      _t =
        ((r = {}),
        (0, o.default)(r, R, vt()),
        (0, o.default)(r, x, ht()),
        (0, o.default)(r, w, vt()),
        (0, o.default)(r, S, ht()),
        (0, o.default)(r, D, vt(!1)),
        (0, o.default)(r, L, ht(!1)),
        (0, o.default)(r, N, vt()),
        (0, o.default)(r, C, ht()),
        (0, o.default)(r, U, {
          types: "ecommerce-cart-open",
          handler: et(Z, tt),
        }),
        (0, o.default)(r, V, {
          types: "ecommerce-cart-close",
          handler: et(Z, tt),
        }),
        (0, o.default)(r, I, {
          types: "click",
          handler: et(
            Z,
            pt(function (t, e) {
              var n,
                r,
                i,
                o = e.clickCount;
              (r = (n = t).store),
                (i = n.event.action.config.autoStopEventId),
                Boolean(J(r, i)) ? 1 === o && tt(t) : tt(t);
            })
          ),
        }),
        (0, o.default)(r, m, {
          types: "click",
          handler: et(
            Z,
            pt(function (t, e) {
              2 === e.clickCount && tt(t);
            })
          ),
        }),
        (0, o.default)(r, T, (0, l.default)({}, nt, { types: "mousedown" })),
        (0, o.default)(r, O, (0, l.default)({}, nt, { types: "mouseup" })),
        (0, o.default)(r, b, {
          types: "mouseover mouseout",
          handler: et(
            Z,
            lt(function (t, e) {
              e.elementHovered && tt(t);
            })
          ),
        }),
        (0, o.default)(r, A, {
          types: "mouseover mouseout",
          handler: et(
            Z,
            lt(function (t, e) {
              e.elementHovered || tt(t);
            })
          ),
        }),
        (0, o.default)(r, P, {
          types: "mousemove mouseout scroll",
          handler: function (t) {
            var e = t.store,
              n = t.element,
              r = t.eventConfig,
              i = t.nativeEvent,
              o = t.eventStateKey,
              a =
                arguments.length > 1 && void 0 !== arguments[1]
                  ? arguments[1]
                  : { clientX: 0, clientY: 0, pageX: 0, pageY: 0 },
              u = r.basedOn,
              c = r.selectedAxis,
              s = r.continuousParameterGroupId,
              f = r.reverse,
              l = r.restingState,
              d = void 0 === l ? 0 : l,
              p = i.clientX,
              v = void 0 === p ? a.clientX : p,
              E = i.clientY,
              g = void 0 === E ? a.clientY : E,
              y = i.pageX,
              I = void 0 === y ? a.pageX : y,
              m = i.pageY,
              T = void 0 === m ? a.pageY : m,
              O = "X_AXIS" === c,
              b = "mouseout" === i.type,
              A = d / 100,
              S = s,
              w = !1;
            switch (u) {
              case h.EventBasedOn.VIEWPORT:
                A = O
                  ? Math.min(v, window.innerWidth) / window.innerWidth
                  : Math.min(g, window.innerHeight) / window.innerHeight;
                break;
              case h.EventBasedOn.PAGE:
                var R = ut(),
                  x = R.scrollLeft,
                  N = R.scrollTop,
                  C = R.scrollWidth,
                  L = R.scrollHeight;
                A = O ? Math.min(x + I, C) / C : Math.min(N + T, L) / L;
                break;
              case h.EventBasedOn.ELEMENT:
              default:
                S = K(o, s);
                var D = 0 === i.type.indexOf("mouse");
                if (D && !0 !== Z({ element: n, nativeEvent: i })) break;
                var P = n.getBoundingClientRect(),
                  M = P.left,
                  j = P.top,
                  F = P.width,
                  k = P.height;
                if (
                  !D &&
                  !(function (t, e) {
                    return (
                      t.left > e.left &&
                      t.left < e.right &&
                      t.top > e.top &&
                      t.top < e.bottom
                    );
                  })({ left: v, top: g }, P)
                )
                  break;
                (w = !0), (A = O ? (v - M) / F : (g - j) / k);
            }
            return (
              b && (A > 0.95 || A < 0.05) && (A = Math.round(A)),
              (u !== h.EventBasedOn.ELEMENT || w || w !== a.elementHovered) &&
                ((A = f ? 1 - A : A),
                e.dispatch((0, _.parameterChanged)(S, A))),
              { elementHovered: w, clientX: v, clientY: g, pageX: I, pageY: T }
            );
          },
        }),
        (0, o.default)(r, B, {
          types: it,
          handler: function (t) {
            var e = t.store,
              n = t.eventConfig,
              r = n.continuousParameterGroupId,
              i = n.reverse,
              o = ut(),
              a = o.scrollTop / (o.scrollHeight - o.clientHeight);
            (a = i ? 1 - a : a), e.dispatch((0, _.parameterChanged)(r, a));
          },
        }),
        (0, o.default)(r, X, {
          types: it,
          handler: function (t) {
            var e = t.element,
              n = t.store,
              r = t.eventConfig,
              i = t.eventStateKey,
              o =
                arguments.length > 1 && void 0 !== arguments[1]
                  ? arguments[1]
                  : { scrollPercent: 0 },
              a = ut(),
              u = a.scrollLeft,
              c = a.scrollTop,
              s = a.scrollWidth,
              f = a.scrollHeight,
              l = a.clientHeight,
              d = r.basedOn,
              p = r.selectedAxis,
              v = r.continuousParameterGroupId,
              E = r.startsEntering,
              g = r.startsExiting,
              y = r.addEndOffset,
              I = r.addStartOffset,
              m = r.addOffsetValue,
              T = void 0 === m ? 0 : m,
              O = r.endOffsetValue,
              b = void 0 === O ? 0 : O,
              A = "X_AXIS" === p;
            if (d === h.EventBasedOn.VIEWPORT) {
              var S = A ? u / s : c / f;
              return (
                S !== o.scrollPercent &&
                  n.dispatch((0, _.parameterChanged)(v, S)),
                { scrollPercent: S }
              );
            }
            var w = K(i, v),
              R = e.getBoundingClientRect(),
              x = (I ? T : 0) / 100,
              N = (y ? b : 0) / 100;
            (x = E ? x : 1 - x), (N = g ? N : 1 - N);
            var C = R.top + Math.min(R.height * x, l),
              L = R.top + R.height * N - C,
              D = Math.min(l + L, f),
              P = Math.min(Math.max(0, l - C), D) / D;
            return (
              P !== o.scrollPercent &&
                n.dispatch((0, _.parameterChanged)(w, P)),
              { scrollPercent: P }
            );
          },
        }),
        (0, o.default)(r, j, Et),
        (0, o.default)(r, F, Et),
        (0, o.default)(
          r,
          M,
          (0, l.default)({}, ot, {
            handler: dt(function (t, e) {
              e.scrollingDown && tt(t);
            }),
          })
        ),
        (0, o.default)(
          r,
          k,
          (0, l.default)({}, ot, {
            handler: dt(function (t, e) {
              e.scrollingDown || tt(t);
            }),
          })
        ),
        (0, o.default)(r, G, {
          types: "readystatechange IX2_PAGE_UPDATE",
          handler: et(
            q,
            (function (t) {
              return function (e, n) {
                var r = { finished: "complete" === document.readyState };
                return !r.finished || (n && n.finshed) || t(e), r;
              };
            })(tt)
          ),
        }),
        (0, o.default)(r, W, {
          types: "readystatechange IX2_PAGE_UPDATE",
          handler: et(
            q,
            (function (t) {
              return function (e, n) {
                return n || t(e), { started: !0 };
              };
            })(tt)
          ),
        }),
        r);
    e.default = _t;
  },
  function (t, e, n) {
    var r = n(294)();
    t.exports = r;
  },
  function (t, e, n) {
    var r = n(62),
      i = n(295),
      o = n(118),
      a = n(119),
      u = n(1),
      c = n(308),
      s = "Expected a function",
      f = 8,
      l = 32,
      d = 128,
      p = 256;
    t.exports = function (t) {
      return i(function (e) {
        var n = e.length,
          i = n,
          v = r.prototype.thru;
        for (t && e.reverse(); i--; ) {
          var h = e[i];
          if ("function" != typeof h) throw new TypeError(s);
          if (v && !E && "wrapper" == a(h)) var E = new r([], !0);
        }
        for (i = E ? i : n; ++i < n; ) {
          h = e[i];
          var _ = a(h),
            g = "wrapper" == _ ? o(h) : void 0;
          E =
            g && c(g[0]) && g[1] == (d | f | l | p) && !g[4].length && 1 == g[9]
              ? E[a(g[0])].apply(E, g[3])
              : 1 == h.length && c(h)
              ? E[_]()
              : E.thru(h);
        }
        return function () {
          var t = arguments,
            r = t[0];
          if (E && 1 == t.length && u(r)) return E.plant(r).value();
          for (var i = 0, o = n ? e[i].apply(this, t) : r; ++i < n; )
            o = e[i].call(this, o);
          return o;
        };
      });
    };
  },
  function (t, e, n) {
    var r = n(296),
      i = n(299),
      o = n(301);
    t.exports = function (t) {
      return o(i(t, void 0, r), t + "");
    };
  },
  function (t, e, n) {
    var r = n(297);
    t.exports = function (t) {
      return null != t && t.length ? r(t, 1) : [];
    };
  },
  function (t, e, n) {
    var r = n(49),
      i = n(298);
    t.exports = function t(e, n, o, a, u) {
      var c = -1,
        s = e.length;
      for (o || (o = i), u || (u = []); ++c < s; ) {
        var f = e[c];
        n > 0 && o(f)
          ? n > 1
            ? t(f, n - 1, o, a, u)
            : r(u, f)
          : a || (u[u.length] = f);
      }
      return u;
    };
  },
  function (t, e, n) {
    var r = n(19),
      i = n(33),
      o = n(1),
      a = r ? r.isConcatSpreadable : void 0;
    t.exports = function (t) {
      return o(t) || i(t) || !!(a && t && t[a]);
    };
  },
  function (t, e, n) {
    var r = n(300),
      i = Math.max;
    t.exports = function (t, e, n) {
      return (
        (e = i(void 0 === e ? t.length - 1 : e, 0)),
        function () {
          for (
            var o = arguments, a = -1, u = i(o.length - e, 0), c = Array(u);
            ++a < u;

          )
            c[a] = o[e + a];
          a = -1;
          for (var s = Array(e + 1); ++a < e; ) s[a] = o[a];
          return (s[e] = n(c)), r(t, this, s);
        }
      );
    };
  },
  function (t, e) {
    t.exports = function (t, e, n) {
      switch (n.length) {
        case 0:
          return t.call(e);
        case 1:
          return t.call(e, n[0]);
        case 2:
          return t.call(e, n[0], n[1]);
        case 3:
          return t.call(e, n[0], n[1], n[2]);
      }
      return t.apply(e, n);
    };
  },
  function (t, e, n) {
    var r = n(302),
      i = n(304)(r);
    t.exports = i;
  },
  function (t, e, n) {
    var r = n(303),
      i = n(116),
      o = n(59),
      a = i
        ? function (t, e) {
            return i(t, "toString", {
              configurable: !0,
              enumerable: !1,
              value: r(e),
              writable: !0,
            });
          }
        : o;
    t.exports = a;
  },
  function (t, e) {
    t.exports = function (t) {
      return function () {
        return t;
      };
    };
  },
  function (t, e) {
    var n = 800,
      r = 16,
      i = Date.now;
    t.exports = function (t) {
      var e = 0,
        o = 0;
      return function () {
        var a = i(),
          u = r - (a - o);
        if (((o = a), u > 0)) {
          if (++e >= n) return arguments[0];
        } else e = 0;
        return t.apply(void 0, arguments);
      };
    };
  },
  function (t, e, n) {
    var r = n(99),
      i = r && new r();
    t.exports = i;
  },
  function (t, e) {
    t.exports = function () {};
  },
  function (t, e) {
    t.exports = {};
  },
  function (t, e, n) {
    var r = n(64),
      i = n(118),
      o = n(119),
      a = n(309);
    t.exports = function (t) {
      var e = o(t),
        n = a[e];
      if ("function" != typeof n || !(e in r.prototype)) return !1;
      if (t === n) return !0;
      var u = i(n);
      return !!u && t === u[0];
    };
  },
  function (t, e, n) {
    var r = n(64),
      i = n(62),
      o = n(63),
      a = n(1),
      u = n(9),
      c = n(310),
      s = Object.prototype.hasOwnProperty;
    function f(t) {
      if (u(t) && !a(t) && !(t instanceof r)) {
        if (t instanceof i) return t;
        if (s.call(t, "__wrapped__")) return c(t);
      }
      return new i(t);
    }
    (f.prototype = o.prototype), (f.prototype.constructor = f), (t.exports = f);
  },
  function (t, e, n) {
    var r = n(64),
      i = n(62),
      o = n(311);
    t.exports = function (t) {
      if (t instanceof r) return t.clone();
      var e = new i(t.__wrapped__, t.__chain__);
      return (
        (e.__actions__ = o(t.__actions__)),
        (e.__index__ = t.__index__),
        (e.__values__ = t.__values__),
        e
      );
    };
  },
  function (t, e) {
    t.exports = function (t, e) {
      var n = -1,
        r = t.length;
      for (e || (e = Array(r)); ++n < r; ) e[n] = t[n];
      return e;
    };
  },
  function (t, e, n) {
    var r = n(313),
      i = n(60);
    t.exports = function (t, e, n) {
      return (
        void 0 === n && ((n = e), (e = void 0)),
        void 0 !== n && (n = (n = i(n)) == n ? n : 0),
        void 0 !== e && (e = (e = i(e)) == e ? e : 0),
        r(i(t), e, n)
      );
    };
  },
  function (t, e) {
    t.exports = function (t, e, n) {
      return (
        t == t &&
          (void 0 !== n && (t = t <= n ? t : n),
          void 0 !== e && (t = t >= e ? t : e)),
        t
      );
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(6);
    r.define(
      "links",
      (t.exports = function (t, e) {
        var n,
          i,
          o,
          a = {},
          u = t(window),
          c = r.env(),
          s = window.location,
          f = document.createElement("a"),
          l = "w--current",
          d = /index\.(html|php)$/,
          p = /\/$/;
        function v(e) {
          var r =
            (n && e.getAttribute("href-disabled")) || e.getAttribute("href");
          if (((f.href = r), !(r.indexOf(":") >= 0))) {
            var a = t(e);
            if (
              f.hash.length > 1 &&
              f.host + f.pathname === s.host + s.pathname
            ) {
              if (!/^#[a-zA-Z0-9\-\_]+$/.test(f.hash)) return;
              var u = t(f.hash);
              u.length && i.push({ link: a, sec: u, active: !1 });
            } else if ("#" !== r && "" !== r) {
              var c = f.href === s.href || r === o || (d.test(r) && p.test(o));
              E(a, l, c);
            }
          }
        }
        function h() {
          var t = u.scrollTop(),
            n = u.height();
          e.each(i, function (e) {
            var r = e.link,
              i = e.sec,
              o = i.offset().top,
              a = i.outerHeight(),
              u = 0.5 * n,
              c = i.is(":visible") && o + a - u >= t && o + u <= t + n;
            e.active !== c && ((e.active = c), E(r, l, c));
          });
        }
        function E(t, e, n) {
          var r = t.hasClass(e);
          (n && r) || ((n || r) && (n ? t.addClass(e) : t.removeClass(e)));
        }
        return (
          (a.ready = a.design = a.preview = function () {
            (n = c && r.env("design")),
              (o = r.env("slug") || s.pathname || ""),
              r.scroll.off(h),
              (i = []);
            for (var t = document.links, e = 0; e < t.length; ++e) v(t[e]);
            i.length && (r.scroll.on(h), h());
          }),
          a
        );
      })
    );
  },
  function (t, e, n) {
    "use strict";
    var r = n(6);
    r.define(
      "scroll",
      (t.exports = function (t) {
        var e,
          n = {
            CLICK_EMPTY: "click.wf-empty-link",
            CLICK_SCROLL: "click.wf-scroll",
          },
          i = t(document),
          o = window,
          a = o.location,
          u = (function () {
            try {
              return Boolean(o.frameElement);
            } catch (t) {
              return !0;
            }
          })()
            ? null
            : o.history,
          c = /^[a-zA-Z0-9][\w:.-]*$/,
          s = 'a[href="#"]',
          f = 'a[href*="#"]:not(.w-tab-link):not(' + s + ")";
        function l(n) {
          if (
            !(
              r.env("design") ||
              (window.$.mobile && t(n.currentTarget).hasClass("ui-link"))
            )
          ) {
            var i = this.href.split("#"),
              s = i[0] === e ? i[1] : null;
            s &&
              (function (e, n) {
                if (!c.test(e)) return;
                var i = t("#" + e);
                if (!i.length) return;
                n && (n.preventDefault(), n.stopPropagation());
                if (
                  a.hash !== e &&
                  u &&
                  u.pushState &&
                  (!r.env.chrome || "file:" !== a.protocol)
                ) {
                  var s = u.state && u.state.hash;
                  s !== e && u.pushState({ hash: e }, "", "#" + e);
                }
                var f = r.env("editor") ? ".w-editor-body" : "body",
                  l = t(
                    "header, " +
                      f +
                      " > .header, " +
                      f +
                      " > .w-nav:not([data-no-scroll])"
                  ),
                  d = "fixed" === l.css("position") ? l.outerHeight() : 0;
                o.setTimeout(
                  function () {
                    !(function (e, n) {
                      var r = t(o).scrollTop(),
                        i = e.offset().top - n;
                      if ("mid" === e.data("scroll")) {
                        var a = t(o).height() - n,
                          u = e.outerHeight();
                        u < a && (i -= Math.round((a - u) / 2));
                      }
                      var c = 1;
                      t("body")
                        .add(e)
                        .each(function () {
                          var e = parseFloat(
                            t(this).attr("data-scroll-time"),
                            10
                          );
                          !isNaN(e) && (0 === e || e > 0) && (c = e);
                        }),
                        Date.now ||
                          (Date.now = function () {
                            return new Date().getTime();
                          });
                      var s = Date.now(),
                        f =
                          o.requestAnimationFrame ||
                          o.mozRequestAnimationFrame ||
                          o.webkitRequestAnimationFrame ||
                          function (t) {
                            o.setTimeout(t, 15);
                          },
                        l =
                          (472.143 * Math.log(Math.abs(r - i) + 125) - 2e3) * c;
                      !(function t() {
                        var e = Date.now() - s;
                        o.scroll(
                          0,
                          (function (t, e, n, r) {
                            if (n > r) return e;
                            return (
                              t +
                              (e - t) *
                                ((i = n / r),
                                i < 0.5
                                  ? 4 * i * i * i
                                  : (i - 1) * (2 * i - 2) * (2 * i - 2) + 1)
                            );
                            var i;
                          })(r, i, e, l)
                        ),
                          e <= l && f(t);
                      })();
                    })(i, d);
                  },
                  n ? 0 : 300
                );
              })(s, n);
          }
        }
        return {
          ready: function () {
            var t = n.CLICK_EMPTY,
              r = n.CLICK_SCROLL;
            (e = a.href.split("#")[0]),
              i.on(r, f, l),
              i.on(t, s, function (t) {
                t.preventDefault();
              });
          },
        };
      })
    );
  },
  function (t, e, n) {
    "use strict";
    n(6).define(
      "touch",
      (t.exports = function (t) {
        var e = {},
          n = window.getSelection;
        function r(e) {
          var r,
            i,
            o = !1,
            a = !1,
            u = Math.min(Math.round(0.04 * window.innerWidth), 40);
          function c(t) {
            var e = t.touches;
            (e && e.length > 1) ||
              ((o = !0),
              e ? ((a = !0), (r = e[0].clientX)) : (r = t.clientX),
              (i = r));
          }
          function s(e) {
            if (o) {
              if (a && "mousemove" === e.type)
                return e.preventDefault(), void e.stopPropagation();
              var r = e.touches,
                c = r ? r[0].clientX : e.clientX,
                s = c - i;
              (i = c),
                Math.abs(s) > u &&
                  n &&
                  "" === String(n()) &&
                  (!(function (e, n, r) {
                    var i = t.Event(e, { originalEvent: n });
                    t(n.target).trigger(i, r);
                  })("swipe", e, { direction: s > 0 ? "right" : "left" }),
                  l());
            }
          }
          function f(t) {
            if (o)
              return (
                (o = !1),
                a && "mouseup" === t.type
                  ? (t.preventDefault(), t.stopPropagation(), void (a = !1))
                  : void 0
              );
          }
          function l() {
            o = !1;
          }
          e.addEventListener("touchstart", c, !1),
            e.addEventListener("touchmove", s, !1),
            e.addEventListener("touchend", f, !1),
            e.addEventListener("touchcancel", l, !1),
            e.addEventListener("mousedown", c, !1),
            e.addEventListener("mousemove", s, !1),
            e.addEventListener("mouseup", f, !1),
            e.addEventListener("mouseout", l, !1),
            (this.destroy = function () {
              e.removeEventListener("touchstart", c, !1),
                e.removeEventListener("touchmove", s, !1),
                e.removeEventListener("touchend", f, !1),
                e.removeEventListener("touchcancel", l, !1),
                e.removeEventListener("mousedown", c, !1),
                e.removeEventListener("mousemove", s, !1),
                e.removeEventListener("mouseup", f, !1),
                e.removeEventListener("mouseout", l, !1),
                (e = null);
            });
        }
        return (
          (t.event.special.tap = { bindType: "click", delegateType: "click" }),
          (e.init = function (e) {
            return (e = "string" == typeof e ? t(e).get(0) : e)
              ? new r(e)
              : null;
          }),
          (e.instance = e.init(document)),
          e
        );
      })
    );
  },
  function (t, e, n) {
    "use strict";
    var r = n(6),
      i = n(37),
      o = {
        ARROW_LEFT: 37,
        ARROW_UP: 38,
        ARROW_RIGHT: 39,
        ARROW_DOWN: 40,
        ESCAPE: 27,
        SPACE: 32,
        ENTER: 13,
        HOME: 36,
        END: 35,
      };
    r.define(
      "navbar",
      (t.exports = function (t, e) {
        var n,
          a,
          u,
          c,
          s = {},
          f = t.tram,
          l = t(window),
          d = t(document),
          p = e.debounce,
          v = r.env(),
          h = '<div class="w-nav-overlay" data-wf-ignore />',
          E = ".w-nav",
          _ = "w--open",
          g = "w--nav-dropdown-open",
          y = "w--nav-dropdown-toggle-open",
          I = "w--nav-dropdown-list-open",
          m = "w--nav-link-open",
          T = i.triggers,
          O = t();
        function b() {
          r.resize.off(A);
        }
        function A() {
          a.each(M);
        }
        function S(n, r) {
          var i = t(r),
            a = t.data(r, E);
          a ||
            (a = t.data(r, E, {
              open: !1,
              el: i,
              config: {},
              selectedIdx: -1,
            })),
            (a.menu = i.find(".w-nav-menu")),
            (a.links = a.menu.find(".w-nav-link")),
            (a.dropdowns = a.menu.find(".w-dropdown")),
            (a.dropdownToggle = a.menu.find(".w-dropdown-toggle")),
            (a.dropdownList = a.menu.find(".w-dropdown-list")),
            (a.button = i.find(".w-nav-button")),
            (a.container = i.find(".w-container")),
            (a.overlayContainerId = "w-nav-overlay-" + n),
            (a.outside = (function (e) {
              e.outside && d.off("click" + E, e.outside);
              return function (n) {
                var r = t(n.target);
                (c && r.closest(".w-editor-bem-EditorOverlay").length) ||
                  P(e, r);
              };
            })(a));
          var s = i.find(".w-nav-brand");
          s &&
            "/" === s.attr("href") &&
            null == s.attr("aria-label") &&
            s.attr("aria-label", "home"),
            a.button.attr("style", "-webkit-user-select: text;"),
            null == a.button.attr("aria-label") &&
              a.button.attr("aria-label", "menu"),
            a.button.attr("role", "button"),
            a.button.attr("tabindex", "0"),
            a.button.attr("aria-controls", a.overlayContainerId),
            a.button.attr("aria-haspopup", "menu"),
            a.button.attr("aria-expanded", "false"),
            a.el.off(E),
            a.button.off(E),
            a.menu.off(E),
            x(a),
            u
              ? (R(a),
                a.el.on(
                  "setting" + E,
                  (function (t) {
                    return function (n, r) {
                      r = r || {};
                      var i = l.width();
                      x(t),
                        !0 === r.open && X(t, !0),
                        !1 === r.open && V(t, !0),
                        t.open &&
                          e.defer(function () {
                            i !== l.width() && C(t);
                          });
                    };
                  })(a)
                ))
              : (!(function (e) {
                  if (e.overlay) return;
                  (e.overlay = t(h).appendTo(e.el)),
                    e.overlay.attr("id", e.overlayContainerId),
                    (e.parent = e.menu.parent()),
                    V(e, !0);
                })(a),
                a.button.on("click" + E, L(a)),
                a.menu.on("click" + E, "a", D(a)),
                a.button.on(
                  "keydown" + E,
                  (function (t) {
                    return function (e) {
                      switch (e.keyCode) {
                        case o.SPACE:
                        case o.ENTER:
                          return (
                            L(t)(), e.preventDefault(), e.stopPropagation()
                          );
                        case o.ESCAPE:
                          return V(t), e.preventDefault(), e.stopPropagation();
                        case o.ARROW_RIGHT:
                        case o.ARROW_DOWN:
                        case o.HOME:
                        case o.END:
                          return t.open
                            ? (e.keyCode === o.END
                                ? (t.selectedIdx = t.links.length - 1)
                                : (t.selectedIdx = 0),
                              N(t),
                              e.preventDefault(),
                              e.stopPropagation())
                            : (e.preventDefault(), e.stopPropagation());
                      }
                    };
                  })(a)
                ),
                a.el.on(
                  "keydown" + E,
                  (function (t) {
                    return function (e) {
                      if (t.open)
                        switch (
                          ((t.selectedIdx = t.links.index(
                            document.activeElement
                          )),
                          e.keyCode)
                        ) {
                          case o.HOME:
                          case o.END:
                            return (
                              e.keyCode === o.END
                                ? (t.selectedIdx = t.links.length - 1)
                                : (t.selectedIdx = 0),
                              N(t),
                              e.preventDefault(),
                              e.stopPropagation()
                            );
                          case o.ESCAPE:
                            return (
                              V(t),
                              t.button.focus(),
                              e.preventDefault(),
                              e.stopPropagation()
                            );
                          case o.ARROW_LEFT:
                          case o.ARROW_UP:
                            return (
                              (t.selectedIdx = Math.max(-1, t.selectedIdx - 1)),
                              N(t),
                              e.preventDefault(),
                              e.stopPropagation()
                            );
                          case o.ARROW_RIGHT:
                          case o.ARROW_DOWN:
                            return (
                              (t.selectedIdx = Math.min(
                                t.links.length - 1,
                                t.selectedIdx + 1
                              )),
                              N(t),
                              e.preventDefault(),
                              e.stopPropagation()
                            );
                        }
                    };
                  })(a)
                )),
            M(n, r);
        }
        function w(e, n) {
          var r = t.data(n, E);
          r && (R(r), t.removeData(n, E));
        }
        function R(t) {
          t.overlay && (V(t, !0), t.overlay.remove(), (t.overlay = null));
        }
        function x(t) {
          var n = {},
            r = t.config || {},
            i = (n.animation = t.el.attr("data-animation") || "default");
          (n.animOver = /^over/.test(i)),
            (n.animDirect = /left$/.test(i) ? -1 : 1),
            r.animation !== i && t.open && e.defer(C, t),
            (n.easing = t.el.attr("data-easing") || "ease"),
            (n.easing2 = t.el.attr("data-easing2") || "ease");
          var o = t.el.attr("data-duration");
          (n.duration = null != o ? Number(o) : 400),
            (n.docHeight = t.el.attr("data-doc-height")),
            (t.config = n);
        }
        function N(t) {
          if (t.links[t.selectedIdx]) {
            var e = t.links[t.selectedIdx];
            e.focus(), D(e);
          }
        }
        function C(t) {
          t.open && (V(t, !0), X(t, !0));
        }
        function L(t) {
          return p(function () {
            t.open ? V(t) : X(t);
          });
        }
        function D(e) {
          return function (n) {
            var i = t(this).attr("href");
            r.validClick(n.currentTarget)
              ? i && 0 === i.indexOf("#") && e.open && V(e)
              : n.preventDefault();
          };
        }
        (s.ready = s.design = s.preview = function () {
          if (
            ((u = v && r.env("design")),
            (c = r.env("editor")),
            (n = t(document.body)),
            !(a = d.find(E)).length)
          )
            return;
          a.each(S), b(), r.resize.on(A);
        }),
          (s.destroy = function () {
            (O = t()), b(), a && a.length && a.each(w);
          });
        var P = p(function (t, e) {
          if (t.open) {
            var n = e.closest(".w-nav-menu");
            t.menu.is(n) || V(t);
          }
        });
        function M(e, n) {
          var r = t.data(n, E),
            i = (r.collapsed = "none" !== r.button.css("display"));
          if ((!r.open || i || u || V(r, !0), r.container.length)) {
            var o = (function (e) {
              var n = e.container.css(j);
              "none" === n && (n = "");
              return function (e, r) {
                (r = t(r)).css(j, ""), "none" === r.css(j) && r.css(j, n);
              };
            })(r);
            r.links.each(o), r.dropdowns.each(o);
          }
          r.open && G(r);
        }
        var j = "max-width";
        function F(t, e) {
          e.setAttribute("data-nav-menu-open", "");
        }
        function k(t, e) {
          e.removeAttribute("data-nav-menu-open");
        }
        function X(t, e) {
          if (!t.open) {
            (t.open = !0),
              t.menu.each(F),
              t.links.addClass(m),
              t.dropdowns.addClass(g),
              t.dropdownToggle.addClass(y),
              t.dropdownList.addClass(I),
              t.button.addClass(_);
            var n = t.config;
            ("none" !== n.animation && f.support.transform) || (e = !0);
            var i = G(t),
              o = t.menu.outerHeight(!0),
              a = t.menu.outerWidth(!0),
              c = t.el.height(),
              s = t.el[0];
            if (
              (M(0, s),
              T.intro(0, s),
              r.redraw.up(),
              u || d.on("click" + E, t.outside),
              e)
            )
              v();
            else {
              var l = "transform " + n.duration + "ms " + n.easing;
              if (
                (t.overlay &&
                  ((O = t.menu.prev()), t.overlay.show().append(t.menu)),
                n.animOver)
              )
                return (
                  f(t.menu)
                    .add(l)
                    .set({ x: n.animDirect * a, height: i })
                    .start({ x: 0 })
                    .then(v),
                  void (t.overlay && t.overlay.width(a))
                );
              var p = c + o;
              f(t.menu).add(l).set({ y: -p }).start({ y: 0 }).then(v);
            }
          }
          function v() {
            t.button.attr("aria-expanded", "true");
          }
        }
        function G(t) {
          var e = t.config,
            r = e.docHeight ? d.height() : n.height();
          return (
            e.animOver
              ? t.menu.height(r)
              : "fixed" !== t.el.css("position") && (r -= t.el.outerHeight(!0)),
            t.overlay && t.overlay.height(r),
            r
          );
        }
        function V(t, e) {
          if (t.open) {
            (t.open = !1), t.button.removeClass(_);
            var n = t.config;
            if (
              (("none" === n.animation ||
                !f.support.transform ||
                n.duration <= 0) &&
                (e = !0),
              T.outro(0, t.el[0]),
              d.off("click" + E, t.outside),
              e)
            )
              return f(t.menu).stop(), void c();
            var r = "transform " + n.duration + "ms " + n.easing2,
              i = t.menu.outerHeight(!0),
              o = t.menu.outerWidth(!0),
              a = t.el.height();
            if (n.animOver)
              f(t.menu)
                .add(r)
                .start({ x: o * n.animDirect })
                .then(c);
            else {
              var u = a + i;
              f(t.menu).add(r).start({ y: -u }).then(c);
            }
          }
          function c() {
            t.menu.height(""),
              f(t.menu).set({ x: 0, y: 0 }),
              t.menu.each(k),
              t.links.removeClass(m),
              t.dropdowns.removeClass(g),
              t.dropdownToggle.removeClass(y),
              t.dropdownList.removeClass(I),
              t.overlay &&
                t.overlay.children().length &&
                (O.length ? t.menu.insertAfter(O) : t.menu.prependTo(t.parent),
                t.overlay.attr("style", "").hide()),
              t.el.triggerHandler("w-close"),
              t.button.attr("aria-expanded", "false");
          }
        }
        return s;
      })
    );
  },
  function (t, e, n) {
    "use strict";
    var r = n(6),
      i = n(37),
      o = {
        ARROW_LEFT: 37,
        ARROW_UP: 38,
        ARROW_RIGHT: 39,
        ARROW_DOWN: 40,
        SPACE: 32,
        ENTER: 13,
        HOME: 36,
        END: 35,
      },
      a =
        'a[href], area[href], [role="button"], input, select, textarea, button, iframe, object, embed, *[tabindex], *[contenteditable]';
    r.define(
      "slider",
      (t.exports = function (t, e) {
        var n,
          u,
          c,
          s,
          f = {},
          l = t.tram,
          d = t(document),
          p = r.env(),
          v = ".w-slider",
          h = '<div class="w-slider-dot" data-wf-ignore />',
          E =
            '<div aria-live="off" aria-atomic="true" class="w-slider-aria-label" data-wf-ignore />',
          _ = i.triggers;
        function g() {
          (n = d.find(v)).length &&
            (n.each(m),
            (s = null),
            c || (y(), r.resize.on(I), r.redraw.on(f.redraw)));
        }
        function y() {
          r.resize.off(I), r.redraw.off(f.redraw);
        }
        function I() {
          n.filter(":visible").each(D);
        }
        function m(e, n) {
          var r = t(n),
            i = t.data(n, v);
          i ||
            (i = t.data(n, v, {
              index: 0,
              depth: 1,
              hasFocus: { keyboard: !1, mouse: !1 },
              el: r,
              config: {},
            })),
            (i.mask = r.children(".w-slider-mask")),
            (i.left = r.children(".w-slider-arrow-left")),
            (i.right = r.children(".w-slider-arrow-right")),
            (i.nav = r.children(".w-slider-nav")),
            (i.slides = i.mask.children(".w-slide")),
            i.slides.each(_.reset),
            s && (i.maskWidth = 0),
            void 0 === r.attr("role") && r.attr("role", "section"),
            void 0 === r.attr("aria-label") && r.attr("aria-label", "carousel");
          var o = i.mask.attr("id");
          if (
            (o || ((o = "w-slider-mask-" + e), i.mask.attr("id", o)),
            (i.ariaLiveLabel = t(E).appendTo(i.mask)),
            i.left.attr("role", "button"),
            i.left.attr("tabindex", "0"),
            i.left.attr("aria-controls", o),
            void 0 === i.left.attr("aria-label") &&
              i.left.attr("aria-label", "previous slide"),
            i.right.attr("role", "button"),
            i.right.attr("tabindex", "0"),
            i.right.attr("aria-controls", o),
            void 0 === i.right.attr("aria-label") &&
              i.right.attr("aria-label", "next slide"),
            !l.support.transform)
          )
            return i.left.hide(), i.right.hide(), i.nav.hide(), void (c = !0);
          i.el.off(v),
            i.left.off(v),
            i.right.off(v),
            i.nav.off(v),
            T(i),
            u
              ? (i.el.on("setting" + v, N(i)), x(i), (i.hasTimer = !1))
              : (i.el.on("swipe" + v, N(i)),
                i.left.on("click" + v, S(i)),
                i.right.on("click" + v, w(i)),
                i.left.on("keydown" + v, A(i, S)),
                i.right.on("keydown" + v, A(i, w)),
                i.nav.on("keydown" + v, "> div", N(i)),
                i.config.autoplay &&
                  !i.hasTimer &&
                  ((i.hasTimer = !0), (i.timerCount = 1), R(i)),
                i.el.on("mouseenter" + v, b(i, !0, "mouse")),
                i.el.on("focusin" + v, b(i, !0, "keyboard")),
                i.el.on("mouseleave" + v, b(i, !1, "mouse")),
                i.el.on("focusout" + v, b(i, !1, "keyboard"))),
            i.nav.on("click" + v, "> div", N(i)),
            p ||
              i.mask
                .contents()
                .filter(function () {
                  return 3 === this.nodeType;
                })
                .remove();
          var a = r.filter(":hidden");
          a.show();
          var f = r.parents(":hidden");
          f.show(), D(e, n), a.css("display", ""), f.css("display", "");
        }
        function T(t) {
          var e = { crossOver: 0 };
          (e.animation = t.el.attr("data-animation") || "slide"),
            "outin" === e.animation &&
              ((e.animation = "cross"), (e.crossOver = 0.5)),
            (e.easing = t.el.attr("data-easing") || "ease");
          var n = t.el.attr("data-duration");
          if (
            ((e.duration = null != n ? parseInt(n, 10) : 500),
            O(t.el.attr("data-infinite")) && (e.infinite = !0),
            O(t.el.attr("data-disable-swipe")) && (e.disableSwipe = !0),
            O(t.el.attr("data-hide-arrows"))
              ? (e.hideArrows = !0)
              : t.config.hideArrows && (t.left.show(), t.right.show()),
            O(t.el.attr("data-autoplay")))
          ) {
            (e.autoplay = !0),
              (e.delay = parseInt(t.el.attr("data-delay"), 10) || 2e3),
              (e.timerMax = parseInt(t.el.attr("data-autoplay-limit"), 10));
            var r = "mousedown" + v + " touchstart" + v;
            u ||
              t.el.off(r).one(r, function () {
                x(t);
              });
          }
          var i = t.right.width();
          (e.edge = i ? i + 40 : 100), (t.config = e);
        }
        function O(t) {
          return "1" === t || "true" === t;
        }
        function b(e, n, r) {
          return function (i) {
            if (n) e.hasFocus[r] = n;
            else {
              if (t.contains(e.el.get(0), i.relatedTarget)) return;
              if (
                ((e.hasFocus[r] = n),
                (e.hasFocus.mouse && "keyboard" === r) ||
                  (e.hasFocus.keyboard && "mouse" === r))
              )
                return;
            }
            n
              ? (e.ariaLiveLabel.attr("aria-live", "polite"),
                e.hasTimer && x(e))
              : (e.ariaLiveLabel.attr("aria-live", "off"), e.hasTimer && R(e));
          };
        }
        function A(t, e) {
          return function (n) {
            switch (n.keyCode) {
              case o.SPACE:
              case o.ENTER:
                return e(t)(), n.preventDefault(), n.stopPropagation();
            }
          };
        }
        function S(t) {
          return function () {
            L(t, { index: t.index - 1, vector: -1 });
          };
        }
        function w(t) {
          return function () {
            L(t, { index: t.index + 1, vector: 1 });
          };
        }
        function R(t) {
          x(t);
          var e = t.config,
            n = e.timerMax;
          (n && t.timerCount++ > n) ||
            (t.timerId = window.setTimeout(function () {
              null == t.timerId || u || (w(t)(), R(t));
            }, e.delay));
        }
        function x(t) {
          window.clearTimeout(t.timerId), (t.timerId = null);
        }
        function N(n) {
          return function (i, a) {
            a = a || {};
            var c = n.config;
            if (u && "setting" === i.type) {
              if ("prev" === a.select) return S(n)();
              if ("next" === a.select) return w(n)();
              if ((T(n), P(n), null == a.select)) return;
              !(function (n, r) {
                var i = null;
                r === n.slides.length && (g(), P(n)),
                  e.each(n.anchors, function (e, n) {
                    t(e.els).each(function (e, o) {
                      t(o).index() === r && (i = n);
                    });
                  }),
                  null != i && L(n, { index: i, immediate: !0 });
              })(n, a.select);
            } else {
              if ("swipe" === i.type) {
                if (c.disableSwipe) return;
                if (r.env("editor")) return;
                return "left" === a.direction
                  ? w(n)()
                  : "right" === a.direction
                  ? S(n)()
                  : void 0;
              }
              if (n.nav.has(i.target).length) {
                var s = t(i.target).index();
                if (
                  ("click" === i.type && L(n, { index: s }),
                  "keydown" === i.type)
                )
                  switch (i.keyCode) {
                    case o.ENTER:
                    case o.SPACE:
                      L(n, { index: s }), i.preventDefault();
                      break;
                    case o.ARROW_LEFT:
                    case o.ARROW_UP:
                      C(n.nav, Math.max(s - 1, 0)), i.preventDefault();
                      break;
                    case o.ARROW_RIGHT:
                    case o.ARROW_DOWN:
                      C(n.nav, Math.min(s + 1, n.pages)), i.preventDefault();
                      break;
                    case o.HOME:
                      C(n.nav, 0), i.preventDefault();
                      break;
                    case o.END:
                      C(n.nav, n.pages), i.preventDefault();
                      break;
                    default:
                      return;
                  }
              }
            }
          };
        }
        function C(t, e) {
          var n = t.children().eq(e).focus();
          t.children().not(n);
        }
        function L(e, n) {
          n = n || {};
          var r = e.config,
            i = e.anchors;
          e.previous = e.index;
          var o = n.index,
            c = {};
          o < 0
            ? ((o = i.length - 1),
              r.infinite &&
                ((c.x = -e.endX), (c.from = 0), (c.to = i[0].width)))
            : o >= i.length &&
              ((o = 0),
              r.infinite &&
                ((c.x = i[i.length - 1].width),
                (c.from = -i[i.length - 1].x),
                (c.to = c.from - c.x))),
            (e.index = o);
          var f = e.nav
            .children()
            .eq(o)
            .addClass("w-active")
            .attr("aria-selected", "true")
            .attr("tabindex", "0");
          e.nav
            .children()
            .not(f)
            .removeClass("w-active")
            .attr("aria-selected", "false")
            .attr("tabindex", "-1"),
            r.hideArrows &&
              (e.index === i.length - 1 ? e.right.hide() : e.right.show(),
              0 === e.index ? e.left.hide() : e.left.show());
          var d = e.offsetX || 0,
            p = (e.offsetX = -i[e.index].x),
            v = { x: p, opacity: 1, visibility: "" },
            h = t(i[e.index].els),
            E = t(i[e.previous] && i[e.previous].els),
            g = e.slides.not(h),
            y = r.animation,
            I = r.easing,
            m = Math.round(r.duration),
            T = n.vector || (e.index > e.previous ? 1 : -1),
            O = "opacity " + m + "ms " + I,
            b = "transform " + m + "ms " + I;
          if (
            (h.find(a).removeAttr("tabindex"),
            h.removeAttr("aria-hidden"),
            h.find("*").removeAttr("aria-hidden"),
            g.find(a).attr("tabindex", "-1"),
            g.attr("aria-hidden", "true"),
            g.find("*").attr("aria-hidden", "true"),
            u || (h.each(_.intro), g.each(_.outro)),
            n.immediate && !s)
          )
            return l(h).set(v), void w();
          if (e.index !== e.previous) {
            if (
              (e.ariaLiveLabel.text(
                "Slide ".concat(o + 1, " of ").concat(i.length, ".")
              ),
              "cross" === y)
            ) {
              var A = Math.round(m - m * r.crossOver),
                S = Math.round(m - A);
              return (
                (O = "opacity " + A + "ms " + I),
                l(E).set({ visibility: "" }).add(O).start({ opacity: 0 }),
                void l(h)
                  .set({ visibility: "", x: p, opacity: 0, zIndex: e.depth++ })
                  .add(O)
                  .wait(S)
                  .then({ opacity: 1 })
                  .then(w)
              );
            }
            if ("fade" === y)
              return (
                l(E).set({ visibility: "" }).stop(),
                void l(h)
                  .set({ visibility: "", x: p, opacity: 0, zIndex: e.depth++ })
                  .add(O)
                  .start({ opacity: 1 })
                  .then(w)
              );
            if ("over" === y)
              return (
                (v = { x: e.endX }),
                l(E).set({ visibility: "" }).stop(),
                void l(h)
                  .set({
                    visibility: "",
                    zIndex: e.depth++,
                    x: p + i[e.index].width * T,
                  })
                  .add(b)
                  .start({ x: p })
                  .then(w)
              );
            r.infinite && c.x
              ? (l(e.slides.not(E))
                  .set({ visibility: "", x: c.x })
                  .add(b)
                  .start({ x: p }),
                l(E)
                  .set({ visibility: "", x: c.from })
                  .add(b)
                  .start({ x: c.to }),
                (e.shifted = E))
              : (r.infinite &&
                  e.shifted &&
                  (l(e.shifted).set({ visibility: "", x: d }),
                  (e.shifted = null)),
                l(e.slides).set({ visibility: "" }).add(b).start({ x: p }));
          }
          function w() {
            (h = t(i[e.index].els)),
              (g = e.slides.not(h)),
              "slide" !== y && (v.visibility = "hidden"),
              l(g).set(v);
          }
        }
        function D(e, n) {
          var r = t.data(n, v);
          if (r)
            return (function (t) {
              var e = t.mask.width();
              if (t.maskWidth !== e) return (t.maskWidth = e), !0;
              return !1;
            })(r)
              ? P(r)
              : void (
                  u &&
                  (function (e) {
                    var n = 0;
                    if (
                      (e.slides.each(function (e, r) {
                        n += t(r).outerWidth(!0);
                      }),
                      e.slidesWidth !== n)
                    )
                      return (e.slidesWidth = n), !0;
                    return !1;
                  })(r) &&
                  P(r)
                );
        }
        function P(e) {
          var n = 1,
            r = 0,
            i = 0,
            o = 0,
            a = e.maskWidth,
            c = a - e.config.edge;
          c < 0 && (c = 0),
            (e.anchors = [{ els: [], x: 0, width: 0 }]),
            e.slides.each(function (u, s) {
              i - r > c &&
                (n++,
                (r += a),
                (e.anchors[n - 1] = { els: [], x: i, width: 0 })),
                (o = t(s).outerWidth(!0)),
                (i += o),
                (e.anchors[n - 1].width += o),
                e.anchors[n - 1].els.push(s);
              var f = u + 1 + " of " + e.slides.length;
              t(s).attr("aria-label", f), t(s).attr("role", "group");
            }),
            (e.endX = i),
            u && (e.pages = null),
            e.nav.length &&
              e.pages !== n &&
              ((e.pages = n),
              (function (e) {
                var n,
                  r = [],
                  i = e.el.attr("data-nav-spacing");
                i && (i = parseFloat(i) + "px");
                for (var o = 0, a = e.pages; o < a; o++)
                  (n = t(h))
                    .attr("aria-label", "Show slide " + (o + 1) + " of " + a)
                    .attr("aria-selected", "false")
                    .attr("role", "button")
                    .attr("tabindex", "-1"),
                    e.nav.hasClass("w-num") && n.text(o + 1),
                    null != i && n.css({ "margin-left": i, "margin-right": i }),
                    r.push(n);
                e.nav.empty().append(r);
              })(e));
          var s = e.index;
          s >= n && (s = n - 1), L(e, { immediate: !0, index: s });
        }
        return (
          (f.ready = function () {
            (u = r.env("design")), g();
          }),
          (f.design = function () {
            (u = !0), g();
          }),
          (f.preview = function () {
            (u = !1), g();
          }),
          (f.redraw = function () {
            (s = !0), g();
          }),
          (f.destroy = y),
          f
        );
      })
    );
  },
]);
/**
 * ----------------------------------------------------------------------
 * Webflow: Interactions 2.0: Init
 */
Webflow.require("ix2").init({
  events: {
    e: {
      id: "e",
      eventTypeId: "SCROLLING_IN_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_CONTINUOUS_ACTION",
        config: { actionListId: "a", affectedElements: {}, duration: 0 },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|6b02bf63-f110-dd70-5bed-1407907c6a22",
      },
      config: [
        {
          continuousParameterGroupId: "a-p",
          smoothing: 10,
          startsEntering: true,
          addStartOffset: false,
          addOffsetValue: 10,
          startsExiting: false,
          addEndOffset: false,
          endOffsetValue: 10,
        },
      ],
      createdOn: 1597948587589,
    },
    "e-3": {
      id: "e-3",
      eventTypeId: "MOUSE_OVER",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-3",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-4",
        },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|c60c5777-a3f3-f495-56c4-b12ef18caad8",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598195619357,
    },
    "e-4": {
      id: "e-4",
      eventTypeId: "MOUSE_OUT",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-4",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-3",
        },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|c60c5777-a3f3-f495-56c4-b12ef18caad8",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598195619358,
    },
    "e-5": {
      id: "e-5",
      eventTypeId: "MOUSE_OVER",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-5",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-6",
        },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|21591a31-fa34-22c7-ca0c-81b0ef462ca0",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598195832578,
    },
    "e-6": {
      id: "e-6",
      eventTypeId: "MOUSE_OUT",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-6",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-5",
        },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|21591a31-fa34-22c7-ca0c-81b0ef462ca0",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598195832621,
    },
    "e-7": {
      id: "e-7",
      eventTypeId: "MOUSE_OVER",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-7",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-8",
        },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|7540778a-1753-5c98-1041-fa9aa21fe104",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598195936803,
    },
    "e-8": {
      id: "e-8",
      eventTypeId: "MOUSE_OUT",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-8",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-7",
        },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|7540778a-1753-5c98-1041-fa9aa21fe104",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598195936847,
    },
    "e-9": {
      id: "e-9",
      eventTypeId: "MOUSE_OVER",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-9",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-10",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|21a17dc5-2121-c101-708c-0d7e00844050",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598366763654,
    },
    "e-10": {
      id: "e-10",
      eventTypeId: "MOUSE_OUT",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-10",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-9",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|21a17dc5-2121-c101-708c-0d7e00844050",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598366763655,
    },
    "e-11": {
      id: "e-11",
      eventTypeId: "MOUSE_OVER",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-11",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-12",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|05efb5b9-b9b0-0033-c024-2180b052f931",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598367313437,
    },
    "e-12": {
      id: "e-12",
      eventTypeId: "MOUSE_OUT",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-12",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-11",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|05efb5b9-b9b0-0033-c024-2180b052f931",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598367313439,
    },
    "e-13": {
      id: "e-13",
      eventTypeId: "MOUSE_OVER",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-13",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-14",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|186de04d-b9be-22d2-9498-df7962af4271",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598367496719,
    },
    "e-14": {
      id: "e-14",
      eventTypeId: "MOUSE_OUT",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-14",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-13",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|186de04d-b9be-22d2-9498-df7962af4271",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598367496763,
    },
    "e-15": {
      id: "e-15",
      eventTypeId: "MOUSE_OVER",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-15",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-16",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|a6766a6f-cf69-61d9-80f6-2fdbd0757c40",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598367688951,
    },
    "e-16": {
      id: "e-16",
      eventTypeId: "MOUSE_OUT",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-16",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-15",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|a6766a6f-cf69-61d9-80f6-2fdbd0757c40",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598367688953,
    },
    "e-17": {
      id: "e-17",
      eventTypeId: "MOUSE_OVER",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-17",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-18",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|97535f43-08fa-82da-1ada-3b0598c4deee",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598367798295,
    },
    "e-18": {
      id: "e-18",
      eventTypeId: "MOUSE_OUT",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-18",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-17",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|97535f43-08fa-82da-1ada-3b0598c4deee",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598367798341,
    },
    "e-19": {
      id: "e-19",
      eventTypeId: "SCROLLING_IN_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_CONTINUOUS_ACTION",
        config: { actionListId: "a-21", affectedElements: {}, duration: 0 },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f454028ec18356275c3236f|6b02bf63-f110-dd70-5bed-1407907c6a22",
      },
      config: [
        {
          continuousParameterGroupId: "a-21-p",
          smoothing: 10,
          startsEntering: true,
          addStartOffset: false,
          addOffsetValue: 10,
          startsExiting: false,
          addEndOffset: false,
          endOffsetValue: 10,
        },
      ],
      createdOn: 1598373928486,
    },
    "e-36": {
      id: "e-36",
      eventTypeId: "NAVBAR_OPEN",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-19",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-37",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f454028ec18356275c3236f|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598374740426,
    },
    "e-37": {
      id: "e-37",
      eventTypeId: "NAVBAR_CLOSE",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-20",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-36",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f454028ec18356275c3236f|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598374740428,
    },
    "e-38": {
      id: "e-38",
      eventTypeId: "NAVBAR_OPEN",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-19",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-39",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598374835624,
    },
    "e-39": {
      id: "e-39",
      eventTypeId: "NAVBAR_CLOSE",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-20",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-38",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598374835667,
    },
    "e-40": {
      id: "e-40",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-22",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-41",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f454028ec18356275c3236f|94740de6-41e6-9c6e-8d21-65422f9979d5",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598379545616,
    },
    "e-42": {
      id: "e-42",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-23",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-43",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f454028ec18356275c3236f|ea4a6f41-b6fb-72c6-8e44-8bf183fbb950",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598379590589,
    },
    "e-44": {
      id: "e-44",
      eventTypeId: "SCROLLING_IN_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_CONTINUOUS_ACTION",
        config: { actionListId: "a-31", affectedElements: {}, duration: 0 },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|6b02bf63-f110-dd70-5bed-1407907c6a22",
      },
      config: [
        {
          continuousParameterGroupId: "a-31-p",
          smoothing: 10,
          startsEntering: true,
          addStartOffset: false,
          addOffsetValue: 10,
          startsExiting: false,
          addEndOffset: false,
          endOffsetValue: 10,
        },
      ],
      createdOn: 1598381338888,
    },
    "e-45": {
      id: "e-45",
      eventTypeId: "NAVBAR_OPEN",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-19",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-46",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598381338888,
    },
    "e-46": {
      id: "e-46",
      eventTypeId: "NAVBAR_CLOSE",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-20",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-45",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598381338888,
    },
    "e-47": {
      id: "e-47",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-22",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-48",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|94740de6-41e6-9c6e-8d21-65422f9979d5",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598381338888,
    },
    "e-49": {
      id: "e-49",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-23",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-50",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|ea4a6f41-b6fb-72c6-8e44-8bf183fbb950",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598381338888,
    },
    "e-51": {
      id: "e-51",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-25",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-52",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|a91dd1c0-16ec-26d6-17cd-ddf5ecd29c35",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598462828261,
    },
    "e-53": {
      id: "e-53",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-26",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-54",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|41c0dc13-41ff-1071-6923-0dfc40deb827",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598462841374,
    },
    "e-55": {
      id: "e-55",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-27",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-56",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|bd2182ae-3c44-c750-ad49-d89097d0db70",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598463269625,
    },
    "e-57": {
      id: "e-57",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-28",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-58",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|ad652829-e0a2-ea10-0f4b-c53836948191",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598463526190,
    },
    "e-59": {
      id: "e-59",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-29",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-60",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|5d424190-ca73-a483-0cbb-d24a551f9fb2",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598465314315,
    },
    "e-61": {
      id: "e-61",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-30",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-62",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|2e08fd53-5c87-bf50-c560-eb49109f0b2a",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598465328713,
    },
    "e-63": {
      id: "e-63",
      eventTypeId: "SCROLLING_IN_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_CONTINUOUS_ACTION",
        config: { actionListId: "a-36", affectedElements: {}, duration: 0 },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|6b02bf63-f110-dd70-5bed-1407907c6a22",
      },
      config: [
        {
          continuousParameterGroupId: "a-36-p",
          smoothing: 10,
          startsEntering: true,
          addStartOffset: false,
          addOffsetValue: 10,
          startsExiting: false,
          addEndOffset: false,
          endOffsetValue: 10,
        },
      ],
      createdOn: 1598468913915,
    },
    "e-64": {
      id: "e-64",
      eventTypeId: "NAVBAR_OPEN",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-19",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-65",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598468913915,
    },
    "e-65": {
      id: "e-65",
      eventTypeId: "NAVBAR_CLOSE",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-20",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-64",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598468913915,
    },
    "e-68": {
      id: "e-68",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-34",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-69",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|ea4a6f41-b6fb-72c6-8e44-8bf183fbb950",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598468913915,
    },
    "e-70": {
      id: "e-70",
      eventTypeId: "SCROLLING_IN_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_CONTINUOUS_ACTION",
        config: { actionListId: "a-37", affectedElements: {}, duration: 0 },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|6b02bf63-f110-dd70-5bed-1407907c6a22",
      },
      config: [
        {
          continuousParameterGroupId: "a-37-p",
          smoothing: 10,
          startsEntering: true,
          addStartOffset: false,
          addOffsetValue: 10,
          startsExiting: false,
          addEndOffset: false,
          endOffsetValue: 10,
        },
      ],
      createdOn: 1598469578688,
    },
    "e-71": {
      id: "e-71",
      eventTypeId: "NAVBAR_OPEN",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-19",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-72",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598469578688,
    },
    "e-72": {
      id: "e-72",
      eventTypeId: "NAVBAR_CLOSE",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-20",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-71",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598469578688,
    },
    "e-73": {
      id: "e-73",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-23",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-74",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|ea4a6f41-b6fb-72c6-8e44-8bf183fbb950",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598469578688,
    },
    "e-75": {
      id: "e-75",
      eventTypeId: "SCROLLING_IN_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_CONTINUOUS_ACTION",
        config: { actionListId: "a-38", affectedElements: {}, duration: 0 },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|6b02bf63-f110-dd70-5bed-1407907c6a22",
      },
      config: [
        {
          continuousParameterGroupId: "a-38-p",
          smoothing: 10,
          startsEntering: true,
          addStartOffset: false,
          addOffsetValue: 10,
          startsExiting: false,
          addEndOffset: false,
          endOffsetValue: 10,
        },
      ],
      createdOn: 1598469898073,
    },
    "e-76": {
      id: "e-76",
      eventTypeId: "NAVBAR_OPEN",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-19",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-77",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598469898073,
    },
    "e-77": {
      id: "e-77",
      eventTypeId: "NAVBAR_CLOSE",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-20",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-76",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598469898073,
    },
    "e-78": {
      id: "e-78",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-23",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-79",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|ea4a6f41-b6fb-72c6-8e44-8bf183fbb950",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598469898073,
    },
    "e-80": {
      id: "e-80",
      eventTypeId: "SCROLLING_IN_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_CONTINUOUS_ACTION",
        config: { actionListId: "a-39", affectedElements: {}, duration: 0 },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b85886346710a41aa683|6b02bf63-f110-dd70-5bed-1407907c6a22",
      },
      config: [
        {
          continuousParameterGroupId: "a-39-p",
          smoothing: 10,
          startsEntering: true,
          addStartOffset: false,
          addOffsetValue: 10,
          startsExiting: false,
          addEndOffset: false,
          endOffsetValue: 10,
        },
      ],
      createdOn: 1598470232875,
    },
    "e-81": {
      id: "e-81",
      eventTypeId: "NAVBAR_OPEN",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-19",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-82",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b85886346710a41aa683|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598470232875,
    },
    "e-82": {
      id: "e-82",
      eventTypeId: "NAVBAR_CLOSE",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-20",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-81",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b85886346710a41aa683|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598470232875,
    },
    "e-83": {
      id: "e-83",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-23",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-84",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b85886346710a41aa683|ea4a6f41-b6fb-72c6-8e44-8bf183fbb950",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598470232875,
    },
    "e-85": {
      id: "e-85",
      eventTypeId: "SCROLLING_IN_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_CONTINUOUS_ACTION",
        config: { actionListId: "a-40", affectedElements: {}, duration: 0 },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46ba5ccd37d182ce8d671a|6b02bf63-f110-dd70-5bed-1407907c6a22",
      },
      config: [
        {
          continuousParameterGroupId: "a-40-p",
          smoothing: 10,
          startsEntering: true,
          addStartOffset: false,
          addOffsetValue: 10,
          startsExiting: false,
          addEndOffset: false,
          endOffsetValue: 10,
        },
      ],
      createdOn: 1598470749529,
    },
    "e-86": {
      id: "e-86",
      eventTypeId: "NAVBAR_OPEN",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-19",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-87",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46ba5ccd37d182ce8d671a|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598470749529,
    },
    "e-87": {
      id: "e-87",
      eventTypeId: "NAVBAR_CLOSE",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-20",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-86",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46ba5ccd37d182ce8d671a|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598470749529,
    },
    "e-88": {
      id: "e-88",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-23",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-89",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46ba5ccd37d182ce8d671a|ea4a6f41-b6fb-72c6-8e44-8bf183fbb950",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598470749529,
    },
    "e-90": {
      id: "e-90",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-32",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-91",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|b66dd577-63b9-ca57-8bab-15124aabeff4",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598471723330,
    },
    "e-1000": {
      id: "e-1000",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-1000",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-91",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|715a421b-a39a-6c58-9b11-692ae2812fe3",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598471723330,
    },
    "e-92": {
      id: "e-92",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-33",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-93",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|25ce379d-3849-52b5-c618-cc879cc45047",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598472003684,
    },
    "e-94": {
      id: "e-94",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-35",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-95",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|95ad57c4-ab81-d4ab-5ed1-36ca21ae4f78",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598472070219,
    },
    "e-96": {
      id: "e-96",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-32",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-97",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|715a421b-a39a-6c58-9b11-692ae2812fe3",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598542513825,
    },
    "e-98": {
      id: "e-98",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-32",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-99",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|dac04843-184d-8ea2-de0a-a2ae13f3bccd",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598542574966,
    },
    "e-100": {
      id: "e-100",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-32",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-101",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|d16912bd-e210-de57-09cb-e5e247512dc9",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598543400784,
    },
    "e-102": {
      id: "e-102",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-32",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-103",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|d16912bd-e210-de57-09cb-e5e247512dce",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598543400784,
    },
    "e-104": {
      id: "e-104",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-32",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-105",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|f9643c87-38ea-1304-5312-9173a851b52a",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598543775501,
    },
    "e-106": {
      id: "e-106",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-32",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-107",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|f9643c87-38ea-1304-5312-9173a851b52f",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598543775501,
    },
    "e-108": {
      id: "e-108",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-32",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-109",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|d9c24b4b-97b3-9241-2c4c-e10790ddbb5e",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598544116844,
    },
    "e-110": {
      id: "e-110",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-32",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-111",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|d9c24b4b-97b3-9241-2c4c-e10790ddbb63",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598544116844,
    },
    "e-112": {
      id: "e-112",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-32",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-113",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|7fbd83f5-606f-de4b-3feb-3520ee77f957",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598544284030,
    },
    "e-114": {
      id: "e-114",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-32",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-115",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|9ad377f3-3581-1bee-9eb5-00c617c707a0",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598547002313,
    },
    "e-116": {
      id: "e-116",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-32",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-117",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|9ad377f3-3581-1bee-9eb5-00c617c707a5",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598547002313,
    },
    "e-118": {
      id: "e-118",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-32",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-119",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|6116cda0-4ece-c025-8bc8-6e29aa039eea",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598547374998,
    },
    "e-120": {
      id: "e-120",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-32",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-121",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|d6f2fa5b-7158-b39d-8649-cf50b8c6b2d5",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598547624131,
    },
    "e-122": {
      id: "e-122",
      eventTypeId: "MOUSE_CLICK",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-32",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-123",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|2763ca7f-5537-c3e2-7863-09bd99ecdb70",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: null,
        scrollOffsetUnit: null,
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598547742177,
    },
    "e-124": {
      id: "e-124",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-41",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-125",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|fee8ad35-a5c7-a156-cb58-feaaf405fcc3",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598955816238,
    },
    "e-126": {
      id: "e-126",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-42",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-127",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598956367364,
    },
    "e-128": {
      id: "e-128",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-43",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-129",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598956549473,
    },
    "e-130": {
      id: "e-130",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-44",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-131",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|b4c46764-d55b-b7af-ab25-b8d8c8d6c8fc",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 30,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598956726320,
    },
    "e-132": {
      id: "e-132",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-45",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-133",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|d369717a-ca48-9d3f-f114-e2c2ff3d0a0b",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598958058531,
    },
    "e-134": {
      id: "e-134",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-46",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-135",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|f8663848-147d-4b26-1c0c-8a779d58b361",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598960604478,
    },

    "e-137": {
      id: "e-137",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-47",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-137",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|39353d10-b99f-d516-e4be-a5c514168f0f",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598960604478,
    },

    "e-136": {
      id: "e-136",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-47",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-137",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|39353d10-b99f-d516-e4be-a5c514168f0f",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598960926355,
    },
    "e-138": {
      id: "e-138",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-48",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-139",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|f87066ed-51e2-81a6-b43f-167fd01e6a70",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598961003859,
    },
    "e-140": {
      id: "e-140",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-49",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-141",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|f69e6d44-452f-7300-ac2e-145a6e3eb7ea",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598974717757,
    },
    "e-142": {
      id: "e-142",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-50",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-143",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|c168d2c6-a8e5-8e66-a1a6-0438fd8e7e75",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598974813337,
    },
    "e-144": {
      id: "e-144",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-51",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-145",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|21591a31-fa34-22c7-ca0c-81b0ef462ca1",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598974913914,
    },
    "e-146": {
      id: "e-146",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-52",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-147",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|7540778a-1753-5c98-1041-fa9aa21fe105",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598974958185,
    },
    "e-148": {
      id: "e-148",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-53",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-149",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|2a2fbcd2-319f-789b-c264-512c90ddc5e5",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598975065224,
    },
    "e-150": {
      id: "e-150",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-54",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-151",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|37b982b4-26f7-688c-e820-dbeb5d391ee1",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598975141536,
    },
    "e-152": {
      id: "e-152",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-55",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-153",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|bc69c8a5-252b-8c1e-71a3-acfb60e227d3",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598975212860,
    },
    "e-154": {
      id: "e-154",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-56",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-155",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|ae199eb8-0274-e629-411d-ef289d237de7",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598975433186,
    },
    "e-156": {
      id: "e-156",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-57",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-157",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|b7d11c88-d2ae-1d95-426a-361fbc11fb36",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598975521026,
    },
    "e-158": {
      id: "e-158",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-58",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-159",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|7c938c1c-de81-cd66-5209-0da248dafae1",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598975845414,
    },
    "e-160": {
      id: "e-160",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-59",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-161",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|da6c3378-577a-29c3-9eb1-bb16429758c0",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598975966010,
    },
    "e-162": {
      id: "e-162",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-60",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-163",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        selector: ".home-block-10-graph-box",
        originalId:
          "5f3954afac18ff01d8edd53d|da6c3378-577a-29c3-9eb1-bb16429758c5",
        appliesTo: "CLASS",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598976057409,
    },
    "e-164": {
      id: "e-164",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-61",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-165",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|ff38a2c9-51f5-1f15-2687-b0ed6c379e2b",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598976344027,
    },
    "e-166": {
      id: "e-166",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-62",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-167",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|534f9084-e5ae-3f63-a3b5-ac3f3fe10d5e",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598976427761,
    },
    "e-168": {
      id: "e-168",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-63",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-169",
        },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f3954afac18ff01d8edd53d|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598977190319,
    },
    "e-170": {
      id: "e-170",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-63",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-171",
        },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f454028ec18356275c3236f|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598977424762,
    },
    "e-172": {
      id: "e-172",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-64",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-173",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f454028ec18356275c3236f|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598977609851,
    },
    "e-174": {
      id: "e-174",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-65",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-175",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f454028ec18356275c3236f|61475a67-e752-7b96-1dbf-193108a9fd5a",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598977887277,
    },
    "e-176": {
      id: "e-176",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-66",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-177",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598977965918,
    },
    "e-178": {
      id: "e-178",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-67",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-179",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598978181368,
    },
    "e-180": {
      id: "e-180",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-68",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-181",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598978258973,
    },
    "e-182": {
      id: "e-182",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-69",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-183",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f454028ec18356275c3236f|0ff3f361-0890-77fb-d63d-3edc89ea76ce",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598978326720,
    },
    "e-184": {
      id: "e-184",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-70",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-185",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f454028ec18356275c3236f|0ff3f361-0890-77fb-d63d-3edc89ea76d1",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598978348472,
    },
    "e-186": {
      id: "e-186",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-71",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-187",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f454028ec18356275c3236f|eccafbe0-9f22-4229-4ffc-26fd3929ed2d",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598978447446,
    },
    "e-188": {
      id: "e-188",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-72",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-189",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598978547198,
    },
    "e-190": {
      id: "e-190",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-73",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-191",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598978727207,
    },
    "e-192": {
      id: "e-192",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-74",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-193",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9fb",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598978820682,
    },
    "e-194": {
      id: "e-194",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-75",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-195",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f454028ec18356275c3236f|af0ac780-ce4e-2477-c183-2ac8c8d5b750",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598978851358,
    },
    "e-196": {
      id: "e-196",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-76",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-197",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f454028ec18356275c3236f|7b48fd05-e568-a016-5824-444882ae1d7c",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598978912428,
    },
    "e-198": {
      id: "e-198",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-77",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-199",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f454028ec18356275c3236f|b150d122-c027-2317-f64a-e389bd2f0e12",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598979043927,
    },
    "e-200": {
      id: "e-200",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-63",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-201",
        },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598979251008,
    },
    "e-202": {
      id: "e-202",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-78",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-203",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598980293563,
    },
    "e-204": {
      id: "e-204",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-79",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-205",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|61475a67-e752-7b96-1dbf-193108a9fd5a",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598980329822,
    },
    "e-206": {
      id: "e-206",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-80",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-207",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|09801874-dc01-623c-9418-90ae9fcc05f0",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598980380749,
    },
    "e-208": {
      id: "e-208",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-81",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-209",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|f4be39ce-a2df-c388-0bc5-161caa04b2b5",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598980466684,
    },
    "e-210": {
      id: "e-210",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-82",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-211",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598980514591,
    },
    "e-212": {
      id: "e-212",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-83",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-213",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|0ff3f361-0890-77fb-d63d-3edc89ea76cf",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598980577877,
    },
    "e-214": {
      id: "e-214",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-84",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-215",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|0ff3f361-0890-77fb-d63d-3edc89ea76d1",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598980611975,
    },
    "e-216": {
      id: "e-216",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-85",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-217",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|eccafbe0-9f22-4229-4ffc-26fd3929ed2d",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598980679957,
    },
    "e-218": {
      id: "e-218",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-86",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-219",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|27922f44-fe55-44a2-2e5f-25110924e9e9",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598980777500,
    },
    "e-220": {
      id: "e-220",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-87",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-221",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|b406d8d6-9af0-6243-6d72-0a3642f28397",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598980825753,
    },
    "e-222": {
      id: "e-222",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-93",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-223",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|a7f8a5d0-c0f3-0e18-17ca-117ebae48a83",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598980892887,
    },
    "e-224": {
      id: "e-224",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-88",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-225",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|27922f44-fe55-44a2-2e5f-25110924e9fb",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598980945360,
    },
    "e-226": {
      id: "e-226",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-89",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-227",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|af0ac780-ce4e-2477-c183-2ac8c8d5b750",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598981007806,
    },
    "e-228": {
      id: "e-228",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-90",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-229",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|b0d8e9b4-b55a-4bb7-8b86-671ef3eb6ab1",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598981122858,
    },
    "e-230": {
      id: "e-230",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-91",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-231",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|7293c537-beb8-6f8e-09ff-e8ce71b54dee",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598981179561,
    },
    "e-232": {
      id: "e-232",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-92",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-233",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|372a504e-0cd4-871d-d193-d8ad66381b89",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598981218764,
    },
    "e-234": {
      id: "e-234",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-94",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-235",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|5c126951-e592-d28c-e241-f9218ae325c8",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598981356984,
    },
    "e-236": {
      id: "e-236",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-95",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-237",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|ea620821-a1aa-f416-93e2-391f5cc337ef",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598981402139,
    },
    "e-238": {
      id: "e-238",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-96",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-239",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        selector: ".page3-block-6-wrapper-item.gradient-2",
        originalId:
          "5f455d1a25f9eb6bc2856a3f|ea620821-a1aa-f416-93e2-391f5cc337fc",
        appliesTo: "CLASS",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598981568357,
    },
    "e-240": {
      id: "e-240",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-97",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-241",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|4356ee61-7585-adb5-db03-efe94e453264",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598981662906,
    },
    "e-242": {
      id: "e-242",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-98",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-243",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|26aeed74-c44d-f4e0-c550-7db13ee42951",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598981763504,
    },
    "e-244": {
      id: "e-244",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-99",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-245",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|26aeed74-c44d-f4e0-c550-7db13ee42956",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598981795715,
    },
    "e-246": {
      id: "e-246",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-100",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-247",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|bd2182ae-3c44-c750-ad49-d89097d0db64",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598981871172,
    },
    "e-248": {
      id: "e-248",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-101",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-249",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|9dceaf4b-bc41-12d9-fc6b-9e641717ae07",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598981929576,
    },
    "e-250": {
      id: "e-250",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-102",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-251",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|bbb756ea-1199-8900-8c70-8a457ffc2bb7",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598982009821,
    },
    "e-252": {
      id: "e-252",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-103",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-253",
        },
      },
      mediaQueries: ["main"],
      target: {
        selector: ".page3-block-10-wrapper-item",
        originalId:
          "5f455d1a25f9eb6bc2856a3f|bbb756ea-1199-8900-8c70-8a457ffc2bbd",
        appliesTo: "CLASS",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598982059303,
    },
    "e-254": {
      id: "e-254",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-104",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-255",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|b9211fd6-b992-84bc-b6ac-e2a97938a58f",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598982186286,
    },
    "e-256": {
      id: "e-256",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-105",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-257",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f455d1a25f9eb6bc2856a3f|a474c016-c727-912a-997d-23b0046f986c",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598982260576,
    },
    "e-258": {
      id: "e-258",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-63",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-259",
        },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598982503457,
    },
    "e-260": {
      id: "e-260",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-106",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-261",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598982524012,
    },
    "e-262": {
      id: "e-262",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-107",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-263",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|61475a67-e752-7b96-1dbf-193108a9fd5a",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598982566054,
    },
    "e-264": {
      id: "e-264",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-108",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-265",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598982602676,
    },
    "e-266": {
      id: "e-266",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-109",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-267",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|f4be39ce-a2df-c388-0bc5-161caa04b2b5",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598982635917,
    },
    "e-268": {
      id: "e-268",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-110",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-269",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598982687118,
    },
    "e-270": {
      id: "e-270",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-111",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-271",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|0ff3f361-0890-77fb-d63d-3edc89ea76cf",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598982725313,
    },
    "e-272": {
      id: "e-272",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-112",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-273",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|0ff3f361-0890-77fb-d63d-3edc89ea76d1",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598982755396,
    },
    "e-274": {
      id: "e-274",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-113",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-275",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|e42461a6-373a-308d-5c10-f0690d6d4009",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598982800564,
    },
    "e-276": {
      id: "e-276",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-114",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-277",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|eaff428f-6423-d503-0161-9d0926628596",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598982859303,
    },
    "e-278": {
      id: "e-278",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-115",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-279",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|eaff428f-6423-d503-0161-9d0926628599",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598982898144,
    },
    "e-280": {
      id: "e-280",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-116",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-281",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|b66dd577-63b9-ca57-8bab-15124aabefec",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598982981542,
    },
    "e-282": {
      id: "e-282",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-117",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-283",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|b66dd577-63b9-ca57-8bab-15124aabeff6",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598983063215,
    },
    "e-284": {
      id: "e-284",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-118",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-285",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|25ce379d-3849-52b5-c618-cc879cc4503f",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598983156306,
    },
    "e-286": {
      id: "e-286",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-119",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-287",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|25ce379d-3849-52b5-c618-cc879cc45049",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598983193016,
    },
    "e-288": {
      id: "e-288",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-120",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-289",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|e7a02ec9-17a8-2d7f-5b61-ab85a375eac6",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598983233643,
    },
    "e-290": {
      id: "e-290",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-121",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-291",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b331cf2a8c23c8dfc9a0|1416bfec-5d92-24c7-3b9e-8d7b70f0e8cd",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598983290888,
    },
    "e-292": {
      id: "e-292",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-63",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-293",
        },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598983603389,
    },
    "e-294": {
      id: "e-294",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-122",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-295",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598983616795,
    },
    "e-296": {
      id: "e-296",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-123",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-297",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|61475a67-e752-7b96-1dbf-193108a9fd5a",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598983646785,
    },
    "e-298": {
      id: "e-298",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-124",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-299",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598983678642,
    },
    "e-300": {
      id: "e-300",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-125",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-301",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|f4be39ce-a2df-c388-0bc5-161caa04b2b5",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598983725778,
    },
    "e-302": {
      id: "e-302",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-126",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-303",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598983774673,
    },
    "e-304": {
      id: "e-304",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-127",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-305",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|0ff3f361-0890-77fb-d63d-3edc89ea76cf",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598983810233,
    },
    "e-306": {
      id: "e-306",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-128",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-307",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|0ff3f361-0890-77fb-d63d-3edc89ea76d1",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598983841122,
    },
    "e-308": {
      id: "e-308",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-129",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-309",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|be62a330-31ec-6456-103d-a2345a66b0e0",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598983888210,
    },
    "e-310": {
      id: "e-310",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-130",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-311",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|3737fcb1-5350-017d-a2d4-577c5ca79942",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598983949503,
    },
    "e-312": {
      id: "e-312",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-131",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-313",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        selector: ".page5-block-3-graph-1",
        originalId:
          "5f46b5cadfb45ea5fdff3380|0db1d1b7-a3a1-005a-1f91-6fddfdb329ed",
        appliesTo: "CLASS",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598984024936,
    },
    "e-314": {
      id: "e-314",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-132",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-315",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|d16912bd-e210-de57-09cb-e5e247512db8",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598984150523,
    },
    "e-316": {
      id: "e-316",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-133",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-317",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        selector: ".page5-block-4-graph-1",
        originalId:
          "5f46b5cadfb45ea5fdff3380|d16912bd-e210-de57-09cb-e5e247512dd1",
        appliesTo: "CLASS",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598984191710,
    },
    "e-318": {
      id: "e-318",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-134",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-319",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|f9643c87-38ea-1304-5312-9173a851b51c",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598984260924,
    },
    "e-320": {
      id: "e-320",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-135",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-321",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        selector: ".page5-block-5-graph-1",
        originalId:
          "5f46b5cadfb45ea5fdff3380|f9643c87-38ea-1304-5312-9173a851b532",
        appliesTo: "CLASS",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598984341288,
    },
    "e-322": {
      id: "e-322",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-136",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-323",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|d9c24b4b-97b3-9241-2c4c-e10790ddbb50",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598984397848,
    },
    "e-324": {
      id: "e-324",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-137",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-325",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        selector: ".page5-block-6-graph-1",
        originalId:
          "5f46b5cadfb45ea5fdff3380|d9c24b4b-97b3-9241-2c4c-e10790ddbb66",
        appliesTo: "CLASS",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598984432283,
    },
    "e-326": {
      id: "e-326",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-138",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-327",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|7fbd83f5-606f-de4b-3feb-3520ee77f94e",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598984538710,
    },
    "e-328": {
      id: "e-328",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-139",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-329",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        selector: ".page5-block-7-graph-1",
        originalId:
          "5f46b5cadfb45ea5fdff3380|7fbd83f5-606f-de4b-3feb-3520ee77f95f",
        appliesTo: "CLASS",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598984566567,
    },
    "e-330": {
      id: "e-330",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-140",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-331",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|44e00754-dae2-2ed3-9d6f-e60905283251",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598984631730,
    },
    "e-332": {
      id: "e-332",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-141",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-333",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b5cadfb45ea5fdff3380|0200bec2-bcf7-772e-de27-f6c8490abf12",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598984693430,
    },
    "e-334": {
      id: "e-334",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-63",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-335",
        },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598985100492,
    },
    "e-336": {
      id: "e-336",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-142",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-337",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598985118324,
    },
    "e-338": {
      id: "e-338",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-143",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-339",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|61475a67-e752-7b96-1dbf-193108a9fd5a",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598985153728,
    },
    "e-340": {
      id: "e-340",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-144",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-341",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598985184954,
    },
    "e-342": {
      id: "e-342",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-145",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-343",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|f4be39ce-a2df-c388-0bc5-161caa04b2b5",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598985213632,
    },
    "e-344": {
      id: "e-344",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-146",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-345",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598985248294,
    },
    "e-346": {
      id: "e-346",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-147",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-347",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|0ff3f361-0890-77fb-d63d-3edc89ea76cf",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598985279236,
    },
    "e-348": {
      id: "e-348",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-148",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-349",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|0ff3f361-0890-77fb-d63d-3edc89ea76d1",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598985305030,
    },
    "e-350": {
      id: "e-350",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-149",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-351",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|7914ee30-f349-9154-e822-21d65da21595",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598985346429,
    },
    "e-352": {
      id: "e-352",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-150",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-353",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|9ad377f3-3581-1bee-9eb5-00c617c7078f",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598985407675,
    },
    "e-354": {
      id: "e-354",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-151",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-355",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|9ad377f3-3581-1bee-9eb5-00c617c707a8",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598985560715,
    },
    "e-356": {
      id: "e-356",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-152",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-357",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|d361d580-a305-9821-1eba-04fbde171df6",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598985789943,
    },
    "e-358": {
      id: "e-358",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-153",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-359",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|6cd90ae9-4cd0-ca8b-6fb2-a309a11110e5",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598985831567,
    },
    "e-360": {
      id: "e-360",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-154",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-361",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|429befa7-889d-5e98-181f-2cc891ad606a",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598985861438,
    },
    "e-362": {
      id: "e-362",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-155",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-363",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|d6f2fa5b-7158-b39d-8649-cf50b8c6b2cd",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598986019048,
    },
    "e-364": {
      id: "e-364",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-156",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-365",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|d6f2fa5b-7158-b39d-8649-cf50b8c6b2d7",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598986054550,
    },
    "e-366": {
      id: "e-366",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-157",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-367",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|2763ca7f-5537-c3e2-7863-09bd99ecdb65",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598986115751,
    },
    "e-368": {
      id: "e-368",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-158",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-369",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|5b932a24-a64e-f062-cb7d-13acb04060da",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598986149228,
    },
    "e-370": {
      id: "e-370",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-159",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-371",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|2c8f6d1d-b1c6-37bb-903c-f17e73c051f3",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598986197990,
    },
    "e-372": {
      id: "e-372",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-160",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-373",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b709d4850bc6bd122bf0|b6ba359c-5bd6-747b-97f9-2ebc34f11ffa",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598986239183,
    },
    "e-374": {
      id: "e-374",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-63",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-375",
        },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b85886346710a41aa683|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598986378086,
    },
    "e-376": {
      id: "e-376",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-161",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-377",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b85886346710a41aa683|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598986392228,
    },
    "e-378": {
      id: "e-378",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-162",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-379",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b85886346710a41aa683|a2356e67-b7dd-e92a-46ff-7f64e70f582d",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598986437484,
    },
    "e-380": {
      id: "e-380",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-163",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-381",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b85886346710a41aa683|cc9b0d4b-9639-e460-beda-65750de19544",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598986495332,
    },
    "e-382": {
      id: "e-382",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-164",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-383",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b85886346710a41aa683|cc9b0d4b-9639-e460-beda-65750de19549",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598986544508,
    },
    "e-384": {
      id: "e-384",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-165",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-385",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b85886346710a41aa683|926dab97-2656-bdaa-dfbc-b6e0f4654671",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598986657342,
    },
    "e-386": {
      id: "e-386",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-166",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-387",
        },
      },
      mediaQueries: ["main"],
      target: {
        selector: ".page7-block-4-wrapper-item",
        originalId:
          "5f46b85886346710a41aa683|926dab97-2656-bdaa-dfbc-b6e0f465467d",
        appliesTo: "CLASS",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598986697907,
    },
    "e-388": {
      id: "e-388",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-167",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-389",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b85886346710a41aa683|9fc02b4b-81f8-eba2-1579-a74b15d40fa3",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598986794047,
    },
    "e-390": {
      id: "e-390",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-168",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-391",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b85886346710a41aa683|9fc02b4b-81f8-eba2-1579-a74b15d40fb4",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598986836920,
    },
    "e-392": {
      id: "e-392",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-169",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-393",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b85886346710a41aa683|9fc02b4b-81f8-eba2-1579-a74b15d40fb5",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598986906441,
    },
    "e-394": {
      id: "e-394",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-170",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-395",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b85886346710a41aa683|2c7b4226-f203-3c37-6de5-18f6b5691f82",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598986985979,
    },
    "e-396": {
      id: "e-396",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-171",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-397",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b85886346710a41aa683|94f9b93a-1904-d643-0674-0b813a2c08ff",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598987055828,
    },
    "e-398": {
      id: "e-398",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-172",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-399",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46b85886346710a41aa683|b9da3479-be09-082d-d702-1db6d2ce57c3",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598987132511,
    },
    "e-400": {
      id: "e-400",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-63",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-401",
        },
      },
      mediaQueries: ["main"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46ba5ccd37d182ce8d671a|42d67e16-4a1c-23b7-eb21-5e3e35427547",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598987358929,
    },
    "e-402": {
      id: "e-402",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-173",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-403",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46ba5ccd37d182ce8d671a|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598987396009,
    },
    "e-404": {
      id: "e-404",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-174",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-405",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46ba5ccd37d182ce8d671a|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 0,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598987430366,
    },
    "e-406": {
      id: "e-406",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-175",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-407",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46ba5ccd37d182ce8d671a|fd1975f5-0604-0fbb-0828-2061e7577bce",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598987558729,
    },
    "e-408": {
      id: "e-408",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-176",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-409",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46ba5ccd37d182ce8d671a|0e62a970-e8cf-1d0c-bde6-f6269a50a2a0",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598987626639,
    },
    "e-410": {
      id: "e-410",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-177",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-411",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46ba5ccd37d182ce8d671a|0e62a970-e8cf-1d0c-bde6-f6269a50a2a3",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598987675504,
    },
    "e-412": {
      id: "e-412",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-178",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-413",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46ba5ccd37d182ce8d671a|a7baa7d9-1a91-ef21-6da0-1bdc5406caac",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598987783026,
    },
    "e-414": {
      id: "e-414",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-179",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-415",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46ba5ccd37d182ce8d671a|a7baa7d9-1a91-ef21-6da0-1bdc5406caaf",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598987812724,
    },
    "e-416": {
      id: "e-416",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-180",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-417",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46ba5ccd37d182ce8d671a|13f23d0a-6015-7922-09c7-91f2e8fa2258",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598987844985,
    },
    "e-418": {
      id: "e-418",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-181",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-419",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46ba5ccd37d182ce8d671a|13f23d0a-6015-7922-09c7-91f2e8fa225b",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598987879510,
    },
    "e-420": {
      id: "e-420",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-182",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-421",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46ba5ccd37d182ce8d671a|0d2430aa-fcfd-62a7-d798-33e71f7226bf",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 10,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598987916279,
    },
    "e-422": {
      id: "e-422",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-183",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-423",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46ba5ccd37d182ce8d671a|0d2430aa-fcfd-62a7-d798-33e71f7226c2",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598987950120,
    },
    "e-424": {
      id: "e-424",
      eventTypeId: "SCROLL_INTO_VIEW",
      action: {
        id: "",
        actionTypeId: "GENERAL_START_ACTION",
        config: {
          delay: 0,
          easing: "",
          duration: 0,
          actionListId: "a-184",
          affectedElements: {},
          playInReverse: false,
          autoStopEventId: "e-425",
        },
      },
      mediaQueries: ["main", "medium", "small", "tiny"],
      target: {
        appliesTo: "ELEMENT",
        styleBlockIds: [],
        id: "5f46ba5ccd37d182ce8d671a|b4a1b7fe-3d3d-71e0-8cfa-44c13bc7def9",
      },
      config: {
        loop: false,
        playInReverse: false,
        scrollOffsetValue: 20,
        scrollOffsetUnit: "%",
        delay: null,
        direction: null,
        effectIn: null,
      },
      createdOn: 1598987986737,
    },
  },
  actionLists: {
    a: {
      id: "a",
      title: "horizontal scroll content",
      continuousParameterGroups: [
        {
          id: "a-p",
          type: "SCROLL_PROGRESS",
          parameterLabel: "Scroll",
          continuousActionGroups: [
            {
              keyframe: 4,
              actionItems: [
                {
                  id: "a-n",
                  actionTypeId: "TRANSFORM_MOVE",
                  config: {
                    delay: 0,
                    easing: "",
                    duration: 0,
                    target: {
                      useEventTarget: "CHILDREN",
                      selector: ".horizontal-scroll-content-1",
                      selectorGuids: ["76ce9e6a-90d2-97cb-20c4-7d9b5cfeab0a"],
                    },
                    xValue: 0,
                    xUnit: "%",
                    yUnit: "PX",
                    zUnit: "PX",
                  },
                },
              ],
            },
            {
              keyframe: 100,
              actionItems: [
                {
                  id: "a-n-2",
                  actionTypeId: "TRANSFORM_MOVE",
                  config: {
                    delay: 0,
                    easing: "",
                    duration: 0,
                    target: {
                      useEventTarget: "CHILDREN",
                      selector: ".horizontal-scroll-content-1",
                      selectorGuids: ["76ce9e6a-90d2-97cb-20c4-7d9b5cfeab0a"],
                    },
                    xValue: -53,
                    xUnit: "%",
                    yUnit: "PX",
                    zUnit: "PX",
                  },
                },
              ],
            },
          ],
        },
      ],
      createdOn: 1597948602796,
    },
    "a-3": {
      id: "a-3",
      title: "what-box-hover-in",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-3-n",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".whw-what-box",
                  selectorGuids: ["3bd3a610-2846-7d46-035a-b157c9e7b903"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-3-n-7",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  selector: ".whw-why-box",
                  selectorGuids: ["b49bc173-c34f-b2c9-2160-93be842580f1"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-3-n-5",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  selector: ".whw-how-box",
                  selectorGuids: ["7c223e23-7677-f93b-dbcb-d01ee474a090"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-3-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".whw-what-content",
                  selectorGuids: ["2cbd1f40-c837-897c-df96-054ad1a631db"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-3-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 300,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".whw-what-box",
                  selectorGuids: ["3bd3a610-2846-7d46-035a-b157c9e7b903"],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-3-n-8",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 300,
                target: {
                  selector: ".whw-why-box",
                  selectorGuids: ["b49bc173-c34f-b2c9-2160-93be842580f1"],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-3-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 300,
                target: {
                  selector: ".whw-how-box",
                  selectorGuids: ["7c223e23-7677-f93b-dbcb-d01ee474a090"],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-3-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 300,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".whw-what-content",
                  selectorGuids: ["2cbd1f40-c837-897c-df96-054ad1a631db"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598195623317,
      useFirstGroupAsInitialState: true,
    },
    "a-4": {
      id: "a-4",
      title: "what-box-hover-out",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-4-n",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 300,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".whw-what-box",
                  selectorGuids: ["3bd3a610-2846-7d46-035a-b157c9e7b903"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-4-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 300,
                target: {
                  selector: ".whw-why-box",
                  selectorGuids: ["b49bc173-c34f-b2c9-2160-93be842580f1"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-4-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 300,
                target: {
                  selector: ".whw-how-box",
                  selectorGuids: ["7c223e23-7677-f93b-dbcb-d01ee474a090"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-4-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 300,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".whw-what-content",
                  selectorGuids: ["2cbd1f40-c837-897c-df96-054ad1a631db"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598195752929,
      useFirstGroupAsInitialState: false,
    },
    "a-5": {
      id: "a-5",
      title: "how-box-hover-in",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-5-n",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".whw-how-box",
                  selectorGuids: ["7c223e23-7677-f93b-dbcb-d01ee474a090"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-5-n-8",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  selector: ".whw-why-box",
                  selectorGuids: ["b49bc173-c34f-b2c9-2160-93be842580f1"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-5-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".whw-how-content",
                  selectorGuids: ["47bc8d84-359e-dfe7-3d8a-f5bc9efe55f5"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-5-n-7",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 300,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".whw-how-box",
                  selectorGuids: ["7c223e23-7677-f93b-dbcb-d01ee474a090"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-5-n-9",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 300,
                target: {
                  selector: ".whw-why-box",
                  selectorGuids: ["b49bc173-c34f-b2c9-2160-93be842580f1"],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-5-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 300,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".whw-how-content",
                  selectorGuids: ["47bc8d84-359e-dfe7-3d8a-f5bc9efe55f5"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598195836231,
      useFirstGroupAsInitialState: true,
    },
    "a-6": {
      id: "a-6",
      title: "how-box-hover-out",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-6-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 300,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".whw-how-box",
                  selectorGuids: ["7c223e23-7677-f93b-dbcb-d01ee474a090"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-6-n-5",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 300,
                target: {
                  selector: ".whw-why-box",
                  selectorGuids: ["b49bc173-c34f-b2c9-2160-93be842580f1"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-6-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 300,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".whw-how-content",
                  selectorGuids: ["47bc8d84-359e-dfe7-3d8a-f5bc9efe55f5"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598195894258,
      useFirstGroupAsInitialState: false,
    },
    "a-7": {
      id: "a-7",
      title: "why-box-hover-in",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-7-n",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".whw-why-box",
                  selectorGuids: ["b49bc173-c34f-b2c9-2160-93be842580f1"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-7-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".whw-why-content",
                  selectorGuids: ["6efcf9db-4e1a-bdf0-12c5-5d0bd3882326"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-7-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 300,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".whw-why-box",
                  selectorGuids: ["b49bc173-c34f-b2c9-2160-93be842580f1"],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-7-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 300,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".whw-why-content",
                  selectorGuids: ["6efcf9db-4e1a-bdf0-12c5-5d0bd3882326"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598195940250,
      useFirstGroupAsInitialState: true,
    },
    "a-8": {
      id: "a-8",
      title: "why-box-hover-out",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-8-n",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 300,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".whw-why-box",
                  selectorGuids: ["b49bc173-c34f-b2c9-2160-93be842580f1"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-8-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 300,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".whw-why-content",
                  selectorGuids: ["6efcf9db-4e1a-bdf0-12c5-5d0bd3882326"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598195993721,
      useFirstGroupAsInitialState: false,
    },
    "a-9": {
      id: "a-9",
      title: "point-1-hover-in",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-9-n",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-content-box",
                  selectorGuids: ["e16bd9d7-aaa1-235e-72c5-e4b4853ad7c9"],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-9-n-7",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-9-n-5",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                widthUnit: "PX",
                heightUnit: "AUTO",
              },
            },
            {
              id: "a-9-n-3",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-content-box",
                  selectorGuids: ["e16bd9d7-aaa1-235e-72c5-e4b4853ad7c9"],
                },
                heightValue: 0,
                widthUnit: "PX",
                heightUnit: "PX",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-9-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-content-box",
                  selectorGuids: ["e16bd9d7-aaa1-235e-72c5-e4b4853ad7c9"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-9-n-8",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-9-n-6",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                heightValue: 0,
                widthUnit: "PX",
                heightUnit: "PX",
              },
            },
            {
              id: "a-9-n-4",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-content-box",
                  selectorGuids: ["e16bd9d7-aaa1-235e-72c5-e4b4853ad7c9"],
                },
                widthUnit: "PX",
                heightUnit: "AUTO",
              },
            },
          ],
        },
      ],
      createdOn: 1598366769671,
      useFirstGroupAsInitialState: true,
    },
    "a-10": {
      id: "a-10",
      title: "point-1-hover-out",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-10-n",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-content-box",
                  selectorGuids: ["e16bd9d7-aaa1-235e-72c5-e4b4853ad7c9"],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-10-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-10-n-3",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                widthUnit: "PX",
                heightUnit: "AUTO",
              },
            },
            {
              id: "a-10-n-2",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-content-box",
                  selectorGuids: ["e16bd9d7-aaa1-235e-72c5-e4b4853ad7c9"],
                },
                heightValue: 0,
                widthUnit: "PX",
                heightUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598366835108,
      useFirstGroupAsInitialState: false,
    },
    "a-11": {
      id: "a-11",
      title: "point-2-hover-in",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-11-n",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-2-content-box",
                  selectorGuids: ["67478741-9b2a-fb0b-70d5-abef5a71207c"],
                },
                heightValue: 0,
                widthUnit: "PX",
                heightUnit: "PX",
              },
            },
            {
              id: "a-11-n-7",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-11-n-5",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                widthUnit: "PX",
                heightUnit: "AUTO",
              },
            },
            {
              id: "a-11-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-2-content-box",
                  selectorGuids: ["67478741-9b2a-fb0b-70d5-abef5a71207c"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-11-n-2",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-2-content-box",
                  selectorGuids: ["67478741-9b2a-fb0b-70d5-abef5a71207c"],
                },
                widthUnit: "PX",
                heightUnit: "AUTO",
              },
            },
            {
              id: "a-11-n-8",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                heightValue: 0,
                widthUnit: "PX",
                heightUnit: "PX",
              },
            },
            {
              id: "a-11-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-11-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-2-content-box",
                  selectorGuids: ["67478741-9b2a-fb0b-70d5-abef5a71207c"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598367316599,
      useFirstGroupAsInitialState: true,
    },
    "a-12": {
      id: "a-12",
      title: "point-2-hover-out",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-12-n",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-2-content-box",
                  selectorGuids: ["67478741-9b2a-fb0b-70d5-abef5a71207c"],
                },
                heightValue: 0,
                widthUnit: "PX",
                heightUnit: "PX",
              },
            },
            {
              id: "a-12-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-12-n-3",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                widthUnit: "PX",
                heightUnit: "AUTO",
              },
            },
            {
              id: "a-12-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-2-content-box",
                  selectorGuids: ["67478741-9b2a-fb0b-70d5-abef5a71207c"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598367381940,
      useFirstGroupAsInitialState: false,
    },
    "a-13": {
      id: "a-13",
      title: "point-3-hover-in",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-13-n",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-3-content-box",
                  selectorGuids: ["0b0236ec-df9c-9d09-5d3d-9bc00ac4687e"],
                },
                heightValue: 0,
                widthUnit: "PX",
                heightUnit: "PX",
              },
            },
            {
              id: "a-13-n-7",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-13-n-5",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                widthUnit: "PX",
                heightUnit: "AUTO",
              },
            },
            {
              id: "a-13-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-3-content-box",
                  selectorGuids: ["0b0236ec-df9c-9d09-5d3d-9bc00ac4687e"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-13-n-2",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-3-content-box",
                  selectorGuids: ["0b0236ec-df9c-9d09-5d3d-9bc00ac4687e"],
                },
                widthUnit: "PX",
                heightUnit: "AUTO",
              },
            },
            {
              id: "a-13-n-8",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-13-n-6",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                heightValue: 0,
                widthUnit: "PX",
                heightUnit: "PX",
              },
            },
            {
              id: "a-13-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-3-content-box",
                  selectorGuids: ["0b0236ec-df9c-9d09-5d3d-9bc00ac4687e"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598367499632,
      useFirstGroupAsInitialState: true,
    },
    "a-14": {
      id: "a-14",
      title: "point-3-hover-out",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-14-n",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-3-content-box",
                  selectorGuids: ["0b0236ec-df9c-9d09-5d3d-9bc00ac4687e"],
                },
                heightValue: 0,
                widthUnit: "PX",
                heightUnit: "PX",
              },
            },
            {
              id: "a-14-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-14-n-3",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                widthUnit: "PX",
                heightUnit: "AUTO",
              },
            },
            {
              id: "a-14-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-3-content-box",
                  selectorGuids: ["0b0236ec-df9c-9d09-5d3d-9bc00ac4687e"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598367590151,
      useFirstGroupAsInitialState: false,
    },
    "a-15": {
      id: "a-15",
      title: "point-4-hover-in",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-15-n",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-4-content-box",
                  selectorGuids: ["bf759ce1-ef27-059a-c42a-b0e5ab5e30e8"],
                },
                heightValue: 0,
                widthUnit: "PX",
                heightUnit: "PX",
              },
            },
            {
              id: "a-15-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-15-n-5",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                widthUnit: "PX",
                heightUnit: "AUTO",
              },
            },
            {
              id: "a-15-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-4-content-box",
                  selectorGuids: ["bf759ce1-ef27-059a-c42a-b0e5ab5e30e8"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-15-n-2",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-4-content-box",
                  selectorGuids: ["bf759ce1-ef27-059a-c42a-b0e5ab5e30e8"],
                },
                widthUnit: "PX",
                heightUnit: "AUTO",
              },
            },
            {
              id: "a-15-n-8",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-15-n-7",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                heightValue: 0,
                widthUnit: "PX",
                heightUnit: "PX",
              },
            },
            {
              id: "a-15-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-4-content-box",
                  selectorGuids: ["bf759ce1-ef27-059a-c42a-b0e5ab5e30e8"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598367691993,
      useFirstGroupAsInitialState: true,
    },
    "a-16": {
      id: "a-16",
      title: "point-4-hover-out",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-16-n",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-4-content-box",
                  selectorGuids: ["bf759ce1-ef27-059a-c42a-b0e5ab5e30e8"],
                },
                heightValue: 0,
                widthUnit: "PX",
                heightUnit: "PX",
              },
            },
            {
              id: "a-16-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-16-n-3",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                widthUnit: "PX",
                heightUnit: "AUTO",
              },
            },
            {
              id: "a-16-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-4-content-box",
                  selectorGuids: ["bf759ce1-ef27-059a-c42a-b0e5ab5e30e8"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598367745299,
      useFirstGroupAsInitialState: false,
    },
    "a-17": {
      id: "a-17",
      title: "point-5-hover-in",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-17-n",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-5-content-box",
                  selectorGuids: ["ad3788fa-8085-2562-5429-9594e19643db"],
                },
                heightValue: 0,
                widthUnit: "PX",
                heightUnit: "PX",
              },
            },
            {
              id: "a-17-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-17-n-5",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                widthUnit: "PX",
                heightUnit: "AUTO",
              },
            },
            {
              id: "a-17-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-5-content-box",
                  selectorGuids: ["ad3788fa-8085-2562-5429-9594e19643db"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-17-n-2",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-5-content-box",
                  selectorGuids: ["ad3788fa-8085-2562-5429-9594e19643db"],
                },
                widthUnit: "PX",
                heightUnit: "AUTO",
              },
            },
            {
              id: "a-17-n-8",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-17-n-7",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                heightValue: 0,
                widthUnit: "PX",
                heightUnit: "PX",
              },
            },
            {
              id: "a-17-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-5-content-box",
                  selectorGuids: ["ad3788fa-8085-2562-5429-9594e19643db"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598367801354,
      useFirstGroupAsInitialState: true,
    },
    "a-18": {
      id: "a-18",
      title: "point-5-hover-out",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-18-n",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-5-content-box",
                  selectorGuids: ["ad3788fa-8085-2562-5429-9594e19643db"],
                },
                heightValue: 0,
                widthUnit: "PX",
                heightUnit: "PX",
              },
            },
            {
              id: "a-18-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-18-n-3",
              actionTypeId: "STYLE_SIZE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                locked: false,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1-title-box",
                  selectorGuids: ["aeaa31cc-133b-af30-e99d-14357155689c"],
                },
                widthUnit: "PX",
                heightUnit: "AUTO",
              },
            },
            {
              id: "a-18-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 200,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-5-content-box",
                  selectorGuids: ["ad3788fa-8085-2562-5429-9594e19643db"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598367848098,
      useFirstGroupAsInitialState: false,
    },
    "a-21": {
      id: "a-21",
      title: "horizontal scroll content 2",
      continuousParameterGroups: [
        {
          id: "a-21-p",
          type: "SCROLL_PROGRESS",
          parameterLabel: "Scroll",
          continuousActionGroups: [
            {
              keyframe: 8,
              actionItems: [
                {
                  id: "a-21-n",
                  actionTypeId: "TRANSFORM_MOVE",
                  config: {
                    delay: 0,
                    easing: "",
                    duration: 100,
                    target: {
                      useEventTarget: "CHILDREN",
                      selector: ".horizontal-scroll-content-2",
                      selectorGuids: ["7673ef84-a55c-f5bc-23d1-7145b4928f6c"],
                    },
                    xValue: 0,
                    xUnit: "%",
                    yUnit: "PX",
                    zUnit: "PX",
                  },
                },
              ],
            },
            {
              keyframe: 100,
              actionItems: [
                {
                  id: "a-21-n-2",
                  actionTypeId: "TRANSFORM_MOVE",
                  config: {
                    delay: 0,
                    easing: "",
                    duration: 500,
                    target: {
                      useEventTarget: "CHILDREN",
                      selector: ".horizontal-scroll-content-2",
                      selectorGuids: ["7673ef84-a55c-f5bc-23d1-7145b4928f6c"],
                    },
                    xValue: -82,
                    xUnit: "%",
                    yUnit: "PX",
                    zUnit: "PX",
                  },
                },
              ],
            },
          ],
        },
      ],
      createdOn: 1597948602796,
    },
    "a-19": {
      id: "a-19",
      title: "nav-side-border",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-19-n",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".nav-right-border",
                  selectorGuids: ["b814a373-f4ea-2450-8996-412f354fdd70"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-19-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 300,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".nav-right-border",
                  selectorGuids: ["b814a373-f4ea-2450-8996-412f354fdd70"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598374744675,
      useFirstGroupAsInitialState: true,
    },
    "a-20": {
      id: "a-20",
      title: "nav-side-border-black",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-20-n",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 300,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".nav-right-border",
                  selectorGuids: ["b814a373-f4ea-2450-8996-412f354fdd70"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598374791957,
      useFirstGroupAsInitialState: false,
    },
    "a-22": {
      id: "a-22",
      title: "popup-1-show",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-22-n",
              actionTypeId: "GENERAL_DISPLAY",
              config: {
                delay: 0,
                easing: "",
                duration: 0,
                value: "none",
                target: {
                  selector: ".popup-overlay-1",
                  selectorGuids: ["51235e77-693b-173a-012b-6accf9fcd531"],
                },
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-22-n-2",
              actionTypeId: "GENERAL_DISPLAY",
              config: {
                delay: 0,
                easing: "",
                duration: 0,
                value: "flex",
                target: {
                  selector: ".popup-overlay-1",
                  selectorGuids: ["51235e77-693b-173a-012b-6accf9fcd531"],
                },
              },
            },
          ],
        },
      ],
      createdOn: 1598379549018,
      useFirstGroupAsInitialState: true,
    },
    "a-23": {
      id: "a-23",
      title: "popup-1-close",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-23-n",
              actionTypeId: "GENERAL_DISPLAY",
              config: {
                delay: 0,
                easing: "",
                duration: 0,
                value: "none",
                target: {
                  useEventTarget: "PARENT",
                  selector: ".popup-overlay-1",
                  selectorGuids: ["51235e77-693b-173a-012b-6accf9fcd531"],
                },
              },
            },
          ],
        },
      ],
      createdOn: 1598379593843,
      useFirstGroupAsInitialState: false,
    },
    "a-31": {
      id: "a-31",
      title: "horizontal scroll content 3",
      continuousParameterGroups: [
        {
          id: "a-31-p",
          type: "SCROLL_PROGRESS",
          parameterLabel: "Scroll",
          continuousActionGroups: [
            {
              keyframe: 7,
              actionItems: [
                {
                  id: "a-31-n",
                  actionTypeId: "TRANSFORM_MOVE",
                  config: {
                    delay: 0,
                    easing: "",
                    duration: 100,
                    target: {
                      useEventTarget: "CHILDREN",
                      selector: ".horizontal-scroll-content-3",
                      selectorGuids: ["eb386473-4f93-3923-e19a-1d80ff1a307d"],
                    },
                    xValue: 0,
                    xUnit: "%",
                    yUnit: "PX",
                    zUnit: "PX",
                  },
                },
              ],
            },
            {
              keyframe: 100,
              actionItems: [
                {
                  id: "a-31-n-2",
                  actionTypeId: "TRANSFORM_MOVE",
                  config: {
                    delay: 0,
                    easing: "",
                    duration: 500,
                    target: {
                      useEventTarget: "CHILDREN",
                      selector: ".horizontal-scroll-content-3",
                      selectorGuids: ["eb386473-4f93-3923-e19a-1d80ff1a307d"],
                    },
                    xValue: -82,
                    xUnit: "%",
                    yUnit: "PX",
                    zUnit: "PX",
                  },
                },
              ],
            },
          ],
        },
      ],
      createdOn: 1597948602796,
    },
    "a-25": {
      id: "a-25",
      title: "popup-2-show",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-25-n",
              actionTypeId: "GENERAL_DISPLAY",
              config: {
                delay: 0,
                easing: "",
                duration: 0,
                value: "none",
                target: {
                  selector: ".popup-overlay-2",
                  selectorGuids: ["143c0040-30cb-7a43-50ec-32384640d50c"],
                },
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-25-n-2",
              actionTypeId: "GENERAL_DISPLAY",
              config: {
                delay: 0,
                easing: "",
                duration: 0,
                value: "flex",
                target: {
                  selector: ".popup-overlay-2",
                  selectorGuids: ["143c0040-30cb-7a43-50ec-32384640d50c"],
                },
              },
            },
          ],
        },
      ],
      createdOn: 1598379549018,
      useFirstGroupAsInitialState: true,
    },
    "a-26": {
      id: "a-26",
      title: "popup-2-close",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-26-n",
              actionTypeId: "GENERAL_DISPLAY",
              config: {
                delay: 0,
                easing: "",
                duration: 0,
                value: "none",
                target: {
                  useEventTarget: "PARENT",
                  selector: ".popup-overlay-2",
                  selectorGuids: ["143c0040-30cb-7a43-50ec-32384640d50c"],
                },
              },
            },
          ],
        },
      ],
      createdOn: 1598379593843,
      useFirstGroupAsInitialState: false,
    },
    "a-27": {
      id: "a-27",
      title: "popup-3-show",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-27-n",
              actionTypeId: "GENERAL_DISPLAY",
              config: {
                delay: 0,
                easing: "",
                duration: 0,
                value: "none",
                target: {
                  selector: ".popup-overlay-3",
                  selectorGuids: ["f9c0bde8-2d23-24a4-e5a2-077f36432340"],
                },
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-27-n-2",
              actionTypeId: "GENERAL_DISPLAY",
              config: {
                delay: 0,
                easing: "",
                duration: 0,
                value: "flex",
                target: {
                  selector: ".popup-overlay-3",
                  selectorGuids: ["f9c0bde8-2d23-24a4-e5a2-077f36432340"],
                },
              },
            },
          ],
        },
      ],
      createdOn: 1598379549018,
      useFirstGroupAsInitialState: true,
    },
    "a-28": {
      id: "a-28",
      title: "popup-3-close",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-28-n",
              actionTypeId: "GENERAL_DISPLAY",
              config: {
                delay: 0,
                easing: "",
                duration: 0,
                value: "none",
                target: {
                  useEventTarget: "PARENT",
                  selector: ".popup-overlay-3",
                  selectorGuids: ["f9c0bde8-2d23-24a4-e5a2-077f36432340"],
                },
              },
            },
          ],
        },
      ],
      createdOn: 1598379593843,
      useFirstGroupAsInitialState: false,
    },
    "a-29": {
      id: "a-29",
      title: "popup-4-show",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-29-n",
              actionTypeId: "GENERAL_DISPLAY",
              config: {
                delay: 0,
                easing: "",
                duration: 0,
                value: "none",
                target: {
                  selector: ".popup-overlay-4",
                  selectorGuids: ["7699eaf9-3f77-7a6e-c249-c60969cb43bb"],
                },
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-29-n-2",
              actionTypeId: "GENERAL_DISPLAY",
              config: {
                delay: 0,
                easing: "",
                duration: 0,
                value: "flex",
                target: {
                  selector: ".popup-overlay-4",
                  selectorGuids: ["7699eaf9-3f77-7a6e-c249-c60969cb43bb"],
                },
              },
            },
          ],
        },
      ],
      createdOn: 1598379549018,
      useFirstGroupAsInitialState: true,
    },
    "a-30": {
      id: "a-30",
      title: "popup-4-close",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-30-n",
              actionTypeId: "GENERAL_DISPLAY",
              config: {
                delay: 0,
                easing: "",
                duration: 0,
                value: "none",
                target: {
                  useEventTarget: "PARENT",
                  selector: ".popup-overlay-4",
                  selectorGuids: ["7699eaf9-3f77-7a6e-c249-c60969cb43bb"],
                },
              },
            },
          ],
        },
      ],
      createdOn: 1598379593843,
      useFirstGroupAsInitialState: false,
    },
    "a-36": {
      id: "a-36",
      title: "horizontal scroll content 4",
      continuousParameterGroups: [
        {
          id: "a-36-p",
          type: "SCROLL_PROGRESS",
          parameterLabel: "Scroll",
          continuousActionGroups: [
            {
              keyframe: 7,
              actionItems: [
                {
                  id: "a-36-n",
                  actionTypeId: "TRANSFORM_MOVE",
                  config: {
                    delay: 0,
                    easing: "",
                    duration: 100,
                    target: {
                      useEventTarget: "CHILDREN",
                      selector: ".horizontal-scroll-content-4",
                      selectorGuids: ["f5129a56-d157-43d9-4087-05a5a01145b9"],
                    },
                    xValue: 0,
                    xUnit: "%",
                    yUnit: "PX",
                    zUnit: "PX",
                  },
                },
              ],
            },
            {
              keyframe: 100,
              actionItems: [
                {
                  id: "a-36-n-2",
                  actionTypeId: "TRANSFORM_MOVE",
                  config: {
                    delay: 0,
                    easing: "",
                    duration: 500,
                    target: {
                      useEventTarget: "CHILDREN",
                      selector: ".horizontal-scroll-content-4",
                      selectorGuids: ["f5129a56-d157-43d9-4087-05a5a01145b9"],
                    },
                    xValue: -82,
                    xUnit: "%",
                    yUnit: "PX",
                    zUnit: "PX",
                  },
                },
              ],
            },
          ],
        },
      ],
      createdOn: 1597948602796,
    },
    "a-34": {
      id: "a-34",
      title: "page4-popup-1-close",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-34-n",
              actionTypeId: "GENERAL_DISPLAY",
              config: {
                delay: 0,
                easing: "",
                duration: 0,
                value: "none",
                target: {
                  useEventTarget: "PARENT",
                  selector: ".page4-popup-overlay-1",
                  selectorGuids: ["4506952c-8336-027f-ff66-a6fa72e24b42"],
                },
              },
            },
          ],
        },
      ],
      createdOn: 1598379593843,
      useFirstGroupAsInitialState: false,
    },
    "a-37": {
      id: "a-37",
      title: "horizontal scroll content 5",
      continuousParameterGroups: [
        {
          id: "a-37-p",
          type: "SCROLL_PROGRESS",
          parameterLabel: "Scroll",
          continuousActionGroups: [
            {
              keyframe: 7,
              actionItems: [
                {
                  id: "a-37-n",
                  actionTypeId: "TRANSFORM_MOVE",
                  config: {
                    delay: 0,
                    easing: "",
                    duration: 100,
                    target: {
                      useEventTarget: "CHILDREN",
                      selector: ".horizontal-scroll-content-5",
                      selectorGuids: ["7d1ec0ef-81ba-5240-01da-1f00a17b4ce2"],
                    },
                    xValue: 0,
                    xUnit: "%",
                    yUnit: "PX",
                    zUnit: "PX",
                  },
                },
              ],
            },
            {
              keyframe: 100,
              actionItems: [
                {
                  id: "a-37-n-2",
                  actionTypeId: "TRANSFORM_MOVE",
                  config: {
                    delay: 0,
                    easing: "",
                    duration: 500,
                    target: {
                      useEventTarget: "CHILDREN",
                      selector: ".horizontal-scroll-content-5",
                      selectorGuids: ["7d1ec0ef-81ba-5240-01da-1f00a17b4ce2"],
                    },
                    xValue: -82,
                    xUnit: "%",
                    yUnit: "PX",
                    zUnit: "PX",
                  },
                },
              ],
            },
          ],
        },
      ],
      createdOn: 1597948602796,
    },
    "a-38": {
      id: "a-38",
      title: "horizontal scroll content 6",
      continuousParameterGroups: [
        {
          id: "a-38-p",
          type: "SCROLL_PROGRESS",
          parameterLabel: "Scroll",
          continuousActionGroups: [
            {
              keyframe: 7,
              actionItems: [
                {
                  id: "a-38-n",
                  actionTypeId: "TRANSFORM_MOVE",
                  config: {
                    delay: 0,
                    easing: "",
                    duration: 100,
                    target: {
                      useEventTarget: "CHILDREN",
                      selector: ".horizontal-scroll-content-6",
                      selectorGuids: ["26ba9a57-6014-bf51-d430-14601eef119e"],
                    },
                    xValue: 0,
                    xUnit: "%",
                    yUnit: "PX",
                    zUnit: "PX",
                  },
                },
              ],
            },
            {
              keyframe: 100,
              actionItems: [
                {
                  id: "a-38-n-2",
                  actionTypeId: "TRANSFORM_MOVE",
                  config: {
                    delay: 0,
                    easing: "",
                    duration: 500,
                    target: {
                      useEventTarget: "CHILDREN",
                      selector: ".horizontal-scroll-content-6",
                      selectorGuids: ["26ba9a57-6014-bf51-d430-14601eef119e"],
                    },
                    xValue: -82,
                    xUnit: "%",
                    yUnit: "PX",
                    zUnit: "PX",
                  },
                },
              ],
            },
          ],
        },
      ],
      createdOn: 1597948602796,
    },
    "a-39": {
      id: "a-39",
      title: "horizontal scroll content 7",
      continuousParameterGroups: [
        {
          id: "a-39-p",
          type: "SCROLL_PROGRESS",
          parameterLabel: "Scroll",
          continuousActionGroups: [
            {
              keyframe: 7,
              actionItems: [
                {
                  id: "a-39-n",
                  actionTypeId: "TRANSFORM_MOVE",
                  config: {
                    delay: 0,
                    easing: "",
                    duration: 100,
                    target: {
                      useEventTarget: "CHILDREN",
                      selector: ".horizontal-scroll-content-7",
                      selectorGuids: ["6479abbe-e93a-1186-0084-ba1b80a17c34"],
                    },
                    xValue: 0,
                    xUnit: "%",
                    yUnit: "PX",
                    zUnit: "PX",
                  },
                },
              ],
            },
            {
              keyframe: 100,
              actionItems: [
                {
                  id: "a-39-n-2",
                  actionTypeId: "TRANSFORM_MOVE",
                  config: {
                    delay: 0,
                    easing: "",
                    duration: 500,
                    target: {
                      useEventTarget: "CHILDREN",
                      selector: ".horizontal-scroll-content-7",
                      selectorGuids: ["6479abbe-e93a-1186-0084-ba1b80a17c34"],
                    },
                    xValue: -82,
                    xUnit: "%",
                    yUnit: "PX",
                    zUnit: "PX",
                  },
                },
              ],
            },
          ],
        },
      ],
      createdOn: 1597948602796,
    },
    "a-40": {
      id: "a-40",
      title: "horizontal scroll content 8",
      continuousParameterGroups: [
        {
          id: "a-40-p",
          type: "SCROLL_PROGRESS",
          parameterLabel: "Scroll",
          continuousActionGroups: [
            {
              keyframe: 7,
              actionItems: [
                {
                  id: "a-40-n",
                  actionTypeId: "TRANSFORM_MOVE",
                  config: {
                    delay: 0,
                    easing: "",
                    duration: 100,
                    target: {
                      useEventTarget: "CHILDREN",
                      selector: ".horizontal-scroll-content-8",
                      selectorGuids: ["aa0110fe-8f60-a74a-9834-a85629f537f3"],
                    },
                    xValue: 0,
                    xUnit: "%",
                    yUnit: "PX",
                    zUnit: "PX",
                  },
                },
              ],
            },
            {
              keyframe: 100,
              actionItems: [
                {
                  id: "a-40-n-2",
                  actionTypeId: "TRANSFORM_MOVE",
                  config: {
                    delay: 0,
                    easing: "",
                    duration: 500,
                    target: {
                      useEventTarget: "CHILDREN",
                      selector: ".horizontal-scroll-content-8",
                      selectorGuids: ["aa0110fe-8f60-a74a-9834-a85629f537f3"],
                    },
                    xValue: -82,
                    xUnit: "%",
                    yUnit: "PX",
                    zUnit: "PX",
                  },
                },
              ],
            },
          ],
        },
      ],
      createdOn: 1597948602796,
    },
    "a-32": {
      id: "a-32",
      title: "page4-popup-1-show",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-32-n",
              actionTypeId: "GENERAL_DISPLAY",
              config: {
                delay: 0,
                easing: "",
                duration: 0,
                value: "none",
                target: {
                  selector: ".page4-popup-overlay-1",
                  selectorGuids: ["4506952c-8336-027f-ff66-a6fa72e24b42"],
                },
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-32-n-2",
              actionTypeId: "GENERAL_DISPLAY",
              config: {
                delay: 0,
                easing: "",
                duration: 0,
                value: "flex",
                target: {
                  selector: ".page4-popup-overlay-1",
                  selectorGuids: ["4506952c-8336-027f-ff66-a6fa72e24b42"],
                },
              },
            },
          ],
        },
      ],
      createdOn: 1598379549018,
      useFirstGroupAsInitialState: true,
    },
    "a-33": {
      id: "a-33",
      title: "page4-popup-2-show",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-33-n",
              actionTypeId: "GENERAL_DISPLAY",
              config: {
                delay: 0,
                easing: "",
                duration: 0,
                value: "none",
                target: {
                  selector: ".page4-popup-overlay-2",
                  selectorGuids: ["3c4d02d0-9701-5a38-3d59-b8eb6b20f714"],
                },
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-33-n-2",
              actionTypeId: "GENERAL_DISPLAY",
              config: {
                delay: 0,
                easing: "",
                duration: 0,
                value: "flex",
                target: {
                  selector: ".page4-popup-overlay-2",
                  selectorGuids: ["3c4d02d0-9701-5a38-3d59-b8eb6b20f714"],
                },
              },
            },
          ],
        },
      ],
      createdOn: 1598379549018,
      useFirstGroupAsInitialState: true,
    },
    "a-35": {
      id: "a-35",
      title: "page4-popup-2-close",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-35-n",
              actionTypeId: "GENERAL_DISPLAY",
              config: {
                delay: 0,
                easing: "",
                duration: 0,
                value: "none",
                target: {
                  useEventTarget: "PARENT",
                  selector: ".page4-popup-overlay-2",
                  selectorGuids: ["3c4d02d0-9701-5a38-3d59-b8eb6b20f714"],
                },
              },
            },
          ],
        },
      ],
      createdOn: 1598379593843,
      useFirstGroupAsInitialState: false,
    },
    "a-1000": {
      id: "a-1000",
      title: "page5-popup-2-close",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-1000-n",
              actionTypeId: "GENERAL_DISPLAY",
              config: {
                delay: 0,
                easing: "",
                duration: 0,
                value: "none",
                target: {
                  useEventTarget: "PARENT",
                  selector: ".page5-popup-overlay-1",
                  selectorGuids: ["3c4d02d0-9701-5a38-3d59-b8eb6b20f714"],
                },
              },
            },
          ],
        },
      ],
      createdOn: 1598379593843,
      useFirstGroupAsInitialState: false,
    },
    "a-41": {
      id: "a-41",
      title: "home-logo",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-41-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|fee8ad35-a5c7-a156-cb58-feaaf405fcc3",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-41-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|fee8ad35-a5c7-a156-cb58-feaaf405fcc3",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-41-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 300,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|fee8ad35-a5c7-a156-cb58-feaaf405fcc3",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-41-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 300,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|fee8ad35-a5c7-a156-cb58-feaaf405fcc3",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598955822411,
      useFirstGroupAsInitialState: true,
    },
    "a-42": {
      id: "a-42",
      title: "home-h1",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-42-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-42-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-42-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 700,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-42-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 700,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598956371206,
      useFirstGroupAsInitialState: true,
    },
    "a-43": {
      id: "a-43",
      title: "home-para",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-43-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-43-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-43-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 1100,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-43-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 1100,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598956552562,
      useFirstGroupAsInitialState: true,
    },
    "a-44": {
      id: "a-44",
      title: "home-block-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-44-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-2-img",
                  selectorGuids: ["5e3b18cc-d39c-3981-fde7-3d2d0ebcf7d6"],
                },
                xValue: 1.5,
                yValue: 1.5,
                locked: true,
              },
            },
            {
              id: "a-44-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-2-overlay",
                  selectorGuids: ["f9a10809-0eb8-97d6-864a-c2884ba710bb"],
                },
                xValue: 0,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-44-n-5",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-2-overlay",
                  selectorGuids: ["f9a10809-0eb8-97d6-864a-c2884ba710bb"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-44-n-2",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 1500,
                easing: "easeInOut",
                duration: 3000,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-2-img",
                  selectorGuids: ["5e3b18cc-d39c-3981-fde7-3d2d0ebcf7d6"],
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
            {
              id: "a-44-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 1500,
                easing: "easeInOut",
                duration: 1500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-2-overlay",
                  selectorGuids: ["f9a10809-0eb8-97d6-864a-c2884ba710bb"],
                },
                xValue: 100,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-44-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 1500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-2-overlay",
                  selectorGuids: ["f9a10809-0eb8-97d6-864a-c2884ba710bb"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956730939,
      useFirstGroupAsInitialState: true,
    },
    "a-45": {
      id: "a-45",
      title: "home-block-3",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-45-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-link-blocks.gradient-1",
                  selectorGuids: [
                    "05f4f1ae-6154-26db-3ad2-147ec996f6fa",
                    "e811cd7c-a2f7-e3b4-3b4b-a4ae2affb269",
                  ],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-45-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-link-blocks.gradient-2",
                  selectorGuids: [
                    "05f4f1ae-6154-26db-3ad2-147ec996f6fa",
                    "87130d36-29e3-7480-70f2-fe5e904cf656",
                  ],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-45-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-link-blocks.gradient-3",
                  selectorGuids: [
                    "05f4f1ae-6154-26db-3ad2-147ec996f6fa",
                    "52af51a7-fe80-4ad0-e1f3-81f0aca305bb",
                  ],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-45-n-7",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-link-blocks.gradient-4",
                  selectorGuids: [
                    "05f4f1ae-6154-26db-3ad2-147ec996f6fa",
                    "070b389f-f38a-f68d-63ab-edbf2e7306aa",
                  ],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-45-n-9",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-link-blocks.gradient-5",
                  selectorGuids: [
                    "05f4f1ae-6154-26db-3ad2-147ec996f6fa",
                    "86e48455-e9c2-e167-c368-cb3631968ee3",
                  ],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-45-n-11",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-link-blocks.gradient-1",
                  selectorGuids: [
                    "05f4f1ae-6154-26db-3ad2-147ec996f6fa",
                    "e811cd7c-a2f7-e3b4-3b4b-a4ae2affb269",
                  ],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-45-n-13",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-link-blocks.gradient-2",
                  selectorGuids: [
                    "05f4f1ae-6154-26db-3ad2-147ec996f6fa",
                    "87130d36-29e3-7480-70f2-fe5e904cf656",
                  ],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-45-n-15",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-link-blocks.gradient-3",
                  selectorGuids: [
                    "05f4f1ae-6154-26db-3ad2-147ec996f6fa",
                    "52af51a7-fe80-4ad0-e1f3-81f0aca305bb",
                  ],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-45-n-17",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-link-blocks.gradient-4",
                  selectorGuids: [
                    "05f4f1ae-6154-26db-3ad2-147ec996f6fa",
                    "070b389f-f38a-f68d-63ab-edbf2e7306aa",
                  ],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-45-n-19",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-link-blocks.gradient-5",
                  selectorGuids: [
                    "05f4f1ae-6154-26db-3ad2-147ec996f6fa",
                    "86e48455-e9c2-e167-c368-cb3631968ee3",
                  ],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-45-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 400,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-link-blocks.gradient-1",
                  selectorGuids: [
                    "05f4f1ae-6154-26db-3ad2-147ec996f6fa",
                    "e811cd7c-a2f7-e3b4-3b4b-a4ae2affb269",
                  ],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-45-n-12",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 400,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-link-blocks.gradient-1",
                  selectorGuids: [
                    "05f4f1ae-6154-26db-3ad2-147ec996f6fa",
                    "e811cd7c-a2f7-e3b4-3b4b-a4ae2affb269",
                  ],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-45-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 400,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-link-blocks.gradient-2",
                  selectorGuids: [
                    "05f4f1ae-6154-26db-3ad2-147ec996f6fa",
                    "87130d36-29e3-7480-70f2-fe5e904cf656",
                  ],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-45-n-14",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 400,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-link-blocks.gradient-2",
                  selectorGuids: [
                    "05f4f1ae-6154-26db-3ad2-147ec996f6fa",
                    "87130d36-29e3-7480-70f2-fe5e904cf656",
                  ],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-45-n-6",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 400,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-link-blocks.gradient-3",
                  selectorGuids: [
                    "05f4f1ae-6154-26db-3ad2-147ec996f6fa",
                    "52af51a7-fe80-4ad0-e1f3-81f0aca305bb",
                  ],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-45-n-16",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 400,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-link-blocks.gradient-3",
                  selectorGuids: [
                    "05f4f1ae-6154-26db-3ad2-147ec996f6fa",
                    "52af51a7-fe80-4ad0-e1f3-81f0aca305bb",
                  ],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-45-n-8",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 400,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-link-blocks.gradient-4",
                  selectorGuids: [
                    "05f4f1ae-6154-26db-3ad2-147ec996f6fa",
                    "070b389f-f38a-f68d-63ab-edbf2e7306aa",
                  ],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-45-n-18",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 400,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-link-blocks.gradient-4",
                  selectorGuids: [
                    "05f4f1ae-6154-26db-3ad2-147ec996f6fa",
                    "070b389f-f38a-f68d-63ab-edbf2e7306aa",
                  ],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-45-n-10",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 400,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-link-blocks.gradient-5",
                  selectorGuids: [
                    "05f4f1ae-6154-26db-3ad2-147ec996f6fa",
                    "86e48455-e9c2-e167-c368-cb3631968ee3",
                  ],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-45-n-20",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 400,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-link-blocks.gradient-5",
                  selectorGuids: [
                    "05f4f1ae-6154-26db-3ad2-147ec996f6fa",
                    "86e48455-e9c2-e167-c368-cb3631968ee3",
                  ],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598958061460,
      useFirstGroupAsInitialState: true,
    },
    "a-46": {
      id: "a-46",
      title: "home-block-4-content-1",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-46-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|f8663848-147d-4b26-1c0c-8a779d58b361",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-46-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|f8663848-147d-4b26-1c0c-8a779d58b361",
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-46-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "SIBLINGS",
                  selector: ".row",
                  selectorGuids: ["407aa114-6726-0e48-cd0e-b6a798edeed0"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-46-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "SIBLINGS",
                  selector: ".row",
                  selectorGuids: ["407aa114-6726-0e48-cd0e-b6a798edeed0"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-46-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 400,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|f8663848-147d-4b26-1c0c-8a779d58b361",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-46-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 400,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|f8663848-147d-4b26-1c0c-8a779d58b361",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-46-n-7",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 400,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: "SIBLINGS",
                  selector: ".row",
                  selectorGuids: ["407aa114-6726-0e48-cd0e-b6a798edeed0"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-46-n-8",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 400,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: "SIBLINGS",
                  selector: ".row",
                  selectorGuids: ["407aa114-6726-0e48-cd0e-b6a798edeed0"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598960607269,
      useFirstGroupAsInitialState: true,
    },
    "a-47": {
      id: "a-47",
      title: "home-block-4-content-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-47-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|f8663848-147d-4b26-1c0c-8a779d58b361",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-47-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|f8663848-147d-4b26-1c0c-8a779d58b361",
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-47-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "SIBLINGS",
                  selector: ".row",
                  selectorGuids: ["407aa114-6726-0e48-cd0e-b6a798edeed0"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-47-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "SIBLINGS",
                  selector: ".row",
                  selectorGuids: ["407aa114-6726-0e48-cd0e-b6a798edeed0"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-47-n-5",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 400,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|f8663848-147d-4b26-1c0c-8a779d58b361",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-47-n-6",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 400,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|f8663848-147d-4b26-1c0c-8a779d58b361",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-47-n-7",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 400,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: "SIBLINGS",
                  selector: ".row",
                  selectorGuids: ["407aa114-6726-0e48-cd0e-b6a798edeed0"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-47-n-8",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 400,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: "SIBLINGS",
                  selector: ".row",
                  selectorGuids: ["407aa114-6726-0e48-cd0e-b6a798edeed0"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598960607269,
      useFirstGroupAsInitialState: true,
    },
    "a-48": {
      id: "a-48",
      title: "home-block-5",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-48-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".title-box",
                  selectorGuids: ["918e5819-5dfa-78bc-1d1f-e024b678e8d2"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-48-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".title-box",
                  selectorGuids: ["918e5819-5dfa-78bc-1d1f-e024b678e8d2"],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-48-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-5-points-item._1",
                  selectorGuids: [
                    "2aed3386-bdd5-2f1b-714c-30fb4668393a",
                    "8942d072-99d1-ab9c-5842-ad2d5302387a",
                  ],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-48-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-5-points-item._1",
                  selectorGuids: [
                    "2aed3386-bdd5-2f1b-714c-30fb4668393a",
                    "8942d072-99d1-ab9c-5842-ad2d5302387a",
                  ],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-48-n-9",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-5-points-item._2",
                  selectorGuids: [
                    "2aed3386-bdd5-2f1b-714c-30fb4668393a",
                    "dd963bd8-3dee-2338-9e9d-f5a3652a1ca8",
                  ],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-48-n-10",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-5-points-item._2",
                  selectorGuids: [
                    "2aed3386-bdd5-2f1b-714c-30fb4668393a",
                    "dd963bd8-3dee-2338-9e9d-f5a3652a1ca8",
                  ],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-48-n-13",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-5-points-item.margin-top._3",
                  selectorGuids: [
                    "2aed3386-bdd5-2f1b-714c-30fb4668393a",
                    "8e006531-aa56-ae8d-1251-b35142ff9503",
                    "cab8a0d3-5900-61e4-e7ee-cbb0b280aabe",
                  ],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-48-n-14",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-5-points-item.margin-top._3",
                  selectorGuids: [
                    "2aed3386-bdd5-2f1b-714c-30fb4668393a",
                    "8e006531-aa56-ae8d-1251-b35142ff9503",
                    "cab8a0d3-5900-61e4-e7ee-cbb0b280aabe",
                  ],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-48-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".title-box",
                  selectorGuids: ["918e5819-5dfa-78bc-1d1f-e024b678e8d2"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-48-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".title-box",
                  selectorGuids: ["918e5819-5dfa-78bc-1d1f-e024b678e8d2"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-48-n-7",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-5-points-item._1",
                  selectorGuids: [
                    "2aed3386-bdd5-2f1b-714c-30fb4668393a",
                    "8942d072-99d1-ab9c-5842-ad2d5302387a",
                  ],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-48-n-8",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-5-points-item._1",
                  selectorGuids: [
                    "2aed3386-bdd5-2f1b-714c-30fb4668393a",
                    "8942d072-99d1-ab9c-5842-ad2d5302387a",
                  ],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-48-n-11",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-5-points-item._2",
                  selectorGuids: [
                    "2aed3386-bdd5-2f1b-714c-30fb4668393a",
                    "dd963bd8-3dee-2338-9e9d-f5a3652a1ca8",
                  ],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-48-n-12",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-5-points-item._2",
                  selectorGuids: [
                    "2aed3386-bdd5-2f1b-714c-30fb4668393a",
                    "dd963bd8-3dee-2338-9e9d-f5a3652a1ca8",
                  ],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-48-n-15",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-5-points-item.margin-top._3",
                  selectorGuids: [
                    "2aed3386-bdd5-2f1b-714c-30fb4668393a",
                    "8e006531-aa56-ae8d-1251-b35142ff9503",
                    "cab8a0d3-5900-61e4-e7ee-cbb0b280aabe",
                  ],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-48-n-16",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-5-points-item.margin-top._3",
                  selectorGuids: [
                    "2aed3386-bdd5-2f1b-714c-30fb4668393a",
                    "8e006531-aa56-ae8d-1251-b35142ff9503",
                    "cab8a0d3-5900-61e4-e7ee-cbb0b280aabe",
                  ],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598961006856,
      useFirstGroupAsInitialState: true,
    },
    "a-49": {
      id: "a-49",
      title: "home-block-6-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-49-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|f69e6d44-452f-7300-ac2e-145a6e3eb7ea",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-49-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|f69e6d44-452f-7300-ac2e-145a6e3eb7ea",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-49-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|f69e6d44-452f-7300-ac2e-145a6e3eb7ea",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-49-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|f69e6d44-452f-7300-ac2e-145a6e3eb7ea",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598974720858,
      useFirstGroupAsInitialState: true,
    },
    "a-50": {
      id: "a-50",
      title: "what-box",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-50-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|c168d2c6-a8e5-8e66-a1a6-0438fd8e7e75",
                },
                xValue: 0,
                yValue: 0,
                locked: true,
              },
            },
            {
              id: "a-50-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|c168d2c6-a8e5-8e66-a1a6-0438fd8e7e75",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-50-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|c168d2c6-a8e5-8e66-a1a6-0438fd8e7e75",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-50-n-2",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|c168d2c6-a8e5-8e66-a1a6-0438fd8e7e75",
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
          ],
        },
      ],
      createdOn: 1598974816347,
      useFirstGroupAsInitialState: true,
    },
    "a-51": {
      id: "a-51",
      title: "how-box",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-51-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|21591a31-fa34-22c7-ca0c-81b0ef462ca1",
                },
                xValue: 0,
                yValue: 0,
                locked: true,
              },
            },
            {
              id: "a-51-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|21591a31-fa34-22c7-ca0c-81b0ef462ca1",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-51-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 1000,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|21591a31-fa34-22c7-ca0c-81b0ef462ca1",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-51-n-2",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 1000,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|21591a31-fa34-22c7-ca0c-81b0ef462ca1",
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
          ],
        },
      ],
      createdOn: 1598974917429,
      useFirstGroupAsInitialState: true,
    },
    "a-52": {
      id: "a-52",
      title: "why-box",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-52-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|7540778a-1753-5c98-1041-fa9aa21fe105",
                },
                xValue: 0,
                yValue: 0,
                locked: true,
              },
            },
            {
              id: "a-52-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|7540778a-1753-5c98-1041-fa9aa21fe105",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-52-n-2",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 1500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|7540778a-1753-5c98-1041-fa9aa21fe105",
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
            {
              id: "a-52-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 1500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|7540778a-1753-5c98-1041-fa9aa21fe105",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598974961214,
      useFirstGroupAsInitialState: true,
    },
    "a-53": {
      id: "a-53",
      title: "home-block-6-title-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-53-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2a2fbcd2-319f-789b-c264-512c90ddc5e5",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-53-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2a2fbcd2-319f-789b-c264-512c90ddc5e5",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-53-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2a2fbcd2-319f-789b-c264-512c90ddc5e5",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-53-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2a2fbcd2-319f-789b-c264-512c90ddc5e5",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598975076540,
      useFirstGroupAsInitialState: true,
    },
    "a-54": {
      id: "a-54",
      title: "home-block-6-slider",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-54-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|37b982b4-26f7-688c-e820-dbeb5d391ee1",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-54-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|37b982b4-26f7-688c-e820-dbeb5d391ee1",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-54-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|37b982b4-26f7-688c-e820-dbeb5d391ee1",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-54-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|37b982b4-26f7-688c-e820-dbeb5d391ee1",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598975144398,
      useFirstGroupAsInitialState: true,
    },
    "a-55": {
      id: "a-55",
      title: "home-block-7",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-55-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-7-img",
                  selectorGuids: ["ac96f73d-fc36-bdbc-cf76-d11422aabd0d"],
                },
                xValue: 1.5,
                yValue: 1.5,
                locked: true,
              },
            },
            {
              id: "a-55-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-7-overlay",
                  selectorGuids: ["a7f9569f-c728-ec38-cc16-5509077b86ea"],
                },
                xValue: 0,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-55-n-5",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-7-overlay",
                  selectorGuids: ["a7f9569f-c728-ec38-cc16-5509077b86ea"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-55-n-3",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 3000,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-7-img",
                  selectorGuids: ["ac96f73d-fc36-bdbc-cf76-d11422aabd0d"],
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
            {
              id: "a-55-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 1500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-7-overlay",
                  selectorGuids: ["a7f9569f-c728-ec38-cc16-5509077b86ea"],
                },
                xValue: 100,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-55-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-7-overlay",
                  selectorGuids: ["a7f9569f-c728-ec38-cc16-5509077b86ea"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956730939,
      useFirstGroupAsInitialState: true,
    },
    "a-56": {
      id: "a-56",
      title: "home-block-8-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-56-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2a2fbcd2-319f-789b-c264-512c90ddc5e5",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-56-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2a2fbcd2-319f-789b-c264-512c90ddc5e5",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-56-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2a2fbcd2-319f-789b-c264-512c90ddc5e5",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-56-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2a2fbcd2-319f-789b-c264-512c90ddc5e5",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598975076540,
      useFirstGroupAsInitialState: true,
    },
    "a-57": {
      id: "a-57",
      title: "home-block-8-5points",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-57-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-5points-header",
                  selectorGuids: ["0c5fc00a-3e9a-a2b0-1fc9-44272b3a157c"],
                },
                yValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-57-n-19",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-4.gradient-4",
                  selectorGuids: [
                    "90735611-94f1-33a4-80f4-0dc2dc7ff844",
                    "2da5219d-8dff-f8b6-0db7-0f9f452e0404",
                  ],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-57-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-5points-header",
                  selectorGuids: ["0c5fc00a-3e9a-a2b0-1fc9-44272b3a157c"],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-57-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1.gradient-1",
                  selectorGuids: [
                    "65551f98-3043-c3e5-24a1-3a674885eccc",
                    "00909faa-e927-7114-5620-99a534e547ed",
                  ],
                },
                yValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-57-n-7",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1.gradient-1",
                  selectorGuids: [
                    "65551f98-3043-c3e5-24a1-3a674885eccc",
                    "00909faa-e927-7114-5620-99a534e547ed",
                  ],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-57-n-9",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-2.gradient-2",
                  selectorGuids: [
                    "bbaf3bc9-98bc-8bd8-5315-09164bdcc03f",
                    "3147b591-44f8-6f47-e94d-459af7175eae",
                  ],
                },
                yValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-57-n-11",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-2.gradient-2",
                  selectorGuids: [
                    "bbaf3bc9-98bc-8bd8-5315-09164bdcc03f",
                    "3147b591-44f8-6f47-e94d-459af7175eae",
                  ],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-57-n-13",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-3.gradient-3",
                  selectorGuids: [
                    "89e6456d-00b2-cb6a-8c62-27d69cb9cf44",
                    "5b0086a6-7e25-ba33-61ce-58119f1ab4ca",
                  ],
                },
                yValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-57-n-15",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-3.gradient-3",
                  selectorGuids: [
                    "89e6456d-00b2-cb6a-8c62-27d69cb9cf44",
                    "5b0086a6-7e25-ba33-61ce-58119f1ab4ca",
                  ],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-57-n-17",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-4.gradient-4",
                  selectorGuids: [
                    "90735611-94f1-33a4-80f4-0dc2dc7ff844",
                    "2da5219d-8dff-f8b6-0db7-0f9f452e0404",
                  ],
                },
                yValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-57-n-21",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-5.gradient-5",
                  selectorGuids: [
                    "6faa3f33-ec8f-592b-ebb4-c4084ef9df1c",
                    "0d0593da-afe9-9b89-b6af-35344d33e8f1",
                  ],
                },
                yValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-57-n-23",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-5.gradient-5",
                  selectorGuids: [
                    "6faa3f33-ec8f-592b-ebb4-c4084ef9df1c",
                    "0d0593da-afe9-9b89-b6af-35344d33e8f1",
                  ],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-57-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-5points-header",
                  selectorGuids: ["0c5fc00a-3e9a-a2b0-1fc9-44272b3a157c"],
                },
                yValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-57-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-5points-header",
                  selectorGuids: ["0c5fc00a-3e9a-a2b0-1fc9-44272b3a157c"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-57-n-6",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1.gradient-1",
                  selectorGuids: [
                    "65551f98-3043-c3e5-24a1-3a674885eccc",
                    "00909faa-e927-7114-5620-99a534e547ed",
                  ],
                },
                yValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-57-n-8",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-1.gradient-1",
                  selectorGuids: [
                    "65551f98-3043-c3e5-24a1-3a674885eccc",
                    "00909faa-e927-7114-5620-99a534e547ed",
                  ],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-57-n-10",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-2.gradient-2",
                  selectorGuids: [
                    "bbaf3bc9-98bc-8bd8-5315-09164bdcc03f",
                    "3147b591-44f8-6f47-e94d-459af7175eae",
                  ],
                },
                yValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-57-n-12",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-2.gradient-2",
                  selectorGuids: [
                    "bbaf3bc9-98bc-8bd8-5315-09164bdcc03f",
                    "3147b591-44f8-6f47-e94d-459af7175eae",
                  ],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-57-n-14",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-3.gradient-3",
                  selectorGuids: [
                    "89e6456d-00b2-cb6a-8c62-27d69cb9cf44",
                    "5b0086a6-7e25-ba33-61ce-58119f1ab4ca",
                  ],
                },
                yValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-57-n-16",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-3.gradient-3",
                  selectorGuids: [
                    "89e6456d-00b2-cb6a-8c62-27d69cb9cf44",
                    "5b0086a6-7e25-ba33-61ce-58119f1ab4ca",
                  ],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-57-n-18",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-4.gradient-4",
                  selectorGuids: [
                    "90735611-94f1-33a4-80f4-0dc2dc7ff844",
                    "2da5219d-8dff-f8b6-0db7-0f9f452e0404",
                  ],
                },
                yValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-57-n-20",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-4.gradient-4",
                  selectorGuids: [
                    "90735611-94f1-33a4-80f4-0dc2dc7ff844",
                    "2da5219d-8dff-f8b6-0db7-0f9f452e0404",
                  ],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-57-n-22",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-5.gradient-5",
                  selectorGuids: [
                    "6faa3f33-ec8f-592b-ebb4-c4084ef9df1c",
                    "0d0593da-afe9-9b89-b6af-35344d33e8f1",
                  ],
                },
                yValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-57-n-24",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-point-5.gradient-5",
                  selectorGuids: [
                    "6faa3f33-ec8f-592b-ebb4-c4084ef9df1c",
                    "0d0593da-afe9-9b89-b6af-35344d33e8f1",
                  ],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598975524184,
      useFirstGroupAsInitialState: true,
    },
    "a-58": {
      id: "a-58",
      title: "home-block-9",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-58-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-9-img",
                  selectorGuids: ["cb08894f-60c4-d885-8bf0-2a16b249393a"],
                },
                xValue: 1.5,
                yValue: 1.5,
                locked: true,
              },
            },
            {
              id: "a-58-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-9-overlay",
                  selectorGuids: ["60575714-b44e-eb0f-fa59-94b70a45d223"],
                },
                xValue: 0,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-58-n-5",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-9-overlay",
                  selectorGuids: ["60575714-b44e-eb0f-fa59-94b70a45d223"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-58-n-3",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 3000,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-9-img",
                  selectorGuids: ["cb08894f-60c4-d885-8bf0-2a16b249393a"],
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
            {
              id: "a-58-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 1500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-9-overlay",
                  selectorGuids: ["60575714-b44e-eb0f-fa59-94b70a45d223"],
                },
                xValue: 100,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-58-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-9-overlay",
                  selectorGuids: ["60575714-b44e-eb0f-fa59-94b70a45d223"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956730939,
      useFirstGroupAsInitialState: true,
    },
    "a-59": {
      id: "a-59",
      title: "home-block10-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-59-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|da6c3378-577a-29c3-9eb1-bb16429758c0",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-59-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|da6c3378-577a-29c3-9eb1-bb16429758c0",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-59-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|da6c3378-577a-29c3-9eb1-bb16429758c0",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-59-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|da6c3378-577a-29c3-9eb1-bb16429758c0",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598975969627,
      useFirstGroupAsInitialState: true,
    },
    "a-60": {
      id: "a-60",
      title: "home-block-10-graph-box",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-60-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-10-graph-box",
                  selectorGuids: ["32f5e09f-442e-0fd5-b114-372fb5b4fb9e"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-60-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-10-graph-box",
                  selectorGuids: ["32f5e09f-442e-0fd5-b114-372fb5b4fb9e"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-60-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-10-graph-box",
                  selectorGuids: ["32f5e09f-442e-0fd5-b114-372fb5b4fb9e"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-60-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-10-graph-box",
                  selectorGuids: ["32f5e09f-442e-0fd5-b114-372fb5b4fb9e"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598976061000,
      useFirstGroupAsInitialState: true,
    },
    "a-61": {
      id: "a-61",
      title: "home-block-11",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-61-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-11-img",
                  selectorGuids: ["cdf00588-7744-4687-cbf2-0be5afa3ddb7"],
                },
                xValue: 1.5,
                yValue: 1.5,
                locked: true,
              },
            },
            {
              id: "a-61-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-11-overlay",
                  selectorGuids: ["d330d90f-3abc-3fd1-e201-23c84289aecc"],
                },
                xValue: 0,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-61-n-5",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-11-overlay",
                  selectorGuids: ["d330d90f-3abc-3fd1-e201-23c84289aecc"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-61-n-3",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 3000,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-11-img",
                  selectorGuids: ["cdf00588-7744-4687-cbf2-0be5afa3ddb7"],
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
            {
              id: "a-61-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 1500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-11-overlay",
                  selectorGuids: ["d330d90f-3abc-3fd1-e201-23c84289aecc"],
                },
                xValue: 102,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-61-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-11-overlay",
                  selectorGuids: ["d330d90f-3abc-3fd1-e201-23c84289aecc"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956730939,
      useFirstGroupAsInitialState: true,
    },
    "a-62": {
      id: "a-62",
      title: "home-block-12",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-62-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-12-arrow-text",
                  selectorGuids: ["3ce2d70a-ba34-0c06-f2b5-1257faf8e747"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-62-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-12-arrow-text",
                  selectorGuids: ["3ce2d70a-ba34-0c06-f2b5-1257faf8e747"],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-62-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".next-page-anchor-link-1",
                  selectorGuids: ["bdf40476-04ac-abe4-3f4e-716394bb645a"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-62-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".next-page-anchor-link-1",
                  selectorGuids: ["bdf40476-04ac-abe4-3f4e-716394bb645a"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-62-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-12-arrow-text",
                  selectorGuids: ["3ce2d70a-ba34-0c06-f2b5-1257faf8e747"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-62-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".home-block-12-arrow-text",
                  selectorGuids: ["3ce2d70a-ba34-0c06-f2b5-1257faf8e747"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-62-n-7",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".next-page-anchor-link-1",
                  selectorGuids: ["bdf40476-04ac-abe4-3f4e-716394bb645a"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-62-n-8",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".next-page-anchor-link-1",
                  selectorGuids: ["bdf40476-04ac-abe4-3f4e-716394bb645a"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598976430836,
      useFirstGroupAsInitialState: true,
    },
    "a-63": {
      id: "a-63",
      title: "nav-bar",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-63-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|42d67e16-4a1c-23b7-eb21-5e3e35427547",
                },
                xValue: -100,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-63-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|42d67e16-4a1c-23b7-eb21-5e3e35427547",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-63-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 1000,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|42d67e16-4a1c-23b7-eb21-5e3e35427547",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-63-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 1000,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|42d67e16-4a1c-23b7-eb21-5e3e35427547",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598977208452,
      useFirstGroupAsInitialState: true,
    },
    "a-64": {
      id: "a-64",
      title: "page2-block-1-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-64-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-64-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-64-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 700,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-64-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 700,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598956371206,
      useFirstGroupAsInitialState: true,
    },
    "a-65": {
      id: "a-65",
      title: "page2-block-1-para",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-65-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-65-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-65-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 1100,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-65-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 1100,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598956552562,
      useFirstGroupAsInitialState: true,
    },
    "a-66": {
      id: "a-66",
      title: "page2-block-1-linkbox",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-66-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-66-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-66-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 1300,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-66-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 1300,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598956552562,
      useFirstGroupAsInitialState: true,
    },
    "a-67": {
      id: "a-67",
      title: "page2-block-2-title-1",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-67-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-67-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-67-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-67-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978199751,
      useFirstGroupAsInitialState: true,
    },
    "a-68": {
      id: "a-68",
      title: "page2-block-1-content-1",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-68-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-68-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-68-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-68-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598978262085,
      useFirstGroupAsInitialState: true,
    },
    "a-69": {
      id: "a-69",
      title: "page2-block-2-title-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-69-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-69-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-69-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-69-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978199751,
      useFirstGroupAsInitialState: true,
    },
    "a-70": {
      id: "a-70",
      title: "page2-block-1-content-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-70-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-70-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-70-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-70-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598978262085,
      useFirstGroupAsInitialState: true,
    },
    "a-71": {
      id: "a-71",
      title: "page2-block-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-71-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page2-block-2-img",
                  selectorGuids: ["bd82f5d0-e633-9c39-8a27-d455d2a99a5e"],
                },
                xValue: 1.5,
                yValue: 1.5,
                locked: true,
              },
            },
            {
              id: "a-71-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page2-block-2-overlay",
                  selectorGuids: ["b7b589dc-1140-f25e-74c2-6755f10b50f8"],
                },
                xValue: 0,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-71-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page2-block-2-overlay",
                  selectorGuids: ["b7b589dc-1140-f25e-74c2-6755f10b50f8"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-71-n-4",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 3000,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page2-block-2-img",
                  selectorGuids: ["bd82f5d0-e633-9c39-8a27-d455d2a99a5e"],
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
            {
              id: "a-71-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 1500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page2-block-2-overlay",
                  selectorGuids: ["b7b589dc-1140-f25e-74c2-6755f10b50f8"],
                },
                xValue: 102,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-71-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page2-block-2-overlay",
                  selectorGuids: ["b7b589dc-1140-f25e-74c2-6755f10b50f8"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956730939,
      useFirstGroupAsInitialState: true,
    },
    "a-72": {
      id: "a-72",
      title: "page2-block-3-title-1",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-72-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-72-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-72-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-72-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-73": {
      id: "a-73",
      title: "page2-block-3-bubble-chart",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-73-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-73-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-73-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-73-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598978730667,
      useFirstGroupAsInitialState: true,
    },
    "a-74": {
      id: "a-74",
      title: "page2-block-3-title-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-74-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-74-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-74-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-74-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-75": {
      id: "a-75",
      title: "page2-block-3-slider",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-75-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-75-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-75-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-75-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598978730667,
      useFirstGroupAsInitialState: true,
    },
    "a-76": {
      id: "a-76",
      title: "page2-block-4",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-76-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page2-block-4-img",
                  selectorGuids: ["ece8afff-b8a9-99b5-1496-09263a5742ad"],
                },
                xValue: 1.5,
                yValue: 1.5,
                locked: true,
              },
            },
            {
              id: "a-76-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page2-block-4-overlay",
                  selectorGuids: ["50b014f8-70ba-6faf-894b-ccbb83a27da6"],
                },
                xValue: 0,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-76-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page2-block-4-overlay",
                  selectorGuids: ["50b014f8-70ba-6faf-894b-ccbb83a27da6"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-76-n-4",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 3000,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page2-block-4-img",
                  selectorGuids: ["ece8afff-b8a9-99b5-1496-09263a5742ad"],
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
            {
              id: "a-76-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 1500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page2-block-4-overlay",
                  selectorGuids: ["50b014f8-70ba-6faf-894b-ccbb83a27da6"],
                },
                xValue: 102,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-76-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page2-block-4-overlay",
                  selectorGuids: ["50b014f8-70ba-6faf-894b-ccbb83a27da6"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956730939,
      useFirstGroupAsInitialState: true,
    },
    "a-77": {
      id: "a-77",
      title: "page2-block-5",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-77-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page2-block-5-arrow-text",
                  selectorGuids: ["75c2734c-fa76-28f0-dcc8-fb3d84876129"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-77-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page2-block-5-arrow-text",
                  selectorGuids: ["75c2734c-fa76-28f0-dcc8-fb3d84876129"],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-77-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".next-page-anchor-link-2",
                  selectorGuids: ["10c43be0-b733-8f50-ef6f-f38e65cc52ec"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-77-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".next-page-anchor-link-2",
                  selectorGuids: ["10c43be0-b733-8f50-ef6f-f38e65cc52ec"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-77-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page2-block-5-arrow-text",
                  selectorGuids: ["75c2734c-fa76-28f0-dcc8-fb3d84876129"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-77-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page2-block-5-arrow-text",
                  selectorGuids: ["75c2734c-fa76-28f0-dcc8-fb3d84876129"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-77-n-7",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".next-page-anchor-link-2",
                  selectorGuids: ["10c43be0-b733-8f50-ef6f-f38e65cc52ec"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-77-n-8",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".next-page-anchor-link-2",
                  selectorGuids: ["10c43be0-b733-8f50-ef6f-f38e65cc52ec"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598976430836,
      useFirstGroupAsInitialState: true,
    },
    "a-78": {
      id: "a-78",
      title: "page3-block-1-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-78-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-78-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-78-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 700,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-78-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 700,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598956371206,
      useFirstGroupAsInitialState: true,
    },
    "a-79": {
      id: "a-79",
      title: "page3-block-1-para",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-79-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-79-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-79-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 1100,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-79-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 1100,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598956552562,
      useFirstGroupAsInitialState: true,
    },
    "a-80": {
      id: "a-80",
      title: "page3-block-1-linkbox",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-80-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-80-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-80-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 1500,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-80-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 1500,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956552562,
      useFirstGroupAsInitialState: true,
    },
    "a-81": {
      id: "a-81",
      title: "page3-block-1-title-1",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-81-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-81-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-81-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-81-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978199751,
      useFirstGroupAsInitialState: true,
    },
    "a-82": {
      id: "a-82",
      title: "page3-block-1-content-1",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-82-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-82-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-82-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-82-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598978262085,
      useFirstGroupAsInitialState: true,
    },
    "a-83": {
      id: "a-83",
      title: "page3-block-1-title-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-83-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-83-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-83-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-83-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978199751,
      useFirstGroupAsInitialState: true,
    },
    "a-84": {
      id: "a-84",
      title: "page3-block-1-content-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-84-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-84-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-84-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-84-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598978262085,
      useFirstGroupAsInitialState: true,
    },
    "a-85": {
      id: "a-85",
      title: "page3-block-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-85-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-2-img",
                  selectorGuids: ["c848978f-9142-3f8a-0339-6c0c28df2362"],
                },
                xValue: 1.5,
                yValue: 1.5,
                locked: true,
              },
            },
            {
              id: "a-85-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-2-overlay",
                  selectorGuids: ["768c97a7-c463-ad47-fab4-20415f3d3e2f"],
                },
                xValue: 0,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-85-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-2-overlay",
                  selectorGuids: ["768c97a7-c463-ad47-fab4-20415f3d3e2f"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-85-n-4",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 3000,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-2-img",
                  selectorGuids: ["c848978f-9142-3f8a-0339-6c0c28df2362"],
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
            {
              id: "a-85-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 1500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-2-overlay",
                  selectorGuids: ["768c97a7-c463-ad47-fab4-20415f3d3e2f"],
                },
                xValue: 102,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-85-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-2-overlay",
                  selectorGuids: ["768c97a7-c463-ad47-fab4-20415f3d3e2f"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956730939,
      useFirstGroupAsInitialState: true,
    },
    "a-86": {
      id: "a-86",
      title: "page3-block-3-title-1",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-86-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-86-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-86-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-86-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-87": {
      id: "a-87",
      title: "page3-block-3-linkbox",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-87-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-87-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-87-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-87-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598978730667,
      useFirstGroupAsInitialState: true,
    },
    "a-93": {
      id: "a-93",
      title: "page3-block-5-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-93-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-93-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-93-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-93-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-88": {
      id: "a-88",
      title: "page3-block-3-title-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-88-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-88-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-88-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-88-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-89": {
      id: "a-89",
      title: "page3-block-3-slider",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-89-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-89-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-89-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-89-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598978730667,
      useFirstGroupAsInitialState: true,
    },
    "a-90": {
      id: "a-90",
      title: "page3-block-4-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-90-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-90-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-90-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-90-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-91": {
      id: "a-91",
      title: "page3-block-4-img-1",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-91-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-91-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-91-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-91-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598978730667,
      useFirstGroupAsInitialState: true,
    },
    "a-92": {
      id: "a-92",
      title: "page3-block-4-img-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-92-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-92-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-92-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 1000,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-92-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 1000,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598978730667,
      useFirstGroupAsInitialState: true,
    },
    "a-94": {
      id: "a-94",
      title: "page3-block-5-slider",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-94-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-94-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-94-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-94-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598978730667,
      useFirstGroupAsInitialState: true,
    },
    "a-95": {
      id: "a-95",
      title: "page3-block-6-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-95-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-95-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-95-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-95-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-96": {
      id: "a-96",
      title: "page3-block-6-wrapper-item",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-96-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-6-wrapper-item.gradient-2",
                  selectorGuids: [
                    "036f4438-3c9c-b916-f3a2-cfdeb23032f2",
                    "d6d38ef6-b855-9a18-674b-d520ce268d40",
                  ],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-96-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-6-wrapper-item.gradient-2",
                  selectorGuids: [
                    "036f4438-3c9c-b916-f3a2-cfdeb23032f2",
                    "d6d38ef6-b855-9a18-674b-d520ce268d40",
                  ],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-96-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-6-wrapper-item.gradient-2",
                  selectorGuids: [
                    "036f4438-3c9c-b916-f3a2-cfdeb23032f2",
                    "d6d38ef6-b855-9a18-674b-d520ce268d40",
                  ],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-96-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-6-wrapper-item.gradient-2",
                  selectorGuids: [
                    "036f4438-3c9c-b916-f3a2-cfdeb23032f2",
                    "d6d38ef6-b855-9a18-674b-d520ce268d40",
                  ],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598981572044,
      useFirstGroupAsInitialState: true,
    },
    "a-97": {
      id: "a-97",
      title: "page3-block-7",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-97-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-7-img",
                  selectorGuids: ["1aaba784-0e48-c9ab-16b2-20a3ec8a4eac"],
                },
                xValue: 1.5,
                yValue: 1.5,
                locked: true,
              },
            },
            {
              id: "a-97-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-7-overlay",
                  selectorGuids: ["fa29fc7f-a71e-732f-6ad3-1fc7fd6e7a91"],
                },
                xValue: 0,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-97-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-7-overlay",
                  selectorGuids: ["fa29fc7f-a71e-732f-6ad3-1fc7fd6e7a91"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-97-n-4",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 3000,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-7-img",
                  selectorGuids: ["1aaba784-0e48-c9ab-16b2-20a3ec8a4eac"],
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
            {
              id: "a-97-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 1500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-7-overlay",
                  selectorGuids: ["fa29fc7f-a71e-732f-6ad3-1fc7fd6e7a91"],
                },
                xValue: 102,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-97-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-7-overlay",
                  selectorGuids: ["fa29fc7f-a71e-732f-6ad3-1fc7fd6e7a91"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956730939,
      useFirstGroupAsInitialState: true,
    },
    "a-98": {
      id: "a-98",
      title: "page3-block-8-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-98-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-98-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-98-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-98-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-99": {
      id: "a-99",
      title: "page3-block-8-slider",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-99-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-99-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-99-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-99-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598978730667,
      useFirstGroupAsInitialState: true,
    },
    "a-100": {
      id: "a-100",
      title: "page3-block-9-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-100-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-100-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-100-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-100-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-101": {
      id: "a-101",
      title: "page3-block-9-flowchart",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-101-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-101-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-101-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-101-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|1b82bc38-34ec-132b-8efc-bf5ae00aa7a9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598978730667,
      useFirstGroupAsInitialState: true,
    },
    "a-102": {
      id: "a-102",
      title: "page3-block-10-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-102-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-102-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-102-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-102-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-103": {
      id: "a-103",
      title: "page3-block-10-wrapper-item",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-103-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-10-wrapper-item",
                  selectorGuids: ["6c95c7b6-ea63-437b-47e8-d7b2564e1282"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-103-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-10-wrapper-item",
                  selectorGuids: ["6c95c7b6-ea63-437b-47e8-d7b2564e1282"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-103-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-10-wrapper-item",
                  selectorGuids: ["6c95c7b6-ea63-437b-47e8-d7b2564e1282"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-103-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-10-wrapper-item",
                  selectorGuids: ["6c95c7b6-ea63-437b-47e8-d7b2564e1282"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598981572044,
      useFirstGroupAsInitialState: true,
    },
    "a-104": {
      id: "a-104",
      title: "page3-block-11",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-104-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-11-img",
                  selectorGuids: ["f0751096-6763-93ce-c4c6-255a335321b4"],
                },
                xValue: 1.5,
                yValue: 1.5,
                locked: true,
              },
            },
            {
              id: "a-104-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-11-overlay",
                  selectorGuids: ["5e874e9e-1bec-2c3a-843c-b5ff356fe4da"],
                },
                xValue: 0,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-104-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-11-overlay",
                  selectorGuids: ["5e874e9e-1bec-2c3a-843c-b5ff356fe4da"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-104-n-4",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 3000,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-11-img",
                  selectorGuids: ["f0751096-6763-93ce-c4c6-255a335321b4"],
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
            {
              id: "a-104-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 1500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-11-overlay",
                  selectorGuids: ["5e874e9e-1bec-2c3a-843c-b5ff356fe4da"],
                },
                xValue: 102,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-104-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page3-block-11-overlay",
                  selectorGuids: ["5e874e9e-1bec-2c3a-843c-b5ff356fe4da"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956730939,
      useFirstGroupAsInitialState: true,
    },
    "a-105": {
      id: "a-105",
      title: "page3-block-12",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-105-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page2-block-5-arrow-text",
                  selectorGuids: ["75c2734c-fa76-28f0-dcc8-fb3d84876129"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-105-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page2-block-5-arrow-text",
                  selectorGuids: ["75c2734c-fa76-28f0-dcc8-fb3d84876129"],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-105-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".next-page-anchor-link-3",
                  selectorGuids: ["f97e6628-605f-6083-d571-336a0a5fbb15"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-105-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".next-page-anchor-link-3",
                  selectorGuids: ["f97e6628-605f-6083-d571-336a0a5fbb15"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-105-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page2-block-5-arrow-text",
                  selectorGuids: ["75c2734c-fa76-28f0-dcc8-fb3d84876129"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-105-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page2-block-5-arrow-text",
                  selectorGuids: ["75c2734c-fa76-28f0-dcc8-fb3d84876129"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-105-n-7",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".next-page-anchor-link-3",
                  selectorGuids: ["f97e6628-605f-6083-d571-336a0a5fbb15"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-105-n-8",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".next-page-anchor-link-3",
                  selectorGuids: ["f97e6628-605f-6083-d571-336a0a5fbb15"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598976430836,
      useFirstGroupAsInitialState: true,
    },
    "a-106": {
      id: "a-106",
      title: "page4-block-1-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-106-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-106-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-106-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 700,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-106-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 700,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598956371206,
      useFirstGroupAsInitialState: true,
    },
    "a-107": {
      id: "a-107",
      title: "page4-block-1-para",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-107-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-107-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-107-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 1100,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-107-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 1100,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598956552562,
      useFirstGroupAsInitialState: true,
    },
    "a-108": {
      id: "a-108",
      title: "page4-block-1-linkbox",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-108-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-108-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-108-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 1500,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-108-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 1500,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956552562,
      useFirstGroupAsInitialState: true,
    },
    "a-109": {
      id: "a-109",
      title: "page4-block-1-title-1",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-109-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-109-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-109-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-109-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978199751,
      useFirstGroupAsInitialState: true,
    },
    "a-110": {
      id: "a-110",
      title: "page4-block-1-content-1",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-110-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-110-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-110-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-110-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598978262085,
      useFirstGroupAsInitialState: true,
    },
    "a-111": {
      id: "a-111",
      title: "page4-block-1-title-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-111-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-111-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-111-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-111-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978199751,
      useFirstGroupAsInitialState: true,
    },
    "a-112": {
      id: "a-112",
      title: "page4-block-1-content-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-112-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-112-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-112-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-112-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598978262085,
      useFirstGroupAsInitialState: true,
    },
    "a-113": {
      id: "a-113",
      title: "page4-block-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-113-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page4-block-2-img",
                  selectorGuids: ["c4f35a12-7cd9-1d91-69b6-e4f4ff1308da"],
                },
                xValue: 1.5,
                yValue: 1.5,
                locked: true,
              },
            },
            {
              id: "a-113-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page4-block-2-overlay",
                  selectorGuids: ["ecb80b97-8927-d932-6e79-bc2f506b0852"],
                },
                xValue: 0,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-113-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page4-block-2-overlay",
                  selectorGuids: ["ecb80b97-8927-d932-6e79-bc2f506b0852"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-113-n-4",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 3000,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page4-block-2-img",
                  selectorGuids: ["c4f35a12-7cd9-1d91-69b6-e4f4ff1308da"],
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
            {
              id: "a-113-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 1500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page4-block-2-overlay",
                  selectorGuids: ["ecb80b97-8927-d932-6e79-bc2f506b0852"],
                },
                xValue: 102,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-113-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page4-block-2-overlay",
                  selectorGuids: ["ecb80b97-8927-d932-6e79-bc2f506b0852"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956730939,
      useFirstGroupAsInitialState: true,
    },
    "a-114": {
      id: "a-114",
      title: "page4-block-3-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-114-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-114-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-114-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-114-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-115": {
      id: "a-115",
      title: "page4-block-3-content",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-115-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-115-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-115-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-115-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598978262085,
      useFirstGroupAsInitialState: true,
    },
    "a-116": {
      id: "a-116",
      title: "page4-block-4-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-116-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-116-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-116-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-116-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-117": {
      id: "a-117",
      title: "page4-block-4-piechart",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-117-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b331cf2a8c23c8dfc9a0|b66dd577-63b9-ca57-8bab-15124aabeff6",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-117-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b331cf2a8c23c8dfc9a0|b66dd577-63b9-ca57-8bab-15124aabeff6",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-117-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b331cf2a8c23c8dfc9a0|b66dd577-63b9-ca57-8bab-15124aabeff6",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-117-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b331cf2a8c23c8dfc9a0|b66dd577-63b9-ca57-8bab-15124aabeff6",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598983083333,
      useFirstGroupAsInitialState: true,
    },
    "a-118": {
      id: "a-118",
      title: "page4-block-5-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-118-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-118-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-118-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-118-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-119": {
      id: "a-119",
      title: "page4-block-5-piechart",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-119-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b331cf2a8c23c8dfc9a0|b66dd577-63b9-ca57-8bab-15124aabeff6",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-119-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b331cf2a8c23c8dfc9a0|b66dd577-63b9-ca57-8bab-15124aabeff6",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-119-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b331cf2a8c23c8dfc9a0|b66dd577-63b9-ca57-8bab-15124aabeff6",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-119-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b331cf2a8c23c8dfc9a0|b66dd577-63b9-ca57-8bab-15124aabeff6",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598983083333,
      useFirstGroupAsInitialState: true,
    },
    "a-120": {
      id: "a-120",
      title: "page4-block-6",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-120-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page4-block-6-img",
                  selectorGuids: ["04f5ad33-853e-f40a-5ec0-95090b44df2d"],
                },
                xValue: 1.5,
                yValue: 1.5,
                locked: true,
              },
            },
            {
              id: "a-120-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page4-block-6-overlay",
                  selectorGuids: ["f3ca9d52-1178-6970-2ff1-fa61a6a76425"],
                },
                xValue: 0,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-120-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page4-block-6-overlay",
                  selectorGuids: ["f3ca9d52-1178-6970-2ff1-fa61a6a76425"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-120-n-4",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 3000,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page4-block-6-img",
                  selectorGuids: ["04f5ad33-853e-f40a-5ec0-95090b44df2d"],
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
            {
              id: "a-120-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 1500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page4-block-6-overlay",
                  selectorGuids: ["f3ca9d52-1178-6970-2ff1-fa61a6a76425"],
                },
                xValue: 102,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-120-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page4-block-6-overlay",
                  selectorGuids: ["f3ca9d52-1178-6970-2ff1-fa61a6a76425"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956730939,
      useFirstGroupAsInitialState: true,
    },
    "a-121": {
      id: "a-121",
      title: "page4-block-7",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-121-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page4-block-7-arrow-text",
                  selectorGuids: ["8fe519fc-fc81-8570-b5fa-66e6721a7765"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-121-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page4-block-7-arrow-text",
                  selectorGuids: ["8fe519fc-fc81-8570-b5fa-66e6721a7765"],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-121-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".next-page-anchor-link-4",
                  selectorGuids: ["573a301c-003a-b606-b58e-8528e838fffa"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-121-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".next-page-anchor-link-4",
                  selectorGuids: ["573a301c-003a-b606-b58e-8528e838fffa"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-121-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page4-block-7-arrow-text",
                  selectorGuids: ["8fe519fc-fc81-8570-b5fa-66e6721a7765"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-121-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page4-block-7-arrow-text",
                  selectorGuids: ["8fe519fc-fc81-8570-b5fa-66e6721a7765"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-121-n-7",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".next-page-anchor-link-4",
                  selectorGuids: ["573a301c-003a-b606-b58e-8528e838fffa"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-121-n-8",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".next-page-anchor-link-4",
                  selectorGuids: ["573a301c-003a-b606-b58e-8528e838fffa"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598976430836,
      useFirstGroupAsInitialState: true,
    },
    "a-122": {
      id: "a-122",
      title: "page5-block-1-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-122-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-122-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-122-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 700,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-122-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 700,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598956371206,
      useFirstGroupAsInitialState: true,
    },
    "a-123": {
      id: "a-123",
      title: "page5-block-1-para",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-123-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-123-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-123-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 1100,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-123-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 1100,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598956552562,
      useFirstGroupAsInitialState: true,
    },
    "a-124": {
      id: "a-124",
      title: "page5-block-1-linkbox",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-124-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-124-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-124-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 1500,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-124-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 1500,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956552562,
      useFirstGroupAsInitialState: true,
    },
    "a-125": {
      id: "a-125",
      title: "page5-block-1-title-1",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-125-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-125-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-125-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-125-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978199751,
      useFirstGroupAsInitialState: true,
    },
    "a-126": {
      id: "a-126",
      title: "page5-block-1-content-1",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-126-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-126-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-126-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-126-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598978262085,
      useFirstGroupAsInitialState: true,
    },
    "a-127": {
      id: "a-127",
      title: "page5-block-1-title-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-127-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-127-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-127-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-127-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978199751,
      useFirstGroupAsInitialState: true,
    },
    "a-128": {
      id: "a-128",
      title: "page5-block-1-content-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-128-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-128-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-128-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-128-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598978262085,
      useFirstGroupAsInitialState: true,
    },
    "a-129": {
      id: "a-129",
      title: "page5-block-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-129-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-2-img",
                  selectorGuids: ["32227435-2d53-ad8c-8242-ced943e4862a"],
                },
                xValue: 1.5,
                yValue: 1.5,
                locked: true,
              },
            },
            {
              id: "a-129-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-2-overlay",
                  selectorGuids: ["6162e825-2083-ec1f-5a7d-1c535201b1ed"],
                },
                xValue: 0,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-129-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-2-overlay",
                  selectorGuids: ["6162e825-2083-ec1f-5a7d-1c535201b1ed"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-129-n-4",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 3000,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-2-img",
                  selectorGuids: ["32227435-2d53-ad8c-8242-ced943e4862a"],
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
            {
              id: "a-129-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 1500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-2-overlay",
                  selectorGuids: ["6162e825-2083-ec1f-5a7d-1c535201b1ed"],
                },
                xValue: 102,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-129-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-2-overlay",
                  selectorGuids: ["6162e825-2083-ec1f-5a7d-1c535201b1ed"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956730939,
      useFirstGroupAsInitialState: true,
    },
    "a-130": {
      id: "a-130",
      title: "page5-block-3-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-130-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-130-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-130-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-130-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-131": {
      id: "a-131",
      title: "page5-block-3-graph",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-131-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-3-graph-1",
                  selectorGuids: ["e79b8087-4907-4ecb-231f-1c1ada8b2cb7"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-131-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-3-graph-1",
                  selectorGuids: ["e79b8087-4907-4ecb-231f-1c1ada8b2cb7"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-131-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-3-graph-1",
                  selectorGuids: ["e79b8087-4907-4ecb-231f-1c1ada8b2cb7"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-131-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-3-graph-1",
                  selectorGuids: ["e79b8087-4907-4ecb-231f-1c1ada8b2cb7"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598984029549,
      useFirstGroupAsInitialState: true,
    },
    "a-132": {
      id: "a-132",
      title: "page5-block-4-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-132-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-132-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-132-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-132-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-133": {
      id: "a-133",
      title: "page5-block-4-graph",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-133-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-4-graph-1",
                  selectorGuids: ["fc0efa68-a92f-3a6a-c2e3-864574dea939"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-133-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-4-graph-1",
                  selectorGuids: ["fc0efa68-a92f-3a6a-c2e3-864574dea939"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-133-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-4-graph-1",
                  selectorGuids: ["fc0efa68-a92f-3a6a-c2e3-864574dea939"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-133-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-4-graph-1",
                  selectorGuids: ["fc0efa68-a92f-3a6a-c2e3-864574dea939"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598984029549,
      useFirstGroupAsInitialState: true,
    },
    "a-134": {
      id: "a-134",
      title: "page5-block-5-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-134-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-134-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-134-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-134-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-135": {
      id: "a-135",
      title: "page5-block-5-graph",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-135-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-5-graph-1",
                  selectorGuids: ["04182b98-3cd1-5d3b-4fde-df26c07e78d9"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-135-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-5-graph-1",
                  selectorGuids: ["04182b98-3cd1-5d3b-4fde-df26c07e78d9"],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-135-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "SIBLINGS",
                  selector: ".page5-block-5-graph-2",
                  selectorGuids: ["ae49ad16-375c-448b-41db-c2c99e976c43"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-135-n-7",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "SIBLINGS",
                  selector: ".page5-block-5-graph-2",
                  selectorGuids: ["ae49ad16-375c-448b-41db-c2c99e976c43"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-135-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-5-graph-1",
                  selectorGuids: ["04182b98-3cd1-5d3b-4fde-df26c07e78d9"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-135-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-5-graph-1",
                  selectorGuids: ["04182b98-3cd1-5d3b-4fde-df26c07e78d9"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-135-n-6",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "SIBLINGS",
                  selector: ".page5-block-5-graph-2",
                  selectorGuids: ["ae49ad16-375c-448b-41db-c2c99e976c43"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-135-n-8",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "SIBLINGS",
                  selector: ".page5-block-5-graph-2",
                  selectorGuids: ["ae49ad16-375c-448b-41db-c2c99e976c43"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598984029549,
      useFirstGroupAsInitialState: true,
    },
    "a-136": {
      id: "a-136",
      title: "page5-block-6-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-136-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-136-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-136-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-136-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-137": {
      id: "a-137",
      title: "page5-block-6-graph",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-137-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-6-graph-1",
                  selectorGuids: ["2ee9dd75-3444-88fb-3475-ed9846275555"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-137-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-6-graph-1",
                  selectorGuids: ["2ee9dd75-3444-88fb-3475-ed9846275555"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-137-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-6-graph-1",
                  selectorGuids: ["2ee9dd75-3444-88fb-3475-ed9846275555"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-137-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-6-graph-1",
                  selectorGuids: ["2ee9dd75-3444-88fb-3475-ed9846275555"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598984029549,
      useFirstGroupAsInitialState: true,
    },
    "a-138": {
      id: "a-138",
      title: "page5-block-7-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-138-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-138-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-138-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-138-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-139": {
      id: "a-139",
      title: "page5-block-7-graph",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-139-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-7-graph-1",
                  selectorGuids: ["6a3579fe-a65a-020c-870c-9913900babe6"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-139-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-7-graph-1",
                  selectorGuids: ["6a3579fe-a65a-020c-870c-9913900babe6"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-139-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-7-graph-1",
                  selectorGuids: ["6a3579fe-a65a-020c-870c-9913900babe6"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-139-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-7-graph-1",
                  selectorGuids: ["6a3579fe-a65a-020c-870c-9913900babe6"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598984029549,
      useFirstGroupAsInitialState: true,
    },
    "a-140": {
      id: "a-140",
      title: "page5-block-8",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-140-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-8-img",
                  selectorGuids: ["0d63f9a5-f033-702c-a55d-dbb730cc1e8b"],
                },
                xValue: 1.5,
                yValue: 1.5,
                locked: true,
              },
            },
            {
              id: "a-140-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-8-overlay",
                  selectorGuids: ["d08b7c6c-a0fb-00ae-8493-e562ee77a01a"],
                },
                xValue: 0,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-140-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-8-overlay",
                  selectorGuids: ["d08b7c6c-a0fb-00ae-8493-e562ee77a01a"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-140-n-4",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 3000,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-8-img",
                  selectorGuids: ["0d63f9a5-f033-702c-a55d-dbb730cc1e8b"],
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
            {
              id: "a-140-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 1500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-8-overlay",
                  selectorGuids: ["d08b7c6c-a0fb-00ae-8493-e562ee77a01a"],
                },
                xValue: 102,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-140-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-8-overlay",
                  selectorGuids: ["d08b7c6c-a0fb-00ae-8493-e562ee77a01a"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956730939,
      useFirstGroupAsInitialState: true,
    },
    "a-141": {
      id: "a-141",
      title: "page5-block-9",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-141-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-9-arrow-text",
                  selectorGuids: ["b029712b-f1ab-ab07-557d-922a2c5f5c8a"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-141-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-9-arrow-text",
                  selectorGuids: ["b029712b-f1ab-ab07-557d-922a2c5f5c8a"],
                },
                value: 0,
                unit: "",
              },
            },
            {
              id: "a-141-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".next-page-anchor-link-5",
                  selectorGuids: ["e6484ac5-b664-a2a9-6f32-f45059d08d04"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-141-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".next-page-anchor-link-5",
                  selectorGuids: ["e6484ac5-b664-a2a9-6f32-f45059d08d04"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-141-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-9-arrow-text",
                  selectorGuids: ["b029712b-f1ab-ab07-557d-922a2c5f5c8a"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-141-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page5-block-9-arrow-text",
                  selectorGuids: ["b029712b-f1ab-ab07-557d-922a2c5f5c8a"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-141-n-7",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".next-page-anchor-link-5",
                  selectorGuids: ["e6484ac5-b664-a2a9-6f32-f45059d08d04"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-141-n-8",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".next-page-anchor-link-5",
                  selectorGuids: ["e6484ac5-b664-a2a9-6f32-f45059d08d04"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598976430836,
      useFirstGroupAsInitialState: true,
    },
    "a-142": {
      id: "a-142",
      title: "page6-block-1-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-142-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-142-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-142-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 700,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-142-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 700,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598956371206,
      useFirstGroupAsInitialState: true,
    },
    "a-143": {
      id: "a-143",
      title: "page6-block-1-para",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-143-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-143-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-143-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 1100,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-143-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 1100,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|35a8c7fb-2b9c-98cf-fdfb-10a12529a456",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598956552562,
      useFirstGroupAsInitialState: true,
    },
    "a-144": {
      id: "a-144",
      title: "page6-block-1-linkbox",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-144-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-144-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-144-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 1500,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-144-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 1500,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956552562,
      useFirstGroupAsInitialState: true,
    },
    "a-145": {
      id: "a-145",
      title: "page6-block-1-title-1",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-145-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-145-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-145-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-145-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978199751,
      useFirstGroupAsInitialState: true,
    },
    "a-146": {
      id: "a-146",
      title: "page6-block-1-content-1",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-146-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-146-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-146-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-146-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598978262085,
      useFirstGroupAsInitialState: true,
    },
    "a-147": {
      id: "a-147",
      title: "page6-block-1-title-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-147-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-147-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-147-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-147-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|d347802c-0f5f-2c1b-4296-8bed4b1f7a35",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978199751,
      useFirstGroupAsInitialState: true,
    },
    "a-148": {
      id: "a-148",
      title: "page6-block-1-content-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-148-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-148-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-148-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-148-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|c08faa30-76e3-3e35-7646-18fd5dcb28b7",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598978262085,
      useFirstGroupAsInitialState: true,
    },
    "a-149": {
      id: "a-149",
      title: "page6-block-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-149-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page6-block-2-img",
                  selectorGuids: ["f550a9cc-66c4-0255-70f9-2f2d5cac67be"],
                },
                xValue: 1.5,
                yValue: 1.5,
                locked: true,
              },
            },
            {
              id: "a-149-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page6-block-2-overlay",
                  selectorGuids: ["de9832b6-942b-91cb-030f-9fdc1058ab91"],
                },
                xValue: 0,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-149-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page6-block-2-overlay",
                  selectorGuids: ["de9832b6-942b-91cb-030f-9fdc1058ab91"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-149-n-4",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 3000,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page6-block-2-img",
                  selectorGuids: ["f550a9cc-66c4-0255-70f9-2f2d5cac67be"],
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
            {
              id: "a-149-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 1500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page6-block-2-overlay",
                  selectorGuids: ["de9832b6-942b-91cb-030f-9fdc1058ab91"],
                },
                xValue: 102,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-149-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page6-block-2-overlay",
                  selectorGuids: ["de9832b6-942b-91cb-030f-9fdc1058ab91"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956730939,
      useFirstGroupAsInitialState: true,
    },
    "a-150": {
      id: "a-150",
      title: "page6-block-3-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-150-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-150-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-150-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-150-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-151": {
      id: "a-151",
      title: "page6-block-3-graph-1",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-151-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|9ad377f3-3581-1bee-9eb5-00c617c707a8",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-151-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|9ad377f3-3581-1bee-9eb5-00c617c707a8",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-151-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|9ad377f3-3581-1bee-9eb5-00c617c707a8",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-151-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|9ad377f3-3581-1bee-9eb5-00c617c707a8",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598985564385,
      useFirstGroupAsInitialState: true,
    },
    "a-152": {
      id: "a-152",
      title: "page6-block-3-graph-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-152-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|9ad377f3-3581-1bee-9eb5-00c617c707a8",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-152-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|9ad377f3-3581-1bee-9eb5-00c617c707a8",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-152-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|9ad377f3-3581-1bee-9eb5-00c617c707a8",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-152-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|9ad377f3-3581-1bee-9eb5-00c617c707a8",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598985564385,
      useFirstGroupAsInitialState: true,
    },
    "a-153": {
      id: "a-153",
      title: "page6-block-3-graph-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-153-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|9ad377f3-3581-1bee-9eb5-00c617c707a8",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-153-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|9ad377f3-3581-1bee-9eb5-00c617c707a8",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-153-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|9ad377f3-3581-1bee-9eb5-00c617c707a8",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-153-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|9ad377f3-3581-1bee-9eb5-00c617c707a8",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598985564385,
      useFirstGroupAsInitialState: true,
    },
    "a-154": {
      id: "a-154",
      title: "page6-block-3-graph-4",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-154-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|9ad377f3-3581-1bee-9eb5-00c617c707a8",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-154-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|9ad377f3-3581-1bee-9eb5-00c617c707a8",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-154-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|9ad377f3-3581-1bee-9eb5-00c617c707a8",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-154-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|9ad377f3-3581-1bee-9eb5-00c617c707a8",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598985564385,
      useFirstGroupAsInitialState: true,
    },
    "a-155": {
      id: "a-155",
      title: "page6-block-4-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-155-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-155-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-155-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-155-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-156": {
      id: "a-156",
      title: "page6-block-4-graph",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-156-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|d6f2fa5b-7158-b39d-8649-cf50b8c6b2d7",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-156-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|d6f2fa5b-7158-b39d-8649-cf50b8c6b2d7",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-156-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|d6f2fa5b-7158-b39d-8649-cf50b8c6b2d7",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-156-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|d6f2fa5b-7158-b39d-8649-cf50b8c6b2d7",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598986066365,
      useFirstGroupAsInitialState: true,
    },
    "a-157": {
      id: "a-157",
      title: "page6-block-5-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-157-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-157-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-157-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-157-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-158": {
      id: "a-158",
      title: "page6-block-5-graph-1",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-158-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|d6f2fa5b-7158-b39d-8649-cf50b8c6b2d7",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-158-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|d6f2fa5b-7158-b39d-8649-cf50b8c6b2d7",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-158-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|d6f2fa5b-7158-b39d-8649-cf50b8c6b2d7",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-158-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|d6f2fa5b-7158-b39d-8649-cf50b8c6b2d7",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598986066365,
      useFirstGroupAsInitialState: true,
    },
    "a-159": {
      id: "a-159",
      title: "page6-block-5-graph-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-159-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|d6f2fa5b-7158-b39d-8649-cf50b8c6b2d7",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-159-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|d6f2fa5b-7158-b39d-8649-cf50b8c6b2d7",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-159-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|d6f2fa5b-7158-b39d-8649-cf50b8c6b2d7",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-159-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b709d4850bc6bd122bf0|d6f2fa5b-7158-b39d-8649-cf50b8c6b2d7",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598986066365,
      useFirstGroupAsInitialState: true,
    },
    "a-160": {
      id: "a-160",
      title: "page6-block-6",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-160-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page6-block-6-img",
                  selectorGuids: ["9a776fc6-acbe-4c1d-4527-8321e515c3bc"],
                },
                xValue: 1.5,
                yValue: 1.5,
                locked: true,
              },
            },
            {
              id: "a-160-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page6-block-6-overlay",
                  selectorGuids: ["cc48b548-7675-2a60-bc55-8675d4474a76"],
                },
                xValue: 0,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-160-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page6-block-6-overlay",
                  selectorGuids: ["cc48b548-7675-2a60-bc55-8675d4474a76"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-160-n-4",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 3000,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page6-block-6-img",
                  selectorGuids: ["9a776fc6-acbe-4c1d-4527-8321e515c3bc"],
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
            {
              id: "a-160-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 1500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page6-block-6-overlay",
                  selectorGuids: ["cc48b548-7675-2a60-bc55-8675d4474a76"],
                },
                xValue: 102,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-160-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page6-block-6-overlay",
                  selectorGuids: ["cc48b548-7675-2a60-bc55-8675d4474a76"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956730939,
      useFirstGroupAsInitialState: true,
    },
    "a-161": {
      id: "a-161",
      title: "page7-block-1-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-161-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-161-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-161-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 700,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-161-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 700,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598956371206,
      useFirstGroupAsInitialState: true,
    },
    "a-162": {
      id: "a-162",
      title: "page7-block-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-162-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page7-block-2-img",
                  selectorGuids: ["52ee12b0-df32-a21a-af12-24ad8afb4bc3"],
                },
                xValue: 1.5,
                yValue: 1.5,
                locked: true,
              },
            },
            {
              id: "a-162-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page7-block-2-overlay",
                  selectorGuids: ["162a9bd5-9d91-7a6a-89b4-9ce4072aea7d"],
                },
                xValue: 0,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-162-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page7-block-2-overlay",
                  selectorGuids: ["162a9bd5-9d91-7a6a-89b4-9ce4072aea7d"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-162-n-4",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 3000,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page7-block-2-img",
                  selectorGuids: ["52ee12b0-df32-a21a-af12-24ad8afb4bc3"],
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
            {
              id: "a-162-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 1500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page7-block-2-overlay",
                  selectorGuids: ["162a9bd5-9d91-7a6a-89b4-9ce4072aea7d"],
                },
                xValue: 102,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-162-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page7-block-2-overlay",
                  selectorGuids: ["162a9bd5-9d91-7a6a-89b4-9ce4072aea7d"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956730939,
      useFirstGroupAsInitialState: true,
    },
    "a-163": {
      id: "a-163",
      title: "page7-block-3-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-163-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-163-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-163-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-163-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-164": {
      id: "a-164",
      title: "page7-block-3-content",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-164-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b85886346710a41aa683|cc9b0d4b-9639-e460-beda-65750de19549",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-164-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b85886346710a41aa683|cc9b0d4b-9639-e460-beda-65750de19549",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-164-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b85886346710a41aa683|cc9b0d4b-9639-e460-beda-65750de19549",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-164-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b85886346710a41aa683|cc9b0d4b-9639-e460-beda-65750de19549",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598986570477,
      useFirstGroupAsInitialState: true,
    },
    "a-165": {
      id: "a-165",
      title: "page7-block-4-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-165-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-165-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-165-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-165-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-166": {
      id: "a-166",
      title: "page7-block-4-wrapper-item",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-166-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page7-block-4-wrapper-item",
                  selectorGuids: ["0d258d34-5fbb-2e25-77cb-16e31a00dc3a"],
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-166-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page7-block-4-wrapper-item",
                  selectorGuids: ["0d258d34-5fbb-2e25-77cb-16e31a00dc3a"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-166-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page7-block-4-wrapper-item",
                  selectorGuids: ["0d258d34-5fbb-2e25-77cb-16e31a00dc3a"],
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-166-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page7-block-4-wrapper-item",
                  selectorGuids: ["0d258d34-5fbb-2e25-77cb-16e31a00dc3a"],
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598986701785,
      useFirstGroupAsInitialState: true,
    },
    "a-167": {
      id: "a-167",
      title: "page7-block-5-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-167-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-167-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-167-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-167-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-168": {
      id: "a-168",
      title: "page7-block-5-content",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-168-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b85886346710a41aa683|9fc02b4b-81f8-eba2-1579-a74b15d40fb4",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-168-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b85886346710a41aa683|9fc02b4b-81f8-eba2-1579-a74b15d40fb4",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-168-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b85886346710a41aa683|9fc02b4b-81f8-eba2-1579-a74b15d40fb4",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-168-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b85886346710a41aa683|9fc02b4b-81f8-eba2-1579-a74b15d40fb4",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598986848777,
      useFirstGroupAsInitialState: true,
    },
    "a-169": {
      id: "a-169",
      title: "page7-block-5-infographic",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-169-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b85886346710a41aa683|9fc02b4b-81f8-eba2-1579-a74b15d40fb5",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-169-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b85886346710a41aa683|9fc02b4b-81f8-eba2-1579-a74b15d40fb5",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-169-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 1000,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b85886346710a41aa683|9fc02b4b-81f8-eba2-1579-a74b15d40fb5",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-169-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 1000,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b85886346710a41aa683|9fc02b4b-81f8-eba2-1579-a74b15d40fb5",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598986909700,
      useFirstGroupAsInitialState: true,
    },
    "a-170": {
      id: "a-170",
      title: "page7-block-6-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-170-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-170-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-170-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-170-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-171": {
      id: "a-171",
      title: "page7-block-6-content",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-171-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b85886346710a41aa683|94f9b93a-1904-d643-0674-0b813a2c08ff",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-171-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b85886346710a41aa683|94f9b93a-1904-d643-0674-0b813a2c08ff",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-171-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b85886346710a41aa683|94f9b93a-1904-d643-0674-0b813a2c08ff",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-171-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46b85886346710a41aa683|94f9b93a-1904-d643-0674-0b813a2c08ff",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598987069447,
      useFirstGroupAsInitialState: true,
    },
    "a-172": {
      id: "a-172",
      title: "page7-block-7",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-172-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page7-block-7-img",
                  selectorGuids: ["85ee1bb2-6af3-004f-d524-3fc37cf25cc3"],
                },
                xValue: 1.5,
                yValue: 1.5,
                locked: true,
              },
            },
            {
              id: "a-172-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page7-block-7-overlay",
                  selectorGuids: ["6c2288d9-de5d-29d0-9b82-c89564dfef2a"],
                },
                xValue: 0,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-172-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page7-block-7-overlay",
                  selectorGuids: ["6c2288d9-de5d-29d0-9b82-c89564dfef2a"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-172-n-4",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 3000,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page7-block-7-img",
                  selectorGuids: ["85ee1bb2-6af3-004f-d524-3fc37cf25cc3"],
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
            {
              id: "a-172-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 1500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page7-block-7-overlay",
                  selectorGuids: ["6c2288d9-de5d-29d0-9b82-c89564dfef2a"],
                },
                xValue: 102,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-172-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page7-block-7-overlay",
                  selectorGuids: ["6c2288d9-de5d-29d0-9b82-c89564dfef2a"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956730939,
      useFirstGroupAsInitialState: true,
    },
    "a-173": {
      id: "a-173",
      title: "page8-block-1-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-173-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-173-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-173-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 700,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-173-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 700,
                easing: "easeInOut",
                duration: 800,
                target: {
                  useEventTarget: true,
                  id:
                    "5f3954afac18ff01d8edd53d|2245bb8f-7a80-a8f8-9c54-c410dca82ba6",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598956371206,
      useFirstGroupAsInitialState: true,
    },
    "a-174": {
      id: "a-174",
      title: "page8-block-1-linkbox",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-174-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-174-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-174-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 1500,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-174-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 1500,
                easing: "easeInOut",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|88a72ca0-1ba5-8640-6574-2a69f649b7fe",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956552562,
      useFirstGroupAsInitialState: true,
    },
    "a-175": {
      id: "a-175",
      title: "page8-block-2",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-175-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page8-block-2-img",
                  selectorGuids: ["1f5d7d93-1d76-65d7-03cd-447e518fd94f"],
                },
                xValue: 1.5,
                yValue: 1.5,
                locked: true,
              },
            },
            {
              id: "a-175-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page8-block-2-overlay",
                  selectorGuids: ["0ef77491-8dcf-a0ad-0b07-25b254b4e2f4"],
                },
                xValue: 0,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-175-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page8-block-2-overlay",
                  selectorGuids: ["0ef77491-8dcf-a0ad-0b07-25b254b4e2f4"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-175-n-4",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 3000,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page8-block-2-img",
                  selectorGuids: ["1f5d7d93-1d76-65d7-03cd-447e518fd94f"],
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
            {
              id: "a-175-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 1500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page8-block-2-overlay",
                  selectorGuids: ["0ef77491-8dcf-a0ad-0b07-25b254b4e2f4"],
                },
                xValue: 102,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-175-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page8-block-2-overlay",
                  selectorGuids: ["0ef77491-8dcf-a0ad-0b07-25b254b4e2f4"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956730939,
      useFirstGroupAsInitialState: true,
    },
    "a-176": {
      id: "a-176",
      title: "page8-block-3-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-176-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-176-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-176-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-176-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-177": {
      id: "a-177",
      title: "GRI-102-table",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-177-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46ba5ccd37d182ce8d671a|0e62a970-e8cf-1d0c-bde6-f6269a50a2a3",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-177-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46ba5ccd37d182ce8d671a|0e62a970-e8cf-1d0c-bde6-f6269a50a2a3",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-177-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46ba5ccd37d182ce8d671a|0e62a970-e8cf-1d0c-bde6-f6269a50a2a3",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-177-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46ba5ccd37d182ce8d671a|0e62a970-e8cf-1d0c-bde6-f6269a50a2a3",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598987680307,
      useFirstGroupAsInitialState: true,
    },
    "a-178": {
      id: "a-178",
      title: "page8-block-4-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-178-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-178-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-178-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-178-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-179": {
      id: "a-179",
      title: "GRI-200-table",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-179-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46ba5ccd37d182ce8d671a|0e62a970-e8cf-1d0c-bde6-f6269a50a2a3",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-179-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46ba5ccd37d182ce8d671a|0e62a970-e8cf-1d0c-bde6-f6269a50a2a3",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-179-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46ba5ccd37d182ce8d671a|0e62a970-e8cf-1d0c-bde6-f6269a50a2a3",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-179-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46ba5ccd37d182ce8d671a|0e62a970-e8cf-1d0c-bde6-f6269a50a2a3",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598987680307,
      useFirstGroupAsInitialState: true,
    },
    "a-180": {
      id: "a-180",
      title: "page8-block-5-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-180-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-180-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-180-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-180-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-181": {
      id: "a-181",
      title: "GRI-300-table",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-181-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46ba5ccd37d182ce8d671a|0e62a970-e8cf-1d0c-bde6-f6269a50a2a3",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-181-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46ba5ccd37d182ce8d671a|0e62a970-e8cf-1d0c-bde6-f6269a50a2a3",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-181-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46ba5ccd37d182ce8d671a|0e62a970-e8cf-1d0c-bde6-f6269a50a2a3",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-181-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46ba5ccd37d182ce8d671a|0e62a970-e8cf-1d0c-bde6-f6269a50a2a3",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598987680307,
      useFirstGroupAsInitialState: true,
    },
    "a-182": {
      id: "a-182",
      title: "page8-block-6-title",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-182-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-182-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-182-n-3",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-182-n-4",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f454028ec18356275c3236f|27922f44-fe55-44a2-2e5f-25110924e9e9",
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598978560889,
      useFirstGroupAsInitialState: true,
    },
    "a-183": {
      id: "a-183",
      title: "GRI-400-table",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-183-n",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46ba5ccd37d182ce8d671a|0e62a970-e8cf-1d0c-bde6-f6269a50a2a3",
                },
                xValue: 100,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-183-n-2",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46ba5ccd37d182ce8d671a|0e62a970-e8cf-1d0c-bde6-f6269a50a2a3",
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-183-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46ba5ccd37d182ce8d671a|0e62a970-e8cf-1d0c-bde6-f6269a50a2a3",
                },
                value: 1,
                unit: "",
              },
            },
            {
              id: "a-183-n-4",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: true,
                  id:
                    "5f46ba5ccd37d182ce8d671a|0e62a970-e8cf-1d0c-bde6-f6269a50a2a3",
                },
                xValue: 0,
                xUnit: "PX",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
          ],
        },
      ],
      createdOn: 1598987680307,
      useFirstGroupAsInitialState: true,
    },
    "a-184": {
      id: "a-184",
      title: "page8-block-7",
      actionItemGroups: [
        {
          actionItems: [
            {
              id: "a-184-n",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page8-block-7-img",
                  selectorGuids: ["6981b6a3-276a-91fc-a2e4-f3ef1087cbc3"],
                },
                xValue: 1.5,
                yValue: 1.5,
                locked: true,
              },
            },
            {
              id: "a-184-n-2",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page8-block-7-overlay",
                  selectorGuids: ["247a513d-06a0-c071-6f92-846c3f924025"],
                },
                xValue: 0,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-184-n-3",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 0,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page8-block-7-overlay",
                  selectorGuids: ["247a513d-06a0-c071-6f92-846c3f924025"],
                },
                value: 1,
                unit: "",
              },
            },
          ],
        },
        {
          actionItems: [
            {
              id: "a-184-n-4",
              actionTypeId: "TRANSFORM_SCALE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 3000,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page8-block-7-img",
                  selectorGuids: ["6981b6a3-276a-91fc-a2e4-f3ef1087cbc3"],
                },
                xValue: 1,
                yValue: 1,
                locked: true,
              },
            },
            {
              id: "a-184-n-5",
              actionTypeId: "TRANSFORM_MOVE",
              config: {
                delay: 500,
                easing: "easeInOut",
                duration: 1500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page8-block-7-overlay",
                  selectorGuids: ["247a513d-06a0-c071-6f92-846c3f924025"],
                },
                xValue: 102,
                xUnit: "%",
                yUnit: "PX",
                zUnit: "PX",
              },
            },
            {
              id: "a-184-n-6",
              actionTypeId: "STYLE_OPACITY",
              config: {
                delay: 500,
                easing: "",
                duration: 500,
                target: {
                  useEventTarget: "CHILDREN",
                  selector: ".page8-block-7-overlay",
                  selectorGuids: ["247a513d-06a0-c071-6f92-846c3f924025"],
                },
                value: 0,
                unit: "",
              },
            },
          ],
        },
      ],
      createdOn: 1598956730939,
      useFirstGroupAsInitialState: true,
    },
  },
  site: {
    mediaQueries: [
      { key: "main", min: 992, max: 10000 },
      { key: "medium", min: 768, max: 991 },
      { key: "small", min: 480, max: 767 },
      { key: "tiny", min: 0, max: 479 },
    ],
  },
});
