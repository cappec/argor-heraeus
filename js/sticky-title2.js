$(document).ready(() => {
  // Second Sticky title in page

  const stickyTitle2 = $("#sticky-title2");
  const stickyContainer2 = $("#sticky-title-container2");
  const width = $(window).width();
  if (width > 991) {
    $(window).scroll(function () {
      const containerLeft2 = stickyContainer2.offset().left;
      const containerRight2 = stickyContainer2.width() + containerLeft2;
      if (containerLeft2 < 80 && containerRight2 - stickyTitle2.width() > 0) {
        stickyTitle2.css("margin-left", -(containerLeft2 - 100));
      }
    });
  }
});
