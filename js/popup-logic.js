$(document).ready(function () {
  // START PAGE 5 HTML

  // Hide Popup 1
  $("#popup-close-btn1").click(function () {
    $(".page5-popup-overlay-1").css("display", "none");
  });

  // Show Popup 1
  $("#pl1").click(function () {
    $(".page5-popup-overlay-1").css("display", "flex");
  });

  // Hide Popup 2
  $("#popup-close-btn2").click(function () {
    $(".page5-popup-overlay-2").css("display", "none");
  });

  // Show Popup 2
  $("#pl2").click(function () {
    $(".page5-popup-overlay-2").css("display", "flex");
  });

  // Hide Popup 3
  $("#popup-close-btn3").click(function () {
    $(".page5-popup-overlay-3").css("display", "none");
  });

  // Show Popup 3
  $("#pl3").click(function () {
    $(".page5-popup-overlay-3").css("display", "flex");
  });

  // Hide Popup 4
  $("#popup-close-btn4").click(function () {
    $(".page5-popup-overlay-4").css("display", "none");
  });

  // Show Popup 4
  $("#pl4").click(function () {
    $(".page5-popup-overlay-4").css("display", "flex");
  });

  // Hide Popup 5
  $("#popup-close-btn5").click(function () {
    $(".page5-popup-overlay-5").css("display", "none");
  });

  // Show Popup 5
  $("#pl5").click(function () {
    $(".page5-popup-overlay-5").css("display", "flex");
  });

  // Hide Popup 6
  $("#popup-close-btn6").click(function () {
    $(".page5-popup-overlay-6").css("display", "none");
  });

  // Show Popup 6
  $("#pl6").click(function () {
    $(".page5-popup-overlay-6").css("display", "flex");
  });

  // Hide Popup 7
  $("#popup-close-btn7").click(function () {
    $(".page5-popup-overlay-7").css("display", "none");
  });

  // Show Popup 7
  $("#pl7").click(function () {
    $(".page5-popup-overlay-7").css("display", "flex");
  });

  // Hide Popup 8
  $("#popup-close-btn8").click(function () {
    $(".page5-popup-overlay-8").css("display", "none");
  });

  // Show Popup 8
  $("#pl8").click(function () {
    $(".page5-popup-overlay-8").css("display", "flex");
  });

  // Hide Popup 9
  $("#popup-close-btn9").click(function () {
    $(".page5-popup-overlay-9").css("display", "none");
  });

  // Show Popup 9
  $("#pl9").click(function () {
    $(".page5-popup-overlay-9").css("display", "flex");
  });

  // START PAGE 6 HTML

  // Hide Popup 1
  $("#popup-close-btn1").click(function () {
    $(".page6-popup-overlay-1").css("display", "none");
  });

  // Show Popup 1
  $("#pl1").click(function () {
    $(".page6-popup-overlay-1").css("display", "flex");
  });

  // Hide Popup 2
  $("#popup-close-btn2").click(function () {
    $(".page6-popup-overlay-2").css("display", "none");
  });

  // Show Popup 2
  $("#pl2").click(function () {
    $(".page6-popup-overlay-2").css("display", "flex");
  });

  // Hide Popup 3
  $("#popup-close-btn3").click(function () {
    $(".page6-popup-overlay-3").css("display", "none");
  });

  // Show Popup 3
  $("#pl3").click(function () {
    $(".page6-popup-overlay-3").css("display", "flex");
  });

  // Hide Popup 4
  $("#popup-close-btn4").click(function () {
    $(".page6-popup-overlay-4").css("display", "none");
  });

  // Show Popup 4
  $("#pl4").click(function () {
    $(".page6-popup-overlay-4").css("display", "flex");
  });

  // Hide Popup 5
  $("#popup-close-btn5").click(function () {
    $(".page6-popup-overlay-5").css("display", "none");
  });

  // Show Popup 5
  $("#pl5").click(function () {
    $(".page6-popup-overlay-5").css("display", "flex");
  });
});
